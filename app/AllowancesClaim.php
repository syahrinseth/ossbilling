<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AllowancesClaim extends Model
{
    protected $table = 'allowances_claim_lt';
    protected $primaryKey = 'id';
    protected $guarded = [];
    // claimPrice property
    static protected $_claimPrices = [
        'OBA' => 4.00,
        'ACTGL' => 3.50,
        'WNA' => 11.00,
        'WNSA' => 14.00,
        'WTA' => 5.00,
        'BREAKFAST' => 4.00,
        'LUNCH' => 4.00,
        'DINNER' => 4.00,
        'MIDNIGHT' => 4.00
    ];

    public static function strafter($string, $substring) {
        $pos = strpos($string, $substring);
        if ($pos === false){
            return $string;
        }else { 
            return(substr($string, $pos+strlen($substring)));
        }
    }

    public static function getclaimPrices(){
        $claim = self::$_claimPrices;
        return $claim;
    }

    public static function claimPrices($claimName){
        $claimPrice = 0.00;
        if($claimName != "0"){
            switch (strtoupper($claimName)) {
                case "OBA":
                    $claimPrice = self::$_claimPrices['OBA'];
                    break;
                case "ACTGL":
                    $claimPrice = self::$_claimPrices['ACTGL'];
                    break;
                case "WNA":
                    $claimPrice = self::$_claimPrices['WNA'];
                    break;
                case "WNSA":
                    $claimPrice = self::$_claimPrices['WNSA'];
                    break;
                case "WTA":
                    $claimPrice = self::$_claimPrices['WTA'];
                    break;
                case "BREAKFAST":
                    $claimPrice = self::$_claimPrices['BREAKFAST'];
                    break;
                case "LUNCH":
                    $claimPrice = self::$_claimPrices['LUNCH'];
                    break;
                case "DINNER":
                    $claimPrice = self::$_claimPrices['DINNER'];
                    break;
                case "MIDNIGHT":
                    $claimPrice = self::$_claimPrices['MIDNIGHT'];
                    break;
                default:
                    $claimPrice = 0.00;
            }
            return $claimPrice;
            
            
        }else{
            return $claimPrice;
        }
        
    }

    // accept input of laravel $request->input();
    public static function totalClaimPrices($array){
        if(count($array["OBA"]) > 0){
            for($i = 0; $i < count($array["OBA"]); $i++){
                $OBA = AllowancesClaim::claimPrices($array["OBA"][$i]);
                $ACTGL = AllowancesClaim::claimPrices($array["ACTGL"][$i]);
                $WNA = AllowancesClaim::claimPrices($array["WNA"][$i]);
                $WNSA = AllowancesClaim::claimPrices($array["WNSA"][$i]);
                $WTA = AllowancesClaim::claimPrices($array["WTA"][$i]);
                $BREAKFAST = AllowancesClaim::claimPrices($array["BREAKFAST"][$i]);
                $LUNCH = AllowancesClaim::claimPrices($array["LUNCH"][$i]);
                $DINNER = AllowancesClaim::claimPrices($array["DINNER"][$i]);
                $MIDNIGHT = AllowancesClaim::claimPrices($array["MIDNIGHT"][$i]);
                $grand_total[$i] = $OBA + $ACTGL + $WNA + $WNSA + $WTA + $BREAKFAST + $LUNCH + $DINNER + $MIDNIGHT;
            }
            return $grand_total;
        }
        

        
        return $grand_total;
    }

    // accept input of laravel $request->input but not multiple html input[]
    public static function totalClaimPrice($input){
        
            
        $OBA = AllowancesClaim::claimPrices($input["OBA"]);
        $ACTGL = AllowancesClaim::claimPrices($input["ACTGL"]);
        $WNA = AllowancesClaim::claimPrices($input["WNA"]);
        $WNSA = AllowancesClaim::claimPrices($input["WNSA"]);
        $WTA = AllowancesClaim::claimPrices($input["WTA"]);
        $BREAKFAST = AllowancesClaim::claimPrices($input["BREAKFAST"]);
        $LUNCH = AllowancesClaim::claimPrices($input["LUNCH"]);
        $DINNER = AllowancesClaim::claimPrices($input["DINNER"]);
        $MIDNIGHT = AllowancesClaim::claimPrices($input["MIDNIGHT"]);
        $grand_total = $OBA + $ACTGL + $WNA + $WNSA + $WTA + $BREAKFAST + $LUNCH + $DINNER + $MIDNIGHT;
        
        return $grand_total;
        
       
    }
}
