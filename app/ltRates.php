<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ltRates extends Model
{
    //
    protected $table = 'lt_rates';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
