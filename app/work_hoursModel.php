<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class work_hoursModel extends Model
{
    //The table associated with model.
    protected $table = 'work_hours';
}
