<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class work_datesModel extends Model
{
    //The table associated with model.
    protected $table = 'work_dates';

    private static $soverseas_night_shift_allow = 17;


    public static function getTotalOverseasNightShiftAllow($sso = null){
        if($sso == null){
            $sum = work_datesModel::where('overseas_night_shift_allow', '!=', null)->where('overseas_night_shift_allow', '!=', 0)->sum('overseas_night_shift_allow');
            return $sum;
        }else{
            $sum = work_datesModel::where('sso_no', '=', $sso)->where('overseas_night_shift_allow', '!=', null)->where('overseas_night_shift_allow', '!=', 0)->sum('overseas_night_shift_allow');
            return $sum;
        }
        
    }

    public static function getTotalOverseasNightShiftAllowRate(){
        $rate = self::$soverseas_night_shift_allow;
        return $rate;
    }
}
