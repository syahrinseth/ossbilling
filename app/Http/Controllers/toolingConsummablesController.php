<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

use App\User;
use App\toolingConsummablesModel;
use App\master_jobsModel;

class toolingConsummablesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $tooling_consummables = toolingConsummablesModel::all();
        return view('tooling_consummables.index', compact('tooling_consummables'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // create page
        $master_jobs = master_jobsModel::all();
        return view('tooling_consummables.create', compact('master_jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $this->validate(request(),[
            'name' => 'required',
            'description' => 'required',
            'job_no' => 'required',
            'cost' => 'required|numeric|between:0,999999.99'
        ]);
        
        if(master_jobsModel::find($request->job_no)){
            $tooling_consummable = new toolingConsummablesModel;
            $tooling_consummable->name = $request->name;
            $tooling_consummable->job_no = $request->job_no;
            $tooling_consummable->description = $request->description;
            $tooling_consummable->cost = $request->cost;
            $tooling_consummable->currency = "USD";
            $tooling_consummable->save();
            $request->session()->flash('alert-success', 'Submited');
            return back();
        }else{
            $request->session()->flash('alert-danger', 'Job dumber does not exists');
            return back();
        }
        

        

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($tc_id)
    {
        //
        $tooling_consummable = toolingConsummablesModel::find($tc_id);
        $master_jobs = master_jobsModel::all();
        return view('tooling_consummables.edit', compact('tooling_consummable', 'master_jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $tc_id)
    {
        //
        $this->validate(request(),[
            'name' => 'required',
            'description' => 'required',
            'cost' => 'required|numeric|between:0,999999.99',
            'job_no' => 'required',
        ]);
        
        if(master_jobsModel::find($request->job_no)){
            $tooling_consummable = toolingConsummablesModel::find($tc_id);
            $tooling_consummable->name = $request->name;
            $tooling_consummable->job_no = $request->job_no;
            $tooling_consummable->description = $request->description;
            $tooling_consummable->cost = $request->cost;
            $tooling_consummable->currency = "USD";
            $tooling_consummable->update();
            $request->session()->flash('alert-success', 'Updated');
            return back();
        }else{
            $request->session()->flash('alert-danger', 'Job number does not exists');
            return back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($tc_id)
    {
        //
        $tooling_consummable = toolingConsummablesModel::find($tc_id);
        if($tooling_consummable){
            $tooling_consummable->delete();
            return back();
        }else{
            return abort(404, 'error');
        }
    }
}
