<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;
use App\master_jobsModel;
use App\overseasTimesheetModel;
use App\work_datesModel;
use App\OverseasExpenseStatementModel;
use App\OesAttachmentModel;

class MasterJobController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->account_type == 1){
            //index master job list
            $jobs = master_jobsModel::all();
            return view('master_job.index', compact('jobs'));
        }else{
            return abort(404);
        }
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::user()->account_type == 1){
            $master_jobs = master_jobsModel::all();
            return view('master_job.create', compact('master_jobs'));
        }else{
            return abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->account_type == 1){
        // validate input
        $this->validate(request(), [
            'job_no' => 'required|unique:master_jobs,job_no',
            'job_description' => 'required',
            'network_no' => 'required',
            'po_no' => 'required',
            'country' => 'required',
        ]);
        
        if($request->job_no){
            $job = new master_jobsModel;
            $job->job_no = $request->job_no;
            $job->job_description = $request->job_description;
            $job->network_no = $request->network_no;
            $job->po_no = $request->po_no;
            $job->country = $request->country;
            $job->save();

            $request->session()->flash('alert-success', 'Job Number '. $request->job_no . ' created successfully.');
            return redirect()->route('index_usr_mjb');
        }

        $request->session()->flash('alert-danger', 'Error');
        return redirect()->route('index_usr_mjb');
        }else{
            return abort(404);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($job_no)
    {
        //
        if(Auth::user()->account_type == 1){
        $job = master_jobsModel::find($job_no);
        return view('master_job.edit', compact('job'));
        }else{
            return abort(404);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $job_no)
    {
        //
        if(Auth::user()->account_type == 1){
        // validate input
        $this->validate(request(), [
            'job_description' => 'required',
            'network_no' => 'required',
            'po_no' => 'required',
            'country' => 'required',
        ]);
        $job = master_jobsModel::find($job_no);
        if($job){
            $job->job_description = $request->job_description;
            $job->network_no = $request->network_no;
            $job->po_no = $request->po_no;
            $job->country = $request->country;
            $job->update();
            $request->session()->flash('alert-success', 'Job Number '. $request->job_no . ' updated successfully.');
            return redirect()->back();
            // return redirect()->route('index_usr_mjb');
        }
        $request->session()->flash('alert-danger', 'Error');
            return redirect()->back();
        // return redirect()->route('index_usr_mjb');
        }else{
            return abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($job_no)
    {
        //
        if(Auth::user()->account_type == 1){
        $job = master_jobsModel::find($job_no);
        if($job){
            $job->delete();
            return redirect()->route('index_usr_mjb');
        }else{
            return abort(404);
        }
        }else{
            return abort(404);
        }
    }
}
