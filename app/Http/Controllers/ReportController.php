<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

use App\master_jobsModel;
use App\overseasTimesheetModel;
use App\work_datesModel;
use App\OverseasExpenseStatementModel;
use App\OesAttachmentModel;
use App\User;
use App\masterCurrencyRatesModel;
use App\currencyRatesDetailsModel;
use App\toolingConsummablesModel;
use App\local_timesheetModel;
use App\local_timesheet_detailModel;

class ReportController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
        }

        function objectToArray($d) {
            if (is_object($d)) {
                // Gets the properties of the given object
                // with get_object_vars function
                $d = get_object_vars($d);
            }
            
            if (is_array($d)) {
                /*
                * Return array converted to object
                * Using __FUNCTION__ (Magic constant)
                * for recursive call
                */
                return array_map(__FUNCTION__, $d);
            }
            else {
                // Return array
                return $d;
            }
        }
        
    }

    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(Auth::user()->account_type != 1){
            return abort(404);
        }
        $master_jobs = master_jobsModel::all();
        return view('report.overseas_report', compact('master_jobs'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    

    public function finalOverseasReport(Request $request){
        if($request->file_type == "pdf"){
            return $this->pdfFinalReport($request);
        }elseif($request->file_type == "excel"){
            return $this->excelFinalReport($request);
        }else{
            return abort(404);
        }
    }





    // ***********************************************************************************
    //This is a function section
    public function excelFinalReport($request){
        if(Auth::user()->account_type != 1){
            return abort(404);
        }
        $this->validate(request(),[
            'job_no' => 'required',
        ]);

        // validate master currency month
        $oes_attachment = \DB::table('oes_attachment')->where('job_no', '=', $request->job_no)->get();
        foreach($oes_attachment as $item){
            $currency = masterCurrencyRatesModel::leftJoin('currency_rates_details', 'master_currency_rates.id', '=', 'currency_rates_details.mcr_id')->whereMonth('master_currency_rates.date', date("m",strtotime($item->date2_from)))->whereYear('master_currency_rates.date', date("Y",strtotime($item->date2_from)))->where('currency_rates_details.code',$item->currency)->exists();
            if(!$currency){
                $request->session()->flash('alert-danger', '"Master Currency Rate" on  '.date("M Y",strtotime($item->date2_from)).' with Currency of "'.$item->currency.'" not available to use in Job Number "'.$request->job_no.'".');
                return back();
            }
        }

        $overseas_timesheets = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('job_no', $request->job_no)->orderBy('overseas_timesheet.sso_no', "ASC")->get();
        $master_job = master_jobsModel::find($request->job_no);
        if($master_job == null){
            $request->session()->flash('alert-danger', 'Job Number '.$request->job_no.' not available');
            return back();
        }
        // return dd($overseas_timesheets[0]);
        // create spreedsheet
        $spreadsheet = new Spreadsheet();
        // edit sheet

        $local_timesheet = local_timesheetModel::where('job_no', '=', $request->job_no)->first();
        
   
            $sheet = $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'Site/Country : '.$master_job->job_description. ' '.$master_job->country)
            ->setCellValue('A2', '')
            ->setCellValue('A3', 'PO# : '.$master_job->po_no )
            ->setCellValue('J3', 'Job No. : '.$request->job_no);

$borders_red = [
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '000000'],
        ],
    ],
];


// $sheet->getStyle('A3')->applyFromArray($borders_red);
            
                // $header_row = 6;
                $row = 6;
                // $footer_row = 6;
            // loop for sso_no disnt

// <!-- ****************************************************************************** -->
if($local_timesheet){


    $local_timesheet_collections = local_timesheet_detailModel::where('lt_id', '=', $local_timesheet->id)->orderBy('local_timesheet_detail.date', "ASC")->get();
    $total_travel_lt = 0;
    $total_1x_lt = 0;
    $total_15x_lt = 0;
    $total_2x_lt = 0;
    if(count($local_timesheet_collections) > 0){
        // $sheet->fromArray([$overseas_timesheets_collections], NULL, 'A6');
        $sheet->setCellValue('A'.$row, 'Local Timesheet')->getStyle('A'.$row);
        
        $row++;
        $sheet->setCellValue('A'.$row, 'Day')->getStyle('A'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('B'.$row, 'Date')->getStyle('B'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('C'.$row, 'Employee Name')->getStyle('C'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('D'.$row, 'Start')->getStyle('D'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('E'.$row, 'End')->getStyle('E'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('F'.$row, 'Travel Hrs')->getStyle('F'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('G'.$row, 'ST Hrs')->getStyle('G'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('H'.$row, '1.5 T')->getStyle('H'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('I'.$row, '2 T')->getStyle('I'.$row)->applyFromArray($borders_red);
        $sheet->setCellValue('J'.$row, 'Remarks')->getStyle('J'.$row)->applyFromArray($borders_red);
        $row++;
        // $row += $header_row;
        
        for($i = 0; $i < count($local_timesheet_collections);$i++){
                    
            $sheet->setCellValue('A'.$row, $local_timesheet_collections[$i]->day)->getStyle('A'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('B'.$row, $local_timesheet_collections[$i]->date)->getStyle('B'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('C'.$row, $local_timesheet_collections[$i]->employee_name)->getStyle('C'.$row)->applyFromArray($borders_red);
                        if($local_timesheet_collections[$i]->travel_start){
                            $sheet->setCellValue('D'.$row, $local_timesheet_collections[$i]->travel_start)->getStyle('D'.$row)->applyFromArray($borders_red);
                        }elseif($local_timesheet_collections[$i]->work_start){
                            $sheet->setCellValue('D'.$row, $local_timesheet_collections[$i]->work_start)->getStyle('D'.$row)->applyFromArray($borders_red);
                        }else{
                            $sheet->setCellValue('D'.$row, "")->getStyle('D'.$row)->applyFromArray($borders_red);
                        }
                        if($local_timesheet_collections[$i]->travel2_finish){
                            $sheet->setCellValue('E'.$row, $local_timesheet_collections[$i]->travel2_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }elseif($local_timesheet_collections[$i]->travel_finish){
                            $sheet->setCellValue('E'.$row, $local_timesheet_collections[$i]->travel_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }elseif($local_timesheet_collections[$i]->work2_finish){
                            $sheet->setCellValue('E'.$row, $local_timesheet_collections[$i]->work2_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }elseif($local_timesheet_collections[$i]->work_finish){
                            $sheet->setCellValue('E'.$row, $local_timesheet_collections[$i]->work_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }else{
                            $sheet->setCellValue('E'.$row, "")->getStyle('E'.$row)->applyFromArray($borders_red);
                        }
                        $sheet->setCellValue('F'.$row, $local_timesheet_collections[$i]->total_travel_hour)->getStyle('F'.$row)->applyFromArray($borders_red);
                        $total_travel_lt += $local_timesheet_collections[$i]->total_travel_hour;
                        $sheet->setCellValue('G'.$row, $local_timesheet_collections[$i]->total_hour_1x)->getStyle('G'.$row)->applyFromArray($borders_red);
                        $total_1x_lt += $local_timesheet_collections[$i]->total_hour_1x;
                        $sheet->setCellValue('H'.$row, $local_timesheet_collections[$i]->total_hour_1_5x)->getStyle('H'.$row)->applyFromArray($borders_red);
                        $total_15x_lt += $local_timesheet_collections[$i]->total_hour_1_5x;
                        $sheet->setCellValue('I'.$row, $local_timesheet_collections[$i]->total_hour_2x)->getStyle('I'.$row)->applyFromArray($borders_red);
                        $total_2x_lt += $local_timesheet_collections[$i]->total_hour_2x;
                        $sheet->setCellValue('J'.$row, $local_timesheet_collections[$i]->remark)->getStyle('J'.$row)->applyFromArray($borders_red);
                        $row++;
                        //footer
                        if($i == (count($local_timesheet_collections) - 1)){
                            // $footer_row = $row;
                            $sheet->setCellValue('F'.$row, $total_travel_lt)->getStyle('F'.$row)->applyFromArray($borders_red);
                            $sheet->setCellValue('G'.$row, $total_1x_lt)->getStyle('G'.$row)->applyFromArray($borders_red);
                            $sheet->setCellValue('H'.$row, $total_15x_lt)->getStyle('H'.$row)->applyFromArray($borders_red);
                            $sheet->setCellValue('I'.$row, $total_2x_lt)->getStyle('I'.$row)->applyFromArray($borders_red);
                            $row++;
                        }
        }

            $row = $row + 4;
            // $header_row = $header_row + count($local_timesheet_collections) + 4;
            // $footer_row = $footer_row + 4;
            

            //total work and travel hour
            $total_lt = local_timesheetModel::where('job_no', $request->job_no)->first();
            $rate_lt = \App\ltRates::first();
            // $new_total_row = $row; 
            $sheet->setCellValue('A'.$row, 'Rate US$')->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, 'Total Hours')->getStyle('B'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$row, 'Sub-Total')->getStyle('C'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, $rate_lt->rate_st)->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, ($total_lt->total_st_hour + $total_lt->total_travel_hours))->getStyle('B'.$row)->applyFromArray($borders_red);
            $sub_total_st = ($total_lt->total_st_hour + $total_lt->total_travel_hours) * $rate_lt->rate_st;
            $sheet->setCellValue('C'.$row, number_format((float)$sub_total_st, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, $rate_lt->rate_1_5x)->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, $total_lt->total_1_5t_hour)->getStyle('B'.$row)->applyFromArray($borders_red);
            $sub_total_1_5t = $total_lt->total_1_5t_hour * $rate_lt->rate_1_5x;
            $sheet->setCellValue('C'.$row, number_format((float)$sub_total_1_5t, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, $rate_lt->rate_2x)->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, $total_lt->total_2t_hour)->getStyle('B'.$row)->applyFromArray($borders_red);
            $sub_total_2t = $total_lt->total_2t_hour * $rate_lt->rate_2x;
            $sheet->setCellValue('C'.$row, number_format((float)$sub_total_2t, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
            $lt_total = $sub_total_st + $sub_total_1_5t + $sub_total_2t;
            $sheet->setCellValue('I'.$row, 'USD Total: ')->getStyle('I'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('J'.$row, number_format((float)$lt_total, 2, '.', ''))->getStyle('J'.$row)->applyFromArray($borders_red);
            $row++;
        // ending row reset
        // $row2 = $row2 + count($local_timesheet_collections) + 2;
        
        // $header_row = $row + 4;
        $row+=4;
        // $footer_row+=4;

    }else{
        $row;
        $lt_total = 0;
        $total_travel_lt = 0;
        $total_1x_lt = 0;
        $total_15x_lt = 0;
        $total_2x_lt = 0;
    }

}else{
    $row;
        $lt_total = 0;
        $total_travel_lt = 0;
        $total_1x_lt = 0;
        $total_15x_lt = 0;
        $total_2x_lt = 0;
}
// <!-- ****************************************************************************** -->
     

        if(count($overseas_timesheets) > 0){

            for($i = 0; $i < count($overseas_timesheets);$i++){
                // return dd($header_row. ' '.$row.' '.$footer_row);
                // return $header_row;
                $sheet->setCellValue('A'.$row, 'Overseas Timesheet')->getStyle('A'.$row);
                $row++;
                // return dd($row);
                
                $sheet->setCellValue('A'.$row, $overseas_timesheets[$i]->name)->getStyle('A'.$row)->applyFromArray($borders_red);
                // return dd($row);
                $row++;
                // $sheet->fromArray([$overseas_timesheets_collections], NULL, 'A6');
                
                // return dd($row);
                $sheet->setCellValue('A'.$row, 'SSO No.')->getStyle('A'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$row, 'Day')->getStyle('B'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$row, 'Date')->getStyle('C'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$row, 'Start')->getStyle('D'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('E'.$row, 'End')->getStyle('E'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('F'.$row, 'Travel Hrs')->getStyle('F'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('G'.$row, 'ST Hrs')->getStyle('G'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('H'.$row, '1.5 T')->getStyle('H'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$row, '2 T')->getStyle('I'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('J'.$row, 'Remarks')->getStyle('J'.$row)->applyFromArray($borders_red);
                $row++;
                // footer
                
                // return dd($footer_row);

                // return dd($overseas_timesheets_collections);
                $overseas_timesheets_collections = overseasTimesheetModel::leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('job_no', $request->job_no)->where('overseas_timesheet.sso_no', $overseas_timesheets[$i]->sso_no)->orderBy('overseas_timesheet.sso_no', "ASC")->get();
                // $footer_row = $footer_row + count($overseas_timesheets_collections);
                // $sheet->setCellValue('K'.$footer_row, 'Total');
                for($ii = 0;$ii < count($overseas_timesheets_collections);$ii++){
                    
                        
                        $sheet->setCellValue('A'.$row, $overseas_timesheets_collections[$ii]->sso_no)->getStyle('A'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('B'.$row, $overseas_timesheets_collections[$ii]->day)->getStyle('B'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('C'.$row, $overseas_timesheets_collections[$ii]->date)->getStyle('C'.$row)->applyFromArray($borders_red);
                        if($overseas_timesheets_collections[$ii]->travel_start){
                            $sheet->setCellValue('D'.$row, $overseas_timesheets_collections[$ii]->travel_start)->getStyle('D'.$row)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->work_start){
                            $sheet->setCellValue('D'.$row, $overseas_timesheets_collections[$ii]->work_start)->getStyle('D'.$row)->applyFromArray($borders_red);
                        }else{
                            $sheet->setCellValue('D'.$row, "")->getStyle('D'.$row)->applyFromArray($borders_red);
                        }
                        if($overseas_timesheets_collections[$ii]->travel2_finish){
                            $sheet->setCellValue('E'.$row, $overseas_timesheets_collections[$ii]->travel2_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->travel_finish){
                            $sheet->setCellValue('E'.$row, $overseas_timesheets_collections[$ii]->travel_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->work2_finish){
                            $sheet->setCellValue('E'.$row, $overseas_timesheets_collections[$ii]->work2_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->work_finish){
                            $sheet->setCellValue('E'.$row, $overseas_timesheets_collections[$ii]->work_finish)->getStyle('E'.$row)->applyFromArray($borders_red);
                        }else{
                            $sheet->setCellValue('E'.$row, "")->getStyle('E'.$row)->applyFromArray($borders_red);
                        }
                        $sheet->setCellValue('F'.$row, $overseas_timesheets_collections[$ii]->total_travel_hour)->getStyle('F'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('G'.$row, $overseas_timesheets_collections[$ii]->total_hour_1x)->getStyle('G'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('H'.$row, $overseas_timesheets_collections[$ii]->total_hour_1_5x)->getStyle('H'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('I'.$row, $overseas_timesheets_collections[$ii]->total_hour_2x)->getStyle('I'.$row)->applyFromArray($borders_red);
                        $sheet->setCellValue('J'.$row, $overseas_timesheets_collections[$ii]->remark)->getStyle('J'.$row)->applyFromArray($borders_red);
                        $row++;
                        if($ii == count($overseas_timesheets_collections) - 1){
                            $footer_row = $row;
                            $sheet->setCellValue('F'.$footer_row, $overseas_timesheets_collections[$ii]->total_travel_hours)->getStyle('F'.$row)->applyFromArray($borders_red);
                            $sheet->setCellValue('G'.$footer_row, $overseas_timesheets_collections[$ii]->total_st_hour)->getStyle('G'.$row)->applyFromArray($borders_red);
                            $sheet->setCellValue('H'.$footer_row, $overseas_timesheets_collections[$ii]->total_1_5t_hour)->getStyle('H'.$row)->applyFromArray($borders_red);
                            $sheet->setCellValue('I'.$footer_row, $overseas_timesheets_collections[$ii]->total_2t_hour)->getStyle('I'.$row)->applyFromArray($borders_red);
                            $row++;
                        }
                        
                }
                
                // ending row reset
                // $row2 = $row2 + 4;
                $row = $row + 4;
                // $header_row = $header_row + count($overseas_timesheets_collections) + 4;
                // $footer_row = $footer_row + 4;
            }

            //total work and travel hour
            $sum_2t = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_2t_hour');
            $sum_1_5t = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_1_5t_hour');
            $sum_st = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_st_hour');
            $sum_travelhours = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_travel_hours');
            $rate = \App\rateModel::first();
            // $new_total_row = $row; 
            $sheet->setCellValue('A'.$row, 'Rate US$')->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, 'Total Hours')->getStyle('B'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$row, 'Sub-Total')->getStyle('C'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, $rate->rate_st)->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, ($sum_st + $sum_travelhours))->getStyle('B'.$row)->applyFromArray($borders_red);
            $sub_total_st = ($sum_st + $sum_travelhours) * $rate->rate_st;
            $sheet->setCellValue('C'.$row, number_format((float)$sub_total_st, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, $rate->rate_1_5x)->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, $sum_1_5t)->getStyle('B'.$row)->applyFromArray($borders_red);
            $sub_total_1_5t = $sum_1_5t * $rate->rate_1_5x;
            $sheet->setCellValue('C'.$row, number_format((float)$sub_total_1_5t, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, $rate->rate_2x)->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, $sum_2t)->getStyle('B'.$row)->applyFromArray($borders_red);
            $sub_total_2t = $sum_2t * $rate->rate_2x;
            $sheet->setCellValue('C'.$row, number_format((float)$sub_total_2t, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
            $ot_total = $sub_total_st + $sub_total_1_5t + $sub_total_2t;
            $sheet->setCellValue('I'.$row, 'USD Total: ')->getStyle('I'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('J'.$row, number_format((float)$ot_total, 2, '.', ''))->getStyle('J'.$row)->applyFromArray($borders_red);
            $row++;
            
            // $header_row = $row + 4;
            $row+=4;
            // $footer_row+=4;
        }else{
            $ot_total = 0;
            $row = $row + 4;
            // $header_row = $header_row + 4;
            // $footer_row = $footer_row + 4;
        }



            


// <!-- ****************************************************************************** -->

// get job_no oes collection
$oes = OverseasExpenseStatementModel::where('job_no', $request->job_no)->get();
if(count($oes) > 0){


            // T&L Section
            $row = $row + 2;
            $sheet->setCellValue('A'.$row, 'Overseas Expense Statement');
            $row++;
            $sheet->setCellValue('A'.$row, 'NAME')->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, 'DATE')->getStyle('B'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$row, 'AIRFARE')->getStyle('C'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('D'.$row, 'HOTEL')->getStyle('D'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('E'.$row, 'HOTEL LAUNDRY')->getStyle('E'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('F'.$row, 'TAXI')->getStyle('F'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('G'.$row, 'MOBILE')->getStyle('G'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('H'.$row, 'VISA')->getStyle('H'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('I'.$row, 'OTHER')->getStyle('I'.$row)->applyFromArray($borders_red);
            $row++;
            $sheet->setCellValue('A'.$row, '')->getStyle('A'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$row, '')->getStyle('B'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$row, 'USD')->getStyle('C'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('D'.$row, 'USD')->getStyle('D'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('E'.$row, 'USD')->getStyle('E'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('F'.$row, 'USD')->getStyle('F'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('G'.$row, 'USD')->getStyle('G'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('H'.$row, 'USD')->getStyle('H'.$row)->applyFromArray($borders_red);
            $sheet->setCellValue('I'.$row, 'USD')->getStyle('I'.$row)->applyFromArray($borders_red);
            $row++;

            
            
                for($i = 0;$i<count($oes);$i++){
                    $sheet->setCellValue('A'.$row, $oes[$i]->name)->getStyle('A'.$row)->applyFromArray($borders_red);
                    $attach = OesAttachmentModel::where('oes_id', $oes[$i]->id)->get();
                    for($ii=0;$ii<count($attach);$ii++) {
                        $thisMonthCurrency = masterCurrencyRatesModel::whereMonth('date', date('m', strtotime($attach[$ii]->date2_from)))->first();
                        if($thisMonthCurrency == null){
                            // else this month currency not upload yet
                            $request->session()->flash('alert-danger', 'Unable to download. Currency for month '.date('m', strtotime($attach[$ii]->date2_from)).' are unavailable in the system');
                            return back();
                        }
                        $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code',$attach[$ii]->currency)->firstOrFail();
                        $expenses_usd = $attach[$ii]->expenses / $currency->currency_per_usd;
                        switch ($attach[$ii]->category) {
                            case "Airfare":
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, '')->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, '')->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, '')->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, '')->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, '')->getStyle('I'.$row)->applyFromArray($borders_red);
                                break;
                            case "Hotel":
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, '')->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, '')->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, '')->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, '')->getStyle('I'.$row)->applyFromArray($borders_red);
                                break;
                            case "Laundry":
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, '')->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, '')->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, '')->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, '')->getStyle('I'.$row)->applyFromArray($borders_red);
                                break;
                            case "Taxi":
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, '')->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, '')->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, '')->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, '')->getStyle('I'.$row)->applyFromArray($borders_red);
                                break;
                            case "Mobile":
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, '')->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, '')->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, '')->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, '')->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, '')->getStyle('I'.$row)->applyFromArray($borders_red);
                                break;
                            case "Visa":
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, '')->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, '')->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, '')->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, '')->getStyle('I'.$row)->applyFromArray($borders_red);
                                break;
                            default:
                                $sheet->setCellValue('B'.$row, $attach[$ii]->date2_from)->getStyle('B'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$row, '')->getStyle('D'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$row, '')->getStyle('E'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$row, '')->getStyle('F'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$row, '')->getStyle('G'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$row, '')->getStyle('H'.$row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$row, number_format($expenses_usd, 2, '.', ''))->getStyle('I'.$row)->applyFromArray($borders_red);
                            
                        }
                    $row++;
                    
                    }
                    // $tl_row++;
                }
                // sub total for T&C
                // sum catagory
                $expenses = OesAttachmentModel::where('job_no', $request->job_no)->get();
                $sum_airfare = 0;
                $sum_hotel = 0;
                $sum_laundry = 0;
                $sum_taxi = 0;
                $sum_mobile = 0;
                $sum_visa = 0;
                $sum_other = 0;
                for($i=0;$i<count($expenses);$i++){
                    switch ($expenses[$i]->category){
                        case "Airfare":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_airfare = $sum_airfare + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Hotel":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_hotel = $sum_hotel + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Laundry":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_laundry = $sum_laundry + ($expenses[$i]->expenses / $currency->currency_per_usd);
                            // return dd($expenses);
                        break;
                        case "Taxi":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_taxi = $sum_taxi + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Mobile":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_mobile = $sum_mobile + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Visa":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_visa = $sum_visa + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        default:
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_other = $sum_other + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                    }
                    
                }
                
                // row for sum
                $sheet->setCellValue('A'.$row, 'Sub-Total')->getStyle('A'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$row, '')->getStyle('B'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$row, number_format($sum_airfare, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$row, number_format($sum_hotel, 2, '.', ''))->getStyle('D'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('E'.$row, number_format($sum_laundry, 2, '.', ''))->getStyle('E'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('F'.$row, number_format($sum_taxi, 2, '.', ''))->getStyle('F'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('G'.$row, number_format($sum_mobile, 2, '.', ''))->getStyle('G'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('H'.$row, number_format($sum_visa, 2, '.', ''))->getStyle('H'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$row, number_format($sum_other, 2, '.', ''))->getStyle('I'.$row)->applyFromArray($borders_red);
                $row++;
                // margin 15%
                $sum_airfare_mgn = $sum_airfare / 0.85;
                $sum_hotel_mgn = $sum_hotel / 0.85;
                $sum_laundry_mgn = $sum_laundry / 0.85;
                $sum_taxi_mgn = $sum_taxi / 0.85;
                $sum_mobile_mgn = $sum_mobile / 0.85;
                $sum_visa_mgn = $sum_visa / 0.85;
                $sum_other_mgn = $sum_other / 0.85;
                $sheet->setCellValue('A'.$row, '15% Margin')->getStyle('A'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$row, '')->getStyle('B'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$row, number_format((float)$sum_airfare_mgn, 2, '.', ''))->getStyle('C'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$row, number_format((float)$sum_hotel_mgn, 2, '.', ''))->getStyle('D'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('E'.$row, number_format((float)$sum_laundry_mgn, 2, '.', ''))->getStyle('E'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('F'.$row, number_format((float)$sum_taxi_mgn, 2, '.', ''))->getStyle('F'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('G'.$row, number_format((float)$sum_mobile_mgn, 2, '.', ''))->getStyle('G'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('H'.$row, number_format((float)$sum_visa_mgn, 2, '.', ''))->getStyle('H'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$row, number_format((float)$sum_other_mgn, 2, '.', ''))->getStyle('I'.$row)->applyFromArray($borders_red);
                $row++;
                // sub total
                $sub_total = $sum_airfare_mgn + $sum_hotel_mgn + $sum_laundry_mgn + $sum_taxi_mgn + $sum_mobile_mgn + $sum_visa_mgn + $sum_other_mgn;
                $sheet->setCellValue('H'.$row, 'USD Total: ')->getStyle('H'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$row, number_format((float)$sub_total, 2, '.', ''))->getStyle('I'.$row)->applyFromArray($borders_red);
                $row++;
                $row++;
}else{
    $row += 4;
    $sub_total = 0;
}

// <!-- ****************************************************************************** -->

$tooling = toolingConsummablesModel::where('job_no', $request->job_no)->get();
if(count($tooling) > 0){
                // tooling and consummables
                $sheet->setCellValue('A'.$row, 'Tooling & Consummables')->getStyle('A'.$row);
                $row++;
                $sheet->setCellValue('A'.$row, 'NAME')->getStyle('A'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$row, 'DESCRIPTION')->getStyle('B'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$row, 'CURRENCY')->getStyle('C'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$row, 'COST')->getStyle('D'.$row)->applyFromArray($borders_red);
                $row++;
                $total_cost = 0;
                for($i=0;$i<count($tooling);$i++){
                    $sheet->setCellValue('A'.$row, $tooling[$i]->name)->getStyle('A'.$row)->applyFromArray($borders_red);
                    $sheet->setCellValue('B'.$row, $tooling[$i]->description)->getStyle('B'.$row)->applyFromArray($borders_red);
                    $sheet->setCellValue('C'.$row, $tooling[$i]->currency)->getStyle('C'.$row)->applyFromArray($borders_red);
                    $sheet->setCellValue('D'.$row, number_format($tooling[$i]->cost, 2, '.', ''))->getStyle('D'.$row)->applyFromArray($borders_red);
                    $total_cost = $total_cost + $tooling[$i]->cost;
                    $row++;
                }
                // total tooling
                $sheet->setCellValue('A'.$row, 'USD Total Cost: ')->getStyle('A'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$row, '')->getStyle('B'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$row, '')->getStyle('C'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$row, number_format($total_cost, 2, '.', ''))->getStyle('D'.$row)->applyFromArray($borders_red);
                $row++;
                $row++;
}else{
    $row += 2;
    $total_cost=0;
}

// <!-- ****************************************************************************** -->



// $oes_details_currency = \DB::table('oes_attachment')->where('job_no', '=', $master_job->job_no)->distinct()->get(['currency']);

$sql = "select avg(currency_per_usd) as currency_per_usd, code from 

(select Concat(Year(date2_from), Month(date2_from)) as oa_date,currency from oes_attachment where job_no = '".$master_job->job_no."') oa

left join

(select Concat(Year(date), Month(date)) as mcr_date, id as mcr_id  from master_currency_rates)mcr

on 
oa.oa_date = mcr.mcr_date

left join
(select currency_per_usd,code, mcr_id from currency_rates_details) crd
on 
crd.mcr_id = mcr.mcr_id and crd.code = oa.currency
group by code";


$oes_details_currency = \DB::select($sql);
$oes_details_currency_final = objectToArray($oes_details_currency);
if(count($oes_details_currency_final) > 0){
    $ascii_row = 65;
    $sheet->setCellValue(chr($ascii_row).$row, 'Currency');
    $row++;
    for($i=0;$i<count($oes_details_currency_final);$i++){
        // $currency_display = masterCurrencyRatesModel::leftJoin('currency_rates_details', 'master_currency_rates.id', '=', 'currency_rates_details.mcr_id')->whereMonth('master_currency_rates.date', date("m",strtotime($oes_details_currency[$i]->date2_from)))->where('currency_rates_details.code',$oes_details_currency[$i]->currency)->first();
        $sheet->setCellValue(chr($ascii_row).$row, $oes_details_currency_final[$i]["code"])->getStyle(chr($ascii_row).$row)->applyFromArray($borders_red);
        $row++;
        $sheet->setCellValue(chr($ascii_row).$row, $oes_details_currency_final[$i]["currency_per_usd"])->getStyle(chr($ascii_row).$row)->applyFromArray($borders_red);
        $row--;
        $ascii_row++;
    }
    $row++;
    $row++;

}else{
    $row += 2;
}

// <!-- ****************************************************************************** -->

                // USD Total amount
                $grand_total = $total_cost + $sub_total + $ot_total + $lt_total;
                $sheet->setCellValue('I'.$row, 'USD Total Amount : ')->getStyle('I'.$row)->applyFromArray($borders_red);
                $sheet->setCellValue('J'.$row, number_format((float)$grand_total, 2, '.', ''))->getStyle('J'.$row)->applyFromArray($borders_red);
                // return dd($tl_row);
                // compile sheet
                $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment; filename="final-report-'.$request->job_no.'.xlsx"');
                return $writer->save("php://output");
            
            
        //download sheet
            
            // $currency_ot = OesAttachmentModel::where('oes_id', $oes->id)->get();
        
    
    }




    public function pdfFinalReport($request){
        // final overseas report output 
        if(Auth::user()->account_type != 1){
            return abort(404);
        }
        $this->validate(request(),[
            'job_no' => 'required',
        ]);

        
        // return dd($pdf);
            // validate master currency month
            $oes_attachment = \DB::table('oes_attachment')->where('job_no', '=', $request->job_no)->get();
            foreach($oes_attachment as $item){
                $currency = masterCurrencyRatesModel::leftJoin('currency_rates_details', 'master_currency_rates.id', '=', 'currency_rates_details.mcr_id')->whereMonth('master_currency_rates.date', date("m",strtotime($item->date2_from)))->whereYear('master_currency_rates.date', date("Y",strtotime($item->date2_from)))->where('currency_rates_details.code',$item->currency)->exists();
                if(!$currency){
                    $request->session()->flash('alert-danger', '"Master Currency Rate" on  '.date("M Y",strtotime($item->date2_from)).' with Currency of "'.$item->currency.'" not available to use in Job Number "'.$request->job_no.'".');
                    return back();
                }
            }
        
        // Query Data
        $master_job = master_jobsModel::find($request->job_no);
        if($master_job == null){
            $request->session()->flash('alert-danger', 'Job Number '.$request->job_no.' not available');
            return back();
        }

        $local_timesheet = local_timesheetModel::where('job_no', '=', $request->job_no)->first();

        

        if($local_timesheet){
            $lt_dates = local_timesheet_detailModel::where('lt_id', $local_timesheet->id)->distinct('date')->get(['date','day']);
            $local_timesheet_details = local_timesheet_detailModel::where('lt_id', '=', $local_timesheet->id)->get();
        }else{
            $lt_dates = null;
            $local_timesheet_details = null;
        }
        
        $overseas_timesheets = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('job_no', $request->job_no)->orderBy('overseas_timesheet.sso_no', "ASC")->get();

        $overseas_expense_statement = OverseasExpenseStatementModel::where('job_no', $request->job_no)->orderBy('overseas_expenses_statement.sso_no', "ASC")->get();
        $tooling_costs = toolingConsummablesModel::where('job_no', '=', $request->job_no)->get();

        // Overseas timesheet document output
        $master_overseas_timesheet = overseasTimesheetModel::where('job_no', '=', $request->job_no)->get();

        // overseas expense statement query
        $master_overseas_expense_statement = OverseasExpenseStatementModel::where('job_no', '=', $request->job_no)->get();

        $sql = "select avg(currency_per_usd) as currency_per_usd, code from 

        (select Concat(Year(date2_from), Month(date2_from)) as oa_date,currency from oes_attachment where job_no = '".$master_job->job_no."') oa

        left join

        (select Concat(Year(date), Month(date)) as mcr_date, id as mcr_id  from master_currency_rates)mcr

        on 
        oa.oa_date = mcr.mcr_date

        left join
        (select currency_per_usd,code, mcr_id from currency_rates_details) crd
        on 
        crd.mcr_id = mcr.mcr_id and crd.code = oa.currency
        group by code";


        $oes_details_currency = \DB::select($sql);
        $oes_details_currency_final = objectToArray($oes_details_currency);
        
        // Generate PDF
        $pdf = \PDF::loadView('report.final_report', compact('master_job', 'local_timesheet', 'lt_dates', 'local_timesheet_details', 'overseas_timesheets', 'overseas_expense_statement', 'tooling_costs', 'master_overseas_timesheet', 'master_overseas_expense_statement', 'oes_details_currency_final'));
        $pdf->setPaper('A4', 'landscape');
        Storage::makeDirectory('public/final_report/'.$request->job_no);
        $pdf->save('storage/final_report/'. $request->job_no .'/final-report-'.$request->job_no.'.pdf');
        $fr_exists = Storage::exists('public/final_report/'. $request->job_no .'/final-report-'.$request->job_no.'.pdf');
        if($fr_exists){
            try {
                $oes_collections = OesAttachmentModel::where('job_no', $request->job_no)->orderBy('oes_id', 'ASC')->get();
                $pdf = new \Nextek\LaraPdfMerger\PdfManage;
                $pdf->addPDF('storage/final_report/'. $request->job_no .'/final-report-'.$request->job_no.'.pdf', 'all', 'L');
                foreach($oes_collections as $item){
                    if($item->attachment != "no-file.png"){
                        if(strtolower(substr(strrchr($item->attachment,'.'),1)) == "pdf"){
                            $pdf->addPDF('storage/overseas_expense_statement/'. $item->oes_id .'/oes'.'/'.$item->attachment, 'all', 'L');
                        }   
                    }
                }
                // $pdf->merge('file', 'storage/final_report/'. $request->job_no .'/merged-final-report-'.$request->job_no.'.pdf', 'L'); // generate the file

                return $pdf->merge('download', 'final-report-'.$request->job_no.'.pdf'); // force download
            } catch (Exception $e) {
                return abort(404);
            }
            
        }
        
       
        // return $pdf->download($request['job_no'].'-final-report.pdf');
        // return view('report.final_report', compact('master_job', 'local_timesheet', 'lt_dates', 'local_timesheet_details', 'overseas_timesheets', 'overseas_expense_statement', 'tooling_costs', 'master_overseas_timesheet', 'master_overseas_expense_statement'));
    }

    
}
