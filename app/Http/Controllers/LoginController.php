<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    public function index(){
        return view('auth.login');
    }

     /**
     * Handle an authentication attempt.
     *
     * @return Response
     */
    public function authenticate(Request $request)
    {
        $this->validate(request(), [
            'sso_no' => 'required|integer',
            'password' => 'required',
        ]);
        if (Auth::attempt(['sso_no' => $request->sso_no, 'password' => $request->password])) {
            // Authentication passed...
            return redirect()->intended('dashboard');
        }else{
            $request->session()->flash('alert-danger', 'Wrong sso_no or password.');
            return redirect()->back();
        }
    }

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        
    }


}
