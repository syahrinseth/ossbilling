<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Hash;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

use App\master_jobsModel;
use App\overseasTimesheetModel;
use App\work_datesModel;
use App\OverseasExpenseStatementModel;
use App\OesAttachmentModel;
use App\User;
use App\masterCurrencyRatesModel;
use App\currencyRatesDetailsModel;
use App\toolingConsummablesModel;

class UserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');

        function getNameFromNumber($num) {
        $numeric = $num % 26;
        $letter = chr(65 + $numeric);
        $num2 = intval($num / 26);
        if ($num2 > 0) {
            return getNameFromNumber($num2 - 1) . $letter;
        } else {
            return $letter;
        }
        }
        
    }
    //
    public function index(){
        if(Auth::user()->account_type == 1){
        $users = User::all();
        return view('users.index', compact('users'));
        }else{
            return abort(404);
        }
    }

    public function destroy($sso_no){
        if(Auth::user()->account_type == 1){
        $user = User::find($sso_no);
        if($user){
            $user->delete();
            return back();
        }else{
            return abort(404, "User not exist.");
        }
        }else{
            return abort(404);
        }
    }

    public function edit($sso_no){
        if(Auth::user()->account_type == 1){
        $user = User::find($sso_no);
        return view('users.edit', compact('user'));
        }else{
            return abort(404);
        }
    }

    public function update(Request $request, $sso_no){
        // check user sso no between parameter sso no
        if(Auth::user()->sso_no != $sso_no){
            return abort(404);
        }

        if(Auth::user()->account_type == 1){
            $user = User::find($sso_no);
            if($user){
                $user->name = $request->name;
                $user->email = $request->email;
                if($request->account_type == "on"){
                    $user->account_type = 1;
                }else{
                    $user->account_type = 0;
                }
                $user->update();
                $request->session()->flash('alert-success', 'Update success.');
                return back();
            }else{
                $request->session()->flash('alert-danger', 'Update failed.');
                return back();
            }
        }else{
            $user = User::find($sso_no);
            if($user){
                $user->name = $request->name;
                $user->email = $request->email;
                $user->update();
                $request->session()->flash('alert-success', 'Update success.');
                return back();
            }else{
                $request->session()->flash('alert-danger', 'Update failed.');
                return back();
            }
        }
    }

    public function changePassword(){
        
            $user = User::find(Auth::user()->sso_no);
            if($user){
                return view('users.change_password', compact('user'));
            }else{
                return abort(404, "Unauthorized Action");
            }
          
    }

    public function updatePassword(Request $request){

        if (!(Hash::check($request->get('current-password'), Auth::user()->password))) {
            // The passwords matches
            $request->session()->flash("alert-danger","Your current password does not matches with the password you provided. Please try again.");
            return redirect()->back();
            
        }
 
        if(strcmp($request->get('current-password'), $request->get('new-password')) == 0){
            //Current password and new password are same
            $request->session()->flash("alert-danger","New Password cannot be same as your current password. Please choose a different password.");
            return redirect()->back();
        }
 
        $validatedData = $request->validate([
            'current-password' => 'required',
            'new-password' => 'required|string|min:6|confirmed',
        ]);
 
        //Change Password
        $user = Auth::user();
        $user->password = bcrypt($request->get('new-password'));
        $user->save();
        $request->session()->flash("alert-success","Password changed successfully !");
        return redirect()->back();
 
    }


    public function addUser(){
        if(Auth::user()->account_type == 1){
        return view('users.add_user');
        }else{
            return abort(404);
        }
    }

    public function storeUser(Request $request){
        if(Auth::user()->account_type == 1){
        $this->validate(request(), [
            'name' => 'required|string|max:255',
            'sso_no' => 'required|integer|unique:users',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'account_type' => 'required'
        ]);
                // return dd($request);
        $request['password'] = bcrypt($request['password']);
        $user = User::create(request(['name', 'email', 'sso_no', 'account_type', 'password']));
        // $user = new User;
        // $user->name = $request['name'];
        // $user->sso_no = $request['sso_no'];
        // $user->email = $request['email'];
        // $user->password = bcrypt($request['password']);
        // $user->save();
        
        $request->session()->flash('alert-success', 'Success');
        return redirect()->back();
        }else{
            return abort(404);
        }
    }

    public function account($sso_no){
        $user = User::find($sso_no);
        return view('users.edit', compact('user'));
    }

    public function download(Request $request){

        $overseas_timesheets = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('job_no', $request->job_no)->orderBy('overseas_timesheet.sso_no', "ASC")->get();
        $master_job = master_jobsModel::find($request->job_no);
        // return dd($overseas_timesheets[0]);
        // create spreedsheet
        $spreadsheet = new Spreadsheet();
        // edit sheet
        
        if($overseas_timesheets){
            $sheet = $spreadsheet->getActiveSheet()
            ->setCellValue('A1', 'Site/Country : '.$master_job->job_description. ' '.$master_job->country)
            ->setCellValue('A2', '')
            ->setCellValue('A3', 'PO# : '.$master_job->po_no )
            ->setCellValue('J3', 'Job No. : '.$request->job_no)
            ->setCellValue('A5', 'Overseas Timesheet');

$borders_red = [
    'borders' => [
        'outline' => [
            'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
            'color' => ['argb' => '000000'],
        ],
    ],
];


// $sheet->getStyle('A3')->applyFromArray($borders_red);
            
                $row = 6;
                $header_row = 7;
                $row2 = 8;
                $footer_row = 8;
            // loop for sso_no disnt
            for($i = 0; $i < count($overseas_timesheets);$i++){
                
                
                
                // $row = $row + $i;
                // $footer_row = $footer_row + $i;
                // $header_row = $header_row + $i;
                $sheet->setCellValue('A'.$row, $overseas_timesheets[$i]->name)->getStyle('A'.$row)->applyFromArray($borders_red);
 
                // $sheet->fromArray([$overseas_timesheets_collections], NULL, 'A6');
                $sheet->setCellValue('A'.$header_row, 'SSO No.')->getStyle('A'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$header_row, 'Day')->getStyle('B'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$header_row, 'Date')->getStyle('C'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$header_row, 'Start')->getStyle('D'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('E'.$header_row, 'End')->getStyle('E'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('F'.$header_row, 'Travel Hrs')->getStyle('F'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('G'.$header_row, 'ST Hrs')->getStyle('G'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('H'.$header_row, '1.5 T')->getStyle('H'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$header_row, '2 T')->getStyle('I'.$header_row)->applyFromArray($borders_red);
                $sheet->setCellValue('J'.$header_row, 'Remarks')->getStyle('J'.$header_row)->applyFromArray($borders_red);

                // footer
                
                // return dd($footer_row);

                // return dd($overseas_timesheets_collections);
                $overseas_timesheets_collections = overseasTimesheetModel::leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('job_no', $request->job_no)->where('overseas_timesheet.sso_no', $overseas_timesheets[$i]->sso_no)->orderBy('overseas_timesheet.sso_no', "ASC")->get();
                // $footer_row = $footer_row + count($overseas_timesheets_collections);
                // $sheet->setCellValue('K'.$footer_row, 'Total');
                for($ii = 0;$ii < count($overseas_timesheets_collections);$ii++){
                    
                        
                        $sheet->setCellValue('A'.$row2, $overseas_timesheets_collections[$ii]->sso_no)->getStyle('A'.$row2)->applyFromArray($borders_red);
                        $sheet->setCellValue('B'.$row2, $overseas_timesheets_collections[$ii]->day)->getStyle('B'.$row2)->applyFromArray($borders_red);
                        $sheet->setCellValue('C'.$row2, $overseas_timesheets_collections[$ii]->date)->getStyle('C'.$row2)->applyFromArray($borders_red);
                        if($overseas_timesheets_collections[$ii]->travel_start){
                            $sheet->setCellValue('D'.$row2, $overseas_timesheets_collections[$ii]->travel_start)->getStyle('D'.$row2)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->work_start){
                            $sheet->setCellValue('D'.$row2, $overseas_timesheets_collections[$ii]->work_start)->getStyle('D'.$row2)->applyFromArray($borders_red);
                        }
                        if($overseas_timesheets_collections[$ii]->travel2_finish){
                            $sheet->setCellValue('E'.$row2, $overseas_timesheets_collections[$ii]->travel2_finish)->getStyle('E'.$row2)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->travel_finish){
                            $sheet->setCellValue('E'.$row2, $overseas_timesheets_collections[$ii]->travel_finish)->getStyle('E'.$row2)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->work2_finish){
                            $sheet->setCellValue('E'.$row2, $overseas_timesheets_collections[$ii]->work2_finish)->getStyle('E'.$row2)->applyFromArray($borders_red);
                        }elseif($overseas_timesheets_collections[$ii]->work_finish){
                            $sheet->setCellValue('E'.$row2, $overseas_timesheets_collections[$ii]->work_finish)->getStyle('E'.$row2)->applyFromArray($borders_red);
                        }
                        $sheet->setCellValue('F'.$row2, $overseas_timesheets_collections[$ii]->total_travel_hour)->getStyle('F'.$row2)->applyFromArray($borders_red);
                        $sheet->setCellValue('G'.$row2, $overseas_timesheets_collections[$ii]->total_hour_1x)->getStyle('G'.$row2)->applyFromArray($borders_red);
                        $sheet->setCellValue('H'.$row2, $overseas_timesheets_collections[$ii]->total_hour_1_5x)->getStyle('H'.$row2)->applyFromArray($borders_red);
                        $sheet->setCellValue('I'.$row2, $overseas_timesheets_collections[$ii]->total_hour_2x)->getStyle('I'.$row2)->applyFromArray($borders_red);
                        $sheet->setCellValue('J'.$row2, $overseas_timesheets_collections[$ii]->remark)->getStyle('J'.$row2)->applyFromArray($borders_red);
                        $row2++;
                        if($ii == count($overseas_timesheets_collections) - 1){
                            $footer_row = $row2;
                            $sheet->setCellValue('F'.$footer_row, $overseas_timesheets_collections[$ii]->total_travel_hours)->getStyle('F'.$row2)->applyFromArray($borders_red);
                            $sheet->setCellValue('G'.$footer_row, $overseas_timesheets_collections[$ii]->total_st_hour)->getStyle('G'.$row2)->applyFromArray($borders_red);
                            $sheet->setCellValue('H'.$footer_row, $overseas_timesheets_collections[$ii]->total_1_5t_hour)->getStyle('H'.$row2)->applyFromArray($borders_red);
                            $sheet->setCellValue('I'.$footer_row, $overseas_timesheets_collections[$ii]->total_2t_hour)->getStyle('I'.$row2)->applyFromArray($borders_red);

                        }
                        
                }
                
                
                // ending row reset
                $row2 = $row2 + 4;
                $row = $row + count($overseas_timesheets_collections) + 4;
                $header_row = $header_row + count($overseas_timesheets_collections) + 4;
                $footer_row = $footer_row + count($overseas_timesheets_collections) + 4;
            }
            //total work and travel hour
            $sum_2t = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_2t_hour');
            $sum_1_5t = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_1_5t_hour');
            $sum_st = overseasTimesheetModel::where('job_no', $request->job_no)->sum('total_st_hour');
            $new_total_row = $row; 
            $sheet->setCellValue('A'.$new_total_row, 'Rate US$')->getStyle('A'.$new_total_row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$new_total_row, 'Total Hours')->getStyle('B'.$new_total_row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$new_total_row, 'Sub-Total')->getStyle('C'.$new_total_row)->applyFromArray($borders_red);
            $new_total_row++;
            $sheet->setCellValue('A'.$new_total_row, 74)->getStyle('A'.$new_total_row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$new_total_row, $sum_st)->getStyle('B'.$new_total_row)->applyFromArray($borders_red);
            $sub_total_st = $sum_st * 74;
            $sheet->setCellValue('C'.$new_total_row, number_format((float)$sub_total_st, 2, '.', ''))->getStyle('C'.$new_total_row)->applyFromArray($borders_red);
            $new_total_row++;
            $sheet->setCellValue('A'.$new_total_row, 11)->getStyle('A'.$new_total_row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$new_total_row, $sum_1_5t)->getStyle('B'.$new_total_row)->applyFromArray($borders_red);
            $sub_total_1_5t = $sum_1_5t * 11;
            $sheet->setCellValue('C'.$new_total_row, number_format((float)$sub_total_1_5t, 2, '.', ''))->getStyle('C'.$new_total_row)->applyFromArray($borders_red);
            $new_total_row++;
            $sheet->setCellValue('A'.$new_total_row, 148)->getStyle('A'.$new_total_row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$new_total_row, $sum_2t)->getStyle('B'.$new_total_row)->applyFromArray($borders_red);
            $sub_total_2t = $sum_2t * 148;
            $sheet->setCellValue('C'.$new_total_row, number_format((float)$sub_total_2t, 2, '.', ''))->getStyle('C'.$new_total_row)->applyFromArray($borders_red);
            $ot_total = $sub_total_st + $sub_total_1_5t + $sub_total_2t;
            $sheet->setCellValue('I'.$new_total_row, 'Total: ')->getStyle('I'.$new_total_row)->applyFromArray($borders_red);
            $sheet->setCellValue('J'.$new_total_row, number_format((float)$ot_total, 2, '.', ''))->getStyle('J'.$new_total_row)->applyFromArray($borders_red);

            // T&L Section
            $tl_row = $new_total_row + 2;
            $sheet->setCellValue('A'.$tl_row, 'Overseas Expense Statement');
            $tl_row++;
            $sheet->setCellValue('A'.$tl_row, 'NAME')->getStyle('A'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$tl_row, 'DATE')->getStyle('B'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$tl_row, 'AIRFARE')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('D'.$tl_row, 'HOTEL')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('E'.$tl_row, 'HOTEL LAUNDRY')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('F'.$tl_row, 'TAXI')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('G'.$tl_row, 'MOBILE')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('H'.$tl_row, 'VISA')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('I'.$tl_row, 'OTHER')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
            $tl_row++;
            $sheet->setCellValue('A'.$tl_row, '')->getStyle('A'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('B'.$tl_row, '')->getStyle('B'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('C'.$tl_row, 'USD')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('D'.$tl_row, 'USD')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('E'.$tl_row, 'USD')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('F'.$tl_row, 'USD')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('G'.$tl_row, 'USD')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('H'.$tl_row, 'USD')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
            $sheet->setCellValue('I'.$tl_row, 'USD')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
            $tl_row++;
            // get job_no oes collection
            $oes = OverseasExpenseStatementModel::where('job_no', $request->job_no)->get();
            $thisMonthCurrency = masterCurrencyRatesModel::whereMonth('date', date('m'))->first();
            if($thisMonthCurrency != null){
                for($i = 0;$i<count($oes);$i++){
                    $sheet->setCellValue('A'.$tl_row, $oes[$i]->name)->getStyle('A'.$tl_row)->applyFromArray($borders_red);
                    $attach = OesAttachmentModel::where('oes_id', $oes[$i]->id)->get();
                    for($ii=0;$ii<count($attach);$ii++) {
                        $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code',$attach[$ii]->currency)->first();
                        $expenses_usd = $attach[$ii]->expenses / $currency->currency_per_usd;
                        switch ($attach[$ii]->category) {
                            case "Airfare":
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, '')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, '')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, '')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, '')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, '')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, '')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                                break;
                            case "Hotel":
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, '')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, '')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, '')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, '')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, '')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                                break;
                            case "Hotel Laundry":
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, '')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, '')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, '')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, '')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, '')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                                break;
                            case "Taxi":
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, '')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, '')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, '')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, '')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, '')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                                break;
                            case "Mobile":
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, '')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, '')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, '')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, '')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, '')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                                break;
                            case "Visa":
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, '')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, '')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, '')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, '')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, '')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                                break;
                            default:
                                $sheet->setCellValue('B'.$tl_row, $attach[$ii]->date2_from)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('D'.$tl_row, '')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('E'.$tl_row, '')->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('F'.$tl_row, '')->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('G'.$tl_row, '')->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('H'.$tl_row, '')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                                $sheet->setCellValue('I'.$tl_row, number_format($expenses_usd, 2, '.', ''))->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                            
                        }
                    $tl_row++;
                    
                    }
                    // $tl_row++;
                }
                // sub total for T&C
                // sum catagory
                $expenses = OesAttachmentModel::where('job_no', $request->job_no)->get();
                $sum_airfare = 0;
                $sum_hotel = 0;
                $sum_laundry = 0;
                $sum_taxi = 0;
                $sum_mobile = 0;
                $sum_visa = 0;
                $sum_other = 0;
                for($i=0;$i<count($expenses);$i++){
                    switch ($expenses[$i]->category){
                        case "Airfare":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_airfare = $sum_airfare + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Hotel":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_hotel = $sum_hotel + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Hotel Laundry":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_laundry = $sum_laundry + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Taxi":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_taxi = $sum_taxi + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Mobile":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_mobile = $sum_mobile + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        case "Visa":
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_visa = $sum_visa + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                        default:
                            $currency = currencyRatesDetailsModel::where('mcr_id', $thisMonthCurrency->id)->where('code', $expenses[$i]->currency)->first();
                            $sum_other = $sum_other + ($expenses[$i]->expenses / $currency->currency_per_usd);
                        break;
                    }
                }

                // row for sum
                $sheet->setCellValue('A'.$tl_row, 'Sub-Total')->getStyle('A'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$tl_row, '')->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$tl_row, number_format($sum_airfare, 2, '.', ''))->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$tl_row, number_format($sum_hotel, 2, '.', ''))->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('E'.$tl_row, number_format($sum_laundry, 2, '.', ''))->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('F'.$tl_row, number_format($sum_taxi, 2, '.', ''))->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('G'.$tl_row, number_format($sum_mobile, 2, '.', ''))->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('H'.$tl_row, number_format($sum_visa, 2, '.', ''))->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$tl_row, number_format($sum_other, 2, '.', ''))->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                $tl_row++;
                // margin 15%
                $sum_airfare_mgn = $sum_airfare / 0.85;
                $sum_hotel_mgn = $sum_hotel / 0.85;
                $sum_laundry_mgn = $sum_laundry / 0.85;
                $sum_taxi_mgn = $sum_taxi / 0.85;
                $sum_mobile_mgn = $sum_mobile / 0.85;
                $sum_visa_mgn = $sum_visa / 0.85;
                $sum_other_mgn = $sum_other / 0.85;
                $sheet->setCellValue('A'.$tl_row, '15% Margin')->getStyle('A'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$tl_row, '')->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$tl_row, number_format((float)$sum_airfare_mgn, 2, '.', ''))->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$tl_row, number_format((float)$sum_hotel_mgn, 2, '.', ''))->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('E'.$tl_row, number_format((float)$sum_laundry_mgn, 2, '.', ''))->getStyle('E'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('F'.$tl_row, number_format((float)$sum_taxi_mgn, 2, '.', ''))->getStyle('F'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('G'.$tl_row, number_format((float)$sum_mobile_mgn, 2, '.', ''))->getStyle('G'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('H'.$tl_row, number_format((float)$sum_visa_mgn, 2, '.', ''))->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$tl_row, number_format((float)$sum_other_mgn, 2, '.', ''))->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                $tl_row++;
                // sub total
                $sub_total = $sum_airfare_mgn + $sum_hotel_mgn + $sum_laundry_mgn + $sum_taxi_mgn + $sum_mobile_mgn + $sum_visa_mgn + $sum_other_mgn;
                $sheet->setCellValue('H'.$tl_row, 'Total: ')->getStyle('H'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('I'.$tl_row, number_format((float)$sub_total, 2, '.', ''))->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                $tl_row++;
                $tl_row++;
                // tooling and consummables
                $sheet->setCellValue('A'.$tl_row, 'Tooling & Consummables')->getStyle('A'.$tl_row);
                $tl_row++;
                $sheet->setCellValue('A'.$tl_row, 'NAME')->getStyle('A'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$tl_row, 'DESCRIPTION')->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$tl_row, 'CURRENCY')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$tl_row, 'COST')->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                $tl_row++;
                $tooling = toolingConsummablesModel::where('job_no', $request->job_no)->get();
                $total_cost = 0;
                for($i=0;$i<count($tooling);$i++){
                    $sheet->setCellValue('A'.$tl_row, $tooling[$i]->name)->getStyle('A'.$tl_row)->applyFromArray($borders_red);
                    $sheet->setCellValue('B'.$tl_row, $tooling[$i]->description)->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                    $sheet->setCellValue('C'.$tl_row, $tooling[$i]->currency)->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                    $sheet->setCellValue('D'.$tl_row, number_format($tooling[$i]->cost, 2, '.', ''))->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                    $total_cost = $total_cost + $tooling[$i]->cost;
                    $tl_row++;
                }
                // total tooling
                $sheet->setCellValue('A'.$tl_row, 'Total Cost: ')->getStyle('A'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('B'.$tl_row, '')->getStyle('B'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('C'.$tl_row, '')->getStyle('C'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('D'.$tl_row, number_format($total_cost, 2, '.', ''))->getStyle('D'.$tl_row)->applyFromArray($borders_red);
                $tl_row++;
                $tl_row++;
                // USD Total amount
                $grand_total = $total_cost + $sub_total + $ot_total;
                $sheet->setCellValue('I'.$tl_row, 'USD Total Amount : ')->getStyle('I'.$tl_row)->applyFromArray($borders_red);
                $sheet->setCellValue('J'.$tl_row, number_format((float)$grand_total, 2, '.', ''))->getStyle('J'.$tl_row)->applyFromArray($borders_red);
                
                // compile sheet
                $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, "Xlsx");
                header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
                header('Content-Disposition: attachment; filename="'.$request->job_no.'.xlsx"');
                return $writer->save("php://output");
            }else{
                // else this month currency not upload yet
                $request->session()->flash('alert-danger', 'Unable to download. Currency for this month are unavailable in the system.');
                return back();
            }
            
        //download sheet
            
            // $currency_ot = OesAttachmentModel::where('oes_id', $oes->id)->get();
        }
    

        

        

        
    }

    
}
