<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\local_timesheet_detailModel;
use App\AllowancesClaim;
use App\local_timesheetModel;

class AllowancesClaimController extends Controller 
{
    public function __construct(){
        $this->middleware('auth');
    }

    public function index(){
        // $allowances = local_timesheet_detailModel::rightJoin('local_timesheet', 'local_timesheet_detail.lt_id', '=', 'local_timesheet.id')->where('local_timesheet_detail.sso_no', Auth::user()->sso_no)->distinct('local_timesheet.job)no')->get();
        if(Auth::user()->account_type == 1){
            $allowances = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet_detail.lt_id', '=', 'local_timesheet.id')->where('local_timesheet_detail.allowances_claim_lt', '=', 1)->distinct('local_timesheet.job_no')->get(['local_timesheet.job_no', 'local_timesheet_detail.sso_no', 'local_timesheet.id', 'local_timesheet_detail.employee_name']);
        }else{
            $allowances = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet_detail.lt_id', '=', 'local_timesheet.id')->where('local_timesheet_detail.sso_no', '=', Auth::user()->sso_no)->where('local_timesheet_detail.allowances_claim_lt', '=', 1)->distinct('local_timesheet.job_no')->get(['local_timesheet.job_no', 'local_timesheet_detail.sso_no', 'local_timesheet.id', 'local_timesheet_detail.employee_name']);
        }
        return view('allowances_claim.index', compact('allowances'));
    }

    public function create(){
        // get data from model
        if(Auth::user()->sso_no != null){
            $allowances = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->where('local_timesheet_detail.sso_no', Auth::user()->sso_no)->where('local_timesheet_detail.allowances_claim_lt', '=', 0)->get();
        // check data
        $claimPrices = AllowancesClaim::getclaimPrices();
        return view('allowances_claim.create', compact('allowances', 'claimPrices'));
        }
        
        
    }

    public function show($id){
        if(Auth::user()->account_type == 1){
            $allowances = local_timesheet_detailModel::rightJoin('allowances_claim_lt', 'local_timesheet_detail.id', '=', 'allowances_claim_lt.ltd_id')->where('local_timesheet_detail.lt_id', '=', $id)->where('local_timesheet_detail.allowances_claim_lt', '=', 1)->get();
        }else{
            $allowances = local_timesheet_detailModel::rightJoin('allowances_claim_lt', 'local_timesheet_detail.id', '=', 'allowances_claim_lt.ltd_id')->where('local_timesheet_detail.lt_id', '=', $id)->where('sso_no', '=', Auth::user()->sso_no)->where('local_timesheet_detail.allowances_claim_lt', '=', 1)->get();
        }
        $claimPrices = AllowancesClaim::getclaimPrices();
        return view('allowances_claim.show', compact('allowances', 'claimPrices'));
    }

    public function store(Request $request){
        // return dd(request()->input());
        // VALIDATE USER INPUT
        $this->validate(request(), [
            'Dollar.*' => 'numeric'
        ],
        [
            'Dollar.*.numeric' => "The $ must be a number."
        ]);
        if($request->input('OBA') == null){
            $request->session()->flash('alert-danger', 'No input was submitted');
            return back();
        }
        // PROCESS USER INPUT
        if($request){
            $total_claim_price = AllowancesClaim::totalClaimPrices($request->input());
            for($i=0;$i<count($request->OBA);$i++){
                // prepare save to db
                try{
                    $allowances_claim = new AllowancesClaim;
                    $allowances_claim->ltd_id = $request->ltd_id[$i];
                    $allowances_claim->on_board_allowance = AllowancesClaim::claimPrices($request->OBA[$i]);
                    $allowances_claim->actg_leadhand = AllowancesClaim::claimPrices($request->ACTGL[$i]);
                    $allowances_claim->workshop_night_allow = AllowancesClaim::claimPrices($request->WNA[$i]);
                    $allowances_claim->workshop_night_shift_allow = AllowancesClaim::claimPrices($request->WNSA[$i]);
                    $allowances_claim->workshop_transport_allow = AllowancesClaim::claimPrices($request->WTA[$i]);
                    $allowances_claim->breakfast = AllowancesClaim::claimPrices($request->Breakfast[$i]);
                    $allowances_claim->lunch = AllowancesClaim::claimPrices($request->Lunch[$i]);
                    $allowances_claim->dinner = AllowancesClaim::claimPrices($request->Dinner[$i]);
                    $allowances_claim->midnight = AllowancesClaim::claimPrices($request->Midnight[$i]);
                    $allowances_claim->descrip = $request->Descript[$i];
                    $allowances_claim->dollar = $request->Dollar[$i];
                    $allowances_claim->sub_total = $total_claim_price[$i];
                    // update local_timesheet_detail allowances claim status
                    $lt_detail = local_timesheet_detailModel::find($request->ltd_id[$i]);
                    if($lt_detail != null){
                        $allowances_claim->save();
                        $lt_detail->allowances_claim_lt = 1;
                        $lt_detail->update();
                    }else{
                        return abort(404);
                    }
                    // set the variable to null
                    $allowances_claim = null;
                    $lt_detail = null;
                } catch(Exception $e) {
                    return abort(404);
                }
                
            }
            $request->session()->flash('alert-success', 'Allowances Claim Save Success');
            return back();
        }
        
        
    }

    public function edit($id){
        $allowance = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->leftJoin('allowances_claim_lt', 'allowances_claim_lt.ltd_id', '=', 'local_timesheet_detail.id')->where('allowances_claim_lt.id', $id)->firstOrFail();
        $claimPrices = AllowancesClaim::getclaimPrices();
        return view('allowances_claim.edit', compact('allowance', 'claimPrices'));
    }

    public function update(Request $request, $id){
        $this->validate(request(), [
            'Dollar' => 'numeric'
        ],
        [
            'Dollar.*.numeric' => "The $ must be a number."
        ]);
        if($request){
        try{
                //INSERT DATA TO DATABASE
                $updateData = $this->updateDataToDatabase($id, $request->input());
                //RETURN RESPONSE TO USER
                $request->session()->flash('alert-success', 'Allowances Claim Update Success');
                return back();
            }  catch(Exception $e){
                return dd($e);
            }
        }
    }


    public function delete(Request $request, $id){
        $local_timesheet_details = local_timesheet_detailModel::where('lt_id', '=', $id)->where('allowances_claim_lt', '=', 1)->get();
        for($i=0;$i<count($local_timesheet_details);$i++){
            $allowance_claim = AllowancesClaim::where('ltd_id', $local_timesheet_details[$i]->id)->first();
            $local_timesheet_detail = local_timesheet_detailModel::find($local_timesheet_details[$i]->id);
            if($allowance_claim != null){
                $allowance_claim->delete();
                $local_timesheet_detail->allowances_claim_lt = 0;
                $local_timesheet_detail->update();
            }else{
                $allowance_claim = null;
                $local_timesheet_detail = null;
            }
        }
        $request->session()->flash('alert-danger', 'Delete Success');
        return back();
    }

    // this is method for ajax function
    public function deleteClaim(Request $request, $id){
        $claim = AllowancesClaim::find($id);
        if($claim != null){
            $local_timesheet_detail = local_timesheet_detailModel::where('id', '=', $claim->ltd_id)->first();
            if($local_timesheet_detail != null){
                $claim->delete();
                $local_timesheet_detail->allowances_claim_lt = 0;
                $local_timesheet_detail->update();
                return response()->json([
                    'data' => true
                ]);
            }
            return response()->json([
                    'data' => false
                ]);
            
        }else{
            return response()->json([
                'data' => false
            ]);
        }
        
        // return dd($claim);
    }



    //FUNCTIONAL FUNCTION (NOT A CONTROLLER)
    public function updateDataToDatabase($id = null, $requestInput = null){
        try{
            if($id != null && $requestInput != null){
                //GET DATA FROM DATABASE
                $allowances_claim = AllowancesClaim::findOrFail($id);
                $claimPrices = AllowancesClaim::getClaimPrices();
                //INSERT DATA
                //LOGIC FOR TARGET PRICES AND DATABASE PRICE COMPARISION
                if($allowances_claim->on_board_allowance != $claimPrices['OBA'] && $allowances_claim->on_board_allowance != 0){
                    if($requestInput['OBA'] == "OBA-NewPrice"){
                        $allowances_claim->on_board_allowance = $claimPrices['OBA'];
                    }elseif($requestInput['OBA'] == "0"){
                        $allowances_claim->on_board_allowance = 0;
                    }
                }else{
                    if($requestInput['OBA'] == "OBA"){
                        $allowances_claim->on_board_allowance = $claimPrices['OBA'];
                    }else{
                        $allowances_claim->on_board_allowance = 0;
                    }
                }

                if($allowances_claim->actg_leadhand != $claimPrices['ACTGL'] && $allowances_claim->actg_leadhand != 0){
                    if($requestInput['ACTGL'] == "ACTGL-NewPrice"){
                        $allowances_claim->actg_leadhand = $claimPrices['ACTGL'];
                    }elseif($requestInput['ACTGL'] == "0"){
                        $allowances_claim->actg_leadhand = 0;
                    }
                }else{
                    if($requestInput['ACTGL'] == "ACTGL"){
                        $allowances_claim->actg_leadhand = $claimPrices['ACTGL'];
                    }else{
                        $allowances_claim->actg_leadhand = 0;
                    }
                }

                if($allowances_claim->workshop_night_allow != $claimPrices['WNA'] && $allowances_claim->workshop_night_allow != 0){
                    if($requestInput['WNA'] == "WNA-NewPrice"){
                        $allowances_claim->workshop_night_allow = $claimPrices['WNA'];
                    }elseif($requestInput['WNA'] == "0"){
                        $allowances_claim->workshop_night_allow = 0;
                    }
                }else{
                    if($requestInput['WNA'] == "WNA"){
                        $allowances_claim->workshop_night_allow = $claimPrices['WNA'];
                    }else{
                        $allowances_claim->workshop_night_allow = 0;
                    }
                }

                if($allowances_claim->workshop_night_shift_allow != $claimPrices['WNSA'] && $allowances_claim->workshop_night_shift_allow != 0){
                    if($requestInput['WNSA'] == "WNSA-NewPrice"){
                        $allowances_claim->workshop_night_shift_allow = $claimPrices['WNSA'];
                    }elseif($requestInput['WNSA'] == "0"){
                        $allowances_claim->workshop_night_shift_allow = 0;
                    }
                }else{
                    if($requestInput['WNSA'] == "WNSA"){
                        $allowances_claim->workshop_night_shift_allow = $claimPrices['WNSA'];
                    }else{
                        $allowances_claim->workshop_night_shift_allow = 0;
                    }
                }

                if($allowances_claim->workshop_transport_allow != $claimPrices['WTA'] && $allowances_claim->workshop_transport_allow != 0){
                    if($requestInput['WTA'] == "WTA-NewPrice"){
                        $allowances_claim->workshop_transport_allow = $claimPrices['WTA'];
                    }elseif($requestInput['WTA'] == "0"){
                        $allowances_claim->workshop_transport_allow = 0;
                    }
                }else{
                    if($requestInput['WTA'] == "WTA"){
                        $allowances_claim->workshop_transport_allow = $claimPrices['WTA'];
                    }else{
                        $allowances_claim->workshop_transport_allow = 0;
                    }
                }

                if($allowances_claim->breakfast != $claimPrices['BREAKFAST'] && $allowances_claim->breakfast != 0){
                    if($requestInput['BREAKFAST'] == "BREAKFAST-NewPrice"){
                        $allowances_claim->breakfast = $claimPrices['BREAKFAST'];
                    }elseif($requestInput['BREAKFAST'] == "0"){
                        $allowances_claim->breakfast = 0;
                    }
                }else{
                    if($requestInput['BREAKFAST'] == "BREAKFAST"){
                        $allowances_claim->breakfast = $claimPrices['BREAKFAST'];
                    }else{
                        $allowances_claim->breakfast = 0;
                    }
                }

                if($allowances_claim->lunch != $claimPrices['LUNCH'] && $allowances_claim->lunch != 0){
                    if($requestInput['LUNCH'] == "LUNCH-NewPrice"){
                        $allowances_claim->lunch = $claimPrices['LUNCH'];
                    }elseif($requestInput['LUNCH'] == "0"){
                        $allowances_claim->lunch = 0;
                    }
                }else{
                    if($requestInput['LUNCH'] == "LUNCH"){
                        $allowances_claim->lunch = $claimPrices['LUNCH'];
                    }else{
                        $allowances_claim->lunch = 0;
                    }
                }

                if($allowances_claim->dinner != $claimPrices['DINNER'] && $allowances_claim->dinner != 0){
                    if($requestInput['DINNER'] == "DINNER-NewPrice"){
                        $allowances_claim->dinner = $claimPrices['DINNER'];
                    }elseif($requestInput['DINNER'] == "0"){
                        $allowances_claim->dinner = 0;
                    }
                }else{
                    if($requestInput['DINNER'] == "DINNER"){
                        $allowances_claim->dinner = $claimPrices['DINNER'];
                    }else{
                        $allowances_claim->dinner = 0;
                    }
                }

                if($allowances_claim->midnight != $claimPrices['MIDNIGHT'] && $allowances_claim->midnight != 0){
                    if($requestInput['MIDNIGHT'] == "MIDNIGHT-NewPrice"){
                        $allowances_claim->midnight = $claimPrices['MIDNIGHT'];
                    }elseif($requestInput['MIDNIGHT'] == "0"){
                        $allowances_claim->midnight = 0;
                    }
                }else{
                    if($requestInput['MIDNIGHT'] == "MIDNIGHT"){
                        $allowances_claim->midnight = $claimPrices['MIDNIGHT'];
                    }else{
                        $allowances_claim->midnight = 0;
                    }
                }

                

                // CALCULATE TOTAL PRICE
                $total_claim_price = $allowances_claim->on_board_allowance + $allowances_claim->actg_leadhand + $allowances_claim->workshop_night_allow + $allowances_claim->workshop_night_shift_allow + $allowances_claim->workshop_transport_allow + $allowances_claim->breakfast + $allowances_claim->lunch + $allowances_claim->dinner + $allowances_claim->midnight + $allowances_claim->dollar;
                $allowances_claim["sub_total"] = $total_claim_price;
                // UPDATE TO DATABASE
                $allowances_claim->update();
                //UPDATE DATA TO DATABASE
                // $allowances_claim->update();
                return $allowances_claim;
            }else{
                return false;
            }
        }catch(Exception $e){
            return $e;
        }
    }

    
}