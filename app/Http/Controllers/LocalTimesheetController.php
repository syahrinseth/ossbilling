<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Storage;

use App\local_timesheetModel;
use App\local_timesheet_detailModel;
use App\master_jobsModel;
use App\User;
use App\ltRates;

class LocalTimesheetController extends Controller
{
    public function __construct(){
        $this->middleware('auth');

        function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%h:%i:%s');
        }

        function decimalHours($time){
            $hms = explode(":", $time);
            return ($hms[0] + ($hms[1]/60) + ($hms[2]/3600));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // index page
            $local_timesheets = local_timesheetModel::all();
            return view('local_timesheet.index', compact('local_timesheets'));
    }

    public function pending(){
         // view pending
        $local_timesheets = local_timesheetModel::where('status', 0 )->get();
        return view('local_timesheet.index', compact('local_timesheets'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // page for create local timesheet
        $master_jobs = master_jobsModel::all();
        return view('local_timesheet.create', compact('master_jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // page for storing created local timesheet
        $this->validate(request(),[
            // local_timesheet data
            'job_no' => 'required',
            // 'customer_signature_name' => 'required',
            // 'customer_signature_attachment' => 'required|image',
            // local_timesheet_detail data
            // 'employee_name' => 'required',
            // 'sso_no' => 'required',
            // 'designation.*' => 'required',
            // 'day.*' => 'required',
            // 'date.*' => 'required',
            // 'remark.*' => 'required',
            // 'travel_start.*' => 'required',
            // 'travel_finish.*' => 'required',
            // 'travel2_start.*' => 'required',
            // 'travel2_finish.*' => 'required',
            // 'work_start.*' => 'required',
            // 'work_finish.*' => 'required',
            // 'work2_start.*' => 'required',
            // 'work2_finish.*' => 'required',
        ]);
        if($request){
            $job = master_jobsModel::find($request->job_no);
            if($job){
                $check = local_timesheetModel::where('job_no', $request->job_no)->exists();
                if($check != true){
                    // where sso within job no does not exists
                    $local_timesheet = new local_timesheetModel;
                    $local_timesheet->job_no = $request->job_no;
                    $local_timesheet->customer_signature_attachment = "no-file.png";
                    $local_timesheet->save();
                    $request->session()->flash('alert-success', 'Local Timesheet Created Successfully.');
                    return back(); 
                }else{
                    $request->session()->flash('alert-danger', 'Job Number Already Exist');
                    return back();
                }
                
            }else{
                $request->session()->flash('alert-danger', 'Job Number Does Not Exists');
                return back(); 
            }
        
        }else{
            $request->session()->flash('alert-danger', 'Unable To Create Local Timesheet');
            return back(); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($lt_id)
    {
        // show local_timesheet page
        $local_timesheet = local_timesheetModel::findOrFail($lt_id);
        if($local_timesheet){
            $job = master_jobsModel::find($local_timesheet->job_no);
        }
        $lt_details = local_timesheet_detailModel::where('lt_id', $lt_id)->orderBy('date', 'ASC')->get();
        return view('local_timesheet.show', compact('local_timesheet', 'job', 'lt_details'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($lt_id)
    {
        // edit page for local_timesheet
        $master_jobs = master_jobsModel::all();
        $local_timesheet = local_timesheetModel::findOrFail($lt_id);
        return view('local_timesheet.edit', compact('local_timesheet', 'master_jobs'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $lt_id)
    {
        // update page for local_timesheet
        $this->validate(request(),[
            // local_timesheet data
            // 'job_no' => 'required',
            // 'customer_signature_name' => 'required',
            'customer_signature_attachment' => 'mimes:jpeg,png,jpg,pdf',
        ]);
        if($request){
        $local_timesheet = local_timesheetModel::findOrFail($lt_id);
        if($request->customer_signature_attachment){
                    // Storage::disk('uploads')->delete('local_timesheet/'.$local_timesheet->customer_signature_attachment);
                    // // Handle File Upload     
                    // // Get filename with the extension
                    // $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // // Get just filename
                    // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // // Get just ext
                    // $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // // Filename to store
                    // $fileNameToStore = $local_timesheet->job_no.'_'.'local_timesheet'.'_'.'cust_sign'.'_'.rand(1, 1000000) .'.'.$extension;
                    // // Upload image
                    // // $request->customer_signature_attachment->storeAs('public/local_timesheet/'. $request->job_no .'/', $fileNameToStore);
                    // $request->file('customer_signature_attachment')->move(public_path('/uploads/local_timesheet/'), $fileNameToStore);

                    Storage::disk('local')->delete('public/local_timesheet/'.$lt_id.'/'.'cust_sign/'.$local_timesheet->customer_signature_attachment);
                    // Handle File Upload
                    // Get filename with the extension
                    $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // Get just filename
                    // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore = $local_timesheet->job_no.'_'.'local_timesheet'.'_'.'cust_sign'.'_'.rand(1, 1000000) .'.'.$extension;
                    // Upload image
                    $path = $request->customer_signature_attachment->storeAs('public/local_timesheet/'.$lt_id .'/'.'cust_sign/', $fileNameToStore);


                    $local_timesheet->customer_signature_attachment = $fileNameToStore;
                    $local_timesheet->customer_signature_date = date("Y-m-d");
                    $local_timesheet->customer_signature_name = $request->customer_signature_name;
                    if($local_timesheet->dept_signature_name && $local_timesheet->customer_signature_name){
                        $local_timesheet->status = 1;
                    }else{
                        $local_timesheet->status = 0;
                    }
  
        }

        if($request->customer_signature_name){
            $local_timesheet->customer_signature_name = $request['customer_signature_name'];
        }
        $local_timesheet->update();
            $request->session()->flash('alert-success', 'Local Timesheet Updated Successfully.');
        return back(); 
        }else{
            $request->session()->flash('alert-danger', 'Unable To Update Local Timesheet.');
            return back(); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($lt_id)
    {
        // delete function for local_timesheet
        if(Auth::user()->account_type == 1){
            $local_timesheet = local_timesheetModel::findOrFail($lt_id);
            if($local_timesheet){
                // Storage::disk('uploads')->delete('local_timesheet/'.$local_timesheet->customer_signature_attachment);
                Storage::deleteDirectory('public/local_timesheet/'.$lt_id.'/'.'cust_sign/'.$local_timesheet->customer_signature_attachment);
                $local_timesheet->delete();
                return redirect()->route('index_lt'); 
            }else{
                return back(); 
            }
        }else{
            return abort(404);
        }
        
        
        
    }

    public function createDetail($lt_id){
        $local_timesheet = local_timesheetModel::findOrFail($lt_id);
        $users = User::all();
        if($local_timesheet){
            return view('local_timesheet.create_detail', compact('local_timesheet', 'users'));
        }
    }

    public function storeDetail(Request $request, $lt_id){
        for($i=0;$i<count($request->date);$i++){
            if($request->standby[$i] == 1){
                $data['remark'][$i] = 'standby';
            }else{
                $this->validate(request(), [
                    'sso_no' => 'required',
                    'designation.'.$i => 'required',
                    'date.'.$i => 'required|date',
                    // 'remark.'.$i => 'required',
                ]);
                $data['remark'][$i] = $request['remark'][$i];
            }
            if($request->nextdaysite[$i] == 1){
                $data['nextdaysite'][$i] = true;
            }else{
                $data['nextdaysite'][$i] = false;
            }
            if($request->nextdayhome[$i] == 1){
                $data['nextdayhome'][$i] = true;
            }else{
                $data['nextdayhome'][$i] = false;
            }
            
        }
        $request->merge($data);
        return $this->storeLocalTimesheets($request, $lt_id);
        
    }

    public function editDetail($ltd_id){
        $lt_detail = local_timesheet_detailModel::findOrFail($ltd_id);
        $users = User::all();
        return view('local_timesheet.edit_detail', compact('lt_detail', 'users'));
    }

    public function updateDetail(Request $request, $ltd_id){

        if($request->standby == 1){
                $request->remark = "standby";
        }else{
                $this->validate(request(),[
                    'remark' => 'required',
                    'date' => 'required|date',
                    'designation' => 'required'
                ]);
        }
        if($request->nextdaysite == 1){
            $request->nextdaysite = true;
        }else{
            $request->nextdaysite = false;
        }
        if($request->nextdayhome == 1){
            $request->nextdayhome = true;
        }else{
            $request->nextdayhome = false;
        }

        $lt_detail = local_timesheet_detailModel::findOrFail($ltd_id);
        $check = local_timesheet_detailModel::where('sso_no', $lt_detail['sso_no'])->whereDate('date', $request->date)->first();
        
        if($lt_detail){
                    // if date exists within user and its the same
                    $lt_detail->date = $request->date;
                    $lt_detail->day = date('l', strtotime($request->date));
                    $lt_detail->designation = $request->designation;
                    $lt_detail->travel_start = $request->travel_start;
                    $lt_detail->travel_finish = $request->travel_finish;
                    $lt_detail->travel2_start = $request->travel2_start;
                    $lt_detail->travel2_finish = $request->travel2_finish;
                    $lt_detail->work_start = $request->work_start;
                    $lt_detail->work_finish = $request->work_finish;
                    $lt_detail->work2_start = $request->work2_start;
                    $lt_detail->work2_finish = $request->work2_finish;
                    $lt_detail->remark = $request->remark;
                    $lt_detail->nextdaysite = $request->nextdaysite;
                    $lt_detail->nextdayhome = $request->nextdayhome;

                    if($request->remark == "standby"){
                        if($lt_detail->work_start && $lt_detail->work_finish){
                            $total_hour1 = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($lt_detail->work2_start && $lt_detail->work2_finish){
                            $total_hour2 = strtotime($lt_detail->work2_finish) - strtotime($lt_detail->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $lt_detail->total_hour_2x = 0;
                        $lt_detail->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $lt_detail->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($request->date)) == "Saturday"){
                        if($lt_detail->work_start && $lt_detail->work_finish){
                            $total_hour1 = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($lt_detail->work2_start && $lt_detail->work2_finish){
                            $total_hour2 = strtotime($lt_detail->work2_finish) - strtotime($lt_detail->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $lt_detail->total_hour_2x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $lt_detail->total_hour_1x = 0;
                        $lt_detail->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($request->date)) == "Sunday"){
                        if($lt_detail->work_start && $lt_detail->work_finish){
                            $total_hour1 = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($lt_detail->work2_start && $lt_detail->work2_finish){
                            $total_hour2 = strtotime($lt_detail->work2_finish) - strtotime($lt_detail->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $lt_detail->total_hour_2x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $lt_detail->total_hour_1x = 0;
                        $lt_detail->total_hour_1_5x = 0;
                    }else{
                        // if days on weekdays
                        $firstEightHour = strtotime('08:00:00') - strtotime('00:00:00');

                            if(strtotime($lt_detail->work2_finish) < strtotime($lt_detail->work2_start)){
                                // start new code for employee has night shift
                                if($lt_detail->work_start && $lt_detail->work_finish){
                                    $beforeLunch = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                                }else{
                                    $beforeLunch = 0;
                                }
                                if($lt_detail->work2_start && $lt_detail->work2_finish){
                                    $afterLunch = strtotime('23:59:59') - strtotime($lt_detail->work2_start);
                                }else{
                                    $afterLunch = 0;
                                }
                                // calculation for total work hour excluded night shift
                                $total_workhours = $afterLunch + $beforeLunch;
                                if($total_workhours < $firstEightHour){
                                    $_1_5x = 0;
                                    $_1x = $total_workhours;
                                }elseif($total_workhours > $firstEightHour){
                                    $_1_5x = $total_workhours - $firstEightHour;
                                    $_1x = $firstEightHour;
                                }
                                // calculation for 2 times
                                $_2_0x = strtotime($lt_detail->work2_finish) - strtotime('00:00:00');
                        
                                // calculate total hour
                                $lt_detail->total_hour_1x = decimalHours(secondsToTime($_1x));
                                $lt_detail->total_hour_1_5x = decimalHours(secondsToTime($_1_5x));
                                $lt_detail->total_hour_2x = decimalHours(secondsToTime($_2_0x));
                                // end new code
                                // $total_hour1 = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                                // $total_hour2 = strtotime("17:00:00") - strtotime($lt_detail->work2_start);
                                // $total_hour3 = strtotime($lt_detail->work2_finish) - strtotime("17:00:00");
                                // $lt_detail->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                                // $lt_detail->total_hour_1_5x = decimalHours(secondsToTime($total_hour3));
                                // $lt_detail->total_hour_2x = 0;
                            }elseif(strtotime($lt_detail->work2_finish) > strtotime($lt_detail->work2_start)){
                                // new code no night shift but have before and after lunch data
                                if($lt_detail->work_start && $lt_detail->work_finish){
                                    $beforeLunch = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                                }else{
                                    $beforeLunch = 0;
                                }
                                if($lt_detail->work2_start && $lt_detail->work2_finish){
                                    $afterLunch = strtotime($lt_detail->work2_finish) - strtotime($lt_detail->work2_start);
                                }else{
                                    $afterLunch = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours = $afterLunch + $beforeLunch;
                                if($total_workhours < $firstEightHour){
                                    $_1_5x = 0;
                                    $_1x = $total_workhours;
                                }else{
                                    $_1_5x = $total_workhours - $firstEightHour;
                                    $_1x = $firstEightHour;
                                }
                                // calculation for 2 times
                                $_2_0x = 0;
                                // calculate total hour
                                $lt_detail->total_hour_1x = decimalHours(secondsToTime($_1x));
                                $lt_detail->total_hour_1_5x = decimalHours(secondsToTime($_1_5x));
                                $lt_detail->total_hour_2x = decimalHours(secondsToTime($_2_0x));
                                // end new code
                                // $total_hour1 = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                                // $total_hour2 = strtotime($lt_detail->work2_finish) - strtotime($lt_detail->work2_start);
                                // $lt_detail->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                                // $lt_detail->total_hour_1_5x = 0;
                                // $lt_detail->total_hour_2x = 0;
                            }else{
                                if($lt_detail->work_start && $lt_detail->work_finish){
                                    $beforeLunch = strtotime($lt_detail->work_finish) - strtotime($lt_detail->work_start);
                                }else{
                                    $beforeLunch = 0;
                                }
                                if($lt_detail->work2_start && $lt_detail->work2_finish){
                                    $afterLunch = strtotime($lt_detail->work2_finish) - strtotime($lt_detail->work2_start);
                                }else{
                                    $afterLunch = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours = $afterLunch + $beforeLunch;
                                if($total_workhours < $firstEightHour){
                                    $_1_5x = 0;
                                    $_1x = $total_workhours;
                                }else{
                                    $_1_5x = $total_workhours - $firstEightHour;
                                    $_1x = $firstEightHour;
                                }
                                // calculation for 2 times
                                $_2_0x = 0;
                        
                                // calculate total hour
                                $lt_detail->total_hour_1x = decimalHours(secondsToTime($_1x));
                                $lt_detail->total_hour_1_5x = decimalHours(secondsToTime($_1_5x));
                                $lt_detail->total_hour_2x = decimalHours(secondsToTime($_2_0x));
                            }
                        
                    } 
                    // end if

                    // calculate total travel (new code)
                    if($lt_detail->travel_finish && $lt_detail->travel_start){
                        if($request->nextdaysite == true){
                            $totaltravel_1 = strtotime('23:59:59') - strtotime($lt_detail->travel_start);
                            $totaltravel_2 = strtotime($lt_detail->travel_finish) - strtotime('00:00:00');
                            $total_travel_hour1 = decimalHours(secondsToTime($totaltravel_1)) + decimalHours(secondsToTime($totaltravel_2));
                        }else{
                            if($lt_detail->travel_finish < $lt_detail->travel_start){
                                $totaltravel_1 = strtotime('23:59:59') - strtotime($lt_detail->travel_start);
                                $totaltravel_2 = strtotime($lt_detail->travel_finish) - strtotime('00:00:00');
                                $total_travel_hour1 = decimalHours(secondsToTime($totaltravel_1 + $totaltravel_2));
                            }else{
                                $total_travel_hour1 = decimalHours(secondsToTime(strtotime($lt_detail->travel_finish) - strtotime($lt_detail->travel_start)));
                            }   
                        }
                    }else{
                        $total_travel_hour1 = 0;
                    }

                    if($lt_detail->travel2_finish && $lt_detail->travel2_start){
                        if($request->nextdayhome == true){
                            $totaltravel_1 = strtotime('23:59:59') - strtotime($lt_detail->travel2_start);
                            $totaltravel_2 = strtotime($lt_detail->travel2_finish) - strtotime('00:00:00');
                            $total_travel_hour2 = decimalHours(secondsToTime($totaltravel_1)) + decimalHours(secondsToTime($totaltravel_2));
                        }else{
                            if($lt_detail->travel2_finish < $lt_detail->travel2_start){
                                $totaltravel_tosite2 = strtotime('23:59:59') - strtotime($lt_detail->travel2_start);
                                $totaltravel_tohome2 = strtotime($lt_detail->travel2_finish) - strtotime('00:00:00');
                                $total_travel_hour2 = decimalHours(secondsToTime($totaltravel_tosite2 + $totaltravel_tohome2));
                            }else{
                                $total_travel_hour2 = decimalHours(secondsToTime(strtotime($lt_detail->travel2_finish) - strtotime($lt_detail->travel2_start)));
                            } 
                        }
                         
                    }else{
                        $total_travel_hour2 = 0;
                    }
                    // // calculate total travel hour
                    // $total_travel_hour1 = strtotime($lt_detail->travel_finish) - strtoti($lt_detail->travel_start);
                    // $total_travel_hour2 = strtotime($lt_detail->travel2_finish) strtotime($lt_detail->travel2_start);
                    // total travel hours
                    $lt_detail->total_travel_hour = $total_travel_hour1 + $total_travel_hour2;
                    $lt_detail->total_work_hour = $lt_detail->total_hour_1x + $lt_detail->total_hour_1_5x + $lt_detail->total_hour_2x;
                    // $lt_detail->total_hour_1x += $lt_detail->total_travel_hour;
                    $lt_detail->update();

                    // insert total hour on local_timesheet
                    $local_timesheets = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->where('local_timesheet.id', $lt_detail->lt_id)->get();
                    if($local_timesheets){
                            $total_st_hour = 0;
                            $total_1_5t_hour = 0;
                            $total_2t_hour = 0;
                            $total_travel_hours = 0;
                            $total_work_hours = 0;
                            for($i = 0;$i < count($local_timesheets);$i++){
                                $total_st_hour = $total_st_hour + $local_timesheets[$i]->total_hour_1x;
                                $total_1_5t_hour = $total_1_5t_hour + $local_timesheets[$i]->total_hour_1_5x;
                                $total_2t_hour = $total_2t_hour + $local_timesheets[$i]->total_hour_2x;
                                $total_travel_hours = $total_travel_hours + $local_timesheets[$i]->total_travel_hour;
                                $total_work_hours = $total_work_hours + $local_timesheets[$i]->total_work_hour;
                            }
                            if($local_timesheets[0]){
                                $lt = local_timesheetModel::find($local_timesheets[0]->lt_id);
                                $lt->total_st_hour = $total_st_hour;
                                $lt->total_1_5t_hour = $total_1_5t_hour;
                                $lt->total_2t_hour = $total_2t_hour;
                                $lt->total_travel_hours = $total_travel_hours;
                                $lt->total_work_hours = $total_work_hours;
                                $lt->update();
                                $request->session()->flash('alert-success', 'Form Successfully Submited');
                                return back();
                            }else{
                                $request->session()->flash('alert-danger', 'Failed');
                                return back();
                            }
                    }else{
                        $request->session()->flash('alert-danger', 'Failed');
                        return back;
                    }
           
        }else{
            $request->session()->flash('alert-danger', 'Failed');
            return back();
        }
    }

    public function showDetail($lt_id){
        
    }

    public function destroyDetail($ltd_id){
        $lt_detail = local_timesheet_detailModel::findOrFail($ltd_id);
        if($lt_detail){
            $lt_detail->delete();
            // insert total hour on local_timesheet
            $local_timesheets = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->where('local_timesheet.id', $lt_detail->lt_id)->get();
            if($local_timesheets){
                    $total_st_hour = 0;
                    $total_1_5t_hour = 0;
                    $total_2t_hour = 0;
                    $total_travel_hours = 0;
                    $total_work_hours = 0;
                    for($i = 0;$i < count($local_timesheets);$i++){
                        $total_st_hour = $total_st_hour + $local_timesheets[$i]->total_hour_1x;
                        $total_1_5t_hour = $total_1_5t_hour + $local_timesheets[$i]->total_hour_1_5x;
                        $total_2t_hour = $total_2t_hour + $local_timesheets[$i]->total_hour_2x;
                        $total_travel_hours = $total_travel_hours + $local_timesheets[$i]->total_travel_hour;
                        $total_work_hours = $total_work_hours + $local_timesheets[$i]->total_work_hour;
                    }
                    
                        if($lt_detail->lt_id){
                        $lt = local_timesheetModel::findOrFail($lt_detail->lt_id);
                        $lt->total_st_hour = $total_st_hour;
                        $lt->total_1_5t_hour = $total_1_5t_hour;
                        $lt->total_2t_hour = $total_2t_hour;
                        $lt->total_travel_hours = $total_travel_hours;
                        $lt->total_work_hours = $total_work_hours;
                        // save update
                        $lt->update();
                        return back();
                        }else{
                            return abort(404, "Error 404");
                        }
                    
                    
                    
            }else{
                return abort(404, "Error 404");
            }
            
        }
        
    }

    public function deptApprove(Request $request, $lt_id){
        // verify user type
        if(Auth::user()->account_type != 1){
            return abort(404);
        }
        $local_timesheet = local_timesheetModel::findOrFail($lt_id);
        if($local_timesheet){
            $local_timesheet->dept_signature_name = Auth::user()->name;
            $local_timesheet->dept_signature_date = date("Y-m-d");
            if($local_timesheet->customer_signature_attachment != 'no-file.png'){
                $local_timesheet->status = 1;
            }
            $local_timesheet->update();
            $request->session()->flash('alert-success', 'Your signature has been submited');
            return back();
        }else{
            $request->session()->flash('alert-danger', 'Failed');
            return back();
        }
    }

    public function print($lt_id){
        $local_timesheets = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->where('local_timesheet.id', $lt_id)->orderBy('date', 'ASC')->get();
        $master_localtimesheet = local_timesheetModel::findOrFail($lt_id);
        if(count($local_timesheets) > 0){
            $lt_date = local_timesheet_detailModel::where('lt_id', $lt_id)->distinct('date')->get(['date','day']);
            $job = master_jobsModel::find($master_localtimesheet->job_no);
            // return view('local_timesheet.print', compact('local_timesheets', 'lt_date', 'job'));
            $pdf = \PDF::loadView('local_timesheet.print', compact('local_timesheets', 'lt_date', 'job', 'master_localtimesheet'));
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download($master_localtimesheet->job_no.'-local_timesheet.pdf');
        }else{
            return abort(404, "Error");
        }
        
    }

    public function custAttDownloadPdf($lt_id){
        $cust_sign_att = local_timesheetModel::findOrFail($lt_id);
        if($cust_sign_att){
            // return response()->download(public_path("/uploads/local_timesheet/".$cust_sign_att['customer_signature_attachment']));
            return response()->download(public_path("/storage/local_timesheet/".$lt_id."/cust_sign"."/".$cust_sign_att['customer_signature_attachment']));
        }else{
            return abort(404);
        }
    }




    // *******************************************************************
    // Function for storing employee Local Timesheets data with logics
    public function storeLocalTimesheets($request, $lt_id){
        $local_timesheet = local_timesheetModel::findOrFail($lt_id);
        if($local_timesheet){
            // check existing localtimesheet date between sso no
            for($i = 0; $i < count($request->date);$i++){
                $lt_detail[$i] = local_timesheet_detailModel::where('lt_id', $local_timesheet->id)->where('sso_no', $request['sso_no'])->whereDate('date', $request->date[$i])->first();
                if($lt_detail[$i] != null){
                    $request->session()->flash('alert-danger', 'Date '.$request->date[$i].' on sso# '.$request['sso_no'].' is already exist in your timesheets.');
                    return back();
                }
            }


            for($i = 0; $i < count($request->date);$i++){
                $lt_detail[$i] = local_timesheet_detailModel::where('lt_id', $local_timesheet->id)->where('sso_no', $request['sso_no'])->whereDate('date', $request->date[$i])->first();
                if($lt_detail[$i] == null){
                    $lt_detail[$i] = new local_timesheet_detailModel;
                    $lt_detail[$i]->lt_id = $lt_id;
                    $lt_detail[$i]->employee_name = User::find($request['sso_no'])->name;
                    $lt_detail[$i]->sso_no = $request['sso_no'];
                    $lt_detail[$i]->date = $request->date[$i];
                    $lt_detail[$i]->day = date('l', strtotime($request->date[$i]));
                    $lt_detail[$i]->designation = $request->designation[$i];
                    $lt_detail[$i]->remark = $request->remark[$i];
                    $lt_detail[$i]->travel_start = $request->travel_start[$i];
                    $lt_detail[$i]->travel_finish = $request->travel_finish[$i];
                    $lt_detail[$i]->travel2_start = $request->travel2_start[$i];
                    $lt_detail[$i]->travel2_finish = $request->travel2_finish[$i];
                    $lt_detail[$i]->work_start = $request->work_start[$i];
                    $lt_detail[$i]->work2_start = $request->work2_start[$i];
                    $lt_detail[$i]->work_finish = $request->work_finish[$i];
                    $lt_detail[$i]->work2_finish = $request->work2_finish[$i];
                    $lt_detail[$i]->nextdaysite = $request->nextdaysite[$i];
                    $lt_detail[$i]->nextdayhome = $request->nextdayhome[$i];
                    

                    if($request->remark[$i] == "standby"){
                        if($lt_detail[$i]->work_start && $lt_detail[$i]->work_finish){
                            $total_hour1 = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($lt_detail[$i]->work2_start && $lt_detail[$i]->work2_finish){
                            $total_hour2 = strtotime($lt_detail[$i]->work2_finish) - strtotime($lt_detail[$i]->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $lt_detail[$i]->total_hour_2x = 0;
                        $lt_detail[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $lt_detail[$i]->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($request->date[$i])) == "Saturday"){
                        if($lt_detail[$i]->work_start && $lt_detail[$i]->work_finish){
                            $total_hour1[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                        }else{
                            $total_hour1[$i] = 0;
                        }
                        if($lt_detail[$i]->work2_start && $lt_detail[$i]->work2_finish){
                            $total_hour2[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime($lt_detail[$i]->work2_start);
                        }else{
                            $total_hour2[$i] = 0;
                        }
                        $lt_detail[$i]->total_hour_2x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                        $lt_detail[$i]->total_hour_1x = 0;
                        $lt_detail[$i]->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($request->date[$i])) == "Sunday"){
                        if($lt_detail[$i]->work_start && $lt_detail[$i]->work_finish){
                            $total_hour1[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                        }else{
                            $total_hour1[$i] = 0;
                        }
                        if($lt_detail[$i]->work2_start && $lt_detail[$i]->work2_finish){
                            $total_hour2[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime($lt_detail[$i]->work2_start);
                        }else{
                            $total_hour2[$i] = 0;
                        }
                        $lt_detail[$i]->total_hour_2x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                        $lt_detail[$i]->total_hour_1x = 0;
                        $lt_detail[$i]->total_hour_1_5x = 0;
                    }else{
                        // if days on weekdays
                        $firstEightHour[$i] = strtotime('08:00:00') - strtotime('00:00:00');

                            if(strtotime($lt_detail[$i]->work2_finish) < strtotime($lt_detail[$i]->work2_start)){
                                // start new code for employee has night shift
                                if($lt_detail[$i]->work_start && $lt_detail[$i]->work_finish){
                                    $beforeLunch[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                                }else{
                                    $beforeLunch[$i] = 0;
                                }
                                if($lt_detail[$i]->work2_start && $lt_detail[$i]->work2_finish){
                                    $afterLunch[$i] = strtotime('23:59:59') - strtotime($lt_detail[$i]->work2_start);
                                }else{
                                    $afterLunch[$i] = 0;
                                }
                                // calculation for total work hour excluded night shift
                                $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
                                if($total_workhours[$i] < $firstEightHour[$i]){
                                    $_1_5x[$i] = 0;
                                    $_1x[$i] = $total_workhours[$i];
                                }elseif($total_workhours[$i] > $firstEightHour[$i]){
                                    $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
                                    $_1x[$i] = $firstEightHour[$i];
                                }
                                // calculation for 2 times
                                $_2_0x[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime('00:00:00');
                        
                                // calculate total hour
                                $lt_detail[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
                                $lt_detail[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
                                $lt_detail[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
                                // end new code
                                // $total_hour1[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                                // $total_hour2[$i] = strtotime("17:00:00") - strtotime($lt_detail[$i]->work2_start);
                                // $total_hour3[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime("17:00:00");
                                // $lt_detail[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                                // $lt_detail[$i]->total_hour_1_5x = decimalHours(secondsToTime($total_hour3[$i]));
                                // $lt_detail[$i]->total_hour_2x = 0;
                            }elseif(strtotime($lt_detail[$i]->work2_finish) > strtotime($lt_detail[$i]->work2_start)){
                                // new code no night shift but have before and after lunch data
                                if($lt_detail[$i]->work_start && $lt_detail[$i]->work_finish){
                                    $beforeLunch[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                                }else{
                                    $beforeLunch[$i] = 0;
                                }
                                if($lt_detail[$i]->work2_start && $lt_detail[$i]->work2_finish){
                                    $afterLunch[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime($lt_detail[$i]->work2_start);
                                }else{
                                    $afterLunch[$i] = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
                                if($total_workhours[$i] < $firstEightHour[$i]){
                                    $_1_5x[$i] = 0;
                                    $_1x[$i] = $total_workhours[$i];
                                }else{
                                    $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
                                    $_1x[$i] = $firstEightHour[$i];
                                }
                                // calculation for 2 times
                                $_2_0x[$i] = 0;
                                // calculate total hour
                                $lt_detail[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
                                $lt_detail[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
                                $lt_detail[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
                                // end new code
                                // $total_hour1[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                                // $total_hour2[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime($lt_detail[$i]->work2_start);
                                // $lt_detail[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                                // $lt_detail[$i]->total_hour_1_5x = 0;
                                // $lt_detail[$i]->total_hour_2x = 0;
                            }else{
                                if($lt_detail[$i]->work_start && $lt_detail[$i]->work_finish){
                                    $beforeLunch[$i] = strtotime($lt_detail[$i]->work_finish) - strtotime($lt_detail[$i]->work_start);
                                }else{
                                    $beforeLunch[$i] = 0;
                                }
                                if($lt_detail[$i]->work2_start && $lt_detail[$i]->work2_finish){
                                    $afterLunch[$i] = strtotime($lt_detail[$i]->work2_finish) - strtotime($lt_detail[$i]->work2_start);
                                }else{
                                    $afterLunch[$i] = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
                                if($total_workhours[$i] < $firstEightHour[$i]){
                                    $_1_5x[$i] = 0;
                                    $_1x[$i] = $total_workhours[$i];
                                }else{
                                    $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
                                    $_1x[$i] = $firstEightHour[$i];
                                }
                                // calculation for 2 times
                                $_2_0x[$i] = 0;
                        
                                // calculate total hour
                                $lt_detail[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
                                $lt_detail[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
                                $lt_detail[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
                            }
                        
                    } 
                    // end if
                    // calculate total travel (new code)
                    if($lt_detail[$i]->travel_finish && $lt_detail[$i]->travel_start){
                        if($request->nextdaysite[$i] == true){
                            $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($lt_detail[$i]->travel_start);
                            $totaltravel_2[$i] = strtotime($lt_detail[$i]->travel_finish) - strtotime('00:00:00');
                            $total_travel_hour1[$i] = decimalHours(secondsToTime($totaltravel_1[$i])) + decimalHours(secondsToTime($totaltravel_2[$i]));
                        }else{
                            if($lt_detail[$i]->travel_finish < $lt_detail[$i]->travel_start){
                                $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($lt_detail[$i]->travel_start);
                                $totaltravel_2[$i] = strtotime($lt_detail[$i]->travel_finish) - strtotime('00:00:00');
                                $total_travel_hour1[$i] = decimalHours(secondsToTime($totaltravel_1[$i] + $totaltravel_2[$i]));
                            }else{
                                $total_travel_hour1[$i] = decimalHours(secondsToTime(strtotime($lt_detail[$i]->travel_finish) - strtotime($lt_detail[$i]->travel_start)));
                            } 
                        }
                    }else{
                        $total_travel_hour1[$i] = 0;
                    }

                    if($lt_detail[$i]->travel2_finish && $lt_detail[$i]->travel2_start){
                        if($request->nextdayhome[$i] == true){
                            $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($lt_detail[$i]->travel2_start);
                            $totaltravel_2[$i] = strtotime($lt_detail[$i]->travel2_finish) - strtotime('00:00:00');
                            $total_travel_hour2[$i] = decimalHours(secondsToTime($totaltravel_1[$i])) + decimalHours(secondsToTime($totaltravel_2[$i]));
                        }else{
                            if($lt_detail[$i]->travel2_finish < $lt_detail[$i]->travel2_start){
                                $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($lt_detail[$i]->travel2_start);
                                $totaltravel_2[$i] = strtotime($lt_detail[$i]->travel2_finish) - strtotime('00:00:00');
                                $total_travel_hour2[$i] = decimalHours(secondsToTime($totaltravel_1[$i] + $totaltravel_2[$i]));
                            }else{
                                $total_travel_hour2[$i] = decimalHours(secondsToTime(strtotime($lt_detail[$i]->travel2_finish) - strtotime($lt_detail[$i]->travel2_start)));
                            }  
                        }
                    }else{
                        $total_travel_hour2[$i] = 0;
                    }
                    // calculate total travel hour
                    // $total_travel_hour1[$i] = strtotime($lt_detail[$i]->travel_finish) - strtotime($lt_detail[$i]->travel_start);
                    // $total_travel_hour2[$i] = strtotime($lt_detail[$i]->travel2_finish) - strtotime($lt_detail[$i]->travel2_start);
                    $lt_detail[$i]->total_travel_hour = $total_travel_hour1[$i] + $total_travel_hour2[$i];
                    $lt_detail[$i]->total_work_hour = $lt_detail[$i]->total_hour_1x + $lt_detail[$i]->total_hour_1_5x + $lt_detail[$i]->total_hour_2x;
                    // $lt_detail[$i]->total_hour_1x += $lt_detail[$i]->total_travel_hour;
                    $lt_detail[$i]->save();

                    

                }else{
                    $request->session()->flash('alert-danger', 'Date '.$request->date[$i].' is already exist in your timesheets.');
                    return back();
                }

            } //endloop
            $local_timesheets = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->where('local_timesheet.job_no', $local_timesheet->job_no)->get();
            if($local_timesheets){
                    $total_st_hour = 0;
                    $total_1_5t_hour = 0;
                    $total_2t_hour = 0;
                    $total_travel_hours = 0;
                    $total_work_hours = 0;
                    for($i = 0;$i < count($local_timesheets);$i++){
                        $total_st_hour = $total_st_hour + $local_timesheets[$i]->total_hour_1x;
                        $total_1_5t_hour = $total_1_5t_hour + $local_timesheets[$i]->total_hour_1_5x;
                        $total_2t_hour = $total_2t_hour + $local_timesheets[$i]->total_hour_2x;
                        $total_travel_hours = $total_travel_hours + $local_timesheets[$i]->total_travel_hour;
                        $total_work_hours = $total_work_hours + $local_timesheets[$i]->total_work_hour;
                    }
                    if($lt_id){
                        $lt = local_timesheetModel::findOrFail($lt_id);
                        $lt->total_st_hour = $total_st_hour;
                        $lt->total_1_5t_hour = $total_1_5t_hour;
                        $lt->total_2t_hour = $total_2t_hour;
                        $lt->total_travel_hours = $total_travel_hours;
                        $lt->total_work_hours = $total_work_hours;
                        $lt->update();
                        $request->session()->flash('alert-success', 'Form Successfully Submited');
                        return back();
                    }else{
                        return abort(404, "Error 404");
                    }
            }else{
                return abort(404, "Error 404");
            }
                    
            
        }
    }

    public function rateSetting(){
        if(Auth::user()->account_type == 1){
            $rate = ltRates::first();
            $user = \App\User::find($rate->sso_no);
            return view('local_timesheet.rate_setting', compact('rate', 'user'));
        }else{
            return abort(404, "Permission Denied");
        }
        
    }

    public function rateSettingUpdate(Request $request){
        $this->validate(request(),[
            'rate_st' => 'required',
            'rate_1_5x' => 'required',
            'rate_2x' => 'required',
        ]);
        $rate = ltRates::first();
        if($rate && Auth::user()->account_type == 1){
            $rate->rate_st = $request->rate_st;
            $rate->rate_1_5x = $request->rate_1_5x;
            $rate->rate_2x = $request->rate_2x;
            $rate->sso_no = Auth::user()->sso_no;
            $rate->update();
            $request->session()->flash('alert-success', 'Saved');
            return back();
        }elseif(Auth::user()->account_type == 1){
            $rate = new ltRates;
            $rate->rate_st = $request->rate_st;
            $rate->rate_1_5x = $request->rate_1_5x;
            $rate->rate_2x = $request->rate_2x;
            $rate->sso_no = Auth::user()->sso_no;
            $rate->save();
            $request->session()->flash('alert-success', 'Saved');
            return back();
        }
    }
}
