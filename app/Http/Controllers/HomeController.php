<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\master_jobsModel;
use App\overseasTimesheetModel;
use App\work_datesModel;
use App\OverseasExpenseStatementModel;
use App\OesAttachmentModel;
use App\local_timesheetModel;
use App\User;
use App\masterCurrencyRatesModel;
use Illuminate\Support\Facades\Auth;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        if(Auth::user()->account_type == 1 || Auth::user()->account_type == 2){
            $overseas_ts = overseasTimesheetModel::where('status', 0)->count();
            $overseas_es = OverseasExpenseStatementModel::where('status', 0)->count();
            $local_ts = local_timesheetModel::where('status',0)->count();
            $master_jobs = master_jobsModel::count();
            $mcr = masterCurrencyRatesModel::whereMonth('date', date('m'))->count();
            return view('dashboard', compact('overseas_ts','overseas_es', 'master_jobs', 'local_ts', 'mcr'));
        }elseif(Auth::user()->account_type == 0){
            $overseas_ts = overseasTimesheetModel::where('sso_no', Auth::user()->sso_no)->count();
            $overseas_es = OverseasExpenseStatementModel::where('sso_no', Auth::user()->sso_no)->count();
            $local_ts = local_timesheetModel::leftJoin('local_timesheet_detail', 'local_timesheet.id', '=', 'local_timesheet_detail.lt_id')->distinct(['job_no'])->get(['job_no'])->count();
            return view('dashboard', compact('overseas_ts','overseas_es', 'local_ts'));
        }
    }
}
