<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Contracts\Routing\ResponseFactory;
use Chumper\Zipper\Zipper;

use App\master_jobsModel;
use App\overseasTimesheetModel;
use App\work_datesModel;
use App\OverseasExpenseStatementModel;
use App\OesAttachmentModel;


class OverseasExpenseStatementController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //index for overseas expense statement
        $oes_collections = OverseasExpenseStatementModel::all();
        return view('overseas_expense_statement.index', compact('oes_collections'));
    }

    public function pending()
    {
        //index for overseas expense statement
        $oes_collections = OverseasExpenseStatementModel::where('status', 0)->get();
        return view('overseas_expense_statement.index', compact('oes_collections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // create page for oes
        $master_jobs = master_jobsModel::all();
        return view('overseas_expense_statement.create', compact('master_jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // storing created form
        $this->validate(request(), [
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            'name' => 'required',
            'department' => 'required',
            'sso_no' => 'required',
            'job_no' => 'required',
            'customer_name' => 'required',
            'date2_from.*' => 'required|date',
            'date2_to.*' => 'required|date',
            'category.*' => 'required',
            'currency.*' => 'required',
            'expenses.*' => 'required|numeric|between:0,9999999.99',
            'payment_method.*' => 'required',
            'attachment.*' => 'required|max:10000|mimes:jpeg,bmp,png',
            'customer_signature_attachment' => 'mimes:jpeg,png,jpg,gif,svg,pdf',
        ]);
            //check if att file is uploded
        // if(count($request->attachment) !== count($request->date2_from)){
        //     $request->session()->flash('alert-danger', 'Please Upload Expenses Attachment Files!');
        //     return back();
        // }
        // if master job not have jobno create one
        if(master_jobsModel::find($request->job_no) == null){
            $request->session()->flash('alert-danger', 'Job number not exists');
            return back();
        }

        // if date from and to within sso inside table are not exist, create one else return back
        if(OverseasExpenseStatementModel::where('sso_no', $request->sso_no)->whereDate('date_from', $request->date_from)->whereDate('date_to', $request->date_to)->exists() == false){
            $overseasExpenseStatement = new OverseasExpenseStatementModel;
            $overseasExpenseStatement->name = Auth::user()->name;
            $overseasExpenseStatement->department = $request->department;
            $overseasExpenseStatement->date_from = $request->date_from;
            $overseasExpenseStatement->date_to = $request->date_to;
            $overseasExpenseStatement->customer_name = $request->customer_name;
            $overseasExpenseStatement->sso_no = Auth::user()->sso_no;
            $overseasExpenseStatement->job_no = $request->job_no;
            $overseasExpenseStatement->purpose_of_trip = $request->purpose_of_trip;
            $overseasExpenseStatement->save();
            if($request->customer_signature_attachment){
                    $oes = OverseasExpenseStatementModel::where('sso_no', $request->sso_no)->whereDate('date_from', $request->date_from)->whereDate('date_to', $request->date_to)->first();
                    // Handle File Upload     
                    // Get filename with the extension
                    $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // Get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore = $request->sso_no.'_'.'cust_sign'.'_'.rand(1, 1000000) .'.'.$extension;
                    // Upload image
                    $path = $request->customer_signature_attachment->storeAs('public/overseas_expense_statement/'. $oes->id .'/'.'cust_sign/', $fileNameToStore);

                    $oes->customer_signature_attachment = $fileNameToStore;
                    $oes->customer_signature_date = date("Y-m-d");
                    if($oes->dept_signature_name){
                        $oes->status = 1;
                    }else{
                        $oes->status = 0;
                    }
                    $oes->update();
                    
            }
            
            $request->session()->flash('alert-success','Success');
            return back();

            }else{
            $request->session()->flash('alert-danger','Overseas expense statement date between '.$request->date_from.' to '. $request->date_to.' is already exist.');
            return back();
        }

        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($oes_id)
    {
        // show oes based on id
        $oes_job = OverseasExpenseStatementModel::leftJoin('master_jobs', 'overseas_expenses_statement.job_no', '=', 'master_jobs.job_no')->where('overseas_expenses_statement.id', $oes_id)->first();
        $oes_att_collections = 
        OverseasExpenseStatementModel::leftJoin('oes_attachment', 'overseas_expenses_statement.id', '=', 'oes_attachment.oes_id')->where('overseas_expenses_statement.id',$oes_id)->get();
        if($oes_att_collections){
            return view('overseas_expense_statement.show', compact('oes_job','oes_att_collections'));
        }else{
            return abort(403, 'Unauthorized action.');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($oes_id)
    {
        //
        $oes = OverseasExpenseStatementModel::find($oes_id);
        $job = master_jobsModel::find($oes->job_no);
        $master_jobs = master_jobsModel::all();
        if($oes && $job){
            return view('overseas_expense_statement.edit', compact('oes', 'job', 'master_jobs'));
        }else{
            return abort(403, "Unauthorized action");
        }
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $oes_id)
    {
        $this->validate(request(), [
            'date_from' => 'required|date',
            'date_to' => 'required|date',
            // 'name' => 'required',
            'department' => 'required',
            // 'sso_no' => 'required',
            // 'job_no' => 'required',
            // 'customer_name' => 'required',
            // 'date2_from.*' => 'required|date',
            // 'date2_to.*' => 'required|date',
            // 'category.*' => 'required',
            // 'currency.*' => 'required',
            // 'expenses.*' => 'required|numeric|between:0,9999999.99',
            // 'payment_method.*' => 'required',
            // 'attachment.*' => 'required|max:10000|mimes:jpeg,png,jpg,gif,svg',
            'customer_signature_attachment' => 'mimes:jpeg,png,jpg,gif,svg,pdf',
        ]);
        //
         $oes = OverseasExpenseStatementModel::find($oes_id);
        if($oes){
            // $oes->name = $request->name;
            $oes->department = $request->department;
            $oes->date_from = $request->date_from;
            $oes->date_to = $request->date_to;
            $oes->customer_name = $request->customer_name;
            // $oes->sso_no = $request->sso_no;
            // $oes->job_no = $request->job_no;
            $oes->purpose_of_trip = $request->purpose_of_trip;
            if($request->customer_signature_attachment){
                if($oes->customer_signature_attachment != 'no-file.png'){
                    Storage::delete('public/overseas_expense_statement/'. $oes->id .'/'.'cust_sign/'. $oes->customer_signature_attachment);
                }
                    // Handle File Upload     
                    // Get filename with the extension
                    $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // Get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore = $oes->sso_no.'_cust_sign_'.rand(1, 1000000).'.'.$extension;
                    // Upload image
                    $path = $request->customer_signature_attachment->storeAs('public/overseas_expense_statement/'. $oes->id .'/'.'cust_sign/', $fileNameToStore);

                    $oes->customer_signature_attachment = $fileNameToStore;
                    $oes->customer_signature_date = date("Y-m-d");
                    if($oes->dept_signature_name){
                        $oes->status = 1;
                    }else{
                        $oes->status = 0;
                    }
                    
            }
            $oes->update();
            $request->session()->flash('alert-success','Update Success!');
            return back();
        }else{
            return abort(403, "Unauthorized action");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($oes_id)
    {
        //
        $oes = OverseasExpenseStatementModel::findOrfail($oes_id);
        // $oes_attachment = OesAttachmentModel::where('oes_id', $oes_id)->get();
            Storage::deleteDirectory('/public/overseas_expense_statement/'.$oes->id);
            $oes->delete();
            return redirect()->route('index_oes');
        
    }


    public function editAtt($oes_id, $oes_att_id){
        $oes_att = OesAttachmentModel::find($oes_att_id);
        $oes = OverseasExpenseStatementModel::find($oes_id);
        if($oes_att){
            return view('overseas_expense_statement.edit_att', compact('oes_att', 'oes'));
        }else{
            abort(403, "Unauthorized action");
        }
    }

    public function updateAtt(Request $request, $oes_id, $oes_att_id){
        $this->validate(request(),[
            'attachment'=>'mimes:pdf',
        ]);
        // return dd($request->base64str);
        // if($request->base64str != ""){
        //     $data = str_replace('data:image/jpeg;base64,', '', $request->base64str);
        //     $imageData = base64_decode($data);
        //     $source = imagecreatefromstring($imageData);
        //     $angle = 0;
        //     $imageName = "test.png";
        //     $rotate = imagerotate($source, $angle, 0); // if want to rotate the image
        //     $imageSave = imagejpeg($rotate,$imageName,100);
        //     imagedestroy($source);
        //     return dd($imageSave);
        // }
        // return dd($request);
            $oes_att = OesAttachmentModel::find($oes_att_id);
            $oes = OverseasExpenseStatementModel::find($oes_id);
        if($oes_att){
            if($request->attachment){
                if($oes->attachment != 'no-file.png'){
                    Storage::delete('public/overseas_expense_statement/'.$oes->id.'/oes'.'/'.$oes_att->attachment);
                }
                // Handle File Upload if file is an image
                // Get filename with the extension
                $filenameWithExt = $request->attachment->getClientOriginalName();
                // Get just filename
                $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                // Get just ext
                $extension = $request->attachment->getClientOriginalExtension();
                    
                
                // If uploded file in pdf format
                // if(strtolower($extension) == 'pdf'){
                if(false){
                    $im1 = new \Imagick();   
                    $im1->readImage($request->attachment); 
                    $im1->resetIterator(); 
                    # Combine multiple images into one, stacked vertically. 
                    $ima = $im1->appendImages(true); 
                    $ima->setImageFormat("png"); 
                    header("Content-Type: image/png");
                    Storage::disk('public')->makeDirectory('overseas_expense_statement/'.$oes->id.'/oes'.'/');
                    $fileNameToStore = $oes->sso_no.'_oes_attach_'.rand(1, 1000000).'.jpeg';
                    $ima->writeImage(storage_path('app/public').'/overseas_expense_statement'.'/'.$oes->id.'/oes'.'/'.$fileNameToStore); 
                }else{
                    // Filename to store
                    $fileNameToStore = $oes->sso_no.'_oes_attach_'.rand(1, 1000000).'.'.$extension;
                    // Upload image
                    $path = $request->attachment->storeAs('public/overseas_expense_statement/'.$oes->id.'/oes'.'/', $fileNameToStore);
                }
                $oes_att->attachment = $fileNameToStore;
                
                
            }
            $oes_att->description = $request->description;
            $oes_att->category = $request->category;
            $oes_att->payment_method = $request->payment_method;
            $oes_att->expenses = $request->expenses;
            $oes_att->date2_from = $request->date2_from;
            $oes_att->date2_to = $request->date2_to;
            $oes_att->currency = $request->currency;
            $oes_att->update();
                   
            $request->session()->flash('alert-success', 'Expense successfully updated!');
            return redirect()->back();
        }else{
            abort(403, "Unauthorized action");
        }
    }

    public function destroyAtt($oes_id, $oes_att_id){
        $oes_att = OesAttachmentModel::find($oes_att_id);
        $oes = OverseasExpenseStatementModel::find($oes_id);
        if($oes_att){
            if($oes_att['attachment'] != 'no-file.png'){
                Storage::delete('public/overseas_expense_statement/'.$oes->id.'/oes'.'/'.$oes_att->attachment);
            }
            $oes_att->delete();
            return redirect()->route('show_oes',[
                'oes_id'=>$oes['id'],
            ]);
        }else{
            return abort(403, "Unauthorized action");
        }
    }

    public function createAtt($oes_id){
        $oes = OverseasExpenseStatementModel::find($oes_id);
        if($oes){
            return view('overseas_expense_statement.create_att', compact('oes'));
        }else{
            return abort(403, "Unauthorized action");
        }
        
    }

    public function storeAtt(Request $request, $oes_id){
        $this->validate(request(), [
            'date2_from.*' => 'required|date',
            'date2_to.*' => 'required|date',
            'category.*' => 'required',
            'currency.*' => 'required',
            'expenses.*' => 'required',
            'payment_method.*' => 'required',
            'attachment.*' => 'mimes:pdf',
        ]);
            $oes = OverseasExpenseStatementModel::find($oes_id);
        if($oes){
            for($i = 0;$i < count($request->date2_from);$i++){
                $oes_att[$i] = new OesAttachmentModel;
                if($request->attachment[$i]){
                    // Handle File Upload     
                    // Get filename with the extension
                    $filenameWithExt[$i] = $request->attachment[$i]->getClientOriginalName();
                    // Get just filename
                    $filename[$i] = pathinfo($filenameWithExt[$i], PATHINFO_FILENAME);
                    // Get just ext
                    $extension[$i] = $request->attachment[$i]->getClientOriginalExtension();
                    // If uploded file in pdf format

                    // if(strtolower($extension[$i]) == 'pdf'){
                    if(false){
                        $im1[$i] = new \Imagick();   
                        $im1[$i]->readImage($request->attachment[$i]); 
                        $im1[$i]->resetIterator(); 
                        # Combine multiple images into one, stacked vertically. 
                        $ima[$i] = $im1[$i]->appendImages(true); 
                        $ima[$i]->setImageFormat("png"); 
                        header("Content-Type: image/png");
                        Storage::disk('public')->makeDirectory('overseas_expense_statement/'.$oes->id.'/oes'.'/');
                        $fileNameToStore[$i] = $oes->sso_no.'_oes_attach_'.rand(1, 1000000).'.jpeg';
                        $ima[$i]->writeImage(storage_path('app/public').'/overseas_expense_statement'.'/'.$oes->id.'/oes'.'/'.$fileNameToStore[$i]); 
                    }else{
                        // Filename to store
                        $fileNameToStore[$i] = $oes->sso_no.'_oes_attach_'.rand(1, 1000000).'.'.$extension[$i];
                        // Upload image
                        $path[$i] = $request->attachment[$i]->storeAs('public/overseas_expense_statement/'.$oes->id.'/oes'.'/', $fileNameToStore[$i]);
                    }
                    $oes_att[$i]->attachment = $fileNameToStore[$i];
                }else{
                    $oes_att[$i]->attachment = 'no-file.png';
                }
                $oes_att[$i]->oes_id = $oes_id;
                $oes_att[$i]->category = $request->category[$i];
                $oes_att[$i]->job_no = $oes->job_no;
                $oes_att[$i]->description = $request->description[$i];
                $oes_att[$i]->payment_method = $request->payment_method[$i];
                $oes_att[$i]->expenses = $request->expenses[$i];
                $oes_att[$i]->date2_from = $request->date2_from[$i];
                $oes_att[$i]->date2_to = $request->date2_to[$i];
                $oes_att[$i]->currency = $request->currency[$i];
                $oes_att[$i]->save();
            }
            
                   
            $request->session()->flash('alert-success', 'Expense successfully created!');
            return back();
        }else{
            abort(403, "Unauthorized action");
        }
    }


    public function print($oes_id){
        $oes_info = OverseasExpenseStatementModel::where('overseas_expenses_statement.id', $oes_id)->first();
        $oes = OverseasExpenseStatementModel::leftJoin('oes_attachment', 'overseas_expenses_statement.id', '=', 'oes_attachment.oes_id')->where('overseas_expenses_statement.id', $oes_id)->get();
        if($oes){
            $job = master_jobsModel::find($oes_info['job_no']);
            $pdf = \PDF::loadView('overseas_expense_statement.print', compact('oes','job', 'oes_info'));
            $pdf->setPaper('A4', 'landscape');
            // $pdf->save('sso-'.$oes_info->sso_no.'-jobno-'.$oes_info->job_no.'-overseas_expense_statement.pdf');
            Storage::makeDirectory('public/overseas_expense_statement/'.$oes_id.'/oes_report');
            $pdf->save('storage/overseas_expense_statement/'. $oes_id .'/oes_report'.'/'.$oes_id.'.pdf');
            $report_exists = Storage::exists('public/overseas_expense_statement/'. $oes_id .'/oes_report'.'/'.$oes_id.'.pdf');
            if($report_exists){
                try {
                    $oes_collections = OesAttachmentModel::where('oes_id', $oes_id)->orderBy('oes_id', 'ASC')->get();
                    $pdf = new \Nextek\LaraPdfMerger\PdfManage;
                    $pdf->addPDF('storage/overseas_expense_statement/'. $oes_id .'/oes_report'.'/'.$oes_id.'.pdf', 'all', 'L');
                    foreach($oes_collections as $item){
                        
                        if($item->attachment != "no-file.png"){
                            if(strtolower(substr(strrchr($item->attachment,'.'),1)) == "pdf"){
                                $pdf->addPDF('storage/overseas_expense_statement/'. $oes_id .'/oes'.'/'.$item->attachment, 'all', 'P');
                            }   
                        }
                    }
                    // $pdf->merge('file', 'storage/overseas_expense_statement/'. $oes_id .'/oes_report'.'/'.$request->job_no.'.pdf', 'L'); // generate the file

                    return $pdf->merge('download', 'report-oes-'.$oes_info->job_no.'-'.$oes_info->sso_no.'.pdf'); // force download
                } catch (Exception $e) {
                    return abort(404);
                }
                
            }else{
                return abort(404);
            }
        }
    }


    public function customerSignature(Request $request, $oes_id){
        $this->validate(request(), [
            'customer_signature_attachment'=>'required',
            'customer_name'=>'required',
        ]);
        $oes_att = OverseasExpenseStatementModel::find($oes_id);
        if($oes_att){
            if($request->customer_signature_attachment){
                if($oes_att->customer_signature_attachment != 'no-file.png'){
                    Storage::delete('public/overseas_expense_statement/'. $oes_att->id .'/'.'cust_sign/'. $oes_att->customer_signature_attachment);
                }
                    // Handle File Upload     
                    // Get filename with the extension
                    $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // Get just filename
                    $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore = $oes_att->sso_no.'_oes_cust_sign'.rand(1, 1000000).'.'.$extension;
                    // Upload image
                    $path = $request->customer_signature_attachment->storeAs('public/overseas_expense_statement/'. $oes_att->id .'/'.'cust_sign/', $fileNameToStore);

                    $oes_att->customer_signature_attachment = $fileNameToStore;
                    $oes_att->customer_signature_date = date("Y-m-d");
                    $oes_att->customer_name = $request->customer_name;
                    $oes_att->update();
                    $request->session()->flash('alert-success', 'Customer Signature Upload Success!');
                    return redirect()->back();
            }else{
                    $request->session()->flash('alert-warning', 'Customer Signature Upload Failed!');
                    return redirect()->back();
            }
        }else{
            $request->session()->flash('alert-warning', 'OES Not Found.');
            return redirect()->back();
        }
            
                
            
        
        
    }

    


    public function approve(Request $request, $oes_id){
        if(Auth::user()->account_type == 1){
            $oes = OverseasExpenseStatementModel::find($oes_id);
            if($oes){
                $oes->dept_signature_name = Auth::user()->name;
                $oes->dept_signature_date = date('y-m-d');
                if($oes->customer_signature_attachment != "no-file.png"){
                    $oes->status = 1;
                }else{
                    $oes->status = 0;
                }
                $oes->update();
                $request->session()->flash('alert-success', 'Success!');
                return back();
            }else{
                $request->session()->flash('alert-danger', 'Unable to perform task.');
                return back();
            }
        }else{
            return abort(404, "Permission Denied");
        }
        
    }


    public function custSign(Request $request, $oes_id){

    }

    public function custAttDownloadPdf($oes_id){
        $cust_sign_att = OverseasExpenseStatementModel::findOrFail($oes_id);
        if($cust_sign_att){
            return response()->download(public_path("/storage/overseas_expense_statement/".$oes_id."/cust_sign"."/".$cust_sign_att['customer_signature_attachment']));
        }
    }
}
