<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Validator;
use Illuminate\Support\Facades\Storage;

use App\master_jobsModel;
use App\overseasTimesheetModel;
use App\work_datesModel;
use App\User;
use App\rateModel;


class OverseasTimesheetController extends Controller
{
    

    public function __construct()
    {
        $this->middleware('auth');

        function secondsToTime($seconds) {
        $dtF = new \DateTime('@0');
        $dtT = new \DateTime("@$seconds");
        return $dtF->diff($dtT)->format('%h:%i:%s');
        }

        function decimalHours($time){
            $hms = explode(":", $time);
            return ($hms[0] + ($hms[1]/60) + ($hms[2]/3600));
        }
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // view form list otf
        // $overseasTimesheetCollections = overseasTimesheetModel::rightJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->get();
        $overseasTimesheetCollections = User::rightJoin('overseas_timesheet', 'users.sso_no', '=', 'overseas_timesheet.sso_no')->get();
        return view('overseas_timesheet.view_form_list', compact('overseasTimesheetCollections'));
    }

    public function pending(){
         // view pending
        $overseasTimesheetCollections = overseasTimesheetModel::join('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('overseas_timesheet.status', 0 )->get();
        return view('overseas_timesheet.view_form_list', compact('overseasTimesheetCollections'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        // New Form
        $master_jobs = master_jobsModel::all();
        return view('overseas_timesheet.new_form', compact('master_jobs'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       
        // store data
        if($request->from == 'Create'){
            // create form
            // $this->validate(request(), [
            // 'job_no' => 'required',
            // 'date.*' => 'required|date',
            // 'remark.*' => 'required',
            // ]);
            // check if job no already exists
            if(master_jobsModel::find($request->job_no) == null){
                $request->session()->flash('alert-danger', 'Job number not exists');
                return back();
            }
            // check if job no and sso no already exist (sso_no & job_no as id)
            if(overseasTimesheetModel::where('job_no', $request->job_no)->where('sso_no', $request->sso_no)->first() == null){
                
                // save cust sign/attach and get overseas_timesheet id
                $overseas_timesheet_id = $this->storeNewOverseasTimesheetWithCustSign($request);
                $request->session()->flash('alert-success', 'Overseas Timesheet Create Successfully');
                return back();
                // get back data from table
            }else{
                $request->session()->flash('alert-danger', 'Job number already exist in your timesheet!');
                return back();
            }
            

            //save work dates with logic.
            // $this->storeNewDatesWithLogic($request, $overseas_timesheet_id);
            
            // calculate total work and travel hour on overseas timesheet and redirect
            // return $this->storeTotalWorkTravelHours($request, $overseas_timesheet_id); 
            

        }elseif($request->from == 'Update'){
            // update form
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($ot_id)
    {
        // view form 
        $overseasTimesheetCollections = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->get();
        $overseasTimesheetInfo = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('overseas_timesheet.id', $ot_id)->firstOrFail();
        $job = master_jobsModel::find($overseasTimesheetInfo->job_no);

        // calculate sum
        $sum1 = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->sum('total_hour_1x');
        $sum1_5 = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->sum('total_hour_1_5x');
        $sum2 = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->sum('total_hour_2x');

        return view('overseas_timesheet.view_form', compact('overseasTimesheetCollections', 'overseasTimesheetInfo', 'sum1', 'sum1_5', 'sum2', 'job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($ot_id, $date_id = null)
    {
        // edit form
        if($ot_id && $date_id == null){
            // edit timesheet
            // $overseasTimesheet = overseasTimesheetModel::find($ot_id);
            $overseasTimesheet = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('overseas_timesheet.id', $ot_id)->firstOrFail();
            $jobs = master_jobsModel::all();
            // $workDatesHourCollections = work_datesModel::where('ot_id', $ot_id)->get();
            return view('overseas_timesheet.edit_form', compact('overseasTimesheet', 'jobs'));
        }elseif($ot_id && $date_id){
            // for edit date within timesheet
            $work_date_hour = work_datesModel::where('id', $date_id)->firstOrFail();
            // $overseasTimesheet = overseasTimesheetModel::find($ot_id);
            $overseasTimesheet = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('overseas_timesheet.id', $ot_id)->firstOrFail();
            $job = master_jobsModel::find($overseasTimesheet->job_no);
            return view('overseas_timesheet.edit_date', compact('work_date_hour','overseasTimesheet', 'job'));
        }
        return back();
        
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $ot_id, $date_id = null)
    {
        ///form/overseas-timesheet/edit-form/{$ot_id}
        //update overseas timesheet
        if($ot_id && $date_id == null){
            
           return $this->updateCustSignAttachment($request, $ot_id);
          
        //update overseas timesheet
        }elseif($ot_id && $date_id){
            
            if($request->standby == 1){
                $request->remark = "standby";
            }else{
                $this->validate(request(),[
                    'remark' => 'required',
                ]);
            }
            if($request->nextdaysite == 1){
                $request->nextdaysite = 1;
            }else{
                $request->nextdaysite = 0;
            }
            if($request->nextdayhome == 1){
                $request->nextdayhome = 1;
            }else{
                $request->nextdayhome = 0;
            }
            if($request->overseas_night_shift_allow == null){
                $request->overseas_night_shift_allow == null;
            }
            return $this->updateWorkDateWithLogic($request, $ot_id, $date_id);
        }  
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($ot_id)
    {
        //delete timesheet
        if($ot_id){
            $delete = overseasTimesheetModel::findOrFail($ot_id);
            if($delete->customer_signature_attachment != 'no-file.png'){
                Storage::deleteDirectory('public/overseas-timesheet/'.$delete->id);
            }
            $delete->delete();
            return redirect()->route('view_form_list_otf');
        }else{
            return redirect()->route('view_form_list_otf');
        }
    }

    public function addNewDate($ot_id){
        $timeSheet = overseasTimesheetModel::findOrFail($ot_id);
        $job = master_jobsModel::findOrFail($timeSheet->job_no);
        if($timeSheet && $job){
            
            return view('overseas_timesheet.add_new_date', compact('timeSheet', 'job'));
        }else{
            $request->session()->flash('alert-danger', 'Request failed');
            return back();
        }
    }

    public function addNewDateUpdate(Request $request, $ot_id){
        
        for($i=0;$i<count($request->date);$i++){
            if($request->standby[$i] == 1){
                $data['remark'][$i] = 'standby';
                // $request->merge($data);
                // $request['remark'][$i] = "standby";
            }else{
                $this->validate(request(), [
                    'date.'.$i => 'required|date',
                    // 'remark.'.$i => 'required',
                ]);
                $data['remark'][$i] = $request->remark[$i];
                
            }
            if($request->nextdaysite[$i] == 1){
                $data['nextdaysite'][$i] = 1;
            }else{
                $data['nextdaysite'][$i] = 0;
            }
            if($request->nextdayhome[$i] == 1){
                $data['nextdayhome'][$i] = 1;
            }else{
                $data['nextdayhome'][$i] = 0;
            }
            // logic for overseas night shift allow
            
        }
        return $this->storeNewDatesWithLogic($request, $ot_id);
        
    }


    public function deleteDate($ot_id, $date_id){
        // /form/overseas-timesheet/delete-date/{ot_id}/{date_id}
        if($ot_id && $date_id){
            $date = work_datesModel::findOrFail($date_id);
            $date->delete();
            $overseas_timesheets = overseasTimesheetModel::leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->get();
            if(count($overseas_timesheets) > 0){
                    $total_st_hour = 0;
                    $total_1_5t_hour = 0;
                    $total_2t_hour = 0;
                    $total_travel_hours = 0;
                    $total_work_hours = 0;
                    for($i = 0;$i < count($overseas_timesheets);$i++){
                        $total_st_hour = $total_st_hour + $overseas_timesheets[$i]->total_hour_1x;
                        $total_1_5t_hour = $total_1_5t_hour + $overseas_timesheets[$i]->total_hour_1_5x;
                        $total_2t_hour = $total_2t_hour + $overseas_timesheets[$i]->total_hour_2x;
                        $total_travel_hours = $total_travel_hours +     $overseas_timesheets[$i]->total_travel_hour;
                        $total_work_hours = $total_work_hours + $overseas_timesheets[$i]->total_work_hour;
                    }
                    if($ot_id){
                        $ot = overseasTimesheetModel::findOrFail($ot_id);
                        $ot->total_st_hour = $total_st_hour;
                        $ot->total_1_5t_hour = $total_1_5t_hour;
                        $ot->total_2t_hour = $total_2t_hour;
                        $ot->total_travel_hours = $total_travel_hours;
                        $ot->total_work_hours = $total_work_hours;
                        $ot->update();
                        return redirect()->route('view_form_otf', [ 'ot_id' => $ot_id]);
                    }else{
                        return redirect()->route('view_form_otf', [ 'ot_id' => $ot_id]);
                    }
                    
                    
            }else{
                if($ot_id){
                        $ot = overseasTimesheetModel::findOrFail($ot_id);
                        $ot->total_st_hour = 0;
                        $ot->total_1_5t_hour = 0;
                        $ot->total_2t_hour = 0;
                        $ot->total_travel_hours = 0;
                        $ot->total_work_hours = 0;
                        $ot->update();
                        return redirect()->route('view_form_otf', [ 'ot_id' => $ot_id]);
                    }else{
                        return abort(404);
                    }
            }
            return redirect()->back();
        }else{
            abort(404);
        }
        return redirect()->back();
    }


    public function downloadForm(Request $request, $ot_id){
        // view form 
        $overseasTimesheetCollections = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->get();
        $overseasTimesheetInfo = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->where('overseas_timesheet.id', $ot_id)->firstOrFail();
        $job = master_jobsModel::find($overseasTimesheetInfo->job_no);

        $first_date = work_datesModel::where('ot_id', $ot_id)->orderBy('date', 'ASC')->first();
        $last_date = work_datesModel::where('ot_id', $ot_id)->orderBy('date', 'DESC')->first();
        if($first_date == null){
            $request->session()->flash('alert-danger', 'Download Fail, Timesheet Unavailable.');
            return back();
        }
        switch($first_date->day){
            case "Monday":
                $temp_monday = $first_date->date;
            break;
            case "Tuesday":
                $newdate = strtotime('-1 day', strtotime($first_date->date));
                $temp_monday = date( 'Y-m-d' , $newdate );
            break;
            case "Wednesday":
                $newdate = strtotime('-2 day', strtotime($first_date->date));
                $temp_monday = date( 'Y-m-d' , $newdate );
            break;
            case "Thursday":
                $newdate = strtotime('-3 day', strtotime($first_date->date));
                $temp_monday = date( 'Y-m-d' , $newdate );
            break;
            case "Friday":
                $newdate = strtotime('-4 day', strtotime($first_date->date));
                $temp_monday = date( 'Y-m-d' , $newdate );
            break;
            case "Saturday":
                $newdate = strtotime('-5 day', strtotime($first_date->date));
                $temp_monday = date( 'Y-m-d' , $newdate );
            break;
            case "Sunday":
                $newdate = strtotime('-6 day', strtotime($first_date->date));
                $temp_monday = date( 'Y-m-d' , $newdate );
            break;
        }

        switch($last_date->day){
            case "Monday":
                $newdate = strtotime('+6 day', strtotime($last_date->date));
                $temp_sunday = date( 'Y-m-d' , $newdate );
            break;
            case "Tuesday":
                $newdate = strtotime('+5 day', strtotime($last_date->date));
                $temp_sunday = date( 'Y-m-d' , $newdate );
            break;
            case "Wednesday":
                $newdate = strtotime('+4 day', strtotime($last_date->date));
                $temp_sunday = date( 'Y-m-d' , $newdate );
            break;
            case "Thursday":
                $newdate = strtotime('+3 day', strtotime($last_date->date));
                $temp_sunday = date( 'Y-m-d' , $newdate );
            break;
            case "Friday":
                $newdate = strtotime('+2 day', strtotime($last_date->date));
                $temp_sunday = date( 'Y-m-d' , $newdate );
            break;
            case "Saturday":
                $newdate = strtotime('+1 day', strtotime($last_date->date));
                $temp_sunday = date( 'Y-m-d' , $newdate );
            break;
            case "Sunday":
                $temp_sunday = $last_date->date;
            break;
        }
                                  
        $now = strtotime($temp_sunday); // or your date as well
        $your_date = strtotime($temp_monday);
        $datediff = $now - $your_date;
        $total_weeks = $datediff / (60 * 60 * 24);
                     

        // calculate sum
        $sum1 = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->sum('total_hour_1x');
        $sum1_5 = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->sum('total_hour_1_5x');
        $sum2 = overseasTimesheetModel::leftJoin('users', 'overseas_timesheet.sso_no', '=', 'users.sso_no')->leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.id', $ot_id)->orderBy('work_dates.date', 'ASC')->sum('total_hour_2x');
        
        $pdf = \PDF::loadView('overseas_timesheet.download_form', compact('overseasTimesheetCollections','overseasTimesheetInfo', 'sum1', 'sum1_5', 'sum2', 'job', 'first_date', 'last_date', 'total_weeks', 'temp_monday', 'temp_sunday'));
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download($overseasTimesheetInfo['sso_no'].'-'.$job['job_no'].'-overseas-timesheet.pdf');
        // return view('overseas_timesheet.download_form', compact('overseasTimesheetCollections','overseasTimesheetInfo', 'sum1', 'sum1_5', 'sum2', 'job', 'first_date', 'last_date', 'total_weeks', 'temp_monday', 'temp_sunday'));
    }


    public function approve(Request $request, $ot_id){
        // put dept signature
        if(Auth::user()->account_type == 1){
            $overseas_timesheet = overseasTimesheetModel::findOrFail($ot_id);
            $overseas_timesheet->dept_signature_name = Auth::user()->name;
            $overseas_timesheet->dept_signature_date = date('y-m-d');
            if($overseas_timesheet->customer_signature_attachment != 'no-file.png'){
                $overseas_timesheet->status = 1;
            }else{
                $overseas_timesheet->status = 0;
            }
            $overseas_timesheet->update();
            $request->session()->flash('alert-success', 'Success.');
            return back();
        }else{
            return abort(404, 'Unauthorized User');
        }
    }

    public function custSign(Request $request, $ot_id){
        $this->validate(request(),[
            'customer_signature_name' => 'required',
            'customer_signature_attachment' => 'required|mimes:jpeg,bmp,png,pdf|max:5000',
        ]);

        return $this->updateCustSignAttachment($request, $ot_id);
        
    }


    public function custAttDownloadPdf($ot_id){
        $cust_sign_att = overseasTimesheetModel::findOrFail($ot_id);
        if($cust_sign_att){
            return response()->download(public_path("/storage/overseas-timesheet/".$ot_id."/cust_sign"."/".$cust_sign_att['customer_signature_attachment']));
        }else{
            return abort(404);
        }
    }

    public function rateSetting(){
        if(Auth::user()->account_type == 1){
            $rate = rateModel::first();
            $user = \App\User::find($rate->sso_no);
            return view('overseas_timesheet.rate_setting', compact('rate', 'user'));
        }else{
            return abort(404, "Permission Denied");
        }
        
    }

    public function rateSettingUpdate(Request $request){
        $this->validate(request(),[
            'rate_st' => 'required',
            'rate_1_5x' => 'required',
            'rate_2x' => 'required',
        ]);
        $rate = rateModel::first();
        if($rate && Auth::user()->account_type == 1){
            $rate->rate_st = $request->rate_st;
            $rate->rate_1_5x = $request->rate_1_5x;
            $rate->rate_2x = $request->rate_2x;
            $rate->sso_no = Auth::user()->sso_no;
            $rate->update();
            $request->session()->flash('alert-success', 'Saved');
            return back();
        }elseif(Auth::user()->account_type == 1){
            $rate = new rateModel;
            $rate->rate_st = $request->rate_st;
            $rate->rate_1_5x = $request->rate_1_5x;
            $rate->rate_2x = $request->rate_2x;
            $rate->sso_no = Auth::user()->sso_no;
            $rate->save();
            $request->session()->flash('alert-success', 'Saved');
            return back();
        }
    }


// *****************************************************************
// ** This is a function section

    // not a controller but a function for store work dates
    // public function storeWorkDates($request, $ot_id){
    //     if($request){
    //     // loop through work date and work hour table
    //         for($i = 0; $i < count($request->date);$i++){
    //             if(work_datesModel::where('sso_no', $request->sso_no)->whereDate('date', $request->date[$i])->first() == null){
    //                 $workDates[$i] = new work_datesModel;
    //                 $workDates[$i]->ot_id = $ot_id;
    //                 $workDates[$i]->sso_no = $request->sso_no;
    //                 $workDates[$i]->day = date('l', strtotime($request->date[$i]));
    //                 $workDates[$i]->date = $request->date[$i];
    //                 $workDates[$i]->remark = $request->remark[$i];
    //                 $workDates[$i]->travel_start = $request->travel_start[$i];
    //                 $workDates[$i]->travel_finish = $request->travel_finish[$i];
    //                 $workDates[$i]->travel2_start = $request->travel2_start[$i];
    //                 $workDates[$i]->travel2_finish = $request->travel2_finish[$i];
    //                 $workDates[$i]->work_start = $request->work_start[$i];
    //                 $workDates[$i]->work_finish = $request->work_finish[$i];
    //                 $workDates[$i]->work2_start = $request->work2_start[$i];
    //                 $workDates[$i]->work2_finish = $request->work2_finish[$i];
    //                 if(date('l', strtotime($request->date[$i])) == "Saturday"){
    //                     if($workDates[$i]->work_start && $workDates[$i]->work_finish){
    //                         $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                     }else{
    //                         $total_hour1[$i] = 0;
    //                     }
    //                     if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
    //                         $total_hour2[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
    //                     }else{
    //                         $total_hour2[$i] = 0;
    //                     }
    //                     $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
    //                     $workDates[$i]->total_hour_1x = 0;
    //                     $workDates[$i]->total_hour_1_5x = 0;
    //                 }elseif(date('l', strtotime($request->date[$i])) == "Sunday"){
    //                     if($workDates[$i]->work_start && $workDates[$i]->work_finish){
    //                         $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                     }else{
    //                         $total_hour1[$i] = 0;
    //                     }
    //                     if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
    //                         $total_hour2[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
    //                     }else{
    //                         $total_hour2[$i] = 0;
    //                     }
    //                     $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
    //                     $workDates[$i]->total_hour_1x = 0;
    //                     $workDates[$i]->total_hour_1_5x = 0;
    //                 }else{
    //                     // if days on weekdays
    //                     $firstEightHour[$i] = strtotime('08:00:00') - strtotime('00:00:00');

    //                         if(strtotime($workDates[$i]->work2_finish) < strtotime($workDates[$i]->work2_start)){
    //                             // start new code for employee has night shift
    //                             if($workDates[$i]->work_start && $workDates[$i]->work_finish){
    //                                 $beforeLunch[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                             }else{
    //                                 $beforeLunch[$i] = 0;
    //                             }
    //                             if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
    //                                 $afterLunch[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->work2_start);
    //                             }else{
    //                                 $afterLunch[$i] = 0;
    //                             }
    //                             // calculation for total work hour excluded night shift
    //                             $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
    //                             if($total_workhours[$i] < $firstEightHour[$i]){
    //                                 $_1_5x[$i] = 0;
    //                                 $_1x[$i] = $total_workhours[$i];
    //                             }elseif($total_workhours[$i] > $firstEightHour[$i]){
    //                                 $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
    //                                 $_1x[$i] = $firstEightHour[$i];
    //                             }
    //                             // calculation for 2 times
    //                             $_2_0x[$i] = strtotime($workDates[$i]->work2_finish) - strtotime('00:00:00');
                        
    //                             // calculate total hour
    //                             $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
    //                             $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
    //                             $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
    //                             // end new code
    //                             // $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                             // $total_hour2[$i] = strtotime("17:00:00") - strtotime($workDates[$i]->work2_start);
    //                             // $total_hour3[$i] = strtotime($workDates[$i]->work2_finish) - strtotime("17:00:00");
    //                             // $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
    //                             // $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($total_hour3[$i]));
    //                             // $workDates[$i]->total_hour_2x = 0;
    //                         }elseif(strtotime($workDates[$i]->work2_finish) > strtotime($workDates[$i]->work2_start)){
    //                             // new code no night shift but have before and after lunch data
    //                             if($workDates[$i]->work_start && $workDates[$i]->work_finish){
    //                                 $beforeLunch[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                             }else{
    //                                 $beforeLunch[$i] = 0;
    //                             }
    //                             if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
    //                                 $afterLunch[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
    //                             }else{
    //                                 $afterLunch[$i] = 0;
    //                             }
    //                             // calculation for total work hour 
    //                             $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
    //                             if($total_workhours[$i] < $firstEightHour[$i]){
    //                                 $_1_5x[$i] = 0;
    //                                 $_1x[$i] = $total_workhours[$i];
    //                             }else{
    //                                 $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
    //                                 $_1x[$i] = $firstEightHour[$i];
    //                             }
    //                             // calculation for 2 times
    //                             $_2_0x[$i] = 0;
    //                             // calculate total hour
    //                             $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
    //                             $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
    //                             $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
    //                             // end new code
    //                             // $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                             // $total_hour2[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
    //                             // $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
    //                             // $workDates[$i]->total_hour_1_5x = 0;
    //                             // $workDates[$i]->total_hour_2x = 0;
    //                         }else{
    //                             if($workDates[$i]->work_start && $workDates[$i]->work_finish){
    //                                 $beforeLunch[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
    //                             }else{
    //                                 $beforeLunch[$i] = 0;
    //                             }
    //                             if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
    //                                 $afterLunch[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
    //                             }else{
    //                                 $afterLunch[$i] = 0;
    //                             }
    //                             // calculation for total work hour 
    //                             $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
    //                             if($total_workhours[$i] < $firstEightHour[$i]){
    //                                 $_1_5x[$i] = 0;
    //                                 $_1x[$i] = $total_workhours[$i];
    //                             }else{
    //                                 $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
    //                                 $_1x[$i] = $firstEightHour[$i];
    //                             }
    //                             // calculation for 2 times
    //                             $_2_0x[$i] = 0;
                        
    //                             // calculate total hour
    //                             $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
    //                             $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
    //                             $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
    //                         }
                        
    //                 } 
    //                 // end if
    //                 // calculate total travel (new code)
    //                 if($workDates[$i]->travel_finish && $workDates[$i]->travel_start){
    //                     if($workDates[$i]->travel_finish < $workDates[$i]->travel_start){
    //                         $totaltravel_tosite[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->travel_start);
    //                         $totaltravel_tohome[$i] = strtotime($workDates[$i]->travel_finish) - strtotime('00:00:00');
    //                         $total_travel_hour1[$i] = $totaltravel_tosite[$i] + $totaltravel_tohome[$i];
    //                     }else{
    //                         $total_travel_hour1[$i] = strtotime($workDates[$i]->travel_finish) - strtotime($workDates[$i]->travel_start);
    //                     }   
    //                 }else{
    //                     $total_travel_hour1[$i] = 0;
    //                 }

    //                 if($workDates[$i]->travel2_finish && $workDates[$i]->travel2_start){
    //                     if($workDates[$i]->travel2_finish < $workDates[$i]->travel2_start){
    //                         $totaltravel_tosite2[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->travel2_start);
    //                         $totaltravel_tohome2[$i] = strtotime($workDates[$i]->travel2_finish) - strtotime('00:00:00');
    //                         $total_travel_hour2[$i] = $totaltravel_tosite2[$i] + $totaltravel_tohome2[$i];
    //                     }else{
    //                         $total_travel_hour2[$i] = strtotime($workDates[$i]->travel2_finish) - strtotime($workDates[$i]->travel2_start);
    //                     }  
    //                 }else{
    //                     $total_travel_hour2[$i] = 0;
    //                 }
    //                 $workDates[$i]->total_travel_hour = decimalHours(secondsToTime($total_travel_hour1[$i] + $total_travel_hour2[$i]));
    //                 $workDates[$i]->save();
                    
    //             }elseif(work_datesModel::where('sso_no', $request->sso_no)->whereDate('date', $request->date[$i])->first()){
    //                 $request->session()->flash('alert-danger', 'Date '.$request->date[$i].' already exist in one of your timesheets.');
    //                 return back();
    //             }
                   
    //         } //end for loop
    //     }
    // }

    // function to store cust sign and attachment in overseas timesheet
    public function storeNewOverseasTimesheetWithCustSign($request){
        $overseasTimesheet = new overseasTimesheetModel;
                $overseasTimesheet->sso_no = $request->sso_no;
                $overseasTimesheet->job_no = $request->job_no;
                $overseasTimesheet->customer_signature_name = $request->customer_signature_name;
                $overseasTimesheet->save();
                if($request->customer_signature_attachment){
                    // Handle File Upload     
                    $overseasTimesheet = overseasTimesheetModel::where('job_no', $request->job_no)->where('sso_no', $request->sso_no)->first();
                    // Get filename with the extension
                    $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // Get just filename
                    // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore = $request->sso_no.'_'.$request->job_no.'_'.'cust_sign'.'_'.rand(1, 1000000) .'.'.$extension;
                    // Upload image
                    $path = $request->customer_signature_attachment->storeAs('public/overseas-timesheet/'.$overseasTimesheet->id .'/'.'cust_sign/', $fileNameToStore);
                    $overseasTimesheet->customer_signature_attachment = $fileNameToStore;
                    $overseasTimesheet->customer_signature_date = date("Y-m-d");
                    $overseasTimesheet->update();
                }else{
                    $overseasTimesheet = overseasTimesheetModel::where('job_no', $request->job_no)->where('sso_no', $request->sso_no)->first();
                }
        return $overseasTimesheet->id;
    }

    public function storeTotalWorkTravelHours($request, $overseas_timesheet_id){
        if($request){
            $ot_collections = overseasTimesheetModel::leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('overseas_timesheet.sso_no', $request->sso_no)->where('overseas_timesheet.job_no', $request->job_no)->get();
                $total_st_hour = 0;
                $total_1_5t_hour = 0;
                $total_2t_hour = 0;
                $total_travel_hours = 0;
                for($i = 0;$i < count($ot_collections);$i++){
                    $total_st_hour = $total_st_hour + $ot_collections[$i]->total_hour_1x;
                    $total_1_5t_hour = $total_1_5t_hour + $ot_collections[$i]->total_hour_1_5x;
                    $total_2t_hour = $total_2t_hour + $ot_collections[$i]->total_hour_2x;
                    $total_travel_hours = $total_travel_hours + $ot_collections[$i]->total_travel_hour;
                }
                if($overseas_timesheet_id){
                    $ot = overseasTimesheetModel::find($overseas_timesheet_id);
                    $ot->total_st_hour = $total_st_hour;
                    $ot->total_1_5t_hour = $total_1_5t_hour;
                    $ot->total_2t_hour = $total_2t_hour;
                    $ot->total_travel_hours = $total_travel_hours;
                    $ot->update();
                    $request->session()->flash('alert-success', 'Form Successfully Submited');
                    return redirect()->route('view_form_list_otf');
                }else{
                    return abort(404);
                    // return redirect()->route('view_form_list_otf');
                }  
            
        }
        
    }

    // Function for update cust sign attachment
    public function updateCustSignAttachment($request, $overseas_timesheet_id){
        if($request){

        // update overseas timesheet
            $overseasTimesheet = overseasTimesheetModel::findOrFail($overseas_timesheet_id);
            // $overseasTimesheet->job_no = $request->job_no;
            $overseasTimesheet->customer_signature_name = $request->customer_signature_name;
            if($request->customer_signature_attachment){
                $this->validate(request(), [
                'customer_signature_attachment'=>'required|mimes:jpeg,bmp,png,pdf|max:5000',
                'customer_signature_name'=>'required',
                ]);
                if($overseasTimesheet->customer_signature_attachment != "no-file.png"){
                    Storage::delete('public/overseas-timesheet/'.$overseasTimesheet->id .'/'.'cust_sign/'.$overseasTimesheet->customer_signature_attachment);
                }
                    // Handle File Upload     
                    // Get filename with the extension
                    $filenameWithExt = $request->customer_signature_attachment->getClientOriginalName();
                    // Get just filename
                    // $filename = pathinfo($filenameWithExt, PATHINFO_FILENAME);
                    // Get just ext
                    $extension = $request->customer_signature_attachment->getClientOriginalExtension();
                    // Filename to store
                    $fileNameToStore = $overseasTimesheet->sso_no.'_'.$overseasTimesheet->job_no.'_'.'cust_sign'.'_'.rand(1, 1000000) .'.'.$extension;
                    // Upload image
                    $path = $request->customer_signature_attachment->storeAs('public/overseas-timesheet/'.$overseasTimesheet->id .'/'.'cust_sign/', $fileNameToStore);

                    $overseasTimesheet->customer_signature_attachment = $fileNameToStore;
                    $overseasTimesheet->customer_signature_date = date("Y-m-d");
                    if($overseasTimesheet->dept_signature_name){
                        $overseasTimesheet->status = 1;
                    }else{
                        $overseasTimesheet->status = 0;
                    }
                    
            }
            $overseasTimesheet->update();
            $request->session()->flash('alert-success', 'Updated!');
            return back();
        }else{
            return false;
        }
    }

    // function for update work date with logic
    public function updateWorkDateWithLogic($request, $ot_id, $date_id){
        $date = work_datesModel::findOrFail($date_id);
            if($date){
                $date->remark = $request->remark;
                $date->travel_start = $request->travel_start;
                $date->travel_finish = $request->travel_finish;
                $date->travel2_start = $request->travel2_start;
                $date->travel2_finish = $request->travel2_finish;
                $date->work_start = $request->work_start;
                $date->work_finish = $request->work_finish;
                $date->work2_start = $request->work2_start;
                $date->work2_finish = $request->work2_finish;
                $date->nextdaysite = $request->nextdaysite;
                $date->nextdayhome = $request->nextdayhome;
                if($request->overseas_night_shift_allow != null){
                    $date->overseas_night_shift_allow = work_datesModel::getTotalOverseasNightShiftAllowRate();
                }elseif($date->overseas_night_shift_allow != work_datesModel::getTotalOverseasNightShiftAllowRate()){
                    $date->overseas_night_shift_allow;
                }
                else{
                    $date->overseas_night_shift_allow = null;
                }
                    if($request->remark == "standby"){
                        if($date->work_start && $date->work_finish){
                            $total_hour1 = strtotime($date->work_finish) - strtotime($date->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($date->work2_start && $date->work2_finish){
                            $total_hour2 = strtotime($date->work2_finish) - strtotime($date->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $date->total_hour_2x = 0;
                        $date->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $date->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($date->date)) == "Saturday"){
                        if($date->work_start && $date->work_finish){
                            $total_hour1 = strtotime($date->work_finish) - strtotime($date->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($date->work2_start && $date->work2_finish){
                            $total_hour2 = strtotime($date->work2_finish) - strtotime($date->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $date->total_hour_2x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $date->total_hour_1x = 0;
                        $date->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($date->date)) == "Sunday"){
                        if($date->work_start && $date->work_finish){
                            $total_hour1 = strtotime($date->work_finish) - strtotime($date->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($date->work2_start && $date->work2_finish){
                            $total_hour2 = strtotime($date->work2_finish) - strtotime($date->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $date->total_hour_2x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $date->total_hour_1x = 0;
                        $date->total_hour_1_5x = 0;
                    }else{
                        // if days on weekdays
                        $firstEightHour = strtotime('08:00:00') - strtotime('00:00:00');

                            if(strtotime($date->work2_finish) < strtotime($date->work2_start)){
                                // start new code for employee has night shift
                                if($date->work_start && $date->work_finish){
                                    $beforeLunch = strtotime($date->work_finish) - strtotime($date->work_start);
                                }else{
                                    $beforeLunch = 0;
                                }
                                if($date->work2_start && $date->work2_finish){
                                    $afterLunch = strtotime('23:59:59') - strtotime($date->work2_start);
                                }else{
                                    $afterLunch = 0;
                                }
                                // calculation for total work hour excluded night shift
                                $total_workhours = $afterLunch + $beforeLunch;
                                if($total_workhours < $firstEightHour){
                                    $_1_5x = 0;
                                    $_1x = $total_workhours;
                                }elseif($total_workhours > $firstEightHour){
                                    $_1_5x = $total_workhours - $firstEightHour;
                                    $_1x = $firstEightHour;
                                }
                                // calculation for 2 times
                                $_2_0x = strtotime($date->work2_finish) - strtotime('00:00:00');
                        
                                // calculate total hour
                                $date->total_hour_1x = decimalHours(secondsToTime($_1x));
                                $date->total_hour_1_5x = decimalHours(secondsToTime($_1_5x));
                                $date->total_hour_2x = decimalHours(secondsToTime($_2_0x));
                                // end new code
                                // $total_hour1 = strtotime($date->work_finish) - strtotime($date->work_start);
                                // $total_hour2 = strtotime("17:00:00") - strtotime($date->work2_start);
                                // $total_hour3 = strtotime($date->work2_finish) - strtotime("17:00:00");
                                // $date->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                                // $date->total_hour_1_5x = decimalHours(secondsToTime($total_hour3));
                                // $date->total_hour_2x = 0;
                            }elseif(strtotime($date->work2_finish) > strtotime($date->work2_start)){
                                // new code no night shift but have before and after lunch data
                                if($date->work_start && $date->work_finish){
                                    $beforeLunch = strtotime($date->work_finish) - strtotime($date->work_start);
                                }else{
                                    $beforeLunch = 0;
                                }
                                if($date->work2_start && $date->work2_finish){
                                    $afterLunch = strtotime($date->work2_finish) - strtotime($date->work2_start);
                                }else{
                                    $afterLunch = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours = $afterLunch + $beforeLunch;
                                if($total_workhours < $firstEightHour){
                                    $_1_5x = 0;
                                    $_1x = $total_workhours;
                                }else{
                                    $_1_5x = $total_workhours - $firstEightHour;
                                    $_1x = $firstEightHour;
                                }
                                // calculation for 2 times
                                $_2_0x = 0;
                                // calculate total hour
                                $date->total_hour_1x = decimalHours(secondsToTime($_1x));
                                $date->total_hour_1_5x = decimalHours(secondsToTime($_1_5x));
                                $date->total_hour_2x = decimalHours(secondsToTime($_2_0x));
                                // end new code
                                // $total_hour1 = strtotime($date->work_finish) - strtotime($date->work_start);
                                // $total_hour2 = strtotime($date->work2_finish) - strtotime($date->work2_start);
                                // $date->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                                // $date->total_hour_1_5x = 0;
                                // $date->total_hour_2x = 0;
                            }else{
                                if($date->work_start && $date->work_finish){
                                    $beforeLunch = strtotime($date->work_finish) - strtotime($date->work_start);
                                }else{
                                    $beforeLunch = 0;
                                }
                                if($date->work2_start && $date->work2_finish){
                                    $afterLunch = strtotime($date->work2_finish) - strtotime($date->work2_start);
                                }else{
                                    $afterLunch = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours = $afterLunch + $beforeLunch;
                                if($total_workhours < $firstEightHour){
                                    $_1_5x = 0;
                                    $_1x = $total_workhours;
                                }else{
                                    $_1_5x = $total_workhours - $firstEightHour;
                                    $_1x = $firstEightHour;
                                }
                                // calculation for 2 times
                                $_2_0x = 0;
                        
                                // calculate total hour
                                $date->total_hour_1x = decimalHours(secondsToTime($_1x));
                                $date->total_hour_1_5x = decimalHours(secondsToTime($_1_5x));
                                $date->total_hour_2x = decimalHours(secondsToTime($_2_0x));
                            }
                        
                    } // end if

                // calculate total travel (new code)
                if($date->travel_finish && $date->travel_start){
                    if($request->nextdaysite == true){
                        $totaltravel_1 = strtotime('23:59:59') - strtotime($date->travel_start);
                        $totaltravel_2 = strtotime($date->travel_finish) - strtotime('00:00:00');
                        $total_travel_hour1 = decimalHours(secondsToTime($totaltravel_1)) + decimalHours(secondsToTime($totaltravel_2));
                    }else{
                        if($date->travel_finish < $date->travel_start){
                            $totaltravel_1 = strtotime('23:59:59') - strtotime($date->travel_start);
                            $totaltravel_2 = strtotime($date->travel_finish) - strtotime('00:00:00');
                            $total_travel_hour1 = decimalHours(secondsToTime($totaltravel_1 + $totaltravel_2));
                        }else{
                            $total_travel_hour1 = decimalHours(secondsToTime(strtotime($date->travel_finish) - strtotime($date->travel_start)));
                        }   
                    }
                }else{
                    $total_travel_hour1 = 0;
                }

                if($date->travel2_finish && $date->travel2_start){
                    if($request->nextdayhome == true){
                        $totaltravel_1 = strtotime('23:59:59') - strtotime($date->travel2_start);
                        $totaltravel_2 = strtotime($date->travel2_finish) - strtotime('00:00:00');
                        $total_travel_hour2 = decimalHours(secondsToTime($totaltravel_1)) + decimalHours(secondsToTime($totaltravel_2));
                    }else{
                        if($date->travel2_finish < $date->travel2_start){
                            $totaltravel_1 = strtotime('23:59:59') - strtotime($date->travel2_start);
                            $totaltravel_2 = strtotime($date->travel2_finish) - strtotime('00:00:00');
                            $total_travel_hour2 = decimalHours(secondsToTime($totaltravel_1 + $totaltravel_2));
                        }else{
                            $total_travel_hour2 = decimalHours(secondsToTime(strtotime($date->travel2_finish) - strtotime($date->travel2_start)));
                        }  
                    }
                }else{
                    $total_travel_hour2 = 0;
                }
                // $total_travel_hour1 = strtotime($date->travel_finish) - strtotime($date->travel_start);
                // $total_travel_hour2 = strtotime($date->travel2_finish) - strtotime($date->travel2_start);
                $date->total_travel_hour = $total_travel_hour1 + $total_travel_hour2;
                $date->total_work_hour = $date->total_hour_1x + $date->total_hour_1_5x + $date->total_hour_2x;
                // $date->total_hour_1x += $date->total_travel_hour;
                $date->update();

                // calculate total work hour
                $ot_collections = overseasTimesheetModel::leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('ot_id', $ot_id)->get();
                $total_st_hour = 0;
                $total_1_5t_hour = 0;
                $total_2t_hour = 0;
                $total_travel_hours = 0;
                $total_work_hours = 0;
                for($i = 0;$i < count($ot_collections);$i++){
                    $total_st_hour = $total_st_hour + $ot_collections[$i]->total_hour_1x;
                    $total_1_5t_hour = $total_1_5t_hour + $ot_collections[$i]->total_hour_1_5x;
                    $total_2t_hour = $total_2t_hour + $ot_collections[$i]->total_hour_2x;
                    $total_travel_hours = $total_travel_hours + $ot_collections[$i]->total_travel_hour;
                    $total_work_hours = $total_work_hours + $ot_collections[$i]->total_work_hour;
                }
                if($ot_id){
                    $ot = overseasTimesheetModel::findOrFail($ot_id);
                    $ot->total_st_hour = $total_st_hour;
                    $ot->total_1_5t_hour = $total_1_5t_hour;
                    $ot->total_2t_hour = $total_2t_hour;
                    $ot->total_travel_hours = $total_travel_hours;
                    $ot->total_work_hours = $total_work_hours;
                    $ot->update();
                    $request->session()->flash('alert-success', 'Updated!');
                    return redirect()->back();
                }else{
                    $request->session()->flash('alert-danger', 'Error');
                    return redirect()->back();
                }

            }
    }

    // function for store new dates with total up logic
    public function storeNewDatesWithLogic($request, $ot_id){
      if($request && $ot_id){
        $overseasTimesheetInfo = overseasTimesheetModel::findOrFail($ot_id);
        // check existing date inside employee sso no
        for($i = 0; $i < count($request->date);$i++){
            $validate[$i] = work_datesModel::where('sso_no', $overseasTimesheetInfo->sso_no)->whereDate('date', $request->date[$i])->first();
            if($validate[$i] != null){
                $request->session()->flash('alert-danger', 'Date '.$request->date[$i].' already exists inside your timesheets!');
                return back();
            }
        }
        // loop through work date and work hour table
            for($i = 0; $i < count($request->date);$i++){
                // validate date
                $validate[$i] = work_datesModel::where('sso_no', $overseasTimesheetInfo->sso_no)->whereDate('date', $request->date[$i])->first();
                if($validate[$i] == null){
                    $workDates[$i] = new work_datesModel;
                    $workDates[$i]->ot_id = $overseasTimesheetInfo->id;
                    $workDates[$i]->sso_no = $overseasTimesheetInfo->sso_no;
                    $workDates[$i]->day = date('l', strtotime($request->date[$i]));
                    $workDates[$i]->date = $request->date[$i];
                    $workDates[$i]->remark = $request->remark[$i];
                    $workDates[$i]->travel_start = $request->travel_start[$i];
                    $workDates[$i]->travel_finish = $request->travel_finish[$i];
                    $workDates[$i]->travel2_start = $request->travel2_start[$i];
                    $workDates[$i]->travel2_finish = $request->travel2_finish[$i];
                    $workDates[$i]->work_start = $request->work_start[$i];
                    $workDates[$i]->work_finish = $request->work_finish[$i];
                    $workDates[$i]->work2_start = $request->work2_start[$i];
                    $workDates[$i]->work2_finish = $request->work2_finish[$i];
                    $workDates[$i]->nextdaysite = $request->nextdaysite[$i];
                    $workDates[$i]->nextdayhome = $request->nextdayhome[$i];
                    if($request->overseas_night_shift_allow[$i] != 0){
                        $workDates[$i]->overseas_night_shift_allow = work_datesModel::getTotalOverseasNightShiftAllowRate();
                    }else{
                        $workDates[$i]->overseas_night_shift_allow = null;
                    }
                
                    if($request->remark == "standby"){
                        if($workDates[$i]->work_start && $workDates[$i]->work_finish){
                            $total_hour1 = strtotime($date->work_finish) - strtotime($workDates[$i]->work_start);
                        }else{
                            $total_hour1 = 0;
                        }
                        if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
                            $total_hour2 = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
                        }else{
                            $total_hour2 = 0;
                        }
                        $date->total_hour_2x = 0;
                        $date->total_hour_1x = decimalHours(secondsToTime($total_hour1 + $total_hour2));
                        $date->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($request->date[$i])) == "Saturday"){
                        if($workDates[$i]->work_start && $workDates[$i]->work_finish){
                            $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                        }else{
                            $total_hour1[$i] = 0;
                        }
                        if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
                            $total_hour2[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
                        }else{
                            $total_hour2[$i] = 0;
                        }
                        $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                        $workDates[$i]->total_hour_1x = 0;
                        $workDates[$i]->total_hour_1_5x = 0;
                    }elseif(date('l', strtotime($request->date[$i])) == "Sunday"){
                        if($workDates[$i]->work_start && $workDates[$i]->work_finish){
                            $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                        }else{
                            $total_hour1[$i] = 0;
                        }
                        if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
                            $total_hour2[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
                        }else{
                            $total_hour2[$i] = 0;
                        }
                        $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                        $workDates[$i]->total_hour_1x = 0;
                        $workDates[$i]->total_hour_1_5x = 0;
                    }else{
                        // if days on weekdays
                        $firstEightHour[$i] = strtotime('08:00:00') - strtotime('00:00:00');

                            if(strtotime($workDates[$i]->work2_finish) < strtotime($workDates[$i]->work2_start)){
                                // start new code for employee has night shift
                                if($workDates[$i]->work_start && $workDates[$i]->work_finish){
                                    $beforeLunch[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                                }else{
                                    $beforeLunch[$i] = 0;
                                }
                                if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
                                    $afterLunch[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->work2_start);
                                }else{
                                    $afterLunch[$i] = 0;
                                }
                                // calculation for total work hour excluded night shift
                                $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
                                if($total_workhours[$i] < $firstEightHour[$i]){
                                    $_1_5x[$i] = 0;
                                    $_1x[$i] = $total_workhours[$i];
                                }elseif($total_workhours[$i] > $firstEightHour[$i]){
                                    $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
                                    $_1x[$i] = $firstEightHour[$i];
                                }
                                // calculation for 2 times
                                $_2_0x[$i] = strtotime($workDates[$i]->work2_finish) - strtotime('00:00:00');
                        
                                // calculate total hour
                                $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
                                $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
                                $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
                                // end new code
                                // $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                                // $total_hour2[$i] = strtotime("17:00:00") - strtotime($workDates[$i]->work2_start);
                                // $total_hour3[$i] = strtotime($workDates[$i]->work2_finish) - strtotime("17:00:00");
                                // $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                                // $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($total_hour3[$i]));
                                // $workDates[$i]->total_hour_2x = 0;
                            }elseif(strtotime($workDates[$i]->work2_finish) > strtotime($workDates[$i]->work2_start)){
                                // new code no night shift but have before and after lunch data
                                if($workDates[$i]->work_start && $workDates[$i]->work_finish){
                                    $beforeLunch[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                                }else{
                                    $beforeLunch[$i] = 0;
                                }
                                if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
                                    $afterLunch[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
                                }else{
                                    $afterLunch[$i] = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
                                if($total_workhours[$i] < $firstEightHour[$i]){
                                    $_1_5x[$i] = 0;
                                    $_1x[$i] = $total_workhours[$i];
                                }else{
                                    $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
                                    $_1x[$i] = $firstEightHour[$i];
                                }
                                // calculation for 2 times
                                $_2_0x[$i] = 0;
                                // calculate total hour
                                $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
                                $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
                                $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
                                // end new code
                                // $total_hour1[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                                // $total_hour2[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
                                // $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($total_hour1[$i] + $total_hour2[$i]));
                                // $workDates[$i]->total_hour_1_5x = 0;
                                // $workDates[$i]->total_hour_2x = 0;
                            }else{
                                if($workDates[$i]->work_start && $workDates[$i]->work_finish){
                                    $beforeLunch[$i] = strtotime($workDates[$i]->work_finish) - strtotime($workDates[$i]->work_start);
                                }else{
                                    $beforeLunch[$i] = 0;
                                }
                                if($workDates[$i]->work2_start && $workDates[$i]->work2_finish){
                                    $afterLunch[$i] = strtotime($workDates[$i]->work2_finish) - strtotime($workDates[$i]->work2_start);
                                }else{
                                    $afterLunch[$i] = 0;
                                }
                                // calculation for total work hour 
                                $total_workhours[$i] = $afterLunch[$i] + $beforeLunch[$i];
                                if($total_workhours[$i] < $firstEightHour[$i]){
                                    $_1_5x[$i] = 0;
                                    $_1x[$i] = $total_workhours[$i];
                                }else{
                                    $_1_5x[$i] = $total_workhours[$i] - $firstEightHour[$i];
                                    $_1x[$i] = $firstEightHour[$i];
                                }
                                // calculation for 2 times
                                $_2_0x[$i] = 0;
                        
                                // calculate total hour
                                $workDates[$i]->total_hour_1x = decimalHours(secondsToTime($_1x[$i]));
                                $workDates[$i]->total_hour_1_5x = decimalHours(secondsToTime($_1_5x[$i]));
                                $workDates[$i]->total_hour_2x = decimalHours(secondsToTime($_2_0x[$i]));
                            }
                        
                    } 
                    // end if
                    // calculate total travel (new code)
                    if($workDates[$i]->travel_finish && $workDates[$i]->travel_start){
                        
                        if($request->nextdaysite[$i] == true){
                            $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->travel_start);
                            $totaltravel_2[$i] = strtotime($workDates[$i]->travel_finish) - strtotime('00:00:00');
                            $total_travel_hour1[$i] = decimalHours(secondsToTime($totaltravel_1[$i])) + decimalHours(secondsToTime($totaltravel_2[$i]));
                        }else{
                            if($workDates[$i]->travel_finish < $workDates[$i]->travel_start){
                                $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->travel_start);
                                $totaltravel_1[$i] = strtotime($workDates[$i]->travel_finish) - strtotime('00:00:00');
                                $total_travel_hour1[$i] = decimalHours(secondsToTime($totaltravel_1[$i] +  $totaltravel_1[$i]));
                            }else{
                                $total_travel_hour1[$i] = decimalHours(secondsToTime(strtotime($workDates[$i]->travel_finish) - strtotime($workDates[$i]->travel_start)));
                            }  
                        } 
                    }else{
                        $total_travel_hour1[$i] = 0;
                    }

                    if($workDates[$i]->travel2_finish && $workDates[$i]->travel2_start){
                        if($request->nextdayhome[$i] == true){
                            $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->travel2_start);
                            $totaltravel_2[$i] = strtotime($workDates[$i]->travel2_finish) - strtotime('00:00:00');
                            $total_travel_hour2[$i] = decimalHours(secondsToTime($totaltravel_1[$i])) + decimalHours(secondsToTime($totaltravel_2[$i]));
                        }else{
                            if($workDates[$i]->travel2_finish < $workDates[$i]->travel2_start){
                                $totaltravel_1[$i] = strtotime('23:59:59') - strtotime($workDates[$i]->travel2_start);
                                $totaltravel_2[$i] = strtotime($workDates[$i]->travel2_finish) - strtotime('00:00:00');
                                $total_travel_hour2[$i] = decimalHours(secondsToTime($totaltravel_1[$i] + $totaltravel_2[$i]));
                            }else{
                                $total_travel_hour2[$i] = decimalHours(secondsToTime(strtotime($workDates[$i]->travel2_finish) - strtotime($workDates[$i]->travel2_start)));
                            } 
                        }
                    }else{
                        $total_travel_hour2[$i] = 0;
                    }
                    
                    // calculate total travel
                    // $total_travel_hour1[$i] = strtotime($workDates[$i]->travel_finish) - strtotime($workDates[$i]->travel_start);
                    // $total_travel_hour2[$i] = strtotime($workDates[$i]->travel2_finish) - strtotime($workDates[$i]->travel2_start);
                    $workDates[$i]->total_travel_hour = $total_travel_hour1[$i] + $total_travel_hour2[$i];
                    $workDates[$i]->total_work_hour = $workDates[$i]->total_hour_1x + $workDates[$i]->total_hour_1_5x + $workDates[$i]->total_hour_2x;
                    // $workDates[$i]->total_hour_1x += $workDates[$i]->total_travel_hour;
                    $workDates[$i]->save();

                }
                

            }
            // calculate total work and travel for master ot
            $ot_collections = overseasTimesheetModel::leftJoin('work_dates', 'overseas_timesheet.id', '=', 'work_dates.ot_id')->where('ot_id', $ot_id)->get();
            $total_st_hour = 0;
            $total_1_5t_hour = 0;
            $total_2t_hour = 0;
            $total_travel_hours = 0;
            $total_work_hours = 0;
            for($i = 0;$i < count($ot_collections);$i++){
                $total_st_hour = $total_st_hour + $ot_collections[$i]->total_hour_1x;
                $total_1_5t_hour = $total_1_5t_hour + $ot_collections[$i]->total_hour_1_5x;
                $total_2t_hour = $total_2t_hour + $ot_collections[$i]->total_hour_2x;
                $total_travel_hours = $total_travel_hours + $ot_collections[$i]->total_travel_hour;
                $total_work_hours = $total_work_hours + $ot_collections[$i]->total_work_hour;
            }
            if($ot_id){
                $ot = overseasTimesheetModel::findOrFail($ot_id);
                $ot->total_st_hour = $total_st_hour;
                $ot->total_1_5t_hour = $total_1_5t_hour;
                $ot->total_2t_hour = $total_2t_hour;
                $ot->total_travel_hours = $total_travel_hours;
                $ot->total_work_hours = $total_work_hours;
                $ot->update();
                $request->session()->flash('alert-success', 'Form Submited!');
                return redirect()->route('view_form_otf', [ 'ot_id' => $ot_id]);
            }else{
                $request->session()->flash('alert-danger', 'error');
                return redirect()->route('view_form_otf', [ 'ot_id' => $ot_id]);
            }
        
      }else{
          return abort(404);
      }
    }

}





