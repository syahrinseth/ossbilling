<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use PhpOffice\PhpSpreadsheet\IOFactory;

use App\masterCurrencyRatesModel;
use App\currencyRatesDetailsModel;
use App\User;


class currencyRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function index()
    {
        //
        if(Auth::user()->account_type == 1){
        $master_cr = masterCurrencyRatesModel::all();
        return view('currency_rates.index', compact('master_cr'));
        }else{
            return abort(404);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        if(Auth::user()->account_type == 1){
        return view('currency_rates.create');
        }else{
            return abort(404);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Auth::user()->account_type == 1){
        $this->validate(request(),[
            'date' => 'required|date|unique:master_currency_rates,date',
            'attachment' => 'required|mimes:xls,csv'
        ]);
        $check = masterCurrencyRatesModel::whereMonth('date', date('m', strtotime($request->date)))->first();
        // return dd($check == null);
        if($check == null){
            $inputFileName = $request->attachment;
            // $helper->log('Loading file ' . pathinfo($inputFileName, PATHINFO_BASENAME) . ' using IOFactory to identify the format');
            $spreadsheet = IOFactory::load($inputFileName);
            $sheetData = $spreadsheet->getActiveSheet()->toArray(null, true, true, true);
            // return dd($sheetData);
            if($sheetData){
                $master_cr = new masterCurrencyRatesModel;
                $master_cr->date = $request->date;
                $master_cr->created_by = Auth::user()->sso_no;
                $master_cr->save();
                $new_mcr = masterCurrencyRatesModel::whereDate('date', $request->date)->where('created_by', Auth::user()->sso_no)->first();
                for($i=1;$i<=count($sheetData);$i++){
                    if(strlen($sheetData[$i]['C']) == 3){
                        // import
                        $cr_details[$i] = new currencyRatesDetailsModel;
                        $cr_details[$i]->mcr_id = $new_mcr->id;
                        $cr_details[$i]->country = $sheetData[$i]['A'];
                        $cr_details[$i]->currency = $sheetData[$i]['B'];
                        $cr_details[$i]->code = $sheetData[$i]['C'];
                        $cr_details[$i]->usd_per_currency = str_replace( ',', '', $sheetData[$i]['D'] );
                        $cr_details[$i]->currency_per_usd = str_replace( ',', '', $sheetData[$i]['E'] );
                        if($sheetData[$i]['A'] != null && $sheetData[$i]['B'] && $sheetData[$i]['C'] && $sheetData[$i]['D'] && $sheetData[$i]['E']){
                            $cr_details[$i]->save();
                        }else{
                            $request->session()->flash('alert-danger', 'Upload Failed!');
                            return back();
                        }
                    }elseif (strpos($sheetData[$i]['C'], '=') !== false) {
                        // import
                        $cr_details[$i] = new currencyRatesDetailsModel;
                        $cr_details[$i]->mcr_id = $new_mcr->id;
                        $cr_details[$i]->country = $sheetData[$i]['A'];
                        $cr_details[$i]->currency = $sheetData[$i]['B'];
                        $cr_details[$i]->code = $sheetData[$i]['C'];
                        $cr_details[$i]->usd_per_currency = str_replace( ',', '', $sheetData[$i]['D'] );
                        $cr_details[$i]->currency_per_usd = str_replace( ',', '', $sheetData[$i]['E'] );
                        if($sheetData[$i]['A'] != null && $sheetData[$i]['B'] && $sheetData[$i]['C'] && $sheetData[$i]['D'] && $sheetData[$i]['E']){
                            $cr_details[$i]->save();
                        }else{
                            $request->session()->flash('alert-danger', 'Upload Failed!');
                            return back();
                        }
                    }
                }
                $request->session()->flash('alert-success', 'Success!');
                return back();
            }else{
            $request->session()->flash('alert-danger', 'Upload Failed!');
            return back();
            }
        }else{
            $request->session()->flash('alert-danger', date('m', strtotime($request->date)).'Month already exists.');
            return back();
        }
        }else{
            return abort(404);
        }
        // return dd($spreadsheet);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($mcr_id)
    {
        //
        if(Auth::user()->account_type == 1){
        $currency_rates = masterCurrencyRatesModel::leftJoin('currency_rates_details', 'master_currency_rates.id', '=', 'currency_rates_details.mcr_id')->where('mcr_id', $mcr_id)->get();
        return view('currency_rates.show', compact('currency_rates'));
        }else{
            return abort(404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($mcr_id)
    {
        //
        if(Auth::user()->account_type == 1){
        $mcr = masterCurrencyRatesModel::find($mcr_id);
        if($mcr){
            $mcr->delete();
            return back();
        }else{
            return abort(404, "Unable To Delete");
        }
        }else{
            return abort(404);
        }
    }
}
