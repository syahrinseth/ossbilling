<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class local_timesheet_detailModel extends Model
{
    // model for table local_timesheet
    protected $table = 'local_timesheet_detail';
    protected $primaryKey = 'id';
}
