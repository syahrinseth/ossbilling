<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class currencyRatesDetailsModel extends Model
{
    //
    protected $table = 'currency_rates_details';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
