<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class local_timesheetModel extends Model
{
    // model for table local_timesheet
    protected $table = 'local_timesheet';
    protected $primaryKey = 'id';
}
