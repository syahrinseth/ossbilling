<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class rateModel extends Model
{
    // table for storing overseas expenses statement hour rate in USD
    protected $table = 'ot_rates';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
