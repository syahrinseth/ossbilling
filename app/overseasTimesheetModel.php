<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class overseasTimesheetModel extends Model
{
    //The table associated with model.
    protected $table = 'overseas_timesheet';
}
