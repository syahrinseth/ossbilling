<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class toolingConsummablesModel extends Model
{
    //
    protected $table = 'tooling_consummables';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
