<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class master_jobsModel extends Model
{
    //
    protected $table = 'master_jobs';
    protected $primaryKey = 'job_no';
    public $incrementing = false;
}
