<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class masterCurrencyRatesModel extends Model
{
    //
    protected $table = 'master_currency_rates';
    protected $primaryKey = 'id';
    protected $guarded = [];
}
