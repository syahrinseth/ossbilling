<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OverseasExpenseStatementModel extends Model
{
    // model for overseas_expenses_statement table
    protected $table = 'overseas_expenses_statement';
}
