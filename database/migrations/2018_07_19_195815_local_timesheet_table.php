<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocalTimesheetTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::create('local_timesheet', function (Blueprint $table) {
            $table->increments('id');
            $table->string('job_no')->nullable();
            $table->string('customer_signature_name')->nullable();
            $table->string('customer_signature_attachment')->default('no-file.png');
            $table->date('customer_signature_date')->nullable();
            $table->string('dept_signature_name')->nullable();
            $table->date('dept_signature_date')->nullable();
            $table->integer('status')->default(0);
            $table->decimal('total_st_hour', 10, 2)->default(0);
            $table->decimal('total_1_5t_hour', 10, 2)->default(0);
            $table->decimal('total_2t_hour', 10, 2)->default(0);
            $table->decimal('total_travel_hours', 10, 2)->default(0);
            $table->decimal('total_work_hours', 10, 2)->default(0);
            $table->timestamps();
        });
        // code to add modifier
        if(Schema::hasTable('local_timesheet')){
            Schema::enableForeignKeyConstraints();
            Schema::table('local_timesheet', function(Blueprint $table){
                $table->foreign('job_no')
                ->references('job_no')
                ->on('master_jobs')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('local_timesheet');
    }
}
