<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

use App\rateModel;

class RateTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('ot_rates', function (Blueprint $table) {
            $table->increments('id');
            $table->decimal('rate_st', 20,2);
            $table->decimal('rate_1_5x', 20,2);
            $table->decimal('rate_2x', 20,2);
            $table->integer('sso_no')->unsigned();
            $table->timestamps();
        });
        if(Schema::hasTable('oes_rates')){
            Schema::enableForeignKeyConstraints();
            Schema::table('oes_rates', function(Blueprint $table){
                $table->foreign('sso_no')
                ->references('sso_no')
                ->on('users')
                ->onDelete('cascade');
            });
            $rate = new rateModel;
            $rate->rate_st = 74;
            $rate->rate_1_5x = 111;
            $rate->rate_2x = 148;
            $rate->sso_no = 123123;
            $rate->save();
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('oes_rates');
    }
}
