<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ToolingConsummablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('tooling_consummables', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('job_no')->nullable();
            $table->string('description');
            $table->string('currency');
            $table->decimal('cost', 10, 2)->default(0);
            $table->timestamps();
        });
        // code to add modifier
        if(Schema::hasTable('tooling_consummables')){
            Schema::enableForeignKeyConstraints();
            Schema::table('tooling_consummables', function(Blueprint $table){
                $table->foreign('job_no')
                ->references('job_no')
                ->on('master_jobs')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('tooling_consummables');
    }
}
