<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMasterJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master_jobs', function (Blueprint $table) {
            $table->string('job_no');
            $table->primary('job_no');
            $table->text('job_description');
            $table->string('network_no');
            $table->string('po_no');
            $table->string('country');
            $table->timestamps();
        });

        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master_jobs');
    }
}
