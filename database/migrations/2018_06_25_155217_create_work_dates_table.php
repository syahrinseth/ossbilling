<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateWorkDatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('work_dates', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ot_id')->unsigned();
            $table->integer('sso_no')->unsigned();
            $table->string('day');
            $table->date('date');
            $table->string('remark');
            $table->decimal('overseas_night_shift_allow')->default(null);
            $table->time('travel_start')->nullable();
            $table->time('travel_finish')->nullable();
            $table->boolean('nextdaysite')->default(0);
            $table->time('travel2_start')->nullable();
            $table->time('travel2_finish')->nullable();
            $table->boolean('nextdayhome')->default(0);
            $table->time('work_start')->nullable();
            $table->time('work_finish')->nullable();
            $table->time('work2_start')->nullable();
            $table->time('work2_finish')->nullable();
            $table->decimal('total_hour_1x', 10, 2)->default(0);
            $table->decimal('total_hour_1_5x', 10, 2)->default(0);
            $table->decimal('total_hour_2x', 10, 2)->default(0);
            $table->decimal('total_travel_hour', 10, 2)->default(0);
            $table->decimal('total_work_hour', 10, 2)->default(0);
            $table->timestamps();
        });

        // code to add modifier
        if(Schema::hasTable('work_dates') && Schema::hasTable('users')){
            Schema::enableForeignKeyConstraints();
            Schema::table('work_dates', function(Blueprint $table){
                $table->foreign('ot_id')
                ->references('id')
                ->on('overseas_timesheet')
                ->onDelete('cascade');
                $table->foreign('sso_no')
                ->references('sso_no')
                ->on('users')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('work_dates');
    }
}
