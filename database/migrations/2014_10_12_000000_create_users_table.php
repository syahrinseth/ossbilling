<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use App\User;
class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->integer('sso_no')->unsigned();
            $table->primary('sso_no');
            $table->string('name');
            $table->integer('account_type')->default(0);
            $table->string('email')->unique();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
        });
        $admin = new User;
        $admin->sso_no = 123123;
        $admin->name = 'admin123';
        $admin->email = 'admin123@test.com';
        $admin->account_type = 1;
        $admin->password = bcrypt(123123);
        $admin->save();
        $user = new User;
        $user->sso_no = 321321;
        $user->name = 'user321';
        $user->email = 'user123@test.com';
        $user->account_type = 0;
        $user->password = bcrypt(123123);
        $user->save();
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
