<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class LocalTimesheetDetailTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('local_timesheet_detail', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('lt_id')->unsigned();
            $table->string('designation');
            $table->string('employee_name');
            $table->integer('sso_no')->unsigned();
            $table->string('day');
            $table->date('date');
            $table->string('remark');
            $table->time('travel_start')->nullable();
            $table->time('travel_finish')->nullable();
            $table->boolean('nextdaysite')->default(0);
            $table->time('travel2_start')->nullable();
            $table->time('travel2_finish')->nullable();
            $table->boolean('nextdayhome')->default(0);
            $table->time('work_start')->nullable();
            $table->time('work_finish')->nullable();
            $table->time('work2_start')->nullable();
            $table->time('work2_finish')->nullable();
            $table->decimal('total_hour_1x', 10, 2)->default(0);
            $table->decimal('total_hour_1_5x', 10, 2)->default(0);
            $table->decimal('total_hour_2x', 10, 2)->default(0);
            $table->decimal('total_travel_hour', 10, 2)->default(0);
            $table->decimal('total_work_hour', 10, 2)->default(0);
            $table->timestamps();
        });
        // code to add modifier
        if(Schema::hasTable('local_timesheet_detail')){
            Schema::enableForeignKeyConstraints();
            Schema::table('local_timesheet_detail', function(Blueprint $table){
                $table->foreign('sso_no')
                ->references('sso_no')
                ->on('users')
                ->onDelete('cascade');
                $table->foreign('lt_id')
                ->references('id')
                ->on('local_timesheet')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
         Schema::dropIfExists('local_timesheet_detail');
    }
}
