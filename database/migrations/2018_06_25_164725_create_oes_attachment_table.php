<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOesAttachmentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('oes_attachment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('oes_id')->unsigned();
            $table->string('job_no');
            $table->string('attachment')->default('no-file.png');
            $table->date('date2_from');
            $table->date('date2_to');
            $table->string('category');
            $table->string('description')->nullable();
            $table->boolean('payment_method')->default(0);
            $table->string('currency');
            $table->decimal('expenses', 10, 2)->default(NULL);
            $table->timestamps();
        });

        // code to add modifier
        if(Schema::hasTable('oes_attachment') && Schema::hasTable('overseas_expenses_statement')){
            Schema::enableForeignKeyConstraints();
            Schema::table('oes_attachment', function(Blueprint $table){
                $table->foreign('oes_id')
                ->references('id')
                ->on('overseas_expenses_statement')
                ->onDelete('cascade');
                $table->foreign('job_no')
                ->references('job_no')
                ->on('master_jobs')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('oes_attachment');
    }
}
