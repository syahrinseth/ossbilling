<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AllowancesClaim extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::creata('allowances_claim', function(Blueprint $table){
            $table->increment('id');
            $table->integer('ltd_id')->unsigned();
            $table->decimal('on_board_allowance', 6, 2)->default(0.00);
            $table->decimal('actg_leadhand', 6, 2)->default(0.00);
            $table->decimal('workshop_night_allow', 6, 2)->default(0.00);
            $table->decimal('workshop_night_shift_allow', 6, 2)->default(0.00);
            $table->decimal('workshop_transport_allow', 6, 2)->default(0.00);
            $table->decimal('breakfast', 6, 2)->default(0.00);
            $table->decimal('lunch', 6, 2)->default(0.00);
            $table->decimal('dinner', 6, 2)->default(0.00);
            $table->decimal('midnight', 6, 2)->default(0.00);
            $table->string('descrip')->nullable();
            $table->decimal('dollar', 6, 2)->nullable();
            $table->decimal('sub_total', 6, 2)->default(0.00);
            $table->timestamps();
        });
        if (Schema::hasTable('local_timesheet_detail')) {
            Schema::table('allowances_claim', function (Blueprint $table) {
                $table->foreign('ltd_id')
                ->references('id')
                ->on('local_timesheet_detail')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (Schema::hasTable('allowances_claim')) {
            Schema::table('allowances_claim', function (Blueprint $table) {
                $table->dropForeign('lt_id');
            });
        }
        Schema::dropIfExists('allowances_claim');
    }
}
