<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOverseasExpensesStatementTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('overseas_expenses_statement', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('sso_no')->unsigned();
            $table->string('job_no');
            $table->string('customer_name');
            $table->string('purpose_of_trip')->nullable();
            $table->string('department');
            $table->date('date_from');
            $table->date('date_to');
            $table->string('customer_signature_attachment')->default('no-file.png');
            $table->date('customer_signature_date')->nullable();
            $table->string('dept_signature_name')->nullable();
            $table->date('dept_signature_date')->nullable();
            $table->integer('status')->default(0);
            $table->timestamps();
        });
        
        // code to add modifier
        if(Schema::hasTable('overseas_expenses_statement')){
            Schema::enableForeignKeyConstraints();
            Schema::table('overseas_expenses_statement', function(Blueprint $table){
                $table->foreign('sso_no')
                ->references('sso_no')
                ->on('users')
                ->onDelete('cascade');
                $table->foreign('job_no')
                ->references('job_no')
                ->on('master_jobs')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('overseas_expenses_statement');
    }
}
