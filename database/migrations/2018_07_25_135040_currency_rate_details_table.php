<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CurrencyRateDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('currency_rates_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('mcr_id')->unsigned();
            $table->string('country');
            $table->string('currency');
            $table->string('code');
            $table->decimal('usd_per_currency', 20, 7)->default(0);
            $table->decimal('currency_per_usd', 20, 7)->default(0);
            $table->timestamps();
        });

         if(Schema::hasTable('currency_rates_details')){
            Schema::enableForeignKeyConstraints();
            Schema::table('currency_rates_details', function(Blueprint $table){
                $table->foreign('mcr_id')
                ->references('id')
                ->on('master_currency_rates')
                ->onDelete('cascade');
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('currency_rates_details');
        
    }
}
