<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/login', 'LoginController@index')->name('login');
Route::post('/login', 'LoginController@authenticate')->name('login');
Route::get('/logout', function(){
    Auth::logout();
    return redirect()->route('home');
})->name('logout');



Route::get('/', 'HomeController@index')->name('home');
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/dashboard', 'HomeController@index')->name('home');


// - Overseas Timesheet
Route::get('/overseas-timesheet-pending','OverseasTimesheetController@pending')->name('indexpending_otf');
Route::get('/overseas-timesheet/new-form', 'OverseasTimesheetController@create')->name('new_form_otf');
Route::post('/overseas-timesheet/new-form/store', 'OverseasTimesheetController@store')->name('new_form_store_otf');

Route::get('/overseas-timesheet/view-form-list', 'OverseasTimesheetController@index')->name('view_form_list_otf');
Route::get('/overseas-timesheet/view-form/{ot_id}', 'OverseasTimesheetController@show')->name('view_form_otf');
Route::post('/overseas-timesheet/view-form/{id}', 'OverseasTimesheetController@show')->name('view_form_otf');

Route::get('/overseas-timesheet/edit-form/{ot_id}', 'OverseasTimesheetController@edit')->name('edit_form_otf');
Route::post('/overseas-timesheet/edit-form/{ot_id}', 'OverseasTimesheetController@update')->name('edit_form_update_otf');
Route::post('/overseas-timesheet/delete-form/{ot_id}','OverseasTimesheetController@destroy')->name('destroy_form_otf');

Route::get('/overseas-timesheet/edit-date/{ot_id}/{date_id}', 'OverseasTimesheetController@edit')->name('edit_date_otf');
Route::post('/overseas-timesheet/edit-date/{ot_id}/{date_id}', 'OverseasTimesheetController@update')->name('update_date_otf');
Route::post('/overseas-timesheet/delete-date/{ot_id}/{date_id}', 'OverseasTimesheetController@deleteDate')->name('delete_date_otf');

Route::get('/overseas-timesheet/view-form/{ot_id}/add-new-date', 'OverseasTimesheetController@addNewDate')->name('add_new_date');
Route::post('/overseas-timesheet/view-form/{ot_id}/add-new-date-update', 'OverseasTimesheetController@addNewDateUpdate')->name('add_new_date_update');

Route::get('/overseas-timesheet/download-form/{ot_id}', 'OverseasTimesheetController@downloadForm')->name('download_form_otf');
Route::get('/overseas-timesheet/view-form/{ot_id}/approve', 'OverseasTimesheetController@approve')->name('approve_otf');

Route::post('/overseas-timesheet/cust-sign/{ot_id}', 'OverseasTimesheetController@custSign')->name('custSign_ot');
Route::get('/overseas-timesheet/download-cust-sign/{ot_id}', 'OverseasTimesheetController@custAttDownloadPdf')->name('custAttDownloadPdf_ot');

Route::get('/overseas-timesheet/rate-setting', 'OverseasTimesheetController@rateSetting')->name('ratesetting_ot');
Route::post('/overseas-timesheet/rate-setting-update', 'OverseasTimesheetController@rateSettingUpdate')->name('ratesettingupdate_ot');


// OVERSEAS EXPENSE STATEMENT FORM
Route::get('/overseas-expense-statement-pending', 'OverseasExpenseStatementController@pending')->name('indexpending_oes');
Route::get('/overseas-expense-statement', 'OverseasExpenseStatementController@index')->name('index_oes');
Route::get('/overseas-expense-statement/show/{oes_id}', 'OverseasExpenseStatementController@show')->name('show_oes');
Route::get('/overseas-expense-statement/create', 'OverseasExpenseStatementController@create')->name('create_oes');
Route::post('/overseas-expense-statement/store', 'OverseasExpenseStatementController@store')->name('store_oes');
Route::get('/overseas-expense-statement/edit/{oes_id}', 'OverseasExpenseStatementController@edit')->name('edit_oes');
Route::post('/overseas-expense-statement/update/{oes_id}', 'OverseasExpenseStatementController@update')->name('update_oes');
Route::post('/overseas-expense-statement/destroy/{oes_id}', 'OverseasExpenseStatementController@destroy')->name('destroy_oes');
Route::get('/overseas-expense-statement/show/{oes_id}/edit-att/{oes_att_id}', 'OverseasExpenseStatementController@editAtt')->name('edit_oes_att');
Route::post('/overseas-expense-statement/show/{oes_id}/update-att/{oes_att_id}', 'OverseasExpenseStatementController@updateAtt')->name('update_oes_att');
Route::post('/overseas-expense-statement/show/{oes_id}/destroy-att/{oes_att_id}', 'OverseasExpenseStatementController@destroyAtt')->name('destroy_oes_att');
Route::get('/overseas-expense-statement/show/{oes_id}/create-att', 'OverseasExpenseStatementController@createAtt')->name('create_oes_att');
Route::post('/overseas-expense-statement/show/{oes_id}/store-att', 'OverseasExpenseStatementController@storeAtt')->name('store_oes_att');
Route::get('/overseas-expense-statement/print/{oes_id}', 'OverseasExpenseStatementController@print')->name('print_oes');
Route::post('/overseas-expense-statement/customer_signature/{oes_id}', 'OverseasExpenseStatementController@customerSignature')->name('customersignature_oes_att');
Route::get('/overseas-expense-statement/approve/{oes_id}', 'OverseasExpenseStatementController@approve')->name('approve_oes');
Route::get('/overseas-expense-statement/download-cust-sign/{oes_id}', 'OverseasExpenseStatementController@custAttDownloadPdf')->name('custAttDownloadPdf_oes');
// Route::post('/overseas-expense-statement/cust-sign/{oes_id}', 'OverseasExpenseStatementController@custSign')->name('custsign_oes');

// CALCULATION 
Route::get('/calculation', 'CalculationController@index')->name('index_cal');
Route::get('/download', 'UserController@download')->name('download_cal');


// USERS
Route::get('/admin', 'UserController@index')->name('index_usr');
Route::get('/admin/edit/{sso_no}', 'UserController@edit')->name('edit_usr');
Route::post('/admin/update/{sso_no}', 'UserController@update')->name('update_usr');
Route::get('/admin/destroy/{sso_no}', 'UserController@destroy')->name('destroy_usr');
Route::get('/admin/change-password', 'UserController@changePassword')->name('changepassword_usr');
Route::post('/admin/update-password', 'UserController@updatePassword')->name('updatepassword_usr');

Route::get('/account/{sso_no}', 'UserController@account')->name('account_usr');
Route::post('/account/{sso_no}/update', 'UserController@accountUpdate')->name('accountupdate_usr');

Route::get('/admin/add-user', 'UserController@addUser')->name('adduser_usr');
Route::post('/admin/store-user', 'UserController@storeUser')->name('storeuser_usr');


//master job
Route::get('/master-job/create', 'MasterJobController@create')->name('create_usr_mjb');
Route::post('/master-job/store', 'MasterJobController@store')->name('store_usr_mjb');
Route::get('/master-job', 'MasterJobController@index')->name('index_usr_mjb');
Route::get('/master-job/destroy/{job_no}', 'MasterJobController@destroy')->name('destroy_usr_mjb');
Route::get('/master-job/edit/{job_no}', 'MasterJobController@edit')->name('edit_usr_mjb');
Route::post('/master-job/update/{job_no}', 'MasterJobController@update')->name('update_usr_mjb');




// LOCAL TIMESHEET
Route::get('/local-timesheet-pending', 'LocalTimesheetController@pending')->name('indexpending_lt');
Route::get('/local-timesheet', 'LocalTimesheetController@index')->name('index_lt');
Route::get('/local-timesheet/create', 'LocalTimesheetController@create')->name('create_lt');
Route::post('/local-timesheet/store', 'LocalTimesheetController@store')->name('store_lt');
Route::get('/local-timesheet/edit/{lt_id}', 'LocalTimesheetController@edit')->name('edit_lt');
Route::post('/local-timesheet/update/{lt_id}', 'LocalTimesheetController@update')->name('update_lt');
Route::get('/local-timesheet/show/{lt_id}', 'LocalTimesheetController@show')->name('show_lt');
Route::post('/local-timesheet/destroy/{lt_id}', 'LocalTimesheetController@destroy')->name('destroy_lt');

Route::get('/local-timesheet/create-detail/{lt_id}', 'LocalTimesheetController@createDetail')->name('create_lt_detail');
Route::post('/local-timesheet/store-detail/{lt_id}', 'LocalTimesheetController@storeDetail')->name('store_lt_detail');
Route::get('/local-timesheet/edit-detail/{ltd_id}', 'LocalTimesheetController@editDetail')->name('edit_lt_detail');
Route::post('/local-timesheet/update-detail/{ltd_id}', 'LocalTimesheetController@updateDetail')->name('update_lt_detail');
Route::get('/local-timesheet/show-detail/{lt_id}', 'LocalTimesheetController@showDetail')->name('show_lt_detail');
Route::post('/local-timesheet/destroy-detail/{ltd_id}', 'LocalTimesheetController@destroyDetail')->name('destroy_lt_detail');
Route::get('/local-timesheet/dept-approve/{lt_id}', 'LocalTimesheetController@deptApprove')->name('deptApprove_lt');
Route::get('/local-timesheet/print/{lt_id}', 'LocalTimesheetController@print')->name('print_lt');
Route::get('local-timesheet/cust-att-download-pdf/{lt_id}', 'LocalTimesheetController@custAttDownloadPdf')->name('custattprint_lt');

Route::get('/local-timesheet/rate-setting', 'LocalTimesheetController@rateSetting')->name('ratesetting_lt');
Route::post('/local-timesheet/rate-setting-update', 'LocalTimesheetController@rateSettingUpdate')->name('ratesettingupdate_lt');


// TOOLING AND CONSUMMABLES
Route::get('/admin/tooling-consummables', 'toolingConsummablesController@index')->name('index_tc');
Route::get('/admin/tooling-consummables/create', 'toolingConsummablesController@create')->name('create_tc');
Route::get('/admin/tooling-consummables/edit/{tc_id}', 'toolingConsummablesController@edit')->name('edit_tc');
Route::post('/admin/tooling-consummables/update/{tc_id}', 'toolingConsummablesController@update')->name('update_tc');
Route::post('/admin/tooling-consummables/store', 'toolingConsummablesController@store')->name('store_tc');
Route::get('/admin/tooling-consummables/destroy/{tc_id}', 'toolingConsummablesController@destroy')->name('destroy_tc');


// CURRENCY RATES
Route::get('/admin/currency-rates', 'currencyRateController@index')->name('index_cr');
Route::get('/admin/currency-rates/create', 'currencyRateController@create')->name('create_cr');
Route::get('/admin/currency-rates/edit/{mcr_id}', 'currencyRateController@edit')->name('edit_cr');
Route::post('/admin/currency-rates/update/{mcr_id}', 'currencyRateController@update')->name('update_cr');
Route::get('/admin/currency-rates/show/{mcr_id}', 'currencyRateController@show')->name('show_cr');
Route::post('/admin/currency-rates/store', 'currencyRateController@store')->name('store_cr');
Route::get('/admin/currency-rates/destroy/{mcr_id}', 'currencyRateController@destroy')->name('destroy_cr');


// Allowances Claim Form
Route::get('/allowances-claim', 'AllowancesClaimController@index')->name('index.acf');
Route::get('/allowances-claim/show/{id}', 'AllowancesClaimController@show')->name('show.acf');
Route::get('/allowances-claim/create', 'AllowancesClaimController@create')->name('create.acf');
Route::post('/allowances-claim/store', 'AllowancesClaimController@store')->name('store.acf');
Route::get('/allowances-claim/edit/{id}', 'AllowancesClaimController@edit')->name('edit.acf');
Route::post('/allowances-claim/update/{id}', 'AllowancesClaimController@update')->name('update.acf');
Route::post('/allowances-claim/delete/{id}', 'AllowancesClaimController@delete')->name('delete.acf');
Route::post('/allowances-claim/delete-claim/{id}', 'AllowancesClaimController@deleteClaim')->name('deleteClaim.acf');

// REPORTS
Route::get('/overseas-report/download', 'ReportController@download')->name('download_rp');
Route::get('overseas-report', 'ReportController@index')->name('index_rp');
Route::get('final-overseas-report/', 'ReportController@finalOverseasReport')->name('final_rp');
Route::post('test', function(Request $request){
    return abort(404);

    $var = model::find($id);
for($i=0;$i<12;$i++){
    $amountMonth[$i] = model::where($var['ic'])->whereMonth('trans_date', str_pad($i, 2, 0, STR_PAD_LEFT))->sum('amount');
}
    // return dd($request);
//     function secondsToTime($seconds) {
//         $dtF = new \DateTime('@0');
//         $dtT = new \DateTime("@$seconds");
//         return $dtF->diff($dtT)->format('%h:%i:%s');
//         }
//         function decimalHours($time){
//             $hms = explode(":", $time);
//             return ($hms[0] + ($hms[1]/60) + ($hms[2]/3600));
//         }
//      $start = strtotime('00:30:00');
//      $end = strtotime('16:00:00');
//      $total = $end - $start;
//     $tol = secondsToTime($total);


// $decimalHours = decimalHours($tol);


// return $decimalHours;

// $timestamp = strtotime('2018-07-18');

// $day = date('l', $timestamp);
// // return dd($day);

// $queryresult;

// $tempsunday = strtotime('Next Sunday',$queryresult[0].date);
// $index=0;
// while ($tempsunday<=$queryresult[max]){
// //generate header
// $currentdate = strtotime('Current Monday',$queryresult[$index].date);
// for(i=0;i<7;i++){
// if($currentdate==$searchsamedate){
//     //generate data row;
//     $index = $currentrecord;
// }else{
//     //generate empty row;
// }
// $currentdate++;
// }

// if($Index not last index){
//     $tempsunday = strtotime('Next Sunday',$queryresult[index+1].date);
// }else{
//     $tempsunday = strtotime('Next next Sunday',$queryresult[index].date);
// }
// //generate footer


// }

});