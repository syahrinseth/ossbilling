@extends('layouts.master_layout')
@section('title', 'User Change Password')
@section('content')

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User Change Password</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('updatepassword_usr')}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Change Password
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            </div>
                           <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                       <div class="form-group">
                                            <label for="sso_no">Old Password</label>
                                            <input type="password" name="current-password" class="form-control" value="" required>
                                       </div>
                                       <div class="form-group">
                                            <label for="name">New Password</label>
                                            <input type="password" name="new-password" class="form-control" value="" required>
                                       </div>
                                       <div class="form-group">
                                            <label for="name">Confirm Password</label>
                                            <input type="password" name="new-password_confirmation" class="form-control" value="" required>
                                       </div>
                                       <hr>
                                       @include('layouts.flash_message')
                                        @include('layouts.validate')
                                       <button type="submit" class="btn btn-primary">Change</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        @if(Auth::user()->account_type == 1)
                                        <a href="{{route('index_usr')}}" class="btn btn-default">Back</a>
                                        @endif
                                        <br>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-3">
                                   
                                      
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
            <!-- /.row -->
            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    

                    $.extend( true, $.fn.dataTable.defaults, {
                        "searching": false,
                        "ordering": false
                    } );
                });
            </script>
 
@endsection