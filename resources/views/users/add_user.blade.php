@extends('layouts.master_layout')
@section('title', 'Add New User')
@section('content')
@if(Auth::user()->account_type == 1)
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Add New User</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
<div class="row">
    <h1></h1>
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">New User</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="post" action="{{ route('storeuser_usr') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="name" class="col-md-4 control-label">Name</label>

                            <div class="col-md-6">
                                <input id="name" type="text" class="form-control" name="name" value="{{ old('name') }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('name') ? ' has-error' : '' }}">
                            <label for="sso_no" class="col-md-4 control-label">SSO#</label>

                            <div class="col-md-6">
                                <input id="sso_no" type="text" class="form-control" name="sso_no" value="" required autofocus>

                                @if ($errors->has('sso_no'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('sso_no') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
      <label for="email" class="col-md-4 control-label">Account Type</label>
      <div class="col-md-4">
        <div class="form-check">
          <input class="form-check-input" type="radio" name="account_type" id="gridRadios1" value="0" checked>
          <label class="form-check-label" for="gridRadios1">
            User
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="account_type" id="gridRadios2" value="1">
          <label class="form-check-label" for="gridRadios2">
            Admin
          </label>
        </div>
        <div class="form-check">
          <input class="form-check-input" type="radio" name="account_type" id="gridRadios3" value="2">
          <label class="form-check-label" for="gridRadios3">
            Human Resources
          </label>
        </div>
      </div>
      </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                @include('layouts.validate')
                                @include('layouts.flash_message')
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endif
@endsection