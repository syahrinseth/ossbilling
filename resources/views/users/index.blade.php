@extends('layouts.master_layout')
@section('title', 'Admin')
@section('content')
@if(Auth::user()->account_type == 1)
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Admin</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Admin List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>SSO No.</th>
                                        <th>Email</th>
                                        <th>Account Type</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($users as $user)
                                    @if(Auth::user()->account_type == 1)
                                    <tr>
                                        <td>
                                            {{$user->name}}
                                        </td>
                                        <td>{{$user->sso_no}}</td>
                                        <td>{{$user->email}}</td>
                                        <td>
                                        @if($user->account_type == 0)
                                            User
                                        @else   
                                            Admin
                                        @endif
                                        </td>
                                        <td>
                                        @if(Auth::user()->sso_no == $user->sso_no)
                                            <a href="{{route('edit_usr', ['sso_no'=>$user->sso_no])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                        @else
                                            <button onclick="deleteFunction({{$user->sso_no}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                        @endif
                                        </td>
                                        
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
                            
                            <script>
                                function deleteFunction(sso_no) {

                                var r = confirm("Are you sure to delete this user?");
                                if (r == true) {
                                    window.location.href = "/admin/destroy/"+sso_no;
                                } else {
            
                                }
                            }
                            </script>
@endif
@endsection