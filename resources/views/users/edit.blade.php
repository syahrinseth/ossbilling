@extends('layouts.master_layout')
@section('title', 'User Edit')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">User Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('update_usr',['sso_no'=>$user->sso_no])}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit
                        </div>
                        <div class="panel-body">
                            <div class="row">
                            </div>
                           <div class="row">
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                       <div class="form-group">
                                            <label for="sso_no">SSO#</label>
                                            <input type="text" class="form-control" value="{{$user->sso_no}}" disabled>
                                       </div>
                                       <div class="form-group">
                                            <label for="name">Name</label>
                                            <input type="text" name="name" class="form-control" value="{{$user->name}}" required>
                                       </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                    <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" name="email" class="form-control" value="{{$user->email}}" required>
                                       </div>
                                    @if(Auth::user()->account_type == 1)
                                      <div class="form-group">
                                      <label for="account_type">Account Type</label>
                                        <div class="form-check">
                                            <input class="form-check-input" name="account_type" type="checkbox" id="gridCheck" @if($user->account_type == 1) {{"checked"}} @endif>
                                                <label class="form-check-label" for="gridCheck">
                                                    Admin 
                                                </label>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        @if(Auth::user()->sso_no == $user->sso_no)
                                        <a href="{{route('changepassword_usr')}}" class="btn btn-info">Change Password</a>
                                        @endif
                                        <a href="{{route('index_usr')}}" class="btn btn-default">Back</a>
                                        <br>
                                        
                                        
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
            <!-- /.row -->
            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    

                    $.extend( true, $.fn.dataTable.defaults, {
                        "searching": false,
                        "ordering": false
                    } );
                });
            </script>
@endsection