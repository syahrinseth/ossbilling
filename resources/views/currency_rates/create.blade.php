@extends('layouts.master_layout')
@section('title', 'Master Currency Rates')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Currency Rates</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('store_cr')}}" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Master Currency Rates
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="date" class="form-control datepicker" name="date" value="" placeholder="Tooling Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Currency Rates (Excel Attachment)</label>
                                            <input type="file" class="form-control" name="attachment" required>
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6 col-sm-6">
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                            <div class="row">
                                <div class="col-lg-12">
                                        @include('layouts.validate')
                                    @include('layouts.flash_message')
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index_cr')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
@endsection