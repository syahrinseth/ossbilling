@extends('layouts.master_layout')
@section('title', 'Master Currency Rates')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Currency Rates</h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
             @include('layouts.validate')
            <!-- /.row -->
            @if(Auth::user()->account_type == 1)
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Master Currency Rates
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Date</th>
                                        <th>Uploaded By</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @if($master_cr)
                                    @foreach($master_cr as $item)
                                   <tr>
                                    <td>
                                        {{$item->date}}
                                    </td>
                                    <td>
                                        @php 
                                        $user = App\User::find($item->created_by);
                                        if($user){
                                            echo $user->name;
                                        }else{
                                            echo 'N/a';
                                        }
                                        @endphp
                                    </td>
                                    <td>
                                        <a href="{{route('show_cr', ['mcr_id'=>$item->id])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                        <button onclick="deleteFunction({{$item->id}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                   </tr>
                                    @endforeach
                                   @endif 
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
           
            @endif

                            <script src="/vendor/jquery/jquery.min.js"></script> 
                            <script>
                                

                                function deleteFunction(mcr_id) {

                                var r = confirm("Are you sure to delete?");
                                if (r == true) {
                                    window.location.href = "/admin/currency-rates/destroy/"+mcr_id;
                                } else {
            
                                }
                            }
                            </script>
@endsection