@extends('layouts.master_layout')
@section('title', 'Master Currency Rates Show')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Currency Rates Show</h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
             @include('layouts.validate')
            <!-- /.row -->
            @if(Auth::user()->account_type == 1)
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Master Currency Rates Show
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Country</th>
                                        <th>Currency</th>
                                        <th>Code</th>
                                        <th>USD Per Currency</th>
                                        <th>Currency Per USD</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @if($currency_rates)
                                    @foreach($currency_rates as $item)
                                   <tr>
                                    <td>{{$item->country}}</td>
                                    <td>{{$item->currency}}</td>
                                    <td>{{$item->code}}</td>
                                    <td>{{$item->usd_per_currency}}</td>
                                    <td>{{$item->currency_per_usd}}</td>
                                   </tr>
                                    @endforeach
                                   @endif 
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="form-group">
                    <a href="{{route('index_cr')}}" class="btn btn-default">Back</a>
                </div>
                
            </div>
           
            @endif

                          
@endsection