@extends('layouts.master_layout')
@section('title', 'Local Timesheet Detail')
@section('content')
<!-- Modal -->
<div class="modal fade" id="autopopulate" tabindex="-1" role="dialog" aria-labelledby="autopopulateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Auto Populate</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="row">
            <div class="col">
                <div class="form-group col-md-6">
                    <label for="date-form">Date From</label>
                    <input type="date" class="form-control datepicker" name="date_from" id="date-form">
                </div>
            </div>
            <div class="col">
                <div class="form-group col-md-6">
                    <label for="date-to">Date To</label>
                    <input type="date" class="form-control datepicker" name="date_to" id="date-to">
                </div>
            </div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="autopopulate">Auto Populate</button>
      </div>
    </div>
  </div>
</div>
 <!-- Model End -->

<form role="form" action="{{route('store_lt_detail', [ 'lt_id' => $local_timesheet->id])}}" method="post">

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Local Timesheet Detail Create</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            
            <!-- /.row -->
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create
                        </div>
                        <div class="panel-body">
                        <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Employee SSO#</label>
                                            <input list="sso_no" type="text" name="sso_no" class="form-control" value="" required>
                                            <datalist id="sso_no">
                                                @foreach($users as $user)
                                                <option value="{{$user['sso_no']}}">{{$user['sso_no']}} - {{$user['name']}}</option>
                                                @endforeach
                                            </datalist>    
                                            
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                         
                                    
                            <div class="row">
                                <div class="col-lg-12">  
                                        <h1>Timesheet</h1>
                                    
                               
                                
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <!-- <th>Days</th> -->
                                                        <th>Dates</th>
                                                        <th>Designation</th>
                                                        <th>Travel Hours</th>
                                                        <th>Work Hours</th>
                                                        <th>Work Hours</th>
                                                        <th>Remarks</th>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tbody id="timesheet-tbody">
                                                    <tr>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Pick:</label>
                                                                <input class="form-control datepicker" name="date[]" placeholder="Date" type="date" id="date-0" required>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Designation</label>
                                                                <input type="text" name="designation[]" class="form-control" placeholder="Designation"  id="designation-0" required>
                                                            </div>
                                                        </td>
                                                        <td id="timesheet-travel-col">
                                                            <div class="form-group">
                                                                <label>To Site:</label><br>
                                                                <label>Start</label>
                                                                <select class="form-control" name="travel_start[]"  id="travel_start-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                                <label>Finish</label>
                                                                <select class="form-control" name="travel_finish[]"  id="travel_finish-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                                <label for="nextdaysite-0">Next Day</label>
                                                                <input type="checkbox" id="nextdaysite-0" name="nextdaysite[]" value="1" class="nextdaysite">
                                                                <input type="hidden" name="nextdaysite[]" class="nextdaysite-hidden" id="nextdaysitehidden-0" value="0">
                                                            </div>
                                                            <hr>
                                                            <div class="form-group">
                                                                <label>To Home:</label><br>
                                                                <label>Start</label>
                                                                <select class="form-control" name="travel2_start[]"  id="travel2_start-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                                <label>Finish</label>
                                                                <select class="form-control" name="travel2_finish[]"  id="travel2_finish-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                                <label for="nextdayhome-0">Next Day</label>
                                                                <input type="checkbox" id="nextdayhome-0" name="nextdayhome[]" value="1" class="nextdayhome">
                                                                <input type="hidden" name="nextdayhome[]" class="nextdayhome-hidden" id="nextdayhomehidden-0" value="0">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                            <label>Before Lunch:</label><br>
                                                                <label>Start</label>
                                                                <select class="form-control" name="work_start[]"  id="work_start-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                                <label>Finish</label>
                                                                <select class="form-control" name="work_finish[]"  id="work_finish-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                            <label>After Lunch:</label><br>
                                                                <label>Start</label>
                                                                <select class="form-control" name="work2_start[]"  id="work2_start-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                                <label>Finish</label>
                                                                <select class="form-control" name="work2_finish[]"  id="work2_finish-0">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00">00:00</option>
                                                                    <option value="00:30:00">00:30</option>
                                                                    <option value="01:00:00">01:00</option>
                                                                    <option value="01:30:00">01:30</option>
                                                                    <option value="02:00:00">02:00</option>
                                                                    <option value="02:30:00">02:30</option>
                                                                    <option value="03:00:00">03:00</option>
                                                                    <option value="03:30:00">03:30</option>
                                                                    <option value="04:00:00">04:00</option>
                                                                    <option value="04:30:00">04:30</option>
                                                                    <option value="05:00:00">05:00</option>
                                                                    <option value="05:30:00">05:30</option>
                                                                    <option value="06:00:00">06:00</option>
                                                                    <option value="06:30:00">06:30</option>
                                                                    <option value="07:00:00">07:00</option>
                                                                    <option value="07:30:00">07:30</option>
                                                                    <option value="08:00:00">08:00</option>
                                                                    <option value="08:30:00">08:30</option>
                                                                    <option value="09:00:00">09:00</option>
                                                                    <option value="09:30:00">09:30</option>
                                                                    <option value="10:00:00">10:00</option>
                                                                    <option value="10:30:00">10:30</option>
                                                                    <option value="11:00:00">11:00</option>
                                                                    <option value="11:30:00">11:30</option>
                                                                    <option value="12:00:00">12:00</option>
                                                                    <option value="12:30:00">12:30</option>
                                                                    <option value="13:00:00">13:00</option>
                                                                    <option value="13:30:00">13:30</option>
                                                                    <option value="14:00:00">14:00</option>
                                                                    <option value="14:30:00">14:30</option>
                                                                    <option value="15:00:00">15:00</option>
                                                                    <option value="15:30:00">15:30</option>
                                                                    <option value="16:00:00">16:00</option>
                                                                    <option value="16:30:00">16:30</option>
                                                                    <option value="17:00:00">17:00</option>
                                                                    <option value="17:30:00">17:30</option>
                                                                    <option value="18:00:00">18:00</option>
                                                                    <option value="18:30:00">18:30</option>
                                                                    <option value="19:00:00">19:00</option>
                                                                    <option value="19:30:00">19:30</option>
                                                                    <option value="20:00:00">20:00</option>
                                                                    <option value="20:30:00">20:30</option>
                                                                    <option value="21:00:00">21:00</option>
                                                                    <option value="21:30:00">21:30</option>
                                                                    <option value="22:00:00">22:00</option>
                                                                    <option value="22:30:00">22:30</option>
                                                                    <option value="23:00:00">23:00</option>
                                                                    <option value="23:30:00">23:30</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Remark:</label>
                                                                <input class="form-control remark" id="remark-0" name="remark[]" value="" placeholder="Remark" type="text" required>
                                                                <hr>
                                                                <label for="standby">Stand By: </label>
                                                                <input type="checkbox" name="standby[]" class="standby" id="standby-0" value="1">
                                                                <input type="hidden" name="standby[]" class="standby-hidden" id="standbyhidden-0" value="0">
                                                                <br>
                                                                <small>(Optional)</small>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-primary" type="button" id="autopopulate" title="Auto Populate" data-toggle="modal" data-target="#autopopulate" data-id="0"><i class="fa fa-clone"></i></a>
                                                        </td>
                                                    </tr>
                                                    









                                                    
                                                </tbody>
                                            </table>

                                            
                                        </div>
                                        <!-- /.table-responsive -->
                                        <a class="btn btn-default" id="timesheet-add-new-row">Add New Row</a>
                                        <!-- <a class="btn btn-default" id="timesheet-remove-row">Remove Row</a> -->
                                        
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('show_lt', [ 'lt_id' => $local_timesheet->id ])}}" class="btn btn-default">Back</a>
                                        <br>
                                        
                                        <input name="from" value="Create" hidden>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
            
</form>


            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    var row = 1;

                    // function for remove timesheet row
                    jQuery('a#timesheet-remove-row').click(function(){
                        $('#timesheet-row').remove();
                    });

                    // jQuery('a#timesheet-remove-row').click(function(){
                    //     var remove = $(this).attr('data-id');
                    //     alert(remove);
                    // });
                    
                    jQuery('a#autopopulate').click(function(){
                        var counter = $(this).attr('data-id');
                    
                        jQuery('button#autopopulate').unbind("click").click(function(){
                            // var counter = $(this).attr('data-counter');
                            var date = $('input[id="date-'+counter+'"]').val();
                            var designation = $('input[id="designation-'+counter+'"]').val();
                            var travel_start = $('select[id="travel_start-'+counter+'"]').val();
                            var travel_finish = $('select[id="travel_finish-'+counter+'"]').val();
                            var travel2_start = $('select[id="travel2_start-'+counter+'"]').val();
                            var travel2_finish = $('select[id="travel2_finish-'+counter+'"]').val();
                            var work_start = $('select[id="work_start-'+counter+'"]').val();
                            var work_finish = $('select[id="work_finish-'+counter+'"]').val();
                            var work2_start = $('select[id="work2_start-'+counter+'"]').val();
                            var work2_finish = $('select[id="work2_finish-'+counter+'"]').val();
                            var remark = $('input[id="remark-'+counter+'"]').val();
                            var standby = $('input[id="standby-'+counter+'"]').prop("checked");
                            var nextdaysite = $('input[id="nextdaysite-'+counter+'"]').prop("checked");
                            var nextdayhome = $('input[id="nextdayhome-'+counter+'"]').prop("checked");
                            var date_to = $('input[name="date_to"]').val();
                            var date_from = $('input[name="date_from"]').val();
                            var d1 = (new Date(date_from)).getDate();
                            var d2 = (new Date(date_to)).getDate();
                            var diff = d2-d1;
                            if(standby == true){
                                standby = "checked";
                                var input_prop = "readonly";
                                var input_type = "hidden"
                                var standby_hidden = "disabled='disabled'";
                            }else{
                                standby = "";
                                var input_prop = "required";
                                var input_type = "text";
                                var standby_hidden = ""
                            }
                            if(nextdaysite == true){
                                nextdaysite = "checked='checked'";
                                var nextdaysite_hidden = "disabled='disabled'";
                            }else{
                                nextdaysite = "";
                                var nextdaysite_hidden = "";
                            }
                            if(nextdayhome == true){
                                nextdayhome = "checked='checked'";
                                var nextdayhome_hidden = "disabled='disabled'";
                            }else{
                                nextdayhome = "";
                                var nextdayhome_hidden = "";
                            }
                            if(diff >= 0){
                                // for loop the data
                                var date_increment = new Date(date_from);
                                var day;
                                var month;
                                var today;

                                for(i=0;i<=diff;i++){
                                    day = ("0" + date_increment.getDate()).slice(-2);
                                    month = ("0" + (date_increment.getMonth() + 1)).slice(-2);
                                    today = date_increment.getFullYear()+"-"+(month)+"-"+(day) ;
                                    $('tbody#timesheet-tbody').append('<tr id="timesheet-row-'+row+'" data-counter="'+row+'"><td><div class="form-group"><label>Pick:</label><input class="form-control datepicker" name="date[]" placeholder="Date" type="date" id="date" value="'+today+'"></div></td><td><div class="form-group"><label>Designation</label><input type="text" name="designation[]" class="form-control" placeholder="Designation" value="'+designation+'" id="designation-'+row+'" required></div></td><td><div class="form-group"><label>To Site:</label><br><label>Start</label><select class="form-control" name="travel_start[]" id="travel_start-'+row+'"><option value="'+travel_start+'" selected="selected">'+travel_start+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="travel_finish[]" id="travel_finish-'+row+'"><option value="'+travel_finish+'" selected="selected">'+travel_finish+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label for="nextdaysite-'+row+'">Next Day</label><input type="checkbox" id="nextdaysite-'+row+'" name="nextdaysite[]" value="1" class="nextdaysite" '+nextdaysite+'><input type="hidden" name="nextdaysite[]" class="nextdaysite-hidden" id="nextdaysitehidden-'+row+'" value="0" '+nextdaysite_hidden+'></div><hr><div class="form-group"><label>To Home:</label><br><label>Start</label><select class="form-control" name="travel2_start[]" id="travel2_start-'+row+'"><option value="'+travel2_start+'" selected="selected">'+travel2_start+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="travel2_finish[]" id="travel2_finish-'+row+'"><option value="'+travel2_finish+'" selected="selected">'+travel2_finish+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label for="nextdayhome-'+row+'">Next Day</label><input type="checkbox" id="nextdayhome-'+row+'" name="nextdayhome[]" value="1" class="nextdayhome" '+nextdayhome+'><input type="hidden" name="nextdayhome[]" class="nextdayhome-hidden" id="nextdayhomehidden-'+row+'" value="0" '+nextdayhome_hidden+'></div></td><td><div class="form-group"><label>Before Lunch:</label><br><label>Start</label><select class="form-control" name="work_start[]" id="work_start-'+row+'"><option value="'+work_start+'" selected="selected">'+work_start+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="work_finish[]" id="work_finish-'+row+'"><option value="'+work_finish+'" selected="selected">'+work_finish+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select></div></td><td><div class="form-group"><label>After Lunch:</label><br><label>Start</label><select class="form-control" name="work2_start[]" id="work2_start-'+row+'"><option value="'+work2_start+'" selected="selected">'+work2_start+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="work2_finish[]" id="work2_finish-'+row+'"><option value="'+work2_finish+'" selected="selected">'+work2_finish+'</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select></div></td><td><div class="form-group"><label>Remark:</label><input class="form-control remark" id="remark-'+row+'" name="remark[]" value="'+remark+'" placeholder="Remark" type="'+input_type+'" '+input_prop+'><hr><label for="standby-'+row+'">Stand By: </label><input type="checkbox" name="standby[]" class="standby" id="standby-'+row+'" value="1" '+standby+'><input type="hidden" name="standby[]" class="standby-hidden" id="standbyhidden-'+row+'" value="0" '+standby_hidden+'><br><small>(Optional)</small></div></td><td><div class="form-group"><a class="btn btn-danger remove-row" type="button" id="remove-row-'+row+'" href="#" title="Remove" data-toggle="modal" data-target="#removerow" data-counter="'+row+'"><i class="fa fa-times"></i></a></div></td></tr>');
                                    row++;
                                    date_increment.setDate(date_increment.getDate() + 1);
                                    // alert(travel2_finish);
                                }
                                alert('Auto Populate Success!');
                            }else{
                                alert('Auto Populate Fail! Please enter valid date range');
                            }
                            
                            
                            date = null;
                            designation = null;
                            travel_start = null;
                            travel_finish = null;
                            travel2_start = null;
                            travel2_finish = null;
                            work_start = null;
                            work_finish = null;
                            work2_start = null;
                            work2_finish = null;
                            remark = null;
                            date_to = null;
                            date_from = null;
                            d1 = null;
                            d2 = null;
                            diff = null;
                            date_increment = null;
                            day = null;
                            month = null;
                            today = null;    
                            counter = null;
                        });
                    });
                    
                    
                    jQuery('a#timesheet-add-new-row').click(function(){
                        $('tbody#timesheet-tbody').append('<tr id="timesheet-row-'+row+'" data-counter="'+row+'"><td><div class="form-group"><label>Pick:</label><input class="form-control datepicker" name="date[]" placeholder="Date" type="date" id="date-'+row+'"></div></td><td><div class="form-group"><label>Designation</label><input type="text" name="designation[]" id="designation-'+row+'" class="form-control" placeholder="Designation" required></div></td><td><div class="form-group"><label>To Site:</label><br><label>Start</label><select class="form-control" name="travel_start[]" id="travel_start-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="travel_finish[]" id="travel_finish-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label for="nextdaysite-'+row+'">Next Day</label><input type="checkbox" id="nextdaysite-'+row+'" value="1" class="nextdaysite" name="nextdaysite[]"><input type="hidden" name="nextdaysite[]" class="nextdaysite-hidden" id="nextdaysitehidden-'+row+'" value="0"></div><hr><div class="form-group"><label>To Home:</label><br><label>Start</label><select class="form-control" name="travel2_start[]" id="travel2_start-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="travel2_finish[]" id="travel2_finish-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label for="nextdayhome-'+row+'">Next Day</label><input type="checkbox" id="nextdayhome-'+row+'" value="1" name="nextdayhome[]" class="nextdayhome"><input type="hidden" name="nextdayhome[]" class="nextdayhome-hidden" id="nextdayhomehidden-'+row+'" value="0"></div></td><td><div class="form-group"><label>Before Lunch:</label><br><label>Start</label><select class="form-control" name="work_start[]" id="work_start-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="work_finish[]" id="work_finish-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select></div></td><td><div class="form-group"><label>After Lunch:</label><br><label>Start</label><select class="form-control" name="work2_start[]" id="work2_start-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select><label>Finish</label><select class="form-control" name="work2_finish[]" id="work2_finish-'+row+'"><option value="">Select</option><option value="00:00:00">00:00</option><option value="00:30:00">00:30</option><option value="01:00:00">01:00</option><option value="01:30:00">01:30</option><option value="02:00:00">02:00</option><option value="02:30:00">02:30</option><option value="03:00:00">03:00</option><option value="03:30:00">03:30</option><option value="04:00:00">04:00</option><option value="04:30:00">04:30</option><option value="05:00:00">05:00</option><option value="05:30:00">05:30</option><option value="06:00:00">06:00</option><option value="06:30:00">06:30</option><option value="07:00:00">07:00</option><option value="07:30:00">07:30</option><option value="08:00:00">08:00</option><option value="08:30:00">08:30</option><option value="09:00:00">09:00</option><option value="09:30:00">09:30</option><option value="10:00:00">10:00</option><option value="10:30:00">10:30</option><option value="11:00:00">11:00</option><option value="11:30:00">11:30</option><option value="12:00:00">12:00</option><option value="12:30:00">12:30</option><option value="13:00:00">13:00</option><option value="13:30:00">13:30</option><option value="14:00:00">14:00</option><option value="14:30:00">14:30</option><option value="15:00:00">15:00</option><option value="15:30:00">15:30</option><option value="16:00:00">16:00</option><option value="16:30:00">16:30</option><option value="17:00:00">17:00</option><option value="17:30:00">17:30</option><option value="18:00:00">18:00</option><option value="18:30:00">18:30</option><option value="19:00:00">19:00</option><option value="19:30:00">19:30</option><option value="20:00:00">20:00</option><option value="20:30:00">20:30</option><option value="21:00:00">21:00</option><option value="21:30:00">21:30</option><option value="22:00:00">22:00</option><option value="22:30:00">22:30</option><option value="23:00:00">23:00</option><option value="23:30:00">23:30</option></select></div></td><td><div class="form-group"><label>Remark:</label><input class="form-control remark" id="remark-'+row+'" name="remark[]" value="" placeholder="Remark" type="text" required><hr><label for="standby">Stand By: </label><input type="checkbox" name="standby[]" class="standby" id="standby-'+row+'" value="1"><input type="hidden" name="standby[]" class="standby-hidden" id="standbyhidden-'+row+'" value="0"><br><small>(Optional)</small></div></td><td><div class="form-group"><a class="btn btn-danger remove-row" type="button" id="remove-row-'+row+'" href="#" title="Remove" data-toggle="modal" data-target="#removerow" data-counter="'+row+'"><i class="fa fa-times"></i></a></div></td></tr>');
                        row++;
                    });
                    
                    
                    $(document).on('click','a.remove-row',function(){
                        //$('a#remove-row-1').css('background','blue');
                        var data_counter = $(this).attr('data-counter');
                        var id_row = 'tr#timesheet-row-'+data_counter;
                        //$(id_row).css('background',red);
                        $(id_row).remove();
                    });

                    $('input.standby').prop('checked', false);
                    $(document).on('change','input.standby',function(){
                        var checkbox_id = $(this).attr('id');
                        var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                        var checkbox = $('input#standby-'+id).prop('checked');
                        if(checkbox == true){
                            $('input#remark-'+id).removeAttr('required');
                            $('input#remark-'+id).attr("type", "hidden");
                            $('input#remark-'+id).prop('readonly', 'readonly');
                            $('input#standbyhidden-'+id).attr('disabled', 'disabled');
                        }else{
                            $('input#remark-'+id).removeAttr('readonly');
                            $('input#remark-'+id).attr("type", "text");
                            $('input#remark-'+id).prop('required', true);
                            $('input#standbyhidden-'+id).removeAttr('disabled');
                        }
                    });


                    $('input.nextdaysite').prop('checked', false);
                    $(document).on('change', 'input.nextdaysite', function(){
                        var checkbox_id = $(this).attr('id');
                        var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                        var checkbox = $('input#nextdaysite-'+id).prop('checked');
                        if(checkbox == true){
                            console.log(checkbox);
                            $('input#nextdaysitehidden-'+id).attr('disabled', 'disabled');
                        }else{
                            $('input#nextdaysitehidden-'+id).removeAttr('disabled');
                        }
                    });
                    $('input.nextdayhome').prop('checked', false);
                    $(document).on('change', 'input.nextdayhome', function(){
                        var checkbox_id = $(this).attr('id');
                        var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                        var checkbox = $('input#nextdayhome-'+id).prop('checked');
                        if(checkbox == true){
                            $('input#nextdayhomehidden-'+id).attr('disabled', 'disabled');
                        }else{
                            $('input#nextdayhomehidden-'+id).removeAttr('disabled');
                        }
                    });
                    

                });
            </script>
@endsection