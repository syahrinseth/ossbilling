@extends('layouts.master_layout')
@section('title', 'Local Timesheet')
@section('content')
<!-- Modal -->
<div class="modal fade" id="custSign" tabindex="-1" role="dialog" aria-labelledby="custSignLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="" method="post" enctype="multipart/form-data" id="custForm">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="custSignLabel">Customer Signature Attachment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="form-group">
                <label>Customer Name</label>
                <input type="text" value class="form-control" name="customer_signature_name">
            </div>
            <div class="form-group">
                <label>Customer Attachment</label>
                <input class="form-control" name="customer_signature_attachment" type="file">
            </div>
      </div>
      <div class="modal-footer">
        <input type="hidden" class="form-control" name="job_no" id="job_no">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="submitForm">Upload</button>
      </div>
    </form>  
    </div>
  </div>
</div>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Local Timesheet</h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
             @include('layouts.validate')
            <!-- /.row -->
            
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Local Timesheet List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Job Number</th>
                                        <th>Status</th>
                                        <th>Customer/Department Signature</th>
                                        <th>Updated</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($local_timesheets as $item)
                                    <tr>
                                        <td>
                                            {{$item->job_no}}
                                        </td>
                                        <td>
                                            @if($item->status == 0)
                                                <p class="text-warning">Pending</p>
                                            @else
                                                <p class="text-success">Approve</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($item->customer_signature_attachment != 'no-file.png')
                                                Cust Sign: <p class="text-success">Yes</p><br>
                                            @else
                                                Cust Sign: <p class="text-warning">N/a</p><br>
                                            @endif
                                            @if($item->dept_signature_name)
                                                Dept Sign: <p class="text-success">Yes</p>
                                            @else
                                                Dept Sign: <p class="text-warning">N/a</p>
                                            @endif
                                        </td>
                                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item->updated_at))->diffForHumans() }}</td>
                                        <td>{{ \Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->formatLocalized('%A %d %B %Y') }}</td>
                                        <td>
                                            @if(Auth::user()->account_type == 1)
                                            <a href="{{route('show_lt',[ 'lt_id'=>$item->id])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            
                                            <a href="{{route('edit_lt',[ 'lt_id'=>$item->id])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            
                                            
                                            <a href="{{route('print_lt',['lt_id'=>$item->id])}}" class="btn btn-success btn-sm" title="Print"><i class="fa fa-download"></i></a>
                                            <button onclick="deleteFunction({{$item->id}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                            <form method="POST" id="delete-{{$item->id}}" action="{{route('destroy_lt', ['lt_id'=>$item->id])}}">
                                                {{csrf_field()}}
                                            </form>
                                            @else
                                            <a href="{{route('show_lt',[ 'lt_id'=>$item->id])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('edit_lt',[ 'lt_id'=>$item->id])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('print_lt',['lt_id'=>$item->id])}}" class="btn btn-success btn-sm" title="Print"><i class="fa fa-download"></i></a>
                                            @endif
                                            
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
            
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
            </div>
            <!-- /.row -->
            <!-- Button trigger modal -->
            
          



                            <script src="/vendor/jquery/jquery.min.js"></script> 
                            <script>
                                jQuery('button#custSign').click(function(){
                                    var lt_id = $(this).attr('data-id');
                                    var job_no = $(this).attr('data-jobno');
                                    $('form#custForm').attr('action', '/local-timesheet/update/'+lt_id);
                                    $('input#job_no').attr('value', job_no);
                                });

                                $("#submitForm").on('click', function() {
                                    $("#custForm").submit();
                                });


                            function deleteFunction(lt_id) {

                                var r = confirm("Are you sure to delete this job number from local timesheet?");
                                if (r == true) {
                                    // window.location.href = "/local-timesheet/destroy/"+lt_id;
                                    document.getElementById('delete-'+lt_id).submit();
                                    
                                }
                            }
                            

                            function approveFunction(lt_id) {

                                var r = confirm("Are you sure to include your digital signature to this local timesheet?");
                                    if (r == true) {
                                        window.location.href = "/local-timesheet/dept-approve/"+lt_id;
                                    } else {
            
                                    }
                                }



                                // jQuery('a#ajaxView').click(function(e){});
                            </script>
@endsection