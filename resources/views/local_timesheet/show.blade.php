@extends('layouts.master_layout')
@section('title', 'Local Timesheet Show')
@section('content')
<!-- Modal -->
<div class="modal fade" id="custSign" tabindex="-1" role="dialog" aria-labelledby="custSignLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="" method="post" enctype="multipart/form-data" id="custForm">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="custSignLabel">Customer Signature Attachment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @if($local_timesheet->customer_signature_attachment != 'no-file.png')
            @php
                $file = pathinfo($local_timesheet->customer_signature_attachment, PATHINFO_EXTENSION)
            @endphp
            @if(strtolower($file) == "pdf")
            <div class="form-group">
                <label>Customer Signature Name</label>
                <input type="text" value="{{$local_timesheet->customer_signature_name}}" class="form-control" disabled>
            </div>
                <div class="form-group" id="attachment">
                    <label for="">Customer Signature Attachment</label>
                    <input type="text" class="form-control" id="signature_name" value="{{$local_timesheet['customer_signature_attachment']}}" disabled>
                    <br>    
                    <embed src="/storage/local_timesheet/{{$local_timesheet->id}}/cust_sign/{{$local_timesheet->customer_signature_attachment}}" alt="..." height="400px" width="100%" id="attachment">
                </div>
            @else
            <div class="form-group">
                <label>Customer Signature Name</label>
                <input type="text" value="{{$local_timesheet->customer_signature_name}}" class="form-control" disabled>
            </div>
                <input type="text" class="form-control" id="signature_name" value="{{$local_timesheet['customer_signature_attachment']}}" disabled>
                <br>
                <img src="/storage/local_timesheet/{{$local_timesheet->id}}/cust_sign/{{$local_timesheet->customer_signature_attachment}}" class="rounded mx-auto d-block" alt="..." width="100%" id="attachment">
            @endif
        @else
            <div class="form-group">
                <label>Customer Signature Name</label>
                <input type="text" class="form-control" name="customer_signature_name" value="{{$local_timesheet->customer_signature_name}}">
            </div>
            <div class="form-group">
                <label>Customer Signature Attachment</label>
                <input class="form-control" name="customer_signature_attachment" type="file">
            </div>
        @endif
          
      </div>
      <div class="modal-footer">
        <input type="hidden" class="form-control" name="job_no" id="job_no">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        @if($local_timesheet->customer_signature_attachment == 'no-file.png')
        <button type="button" class="btn btn-primary" id="submitForm">Upload</button>
        @endif
      </div>
    </form>  
    </div>
  </div>
</div>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Local Timesheet</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Detail
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                    <h1>Timesheet Infomation</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Job Number</label>
                                            <input class="form-control" name="name" value="{{$local_timesheet->job_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Signature Name</label>
                                            <input class="form-control" name="customer_name" value="{{$local_timesheet->customer_signature_name}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Network no.</label>
                                            <input class="form-control" name="network_no" value="{{$job->network_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Signature</label>
                                            @if($local_timesheet->customer_signature_name)
                                                <p class="text-success">{{$local_timesheet->customer_signature_name}}</p>
                                            @else
                                                <p class="text-warning">N/a</p>
                                            @endif
                                        </div>
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Job description</label>
                                            <input class="form-control" name="job_no" value="{{$job->job_description}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" name="country" value="{{$job->country}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>PO#</label>
                                            <input class="form-control" name="po_no" value="{{$job->po_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Dept Signature Name</label>
                                            @if($local_timesheet->dept_signature_name)
                                                <p class="text-success">{{$local_timesheet->dept_signature_name}}</p>
                                            @else
                                                <p class="text-warning">N/a</p>
                                            @endif
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Local Timesheet</h1>
                                        <div class="table-responsive">
                                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th colspan="4"></th>
                                        <th colspan="2">Travel Hour(s)</th>
                                        <th colspan="2">Work Hour(s)</th>
                                        <th colspan="2">Work Hour(s)</th>
                                        <th colspan="1"></th>
                                        <th colspan="3">Total Hour(s)</th>
                                        <th></th>
                                        <th></th>
                                        <th colspan="1"></th>
                                    </tr>
                                    <tr>
                                        <th>Date(s)</th>
                                        <th>Day(s)</th>
                                        <th>Employee Name/SSO no.</th>
                                        <th>Designation</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Remark(s)</th>
                                        <th>1.0x</th>
                                        <th>1.5x</th>
                                        <th>2.0x</th>
                                        <th>Total Travel Hour(s)</th>
                                        <th>Total Work Hour(s)</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    @if($lt_details)
                                        @foreach($lt_details as $item)
                                        <tr>
                                            <td>
                                                {{\Carbon\Carbon::createFromTimeStamp(strtotime($item->date))->formatLocalized('%d %B %Y')}}
                                            </td>
                                            <td>
                                                {{$item->day}}
                                            </td>
                                            <td>
                                                {{$item->employee_name}} | {{$item->sso_no}}
                                            </td>
                                            <td>
                                                {{$item->designation}}
                                            </td>
                                            <td>
                                                {{$item->travel_start}}
                                                <hr>
                                                {{$item->travel2_start}}
                                            </td>
                                            <td>
                                                {{$item->travel_finish}}
                                                <hr>
                                                {{$item->travel2_finish}}
                                            </td>
                                            <td>
                                                {{$item->work_start}}
                                            </td>
                                            <td>
                                                {{$item->work_finish}}
                                            </td>
                                            <td>
                                                {{$item->work2_start}}
                                            </td>
                                            <td>
                                                {{$item->work2_finish}}
                                            </td>
                                            <td>
                                                {{ucfirst($item->remark)}}
                                            </td>
                                            <td>
                                                {{$item->total_hour_1x}}
                                            </td>
                                            <td>
                                                {{$item->total_hour_1_5x}}
                                            </td>
                                            <td>
                                                {{$item->total_hour_2x}}
                                            </td>
                                            <td>
                                                {{$item->total_travel_hour}}
                                            </td>
                                            <td>
                                                {{$item->total_work_hour}}
                                            </td>
                                            <td>
                                            @if(Auth::user()->account_type == 1)
                                                <a href="{{route('edit_lt_detail',['oes_id' => $item->id])}}" class="btn btn-default"><i class="fa fa-edit"></i></a></a>
                                                <button class="btn btn-danger" onclick="deleteRowFunction({{$item->id}})"><i class="fa fa-trash"></i></button>
                                                <form method="POST" id="delete-{{$item->id}}" action="{{route('destroy_lt_detail', ['ltd_id'=>$item->id])}}">
                                                    {{csrf_field()}}
                                                </form>
                                            @else
                                                @if(Auth::user()->sso_no == $item->sso_no)
                                                <a href="{{route('edit_lt_detail',['oes_id' => $item->id])}}" class="btn btn-default"><i class="fa fa-edit"></i></a></a>
                                                <button class="btn btn-danger" onclick="deleteRowFunction({{$item->id}})"><i class="fa fa-trash"></i></button>
                                                <form method="POST" id="delete-{{$item->id}}" action="{{route('destroy_lt_detail', ['ltd_id'=>$item->id])}}">
                                                    {{csrf_field()}}
                                                </form>
                                                @endif
                                            @endif
                                            </td>
                                        </tr>
                                        @endforeach
                                    @endif
                                
                                
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="11">Total Hour(s)</th>
                                        
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- /.table-responsive -->
                        <a href="{{route('create_lt_detail', ['lt_id'=>$local_timesheet->id])}}" class="btn btn-primary">Add New Timesheet</a>
                                        </div>
                                        <!-- /.table-responsive -->
                                        
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                @if(Auth::user()->account_type == 1)
                                        <a href="{{route('edit_lt',['oes_id' => $local_timesheet->id])}}" class="btn btn-default">Edit</a>

                                        <a href="{{route('print_lt',['lt_id'=>$local_timesheet->id])}}" class="btn btn-success">Download</a>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#custSign" data-id="{{$local_timesheet->id}}" data-jobno="{{$local_timesheet->job_no}}" id="custSign">Customer Signature</button>
                                        <button class="btn btn-info" onclick="approveFunction({{$local_timesheet->id}})">Approve</button>

                                        <button class="btn btn-danger" onclick="deleteFunction({{$local_timesheet->id}})">Delete</button>
                                        
                                        <a href="{{route('index_lt')}}" class="btn btn-default">Back</a>
                                        <form method="POST" id="delete-{{$local_timesheet->id}}" action="{{route('destroy_lt', ['lt_id'=>$local_timesheet->id])}}">
                                                {{csrf_field()}}
                                        </form>
                                        <br>
                                @else
                                        <a href="{{route('edit_lt',['oes_id' => $local_timesheet->id])}}" class="btn btn-default">Edit</a>
                                        <a href="{{route('print_lt',['lt_id'=>$local_timesheet->id])}}" class="btn btn-success">Download</a>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#custSign" data-id="{{$local_timesheet->id}}" data-jobno="{{$local_timesheet->job_no}}" id="custSign">Customer Signature</button>
                                        
                                        <a href="{{route('index_lt')}}" class="btn btn-default">Back</a>
                                        
                                        <br>
                                @endif
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Attachment</h4>
                                        </div>
                                        <div class="modal-body">
                                           <img style="width:100%;" id="attachment" src="">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
            <script src="/vendor/jquery/jquery.min.js"></script> 
                            <script>
                                function deleteFunction(lt_id) {

                                var r = confirm("Are you sure to delete this local timesheet?");
                                    if (r == true) {
                                        // window.location.href = "/local-timesheet/destroy/"+lt_id;
                                        document.getElementById('delete-'+lt_id).submit();
                                    } else {
            
                                    }
                                }
                                
                                function approveFunction(lt_id) {

                                var r = confirm("Are you sure to include your digital signature to this local timesheet?");
                                    if (r == true) {
                                        window.location.href = "/local-timesheet/dept-approve/"+lt_id;
                                    } else {
            
                                    }
                                }


                                function deleteRowFunction(ltd_id) {
                                    var r = confirm("Are you sure to delete this date from this local timesheet?");
                                    if (r == true) {
                                        // window.location.href = "/local-timesheet/destroy-detail/"+ltd_id;
                                        document.getElementById('delete-'+ltd_id).submit();
                                    } else {
            
                                    }
                                }
                                
            jQuery(document).ready(function(){
                jQuery('button#custSign').click(function(){
                                    var lt_id = $(this).attr('data-id');
                                    var job_no = $(this).attr('data-jobno');
                                    $('form#custForm').attr('action', '/local-timesheet/update/'+lt_id);
                                    $('input#job_no').attr('value', job_no);
            });

                                $("#submitForm").on('click', function() {
                                    $("#custForm").submit();
                                });
            });
                            
            </script>

@endsection