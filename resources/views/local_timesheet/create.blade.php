@extends('layouts.master_layout')
@section('title', 'Local Timesheet Create')
@section('content')

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Local Timesheet</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('store_lt')}}" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create
                        </div>
                        <div class="panel-body">
                            
                            <div class="row">
                                <div class="col-lg-3"></div>
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Job no.</label>
                                            <input list="job_no" name="job_no" class="form-control" placeholder="Job Number" required>
                                            <datalist id="job_no">
                                                @if($master_jobs)
                                                    @foreach($master_jobs as $item)
                                                        <option value="{{$item->job_no}}">
                                                    @endforeach
                                                @endif
                                            </datalist>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                    @include('layouts.flash_message')
                                                    @include('layouts.validate')
                                                </div>
                                            </div>
                                        <div class="row">
                                                <div class="col-lg-12">
                                        
                                                        <button type="submit" class="btn btn-primary">Submit</button>
                                                        <button type="reset" class="btn btn-default">Reset</button>
                                                        <a href="{{route('index_lt')}}" class="btn btn-default">Back</a>
                                                        <br>
                                    
                                                </div>
                                        </div>
                                <div class="col-lg-3"></div>
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
            <!-- /.row -->
           

@endsection