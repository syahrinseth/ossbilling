@extends('layouts.master_layout')
@section('title', 'Local Timesheet Edit')
@section('content')

<form role="form" action="{{route('update_lt', [ 'lt_id'=>$local_timesheet->id])}}" method="post" enctype="multipart/form-data">
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Local Timesheet</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit
                        </div>
                        <div class="panel-body">
                            
                            <div class="row">
                            <div class="col-lg-3">
                                </div>
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Job no.</label>
                                            <input list="job_no" name="job_no" class="form-control" placeholder="Job Number" value="{{$local_timesheet->job_no}}" disabled>
                                            <datalist id="job_no">
                                                @if($master_jobs)
                                                    @foreach($master_jobs as $item)
                                                        <option value="{{$item->job_no}}">
                                                    @endforeach
                                                @endif
                                            </datalist>
                                        </div>
                                        
                                            @if($local_timesheet->customer_signature_attachment != 'no-file.png')
                                                <div class="form-group">
                                                <label>Customer Signature Attachment</label><br>
                                                <a class="btn btn-primary" id="view_attachment" data-id="{{$local_timesheet->customer_signature_attachment}}" data-toggle="modal" data-target="#viewAttachment">View Attachment</a>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                <label>Customer Signature Name</label>
                                                @if($local_timesheet->customer_signature_name)
                                                <input type="text" class="form-control" name="customer_signature_name" value="{{$local_timesheet->customer_signature_name}}">
                                                @else
                                                <input type="text" class="form-control" name="customer_signature_name">
                                                @endif
                                                </div>
                                                <div class="form-group">
                                                <label>Customer Signature Attachment</label><br>
                                                <input class="form-control" name="customer_signature_attachment" type="file">
                                                </div>
                                            @endif
                                       <div class="row">
                                            <div class="col-lg-12">
                                                @include('layouts.flash_message')
                                                @include('layouts.validate')
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-lg-12">
                                                    <input list="job_no" name="job_no" class="form-control" placeholder="Job Number" value="{{$local_timesheet->job_no}}" type="hidden">
                                                    <button type="submit" class="btn btn-primary">Update</button>
                                                    <button type="reset" class="btn btn-default">Reset</button>
                                                    <a href="{{route('show_lt',['lt_id'=>$local_timesheet->id])}}" class="btn btn-default">Cancel</a>
                                                    <br>
                                    
                                            </div>
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-3">
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->

            <!-- Modal -->
<div class="modal fade" id="viewAttachment" tabindex="-1" role="dialog" aria-labelledby="viewAttachmentLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="viewAttachmentLabel">Customer Signature Attachment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
        <div class="form-group">
            <label for="signature_name" id="attachment">Customer Signature Name</label>
            <input type="text" class="form-control" id="signature_name" value="{{$local_timesheet['customer_signature_name']}}" disabled>
        </div>
        @if($local_timesheet->customer_signature_attachment != 'no-file.png')
            @php
                $file = pathinfo($local_timesheet->customer_signature_attachment, PATHINFO_EXTENSION)
            @endphp
            @if(strtolower($file) == "pdf")
                <div class="form-group" id="attachment">
                    <label for="">Customer Signature Attachment</label>
                    <input type="text" class="form-control" id="signature_name" value="{{$local_timesheet['customer_signature_attachment']}}" disabled>
                    <br>
                    <embed src="/storage/local_timesheet/{{$local_timesheet->id}}/cust_sign/{{$local_timesheet->customer_signature_attachment}}" height="400" width="100%">
                </div>
            @else
            <div class="form-group">
            <label for="">Customer Signature Attachment</label>
            <input type="text" class="form-control" id="signature_name" value="{{$local_timesheet['customer_signature_attachment']}}" disabled>
            <br>
                <img src="/storage/local_timesheet/{{$local_timesheet->id}}/cust_sign/{{$local_timesheet->customer_signature_attachment}}" class="rounded mx-auto d-block" alt="..." width="100%" id="attachment">
            </div>
            @endif
        @else

        @endif
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Done</button>
        <button type="button" class="btn btn-danger" id="remove_att">Remove Attachment</button>
      </div>
    </div>
  </div>
</div>
           </form>
<script src="/vendor/jquery/jquery.min.js"></script> 
<script>
jQuery(document).ready(function(){

                jQuery('button#remove_att').one('click', function(){
                    $('label#attachment').remove();
                    $('div#attachment').remove();
                    $('img#attachment').remove();
                    $('input#signature_name').remove();
                    $('div#modal-body').append('<div class="form-group"><label>Customer Signature Name</label><input class="form-control" name="customer_signature_name" value="{{$local_timesheet->customer_signature_name}}"></div><label>Customer Signature Attachment</label><input type="file" id="file" class="form-control" name="customer_signature_attachment">');
                });

            });

</script>
@endsection