@if (count($errors))
<div class="form-group">

<div class="alert alert-danger">
    @foreach($errors->all() as $error)
    <span class="help-block" style="color:red;">-{{ $error }}</span>
    @endforeach
</div>

</div>
@endif