<!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a><img src="/img/ge.png" style="width: 60px;float: left;"></a><a class="navbar-brand" href="/home" style="margin-left: -15px;">GE OSS Billing</a>
            </div>
            <!-- /.navbar-header -->
        @if(Auth::user())
            <ul class="nav navbar-top-links navbar-right">
                
                <li class="dropdown">
                    <a class="dropdown-toggle" data-toggle="dropdown" href="#">
                        <i class="fa fa-user fa-fw"></i> <i class="fa fa-caret-down"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-user">
                        <li><a href="#"><i class="fa fa-user fa-fw"></i> {{Auth::user()->name}}</a>
                        </li>
                        <li><a href="{{route('account_usr', ['sso_no'=>Auth::user()->sso_no])}}"><i class="fa fa-gear fa-fw"></i> Account</a>
                        </li>
                        <li><a href="{{route('changepassword_usr')}}"><i class="fa fa-gear fa-fw"></i> Change Password</a>
                        </li>
                        <li class="divider"></li>
                        <li><a href="{{route('logout')}}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>
                        </li>
                    </ul>
                    <!-- /.dropdown-user -->
                </li>
                <!-- /.dropdown -->
            </ul>
            <!-- /.navbar-top-links -->
        @endif
            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <a class="text-left">Hello! {{ucwords(Auth::user()->name)}}</a>
                            <!-- <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div> -->
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="{{route('home')}}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <!-- <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Charts<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="flot.html">Flot Charts</a>
                                </li>
                                <li>
                                    <a href="morris.html">Morris.js Charts</a>
                                </li>
                            </ul>
                            <!- /.nav-second-level ->
                        </li> -->
                        <!-- <li>
                            <a href="tables.html"><i class="fa fa-table fa-fw"></i> Tables</a>
                        </li> -->
                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i> Local Timesheet <span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="{{route('create_lt')}}"><span class="fa fa-plus"></span> Create New LT</a>
                                        </li>
                                        <li>
                                            <a href="{{route('index_lt')}}"><span class="fa fa-list"></span> LT List</a>
                                        </li>
                                        @if(Auth::user()->account_type == 1)
                                            <li>
                                                <a href="{{route('ratesetting_lt')}}"><span class="fa fa-gear"></span> LT Rate Setting</a>
                                            </li>
                                        @endif
                                    </ul>
                                    <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i> Overseas Timesheet<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                @if(Auth::user()->account_type == 1)
                                <li>
                                    <a href="{{route('new_form_otf')}}"><span class="fa fa-plus"></span> Create New OT</a>
                                </li>
                                <li>
                                    <a href="{{route('view_form_list_otf')}}"><span class="fa fa-list"></span> OT List</a>
                                </li>
                                <li>
                                    <a href="{{route('ratesetting_ot')}}"><span class="fa fa-gear"></span> OT Rate Setting</a>
                                </li>
                                <!-- <li>
                                    <a href="#"><span class="fa fa-list"></span> Pending OT List</a>
                                </li> -->
                                @else
                                <li>
                                    <a href="{{route('new_form_otf')}}"><span class="fa fa-plus"></span> Create New OT</a>
                                </li>
                                <li>
                                    <a href="{{route('view_form_list_otf')}}"><span class="fa fa-list"></span> OT List</a>
                                </li>
                                @endif
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-file-text fa-fw"></i> Overseas Expense Statement <span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                    @if(Auth::user()->account_type == 1)
                                        <li>
                                            <a href="{{route('create_oes')}}"><span class="fa fa-plus"></span> Create New OES</a>
                                        </li>
                                        <li>
                                            <a href="{{route('index_oes')}}"><span class="fa fa-list"></span> OES List</a>
                                        </li>
                                        
                                    @else
                                        <li>
                                            <a href="{{route('create_oes')}}"><span class="fa fa-plus"></span> Create New OES</a>
                                        </li>
                                        <li>
                                            <a href="{{route('index_oes')}}"><span class="fa fa-list"></span> OES List</a>
                                        </li>
                                    @endif
                                    </ul>
                                    <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-wrench fa-fw"></i> Allowances Claim Form (LT) <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                    <li>
                                        <a href="{{route('create.acf')}}"><span class="fa fa-plus"></span> Create New ACF</a>
                                    </li>
                                    <li>
                                        <a href="{{route('index.acf')}}"><span class="fa fa-list"></span> ACF List</a>
                                    </li>
                            </ul>
                        </li>
                        <li>
                                    <a href="#"><i class="fa fa-wrench fa-fw"></i> Tooling & Consummables <span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="{{route('create_tc')}}"><span class="fa fa-plus"></span> Create New T&C</a>
                                        </li>
                                        <li>
                                            <a href="{{route('index_tc')}}"><span class="fa fa-list"></span> T&C List</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                        </li>
                        @if(Auth::user()->account_type == 1)
                        <li>
                            <a href="#"><i class="fa fa-briefcase fa-fw"></i> Master Job <span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                <li>
                                    <a href="{{route('create_usr_mjb')}}"><span class="fa fa-plus"></span> Create New Job</a>
                                </li>
                                <li>
                                    <a href="{{route('index_usr_mjb')}}"><span class="fa fa-list"></span> Master Job List</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li>
                                    <a href="#"><i class="fa fa-dollar fa-fw"></i> Master Currency Rate <span class="fa arrow"></span></a>
                                    <ul class="nav nav-second-level">
                                        <li>
                                            <a href="{{route('create_cr')}}"><span class="fa fa-upload"></span> Upload New MCR</a>
                                        </li>
                                        <li>
                                            <a href="{{route('index_cr')}}"><span class="fa fa-list"></span> Master Currency Rate List</a>
                                        </li>
                                    </ul>
                                    <!-- /.nav-third-level -->
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-user fa-fw"></i> Admin<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
                                
                                
                                <li>
                                    <a href="{{route('adduser_usr')}}"><span class="fa fa-users"></span> Create New User</a>
                                </li>
                                <li>
                                    <a href="{{route('index_usr')}}"><span class="fa fa-list"></span> User List</a>
                                </li>
                                
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <li>
                            <a href="{{route('index_rp')}}"><i class="fa fa-file-excel-o fa-fw"></i> Report</a>
                        </li>
                        @endif
                        <li>
                            <a href="{{route('logout')}}"><span class="fa fa-sign-out"></span> Logout</a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>