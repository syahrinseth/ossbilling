@extends('layouts.master_layout')
@section('title', 'Tooling & Consummables Edit')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tooling & Consummables Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('update_tc', ['tc_id'=>$tooling_consummable->id])}}" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Tooling & Consummables
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-sm-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" name="name" value="{{$tooling_consummable->name}}" placeholder="Tooling Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Description</label>
                                            <input class="form-control" name="description" value="{{$tooling_consummable->description}}" placeholder="Tooling Description" required>
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6 col-sm-6">
                                    <div class="form-group">
                                            <label>Job Number</label>
                                            <input list="job_no" name="job_no" class="form-control" value="{{$tooling_consummable->job_no}}" placeholder="Job Number" required>
                                            <datalist id="job_no">
                                                @if($master_jobs)
                                                    @foreach($master_jobs as $item)
                                                        <option value="{{$item->job_no}}">
                                                    @endforeach
                                                @endif
                                            </datalist>
                                        </div>
                                        <div class="form-group">
                                            <label>Cost (USD)</label>
                                            <input class="form-control" name="cost" placeholder="Tooling Cost USD Eg: 10.00" value="{{$tooling_consummable->cost}}" required>
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                            <div class="row">
                                <div class="col-lg-12">
                                        @include('layouts.validate')
                                    @include('layouts.flash_message')
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index_tc')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
@endsection