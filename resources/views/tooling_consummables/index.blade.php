@extends('layouts.master_layout')
@section('title', 'Tooling & Consummables')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Tooling & Consummables</h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
             @include('layouts.validate')
            <!-- /.row -->
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Local Timesheet List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Description</th>
                                        <th>Job Number</th>
                                        <th>currency</th>
                                        <th>Cost</th>
                                        <th>Updated</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                @if($tooling_consummables)
                                    @foreach($tooling_consummables as $item)
                                    <tr>
                                        <td>{{$item->name}}</td>
                                        <td>{{$item->description}}</td>
                                        <td>{{$item->job_no}}</td>
                                        <td>{{$item->currency}}</td>
                                        <td>{{$item->cost}}</td>
                                        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($item->updated_at))->diffForHumans()}}</td>
                                        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($item->created_at))->formatLocalized('%A %d %B %Y')}}</td>
                                        <td>
                                            <a href="{{route('edit_tc', ['tc_id'=>$item->id])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <button onclick="deleteFunction({{$item->id}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
            
            



                            <script src="/vendor/jquery/jquery.min.js"></script> 
                            <script>

                                function deleteFunction(tc_id) {

                                var r = confirm("Are you sure to delete?");
                                if (r == true) {
                                    window.location.href = "/admin/tooling-consummables/destroy/"+tc_id;
                                } else {
            
                                }
                            }

                            </script>
@endsection