@extends('layouts.master_layout')
@section('title', 'Master Job Edit')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Job</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('update_usr_mjb', [ 'job_no' => $job->job_no])}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit
                        </div>
                        <div class="panel-body">
                            @if($job)
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Job Number</label>
                                            <input class="form-control" name="job_no" value="{{$job->job_no}}" placeholder="Job No" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job Description</label>
                                            <textarea class="form-control" name="job_description" rows="3" required>{{$job->job_description}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input type="text" class="form-control" name="country" placeholder="Country" value="{{$job->country}}" required>
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Network Number</label>
                                            <input class="form-control" name="network_no" value="{{$job->network_no}}" placeholder="Network Number" required>
                                        </div>
                                        <div class="form-group">
                                            <label>PO#</label>
                                            <input class="form-control" name="po_no" value="{{$job->po_no}}" placeholder="PO#" required>
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                            </div>
                            <!-- /.row (nested) -->
                            @endif
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <div class="row">
                            
                                <div class="col-lg-12">
                                        
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index_usr_mjb')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                            </form>
                            
            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    jQuery('a#overseas_timesheet_add_row').click(function(){
                        $('tbody#timesheet-tbody').append('<tr id="overseas_expenses_row"><td><div class="form-group"><label>From</label><input type="date" name="date2_from[]" class="form-control" required><hr><label>To</label><input type="date" name="date2_to[]" class="form-control" required></div></td><td><div class="form-group"><label>Category</label><select name="category[]" class="form-control"><option value="">Select</option><option value="Airfare">Airfare</option><option value="Hotel">Hotel</option><option value="Laundry">Laundry</option><option value="Taxi">Taxi</option><option value="Mobile">Mobile</option><option value="Other">Other</option></select></div></td><td><div class="form-group"><label>Description</label><textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description[]" placeholder="Description" ></textarea></div></td><td id="timesheet-travel-col"><div class="form-group"><label>Currency</label><select name="currency[]" class="form-control"><option value="USD">United States Dollars</option><option value="EUR">Euro</option><option value="GBP">United Kingdom Pounds</option><option value="DZD">Algeria Dinars</option><option value="ARP">Argentina Pesos</option><option value="AUD">Australia Dollars</option><option value="ATS">Austria Schillings</option><option value="BSD">Bahamas Dollars</option><option value="BBD">Barbados Dollars</option><option value="BEF">Belgium Francs</option><option value="BMD">Bermuda Dollars</option><option value="BRR">Brazil Real</option><option value="BGL">Bulgaria Lev</option><option value="CAD">Canada Dollars</option><option value="CLP">Chile Pesos</option><option value="CNY">China Yuan Renmimbi</option><option value="CYP">Cyprus Pounds</option><option value="CSK">Czech Republic Koruna</option><option value="DKK">Denmark Kroner</option><option value="NLG">Dutch Guilders</option><option value="XCD">Eastern Caribbean Dollars</option><option value="EGP">Egypt Pounds</option><option value="FJD">Fiji Dollars</option><option value="FIM">Finland Markka</option><option value="FRF">France Francs</option><option value="DEM">Germany Deutsche Marks</option><option value="XAU">Gold Ounces</option><option value="GRD">Greece Drachmas</option><option value="HKD">Hong Kong Dollars</option><option value="HUF">Hungary Forint</option><option value="ISK">Iceland Krona</option><option value="INR">India Rupees</option><option value="IDR">Indonesia Rupiah</option><option value="IEP">Ireland Punt</option><option value="ILS">Israel New Shekels</option><option value="ITL">Italy Lira</option><option value="JMD">Jamaica Dollars</option><option value="JPY">Japan Yen</option><option value="JOD">Jordan Dinar</option><option value="KRW">Korea (South) Won</option><option value="LBP">Lebanon Pounds</option><option value="LUF">Luxembourg Francs</option><option value="MYR">Malaysia Ringgit</option><option value="MXP">Mexico Pesos</option><option value="NLG">Netherlands Guilders</option><option value="NZD">New Zealand Dollars</option><option value="NOK">Norway Kroner</option><option value="PKR">Pakistan Rupees</option><option value="XPD">Palladium Ounces</option><option value="PHP">Philippines Pesos</option><option value="XPT">Platinum Ounces</option><option value="PLZ">Poland Zloty</option><option value="PTE">Portugal Escudo</option><option value="ROL">Romania Leu</option><option value="RUR">Russia Rubles</option><option value="SAR">Saudi Arabia Riyal</option><option value="XAG">Silver Ounces</option><option value="SGD" selected="selected">Singapore Dollars</option><option value="SKK">Slovakia Koruna</option><option value="ZAR">South Africa Rand</option><option value="KRW">South Korea Won</option><option value="ESP">Spain Pesetas</option><option value="XDR">Special Drawing Right (IMF)</option><option value="SDD">Sudan Dinar</option><option value="SEK">Sweden Krona</option><option value="CHF">Switzerland Francs</option><option value="TWD">Taiwan Dollars</option><option value="THB">Thailand Baht</option><option value="TTD">Trinidad and Tobago Dollars</option><option value="TRL">Turkey Lira</option><option value="VEB">Venezuela Bolivar</option><option value="ZMK">Zambia Kwacha</option><option value="EUR">Euro</option><option value="XCD">Eastern Caribbean Dollars</option><option value="XDR">Special Drawing Right (IMF)</option><option value="XAG">Silver Ounces</option><option value="XAU">Gold Ounces</option><option value="XPD">Palladium Ounces</option><option value="XPT">Platinum Ounces</option></select></div></td><td><div class="form-check form-check-inline"><label class="form-check-label" for="payment_method">Cash</label><select class="form-control" name="payment_method[]" id="payment_method"><option value="0" checked="checked">Cash</option><option value="1" checked="checked">Amex Card</option></select></div></td><td><div class="form-group"><label>Expenses</label><input class="form-control" name="expenses[]" placeholder="$ 00.00" type="text" required></div></td><td><div class="form-group"><label>Attachment</label><input class="form-control" name="attachment[]" type="file"></div></td></tr>');
                    });
                    
                    // function for remove timesheet row
                    jQuery('a#overseas_timesheet_remove_row').click(function(){
                        $('#overseas_expenses_row').remove();
                    });


                });
            </script>
@endsection