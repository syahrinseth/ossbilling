@extends('layouts.master_layout')
@section('title', 'Master Job Index')
@section('content')
@if(Auth::user()->account_type == 1)
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Job</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Jobs List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Job Number</th>
                                        <th>Job Description</th>
                                        <th>Network Number</th>
                                        <th>PO#</th>
                                        <th>Country</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($jobs as $item)
                                    
                                    <tr>
                                        <td>
                                            {{$item->job_no}}
                                        </td>
                                        <td>{{$item->job_description}}</td>
                                        <td>{{$item->network_no}}</td>
                                        <td>{{$item->po_no}}</td>
                                        <td>{{$item->country}}</td>
                                        <td>
                                            <a href="{{route('edit_usr_mjb', [ 'job_no' => $item->job_no])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <button onclick="deleteFunction('{{$item->job_no}}')" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                        </td>
                                        
                                    </tr>
                                    
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->

                            <script>
                                function deleteFunction(job_no) {
                                
                                var r = confirm("Are you sure to delete job number "+job_no+" from your master job?");
                                if (r == true) {
                                    window.location.href = "/master-job/destroy/"+job_no;
                                } else {
            
                                }
                            }
                            </script>
@endif
@endsection