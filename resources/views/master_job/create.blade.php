@extends('layouts.master_layout')
@section('title', 'Master Job Create')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Master Job</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('store_usr_mjb')}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create
                        </div>
                        <div class="panel-body">
                            
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Job Number</label>
                                            <input class="form-control" name="job_no" value="" placeholder="Job No" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Job Description</label>
                                            <textarea class="form-control" name="job_description" rows="3" required></textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input type="text" class="form-control" name="country" placeholder="Country" required>
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>Network Number</label>
                                            <input class="form-control" name="network_no" value="" placeholder="Network Number" required>
                                        </div>
                                        <div class="form-group">
                                            <label>PO#</label>
                                            <input class="form-control" name="po_no" value="" placeholder="PO#" required>
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                
                            </div>
                            <!-- /.row (nested) -->
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <div class="row">
                            
                                <div class="col-lg-12">
                                        
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index_usr_mjb')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                            </form>
                            
           
@endsection