@extends('layouts.master_layout')
@section('title', 'Overseas Report')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Report</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('final_rp')}}" method="get">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Final Overseas Report
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                </div>
                            </div>
                            <div class="row">
                            <div class="col-lg-3 col-sm-3">
                                        
                                </div>
                                <div class="col-lg-6 col-sm-6">
                                        {{ csrf_field() }}
                                <div class="from-group">
                                    <label for="job_no">Job Number</label>
                                    <input type="text" list="job_no" name="job_no" class="form-control" required>
                                    @if($master_jobs)
                                    <datalist id="job_no">
                                    @foreach($master_jobs as $item)
                                    
                                        <option value="{{$item['job_no']}}">{{$item['job_no']}}</option>
                                    
                                    @endforeach
                                    </datalist>
                                    @endif
                                    <br>
                                    <label for="pdf">PDF</label>
                                    <input type="radio" id="pdf" name="file_type" value="pdf" checked>
                                    <label for="excel">Excel</label>
                                    <input type="radio" id="excel" name="file_type" value="excel">
                                    <br>
                                    <small>Note: Please select a PDF or Excel file format to download final report.</small>
                                </div>   
                                <br>  
                                @include('layouts.validate')
                                @include('layouts.flash_message')
                                <div class="from-group">
                                    <button type="submit" class="btn btn-primary">Download</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index_cr')}}" class="btn btn-default">Back</a>
                                        <br>
                                </div>
                                        
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-3 col-sm-3">
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                           
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
@endsection