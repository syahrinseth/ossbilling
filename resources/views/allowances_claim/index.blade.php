@extends('layouts.master_layout')
@section('title', 'Allowances Claim')
@section('content')

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Allowances Claim</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Allowances Claim List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Job Number</th>
                                        <th>SSO#</th>
                                        <th>Employee Name</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   @foreach($allowances as $allowance)
                                        <tr>
                                            <td>{{$allowance->job_no}}</td>
                                            <td>{{$allowance->sso_no}}</td>
                                            <td>{{$allowance->employee_name}}</td>
                                            <td>
                                                <form action="{{route('delete.acf', [ 'id' => $allowance->id ])}}" method="post" id="form-{{$allowance->id}}">
                                                    {{csrf_field()}}
                                                    </form>
                                                <a href="{{route('show.acf', ['id'=>$allowance->id])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                                    <!-- <button type="submit" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button> -->
                                                
                                                <a href="#" class="btn btn-danger delete-allowance-claim" id="delete-{{$allowance->id}}"><i class="fa fa-trash"></i></a>
                                            </td>
                                        </tr>
                                   @endforeach 
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->

 
@endsection
@section('script')
<script>
    function deleteFunction(job_no) {
                                
        var r = confirm("Are you sure to delete job number "+job_no+" from your master job?");
        if (r == true) {
            window.location.href = "/master-job/destroy/"+job_no;
        }
    }
    $(document).ready(function(){
        $(document).on('click', 'a.delete-allowance-claim', function(){
            var element_id = $(this).attr('id');
            var id = element_id.substring(element_id.indexOf("-") + 1);
            var dialog_box = confirm("Are you sure to delete this job number from allowances claim?");
            if (dialog_box == true) {
                $("form#form-"+id).submit();
            }
        });
    });
</script>
@endsection