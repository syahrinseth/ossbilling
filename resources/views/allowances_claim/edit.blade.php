@extends('layouts.master_layout')
@section('title', 'Allowance Claim Edit')
@section('style')
    <style>
        table > thead > tr > th, table > tbody > tr > td {
            text-align: center;
        }
    </style>
@endsection
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Allowance Claim Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('update.acf', ['id' => $allowance->id])}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Edit
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" name="job_no"placeholder="Job No" value="{{Auth::user()->name}}" disabled>
                                        </div>        
                                </div>
                                <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>SS0#</label>
                                            <input type="text" class="form-control" name="sso_no" placeholder="SSO#" value="{{Auth::user()->sso_no}}" disabled>
                                        </div>
                                </div>  
                                
                                
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12" style="overflow-x:auto;">
                                   

                                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-allowances">
                                        <thead class="text-center">
                                            <tr>
                                                
                                                <th></th>
                                                <th></th>
                                                <th colspan="2">Hours Worked</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <!-- <th></th> -->
                                                <th colspan="4">Meals Subsistence</th>
                                                <th colspan="2">Miscellaneous</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                
                                                <th>Day</th>
                                                <th>Date</th>
                                                <th>From:</th>
                                                <th>To:</th>
                                                <th>Job No:</th>
                                                <th>SSO#</th>
                                                <th>On Board Allowance<br>${{$claimPrices['OBA']}}</th>
                                                <th>ACTG Leadhand<br>${{$claimPrices['ACTGL']}}</th>
                                                <th>Workshop Night Allow<br>${{$claimPrices['WNA']}}</th>
                                                <th>Workshop Night Shift Allow<br>${{$claimPrices['WNSA']}}</th>
                                                <th>Workshop Transport Allow<br>${{$claimPrices['WTA']}}</th>
                                                <!-- <th>SUB <br>Total <br>(A) <br> $</th> -->
                                                <th>Breakfast <br>${{$claimPrices['BREAKFAST']}}</th>
                                                <th>Lunch <br>${{$claimPrices['LUNCH']}}</th>
                                                <th>Dinner <br>${{$claimPrices['DINNER']}}</th>
                                                <th>Midnight <br>${{$claimPrices['MIDNIGHT']}}</th>
                                                <th>Descrip </th>
                                                <th>$ </th>
                                                <th>SUB <br> Total <br> (A) <br> $ </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        
                                            <tr>
                                                <td>
                                                    {{$allowance['day']}}
                                                </td>
                                                <td>{{$allowance['date']}}</td>
                                                <td>
                                                    @if($allowance['work_start'] != null)
                                                        {{$allowance['work_start']}}
                                                    @elseif($allowance['work2_start'] != null)
                                                        {{$allowance['work2_start']}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($allowance['work2_finish'] != null)
                                                        {{$allowance['work2_finish']}}
                                                    @elseif($allowance['work_finish'] != null)
                                                        {{$allowance['work_finish']}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @php
                                                        $local_timesheet = App\local_timesheetModel::find($allowance['lt_id']);
                                                        echo $local_timesheet->job_no;
                                                    @endphp
                                                </td>
                                                <td>{{$allowance['sso_no']}}</td>
                                                <td>
                                                    <input class="form-check-input OBA" type="hidden" id="OBAHidden" value="0" name="OBA">
                                                    <input class="form-check-input OBA" type="checkbox" id="OBA" value="OBA" name="OBA" {{$allowance->on_board_allowance == 0 ? "" : "checked"}}>

                                                    @if($allowance->on_board_allowance != $claimPrices["OBA"] && $allowance->on_board_allowance != 0)
                                                        ${{$allowance->on_board_allowance}} <br>
                                                        <input class="form-check-input OBA" type="checkbox" id="OBA-NewPrice" value="OBA-NewPrice" name="OBA"> ${{$claimPrices["OBA"]}} <small>Check here to replace with new price</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="ACTGLHidden" value="0" name="ACTGL">
                                                    <input class="form-check-input" type="checkbox" id="ACTGL" name="ACTGL" value="ACTGL" {{$allowance->actg_leadhand == 0 ? "" : "checked='checked'"}}>

                                                    @if($allowance->actg_leadhand != $claimPrices["ACTGL"] && $allowance->actg_leadhand != 0)
                                                        ${{$allowance->actg_leadhand}} <br>
                                                        <input class="form-check-input ACTGL" type="checkbox" id="ACTGL-NewPrice" value="ACTGL-NewPrice" name="ACTGL"> ${{$claimPrices["ACTGL"]}} <small>Check here to replace with new price</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="WNAHidden" value="0" name="WNA">
                                                    <input class="form-check-input WorkNightAllowClass" type="checkbox" value="WNA" id="WNA" name="WNA" {{$allowance->workshop_night_allow == 0 ? "" : "checked='checked'"}}>

                                                    @if($allowance->workshop_night_allow != $claimPrices["WNA"] && $allowance->workshop_night_allow != 0)
                                                        ${{$allowance->workshop_night_allow}} <br>
                                                        <input class="form-check-input WNA" type="checkbox" id="WNA-NewPrice" value="WNA-NewPrice" name="WNA"> ${{$claimPrices["WNA"]}} <small>Check here to replace with new price</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="WNSAHidden" value="0" name="WNSA">
                                                    <input class="form-check-input WorkNightShiftAllowClass" type="checkbox" value="WNSA" id="WNSA" name="WNSA" {{$allowance->workshop_night_shift_allow == 0 ? "" : "checked='checked'"}}>

                                                    @if($allowance->workshop_night_shift_allow != $claimPrices["WNSA"] && $allowance->workshop_night_shift_allow != 0)
                                                        ${{$allowance->workshop_night_shift_allow}} <br>
                                                        <input class="form-check-input WNSA" type="checkbox" id="WNSA-NewPrice" value="WNSA-NewPrice" name="WNSA"> ${{$claimPrices["WNSA"]}} <small>Check here to replace with new price</small>
                                                    @endif
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="WTAHidden" value="0" name="WTA"> 
                                                    <input class="form-check-input" type="checkbox" id="WTA" name="WTA" value="WTA" {{$allowance->workshop_transport_allow == 0 ? "" : "checked='checked'"}}>

                                                    @if($allowance->workshop_transport_allow != $claimPrices["WTA"] && $allowance->workshop_transport_allow != 0)
                                                        ${{$allowance->workshop_transport_allow}} <br>
                                                        <input class="form-check-input WTA" type="checkbox" id="WTA-NewPrice" value="WTA-NewPrice" name="WTA"> ${{$claimPrices["WTA"]}} <small>Check here to replace with new price</small>
                                                    @endif
                                                </td>
                                                @if($allowance->on_board_allowance != 0)
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="BreakfastHidden" value="0" name="BREAKFAST">
                                                        <input class="form-check-input" type="checkbox" id="Breakfast" name="BREAKFAST" value="BREAKFAST" {{$allowance->breakfast == 0 ? "" : "checked='checked'"}}>

                                                        @if($allowance->breakfast != $claimPrices["BREAKFAST"] && $allowance->breakfast != 0)
                                                            ${{$allowance->breakfast}} <br>
                                                            <input class="form-check-input BREAKFAST" type="checkbox" id="BREAKFAST-NewPrice" value="BREAKFAST-NewPrice" name="BREAKFAST"> ${{$claimPrices["BREAKFAST"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="LunchHidden" value="0" name="LUNCH">
                                                        <input class="form-check-input" type="checkbox" id="Lunch" name="LUNCH" value="LUNCH" {{$allowance->lunch == 0 ? "" : "checked='checked'"}}>

                                                        @if($allowance->lunch != $claimPrices["LUNCH"] && $allowance->lunch != 0)
                                                            ${{$allowance->lunch}} <br>
                                                            <input class="form-check-input LUNCH" type="checkbox" id="LUNCH-NewPrice" value="LUNCH-NewPrice" name="LUNCH"> ${{$claimPrices["LUNCH"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="DinnerHidden" value="0" name="DINNER">
                                                        <input class="form-check-input" type="checkbox" id="Dinner" name="DINNER" value="DINNER" {{$allowance->dinner == 0 ? "" : "checked='checked'"}}>

                                                        @if($allowance->dinner != $claimPrices["DINNER"] && $allowance->dinner != 0)
                                                            ${{$allowance->dinner}} <br>
                                                            <input class="form-check-input DINNER" type="checkbox" id="DINNER-NewPrice" value="DINNER-NewPrice" name="DINNER"> ${{$claimPrices["DINNER"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="MidnightHidden" value="0" name="MIDNIGHT">
                                                        <input class="form-check-input" type="checkbox" id="Midnight" name="MIDNIGHT" value="MIDNIGHT" {{$allowance->midnight == 0 ? "" : "checked='checked'"}}>

                                                        @if($allowance->midnight != $claimPrices["MIDNIGHT"] && $allowance->midnight != 0)
                                                            ${{$allowance->midnight}} <br>
                                                            <input class="form-check-input MIDNIGHT" type="checkbox" id="MIDNIGHT-NewPrice" value="MIDNIGHT-NewPrice" name="MIDNIGHT"> ${{$claimPrices["MIDNIGHT"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                @else
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="BreakfastHidden" value="0" name="BREAKFAST">
                                                        <input class="form-check-input" type="checkbox" id="Breakfast" name="BREAKFAST" value="BREAKFAST" {{$allowance->breakfast == 0 ? "" : "checked='checked'"}} disabled>

                                                        @if($allowance->breakfast != $claimPrices["BREAKFAST"] && $allowance->breakfast != 0)
                                                            ${{$allowance->breakfast}} <br>
                                                            <input class="form-check-input BREAKFAST" type="checkbox" id="BREAKFAST-NewPrice" value="BREAKFAST-NewPrice" name="BREAKFAST"> ${{$claimPrices["BREAKFAST"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="LunchHidden" value="0" name="LUNCH">
                                                        <input class="form-check-input" type="checkbox" id="Lunch" name="LUNCH" value="LUNCH" {{$allowance->lunch == 0 ? "" : "checked='checked'"}} disabled>

                                                        @if($allowance->lunch != $claimPrices["LUNCH"] && $allowance->lunch != 0)
                                                            ${{$allowance->lunch}} <br>
                                                            <input class="form-check-input LUNCH" type="checkbox" id="LUNCH-NewPrice" value="LUNCH-NewPrice" name="LUNCH"> ${{$claimPrices["LUNCH"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="DinnerHidden" value="0" name="DINNER">
                                                        <input class="form-check-input" type="checkbox" id="Dinner" name="DINNER" value="DINNER" {{$allowance->dinner == 0 ? "" : "checked='checked'"}} disabled>

                                                        @if($allowance->dinner != $claimPrices["DINNER"] && $allowance->dinner != 0)
                                                            ${{$allowance->dinner}} <br>
                                                            <input class="form-check-input DINNER" type="checkbox" id="DINNER-NewPrice" value="DINNER-NewPrice" name="DINNER"> ${{$claimPrices["DINNER"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <input class="form-check-input" type="hidden" id="MidnightHidden" value="0" name="MIDNIGHT">
                                                        <input class="form-check-input" type="checkbox" id="Midnight" name="MIDNIGHT" value="MIDNIGHT" {{$allowance->midnight == 0 ? "" : "checked='checked'"}} disabled>

                                                        @if($allowance->midnight != $claimPrices["MIDNIGHT"] && $allowance->midnight != 0)
                                                            ${{$allowance->midnight}} <br>
                                                            <input class="form-check-input MIDNIGHT" type="checkbox" id="MIDNIGHT-NewPrice" value="MIDNIGHT-NewPrice" name="MIDNIGHT"> ${{$claimPrices["MIDNIGHT"]}} <small>Check here to replace with new price</small>
                                                        @endif
                                                    </td>
                                                @endif
                                                <td>
                                                    <input type="text" class="form-control text-center" value="{{$allowance->descrip}}" id="Descript" name="Descript">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="{{$allowance->dollar == null ? '0' : $allowance->dollar}}" id="Dollar" name="Dollar">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="{{$allowance->sub_total}}" name="SUBTotalA2" id="SUBTotalA2" disabled>
                                                </td>
                                            </tr>
                                            
                                        
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            
                            <div class="row">
                            
                                <div class="col-lg-12">
                                        
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('show.acf', ['id' => $allowance->lt_id])}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                            <div class="hidden-inputs">

                            </div>
                            </form>
                            
           
@endsection
@section('script')
<script>
    jQuery(document).ready(function(){
        // if user go back to the page, checked false.
        // $('*').prop("checked", false);
        var __OBA = {{$claimPrices['OBA']}};
        var __ACTGL = {{$claimPrices['ACTGL']}};
        var __WNA = {{$claimPrices['WNA']}};
        var __WNSA = {{$claimPrices['WNSA']}};
        var __WTA = {{$claimPrices['WTA']}};
        var __BREAKFAST = {{$claimPrices['BREAKFAST']}};
        var __LUNCH = {{$claimPrices['LUNCH']}};
        var __DINNER = {{$claimPrices['DINNER']}};
        var __MIDNIGHT = {{$claimPrices['MIDNIGHT']}};


        var _OBA = $('input#OBA').prop('checked');
        var _WNA = $('input#WNA').prop('checked');
        var _WNSA = $('input#WNSA').prop('checked');
        if(_OBA != true){
            $('input#Breakfast').prop("disabled", true);
            $('input#Lunch').prop("disabled", true);
            $('input#Dinner').prop("disabled", true);
            $('input#Midnight').prop("disabled", true);
        }
        if(_WNA == true){
            $('input#WNSA').prop("disabled", true);
        }else{
            $('input#WNSA').prop("disabled", false);
        }
        if(_WNSA == true){
            $('input#WNA').prop("disabled", true);
        }else{
            $('input#WNA').prop("disabled", false);
        }
        // Check button logic for On Board Allowance
        $(document).on('change', '.OBA', function(){
            if(this.checked){
                // Get checkbox id
                var checkbox_id = $(this).attr('id');
                // Change prop and remove attr
                $('input#Breakfast').prop("disabled", true);
                $('input#Breakfast').removeAttr("disabled");
                $('input#Lunch').prop("disabled", true);
                $('input#Lunch').removeAttr("disabled");
                $('input#Dinner').prop("disabled", true);
                $('input#Dinner').removeAttr("disabled");
                $('input#Midnight').prop("disabled", true);
                $('input#Midnight').removeAttr("disabled");
            }else{
                // Get checkbox id
                var checkbox_id = $(this).attr('id');
                // Uncheck box
                $('input#Breakfast').prop("checked", false);
                $('input#Lunch').prop("checked", false);
                $('input#Dinner').prop("checked", false);
                $('input#Midnight').prop("checked", false);
                // Remove attr
                $('input#Breakfast').prop("disabled", false);
                $('input#Breakfast').attr("disabled", 'disabled');
                $('input#Lunch').prop("disabled", false);
                $('input#Lunch').attr("disabled", 'disabled');
                $('input#Dinner').prop("disabled", false);
                $('input#Dinner').attr("disabled", 'disabled');
                $('input#Midnight').prop("disabled", false);
                $('input#Midnight').attr("disabled", 'disabled');
            }
        });
        // Checkbox logic for Work Night Allow
        $(document).on('change', '.WorkNightAllowClass', function(){
            if(this.checked){
                var checkbox_id = $(this).attr('id');
                $('input#WNSA').prop("disabled", false);
                $('input#WNSA').attr("disabled", 'disabled');
            }else{
                var checkbox_id = $(this).attr('id');
                $('input#WNSA').prop("disabled", true);
                $('input#WNSA').removeAttr("disabled");
            }
        });
        // Checkbox logic for Work Night Shift Allow
        $(document).on('change', '.WorkNightShiftAllowClass', function(){
            if(this.checked){
                var checkbox_id = $(this).attr('id');
                $('input#WNA').prop("disabled", false);
                $('input#WNA').attr("disabled", 'disabled');
            }else{
                var checkbox_id = $(this).attr('id');
                $('input#WNA').prop("disabled", true);
                $('input#WNA').removeAttr("disabled");
            }
        });
        // Logic for SUB Total A Part 1,2
        $(document).on('change', 'input.form-check-input', function(){
            // Get id
            var checkbox_id = $(this).attr('id');
            // get checked prop
            var OBA = $('input#OBA').prop('checked');
            var ACTGL = $('input#ACTGL').prop('checked');
            var WNA = $('input#WNA').prop('checked');
            var WNSA = $('input#WNSA').prop('checked');
            var WTA = $('input#WTA').prop('checked');
            var Breakfast = $('input#Breakfast').prop('checked');
            var Lunch = $('input#Lunch').prop('checked');
            var Dinner = $('input#Dinner').prop('checked');
            var Midnight = $('input#Midnight').prop('checked');
            // check if checkbox is checked 
            var subTotal = 0.00;
            var subTotal2 = 0.00;
            if(OBA == true){
                subTotal += __OBA;
                $('input#OBAHidden').attr('disabled', 'disabled');
            }else{
                    $('input#OBAHidden').removeAttr('disabled');
            }
            if(ACTGL == true){
                subTotal += __ACTGL;
                $('input#ACTGLHidden').attr('disabled', 'disabled');
            }else{
                $('input#ACTGLHidden').removeAttr('disabled');
            }
            if(WNA == true){
                subTotal += __WNA;
                $('input#WNAHidden').attr('disabled', 'disabled');
            }else{
                $('input#WNAHidden').removeAttr('disabled');
            }
            if(WNSA == true){
                subTotal += __WNSA;
                $('input#WNSAHidden').attr('disabled', 'disabled');
            }else{
                $('input#WNSAHidden').removeAttr('disabled');
            }
            if(WTA == true){
                subTotal += __WTA;
                $('input#WTAHidden').attr('disabled', 'disabled');
            }else{
                $('input#WTAHidden').removeAttr('disabled');
            }
            if(Breakfast == true){
                subTotal2 += __BREAKFAST;
                $('input#BreakfastHidden').attr('disabled', 'disabled');
            }else{
                $('input#BreakfastHidden').removeAttr('disabled');
            }
            if(Lunch == true){
                subTotal2 += __LUNCH;
                $('input#LunchHidden').attr('disabled', 'disabled');
            }else{
                $('input#LunchHidden').removeAttr('disabled');
            }
            if(Dinner == true){
                subTotal2 += __DINNER;
                $('input#DinnerHidden').attr('disabled', 'disabled');
            }else{
                $('input#DinnerHidden').removeAttr('disabled');
            }
            if(Midnight == true){
                subTotal2 += __MIDNIGHT;
                $('input#MidnightHidden').attr('disabled', 'disabled');
            }else{
                $('input#MidnightHidden').removeAttr('disabled');
            }
            // Output total in to input
            $('input#SUBTotalA').attr('value', '$'+subTotal);
            var grandTotal2 = subTotal + subTotal2;
            $('input#SUBTotalA2').attr('value', '$'+grandTotal2);
        });
        // Logic for hidden input
        // $('input.form-check-input').change(function(){
        //     var checkbox_id = $(this).attr('id');
        //     var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
        //     var OBA = $('input#OBA').prop('checked');
        //     var ACTGL = $('input#ACTGL').prop('checked');
        //     var WNA = $('input#WNA').prop('checked');
        //     var WNSA = $('input#WNSA').prop('checked');
        //     var WTA = $('input#WTA').prop('checked');
        //     var Breakfast = $('input#Breakfast').prop('checked');
        //     var Lunch = $('input#Lunch').prop('checked');
        //     var Dinner = $('input#Dinner').prop('checked');
        //     var Midnight = $('input#Midnight').prop('checked');
        // });
    });
</script>
@endsection