@extends('layouts.master_layout')
@section('title', 'Allowances Claim List')
@section('style')
    <style>
        table > thead > tr > th, table > tbody > tr > td {
            text-align: center;
        }
    </style>
@endsection
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Allowances Claim List</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
            
            <!-- /.row -->
            <div class="row">
                <!-- <form role="form" action="" method="post"> -->
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Show
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12" style="overflow-x:auto;">
                                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-allowances">
                                        <thead class="text-center">
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th colspan="2">Hours Worked</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <!-- <th></th> -->
                                                <th colspan="4">Meals Subsistence</th>
                                                <th colspan="2">Miscellaneous</th>
                                            </tr>
                                            <tr>
                                                <td></td>
                                                <th>Day</th>
                                                <th>Date</th>
                                                <th>From:</th>
                                                <th>To:</th>
                                                <th>Job No:</th>
                                                <th>SSO#</th>
                                                <th>On Board Allowance<br>${{$claimPrices['OBA']}}</th>
                                                <th>ACTG Leadhand<br>${{$claimPrices['ACTGL']}}</th>
                                                <th>Workshop Night Allow<br>${{$claimPrices['WNA']}}</th>
                                                <th>Workshop Night Shift Allow<br>${{$claimPrices['WNSA']}}</th>
                                                <th>Workshop Transport Allow<br>${{$claimPrices['WTA']}}</th>
                                                <!-- <th>SUB <br>Total <br>(A) <br> $</th> -->
                                                <th>Breakfast <br>${{$claimPrices['BREAKFAST']}}</th>
                                                <th>Lunch <br>${{$claimPrices['LUNCH']}}</th>
                                                <th>Dinner <br>${{$claimPrices['DINNER']}}</th>
                                                <th>Midnight <br>${{$claimPrices['MIDNIGHT']}}</th>
                                                <th>Descrip </th>
                                                <th>$ </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                        @php
                                        $claimPrices = App\AllowancesClaim::getclaimPrices();
                                        $total_oba = null;
                                        $total_actgl = null;
                                        $total_wna = null;
                                        $total_wnsa = null;
                                        $total_wta = null;
                                        $total_breakfast = null;
                                        $total_lunch = null;
                                        $total_dinner = null;
                                        $total_midnight = null;
                                        @endphp    
                                        @for($i = 0; $i < count($allowances);$i++)
                                            <tr>
                                                <td>
                                                    <a href="{{route('edit.acf', ['id' => $allowances[$i]['id']])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                                    <button onclick="deleteClaim({{$allowances[$i]['id']}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                                </td>
                                                <td>
                                                    {{$allowances[$i]['day']}}
                                                </td>
                                                <td>{{$allowances[$i]['date']}}</td>
                                                <td>
                                                    @if($allowances[$i]['work_start'] != null)
                                                        {{$allowances[$i]['work_start']}}
                                                    @elseif($allowances[$i]['work2_start'] != null)
                                                        {{$allowances[$i]['work2_start']}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($allowances[$i]['work2_finish'] != null)
                                                        {{$allowances[$i]['work2_finish']}}
                                                    @elseif($allowances[$i]['work_finish'] != null)
                                                        {{$allowances[$i]['work_finish']}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @php
                                                        $local_timesheet = App\local_timesheetModel::find($allowances[$i]['lt_id']);
                                                        echo $local_timesheet->job_no;
                                                    @endphp
                                                </td>
                                                <td>{{$allowances[$i]['sso_no']}}</td>
                                                <td>
                                                    <input class="form-check-input OBA" type="checkbox" id="OBA-{{$i}}" value="1" name="OBA[]" {{$allowances[$i]->on_board_allowance == 0 ? "" : "checked='checked'"}} disabled> {{$allowances[$i]->on_board_allowance == $claimPrices['OBA'] || $allowances[$i]->on_board_allowance == 0 ? "" : $allowances[$i]->on_board_allowance}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="checkbox" id="ACTGL-{{$i}}" name="ACTGL[]" value="1" {{$allowances[$i]->actg_leadhand == 0 ? "" : "checked='checked'"}} disabled> 
                                                    {{$allowances[$i]->actg_leadhand == $claimPrices['ACTGL'] || $allowances[$i]->actg_leadhand == 0 ? "" : $allowances[$i]->actg_leadhand}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input WorkNightAllowClass" type="checkbox" value="1" id="WNA-{{$i}}" name="WNA[]" {{$allowances[$i]->workshop_night_allow == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->workshop_night_allow == $claimPrices['WNA'] || $allowances[$i]->workshop_night_allow == 0 ? "" : $allowances[$i]->workshop_night_allow}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input WorkNightShiftAllowClass" type="checkbox" value="1" id="WNSA-{{$i}}" name="WNSA[]" {{$allowances[$i]->workshop_night_shift_allow == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->workshop_night_shift_allow == $claimPrices['WNSA'] || $allowances[$i]->workshop_night_shift_allow == 0 ? "" : $allowances[$i]->workshop_night_shift_allow}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="checkbox" id="WTA-{{$i}}" name="WTA[]" value="1" {{$allowances[$i]->workshop_transport_allow == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->workshop_transport_allow == $claimPrices['WTA'] || $allowances[$i]->workshop_transport_allow == 0 ? "" : $allowances[$i]->workshop_transport_allow}}
                                                </td>
                                                <!-- <td>
                                                    <input type="text" class="form-control text-center"id="SUBTotalA-{{$i}}" name="SUBTotalA" disabled>
                                                </td> -->
                                                <td>
                                                    <input class="form-check-input" type="checkbox" id="Breakfast-{{$i}}" name="Breakfast[]" value="1" {{$allowances[$i]->breakfast == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->breakfast == $claimPrices['BREAKFAST'] || $allowances[$i]->breakfast == 0 ? "" : $allowances[$i]->breakfast}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="checkbox" id="Lunch-{{$i}}" name="Lunch[]" value="1" {{$allowances[$i]->lunch == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->lunch == $claimPrices['LUNCH'] || $allowances[$i]->lunch == 0 ? "" : $allowances[$i]->lunch}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="checkbox" id="Dinner-{{$i}}" name="Dinner[]" value="1" {{$allowances[$i]->dinner == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->dinner == $claimPrices['DINNER'] || $allowances[$i]->dinner == 0 ? "" : $allowances[$i]->dinner}}
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="checkbox" id="Midnight-{{$i}}" name="Midnight[]" value="1" {{$allowances[$i]->midnight == 0 ? "" : "checked='checked'"}} disabled>
                                                    {{$allowances[$i]->midnight == $claimPrices['MIDNIGHT'] || $allowances[$i]->midnight == 0 ? "" : $allowances[$i]->midnight}}
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="{{$allowances[$i]->descrip}}" id="Descript-{{$i}}" name="Descript[]" disabled>
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="{{$allowances[$i]->dollar == null ? '0' : $allowances[$i]->dollar}}" id="Dollar-{{$i}}" name="Dollar[]" disabled>
                                                </td>
                                            </tr>
                                            @php
                                                $total_oba += $allowances[$i]->on_board_allowance;
                                                $total_actgl += $allowances[$i]->actg_leadhand;
                                                $total_wna += $allowances[$i]->workshop_night_allow;
                                                $total_wnsa += $allowances[$i]->workshop_night_shift_allow;
                                                $total_wta += $allowances[$i]->workshop_transport_allow;
                                                $total_breakfast += $allowances[$i]->breakfast;
                                                $total_lunch += $allowances[$i]->lunch;
                                                $total_dinner += $allowances[$i]->dinner;
                                                $total_midnight += $allowances[$i]->midnight;
                                            @endphp
                                        @endfor
                                        </tbody>
                                        <thead class="text-center">
                                            <tr>
                                                <td colspan="7"></td>
                                                <th>Subtotal:<br>$ {{$total_oba}}</th>
                                                <th>Subtotal:<br>$ {{$total_actgl}}</th>
                                                <th>Subtotal:<br>$ {{$total_wna}}</th>
                                                <th>Subtotal:<br>$ {{$total_wnsa}}</th>
                                                <th>Subtotal:<br>$ {{$total_wta}}</th>
                                                <!-- <th>SUB <br>Total <br>(A) <br> $</th> -->
                                                <th colspan="4">Subtotal:<br>$ {{ $total_breakfast + $total_lunch + $total_dinner + $total_midnight }}</th>
                                                <th colspan="2"></th>
                                            </tr>
                                        </thead>
                                    </table>
                                </div>
                            </div>

                            
                            <div class="row">
                            
                                <div class="col-lg-12">
                                        
                                        <!-- <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button> -->
                                        <a href="{{route('index.acf')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                            <!-- </form> -->
                            
           
@endsection
@section('script')
    <script>
        function deleteClaim(claimId){
            // window.location.href = "/allowances-claim/delete-claim/" + claimId;
            // conferm logic
            var ltId = {{count($allowances) > 0 ? $allowances[0]->lt_id : null}};
            var r = confirm("Are you sure to delete this claim?");
            if (r == true) {
                
                
                jQuery.ajax({
                    url: "/allowances-claim/delete-claim/" + claimId,
                    method: 'post',
                    dataType: 'json',
                    data : { _token: "{{csrf_token()}}" },
                    success: function(result){
                        console.log(result);
                        var jsonStr = JSON.stringify(result);
                        var obj = JSON.parse(jsonStr);
                        if(obj.data == true){
                            window.location.href = "/allowances-claim/show/" + ltId;
                        }else{
                            alert("Delete Failed!");
                        }
                        
                        
                            
                    },
                    error: function(data) {
                        var errors = data.responseJSON;
                        console.log(errors);
                    }
                });
            } else {
                        
            }

            
        }
    </script>
@endsection