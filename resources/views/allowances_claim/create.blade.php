@extends('layouts.master_layout')
@section('title', 'Allowances Claim Form')
@section('content')
<style>
    table > thead > tr > th, table > tbody > tr > td {
        text-align: center;
    }
</style>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Allowances Claim Form</h1>
                </div>
                <!-- /.col-lg-12 -->
</div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('store.acf')}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Name</label>
                                            <input class="form-control" name="job_no"placeholder="Job No" value="{{Auth::user()->name}}" disabled>
                                        </div>        
                                </div>
                                <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>SS0#</label>
                                            <input type="text" class="form-control" name="sso_no" placeholder="SSO#" value="{{Auth::user()->sso_no}}" disabled>
                                        </div>
                                </div>  
                                
                                
                            </div>
                            <!-- /.row -->
                            <div class="row">
                                <div class="col-lg-12" style="overflow-x:auto;">
                                    <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-allowances">
                                        <thead class="text-center">
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th colspan="2">Hours Worked</th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <th colspan="4">Meals Subsistence</th>
                                                <th colspan="2">Miscellaneous</th>
                                                <th></th>
                                            </tr>
                                            <tr>
                                                <th>Day</th>
                                                <th>Date</th>
                                                <th>From:</th>
                                                <th>To:</th>
                                                <th>Job No:</th>
                                                <th>On Board Allowance<br>${{$claimPrices['OBA']}}</th>
                                                <th>ACTG Leadhand<br>${{$claimPrices['ACTGL']}}</th>
                                                <th>Workshop Night Allow<br>${{$claimPrices['WNA']}}</th>
                                                <th>Workshop Night Shift Allow<br>${{$claimPrices['WNSA']}}</th>
                                                <th>Workshop Transport Allow<br>${{$claimPrices['WTA']}}</th>
                                                <th>SUB <br>Total <br>(A) <br> $</th>
                                                <th>Breakfast <br>${{$claimPrices['BREAKFAST']}}</th>
                                                <th>Lunch <br>${{$claimPrices['LUNCH']}}</th>
                                                <th>Dinner <br>${{$claimPrices['DINNER']}}</th>
                                                <th>Midnight <br>${{$claimPrices['MIDNIGHT']}}</th>
                                                <th>Descrip </th>
                                                <th>$ </th>
                                                <th>SUB <br> Total <br> (A) <br> $ </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            
                                        @for($i = 0; $i < count($allowances);$i++)
                                            <tr>
                                                <td>
                                                    {{$allowances[$i]['day']}}
                                                    <input type="hidden" name="ltd_id[]" value="{{$allowances[$i]->id}}">
                                                </td>
                                                <td>{{$allowances[$i]['date']}}</td>
                                                <td>
                                                    @if($allowances[$i]['work_start'] != null)
                                                        {{$allowances[$i]['work_start']}}
                                                    @elseif($allowances[$i]['work2_start'] != null)
                                                        {{$allowances[$i]['work2_start']}}
                                                    @endif
                                                </td>
                                                <td>
                                                    @if($allowances[$i]['work2_finish'] != null)
                                                        {{$allowances[$i]['work2_finish']}}
                                                    @elseif($allowances[$i]['work_finish'] != null)
                                                        {{$allowances[$i]['work_finish']}}
                                                    @endif
                                                </td>
                                                <td>{{$allowances[$i]['job_no']}}</td>
                                                <td>
                                                    <input class="form-check-input OBA" type="hidden" id="OBAHidden-{{$i}}" value="0" name="OBA[]">
                                                    <input class="form-check-input OBA" type="checkbox" id="OBA-{{$i}}" value="OBA" name="OBA[]">
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="ACTGLHidden-{{$i}}" value="0" name="ACTGL[]">
                                                    <input class="form-check-input" type="checkbox" id="ACTGL-{{$i}}" name="ACTGL[]" value="ACTGL">
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="WNAHidden-{{$i}}" value="0" name="WNA[]">
                                                    <input class="form-check-input WorkNightAllowClass" type="checkbox" value="WNA" id="WNA-{{$i}}" name="WNA[]">
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="WNSAHidden-{{$i}}" value="0" name="WNSA[]">
                                                    <input class="form-check-input WorkNightShiftAllowClass" type="checkbox" value="WNSA" id="WNSA-{{$i}}" name="WNSA[]">
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="WTAHidden-{{$i}}" value="0" name="WTA[]">  
                                                    <input class="form-check-input" type="checkbox" id="WTA-{{$i}}" name="WTA[]" value="WTA">
                                                      
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="SUBTotalAHidden-{{$i}}" value="0" name="SUBTotalA[]">
                                                    <input type="text" class="form-control text-center"id="SUBTotalA-{{$i}}" name="SUBTotalA" disabled>
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="BreakfastHidden-{{$i}}" value="0" name="BREAKFAST[]">
                                                    <input class="form-check-input" type="checkbox" id="Breakfast-{{$i}}" name="BREAKFAST[]" value="BREAKFAST" disabled>
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="LunchHidden-{{$i}}" value="0" name="LUNCH[]">
                                                    <input class="form-check-input" type="checkbox" id="Lunch-{{$i}}" name="LUNCH[]" value="LUNCH" disabled>
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="DinnerHidden-{{$i}}" value="0" name="DINNER[]">
                                                    <input class="form-check-input" type="checkbox" id="Dinner-{{$i}}" name="DINNER[]" value="DINNER" disabled>
                                                    
                                                </td>
                                                <td>
                                                    <input class="form-check-input" type="hidden" id="MidnightHidden-{{$i}}" value="0" name="MIDNIGHT[]">
                                                    <input class="form-check-input" type="checkbox" id="Midnight-{{$i}}" name="MIDNIGHT[]" value="MIDNIGHT" disabled>
                                                    
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="" id="Descript-{{$i}}" name="Descript[]">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="0" id="Dollar-{{$i}}" name="Dollar[]">
                                                </td>
                                                <td>
                                                    <input type="text" class="form-control text-center" value="" id="SUBTotalA2-{{$i}}" name="SUBTotalA2[]" readonly>
                                                </td>
                                            </tr>
                                            
                                        @endfor
                                        </tbody>
                                    </table>
                                </div>
                            </div>

                            
                            <div class="row">
                            
                                <div class="col-lg-12">
                                        
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index.acf')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                            <div class="hidden-inputs">

                            </div>
                            </form>
                            
           
@endsection
@section('script')
<script>
    jQuery(document).ready(function(){
        // if user go back to the page, checked false.
        $('*').prop("checked", false);
        // declare claimPrices to js variable from server
        var __OBA = {{$claimPrices['OBA']}};
        var __ACTGL = {{$claimPrices['ACTGL']}};
        var __WNA = {{$claimPrices['WNA']}};
        var __WNSA = {{$claimPrices['WNSA']}};
        var __WTA = {{$claimPrices['WTA']}};
        var __BREAKFAST = {{$claimPrices['BREAKFAST']}};
        var __LUNCH = {{$claimPrices['LUNCH']}};
        var __DINNER = {{$claimPrices['DINNER']}};
        var __MIDNIGHT = {{$claimPrices['MIDNIGHT']}};

        // Check button logic for On Board Allowance
        $(document).on('change', '.OBA', function(){
            if(this.checked){
                // Get checkbox id
                var checkbox_id = $(this).attr('id');
                var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                // Change prop and remove attr
                $('input#Breakfast-'+id).prop("disabled", true);
                $('input#Breakfast-'+id).removeAttr("disabled");
                $('input#Lunch-'+id).prop("disabled", true);
                $('input#Lunch-'+id).removeAttr("disabled");
                $('input#Dinner-'+id).prop("disabled", true);
                $('input#Dinner-'+id).removeAttr("disabled");
                $('input#Midnight-'+id).prop("disabled", true);
                $('input#Midnight-'+id).removeAttr("disabled");
            }else{
                // Get checkbox id
                var checkbox_id = $(this).attr('id');
                var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                // Uncheck box
                $('input#Breakfast-'+id).prop("checked", false);
                $('input#Lunch-'+id).prop("checked", false);
                $('input#Dinner-'+id).prop("checked", false);
                $('input#Midnight-'+id).prop("checked", false);
                // Remove attr
                $('input#Breakfast-'+id).prop("disabled", false);
                $('input#Breakfast-'+id).attr("disabled", 'disabled');
                $('input#Lunch-'+id).prop("disabled", false);
                $('input#Lunch-'+id).attr("disabled", 'disabled');
                $('input#Dinner-'+id).prop("disabled", false);
                $('input#Dinner-'+id).attr("disabled", 'disabled');
                $('input#Midnight-'+id).prop("disabled", false);
                $('input#Midnight-'+id).attr("disabled", 'disabled');
            }
        });
        // Checkbox logic for Work Night Allow
        $(document).on('change', '.WorkNightAllowClass', function(){
            if(this.checked){
                var checkbox_id = $(this).attr('id');
                var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                $('input#WNSA-'+id).prop("disabled", false);
                $('input#WNSA-'+id).attr("disabled", 'disabled');
            }else{
                var checkbox_id = $(this).attr('id');
                var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                $('input#WNSA-'+id).prop("disabled", true);
                $('input#WNSA-'+id).removeAttr("disabled");
            }
        });
        // Checkbox logic for Work Night Shift Allow
        $(document).on('change', '.WorkNightShiftAllowClass', function(){
            if(this.checked){
                var checkbox_id = $(this).attr('id');
                var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                $('input#WNA-'+id).prop("disabled", false);
                $('input#WNA-'+id).attr("disabled", 'disabled');
            }else{
                var checkbox_id = $(this).attr('id');
                var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
                $('input#WNA-'+id).prop("disabled", true);
                $('input#WNA-'+id).removeAttr("disabled");
            }
        });
        // Logic for SUB Total A Part 1,2
        $(document).on('change', 'input.form-check-input', function(){
            // Get id
            var checkbox_id = $(this).attr('id');
            var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
            // get checked prop
            var OBA = $('input#OBA-'+id).prop('checked');
            var ACTGL = $('input#ACTGL-'+id).prop('checked');
            var WNA = $('input#WNA-'+id).prop('checked');
            var WNSA = $('input#WNSA-'+id).prop('checked');
            var WTA = $('input#WTA-'+id).prop('checked');
            var Breakfast = $('input#Breakfast-'+id).prop('checked');
            var Lunch = $('input#Lunch-'+id).prop('checked');
            var Dinner = $('input#Dinner-'+id).prop('checked');
            var Midnight = $('input#Midnight-'+id).prop('checked');
            // check if checkbox is checked 
            var subTotal = 0.00;
            var subTotal2 = 0.00;
            if(OBA == true){
                subTotal += __OBA;
                $('input#OBAHidden-'+id).attr('disabled', 'disabled');
            }else{
                    $('input#OBAHidden-'+id).removeAttr('disabled');
            }
            if(ACTGL == true){
                subTotal += __ACTGL;
                $('input#ACTGLHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#ACTGLHidden-'+id).removeAttr('disabled');
            }
            if(WNA == true){
                subTotal += __WNA;
                $('input#WNAHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#WNAHidden-'+id).removeAttr('disabled');
            }
            if(WNSA == true){
                subTotal += __WNSA;
                $('input#WNSAHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#WNSAHidden-'+id).removeAttr('disabled');
            }
            if(WTA == true){
                subTotal += __WTA;
                $('input#WTAHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#WTAHidden-'+id).removeAttr('disabled');
            }
            if(Breakfast == true){
                subTotal2 += __BREAKFAST;
                $('input#BreakfastHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#BreakfastHidden-'+id).removeAttr('disabled');
            }
            if(Lunch == true){
                subTotal2 += __LUNCH;
                $('input#LunchHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#LunchHidden-'+id).removeAttr('disabled');
            }
            if(Dinner == true){
                subTotal2 += __DINNER;
                $('input#DinnerHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#DinnerHidden-'+id).removeAttr('disabled');
            }
            if(Midnight == true){
                subTotal2 += __MIDNIGHT;
                $('input#MidnightHidden-'+id).attr('disabled', 'disabled');
            }else{
                $('input#MidnightHidden-'+id).removeAttr('disabled');
            }
            // Output total in to input
            $('input#SUBTotalA-'+id).attr('value', '$'+subTotal);
            var grandTotal2 = subTotal + subTotal2;
            $('input#SUBTotalA2-'+id).attr('value', '$'+grandTotal2);
        });
        // Logic for hidden input
        // $('input.form-check-input').change(function(){
        //     var checkbox_id = $(this).attr('id');
        //     var id = checkbox_id.substring(checkbox_id.indexOf("-") + 1);
        //     var OBA = $('input#OBA-'+id).prop('checked');
        //     var ACTGL = $('input#ACTGL-'+id).prop('checked');
        //     var WNA = $('input#WNA-'+id).prop('checked');
        //     var WNSA = $('input#WNSA-'+id).prop('checked');
        //     var WTA = $('input#WTA-'+id).prop('checked');
        //     var Breakfast = $('input#Breakfast-'+id).prop('checked');
        //     var Lunch = $('input#Lunch-'+id).prop('checked');
        //     var Dinner = $('input#Dinner-'+id).prop('checked');
        //     var Midnight = $('input#Midnight-'+id).prop('checked');
        // });
    });
</script>
@endsection