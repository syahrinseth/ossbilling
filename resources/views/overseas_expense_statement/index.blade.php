@extends('layouts.master_layout')
@section('title', 'Overseas Expense Statement')
@section('content')
<!-- Modal -->
<div class="modal fade" id="custSign" tabindex="-1" role="dialog" aria-labelledby="custSignModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
    
      <div class="modal-header">
        <h3 class="modal-title" id="custSignModalLabel">Customer Signature Attachment</h3>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="cust_form" action="" method="POST" enctype="multipart/form-data">
            {{csrf_field()}}
            <div class="form-group">
                <label>Customer Name</label>
                <input type="text" value class="form-control" name="customer_signature_name">
            </div>
                <div class="form-group">
                    <label>Customer Signature Attachment</label>
                    <input type="file" class="form-control" name="customer_name" required>
                </div>
        </form>
        
        
      </div>
      <div class="modal-footer">
        <button class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="submit" class="btn btn-primary" id="submitForm">Upload</button>
      </div>
    
    </div>
  </div>
</div>

<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Expense Statement</h1>
                </div>
                <!-- /.col-lg-12 -->
               
            </div>
             @include('layouts.validate')
            <!-- /.row -->
            @if(Auth::user()->account_type == 1)
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Employee Overseas Expense Statement List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Job Number</th>
                                        <th>Date from</th>
                                        <th>Date to</th>
                                        <th>Status</th>
                                        <th>Customer/Department Signature</th>
                                        <th>Updated</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($oes_collections as $collection)
                                    @if($collection->sso_no)
                                    <tr>
                                        <td>
                                            {{$collection->name}}
                                        </td>
                                        <td>
                                            {{$collection->job_no}}
                                        </td>
                                        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->date_from))->formatLocalized('%A %d %B %Y')}}</td>
                                        <td>{{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->date_to))->formatLocalized('%A %d %B %Y')}}</td>
                                        <td>
                                            @if($collection->status == 0)
                                                <p class="text-warning">Waiting For Approval</p>
                                            @else
                                                <p class="text-success">Approve</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($collection->customer_signature_attachment != 'no-file.png')
                                                <p class="text-success">Cust Sign: Yes</p>
                                            @else
                                                <p class="text-warning">Cust Sign: N/a</p>
                                            @endif
                                            <hr>
                                            @if($collection->dept_signature_name)
                                                <p class="text-success">Dept Sign: Yes</p>
                                            @else
                                                <p class="text-warning">Dept Sign: N/a</p>
                                            @endif
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->updated_at))->diffForHumans()}}
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->created_at))->formatLocalized('%d %B %Y')}}
                                        </td>
                                        <td>
                                            @if($collection->sso_no == Auth::user()->sso_no)
                                            <a href="{{route('show_oes', [ 'oes_id' => $collection->id ])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('edit_oes', [ 'id' => $collection->id ])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('print_oes', ['oes_id' => $collection->id])}}" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                            <button onclick="deleteFunction({{$collection->id}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                            @else
                                            <a href="{{route('show_oes', [ 'oes_id' => $collection->id ])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('print_oes', ['oes_id' => $collection->id])}}" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                            @endif
                                            <form method="POST" id="delete-{{$collection['id']}}" action="{{route('destroy_oes', ['oes_id'=>$collection['id']])}}">
                                                {{csrf_field()}}
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
            @elseif(Auth::user()->account_type == 0)
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Employee Overseas Expense Statement List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Job Number</th>
                                        <th>Date from</th>
                                        <th>Date to</th>
                                        <th>Status</th>
                                        <th>Updated</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($oes_collections as $collection)
                                    @if($collection->sso_no == Auth::user()->sso_no)
                                    <tr>
                                        <td>
                                            {{$collection->job_no}}
                                        </td>
                                        <td>{{$collection->date_from}}</td>
                                        <td>{{$collection->date_to}}</td>
                                        <td>
                                            @if($collection->status == 0)
                                                <p class="text-warning">Waiting For Approval</p>
                                            @endif
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->updated_at))->diffForHumans()}}
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->created_at))->formatLocalized('%A %d %B %Y')}}
                                        </td>
                                        <td>
                                            <a href="{{route('show_oes', [ 'oes_id' => $collection->id ])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('edit_oes', [ 'id' => $collection->id ])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('print_oes', ['oes_id' => $collection->id])}}" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                            <button onclick="deleteFunction({{$collection->id}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                            <form method="POST" id="delete-{{$collection['id']}}" action="{{route('destroy_oes', ['oes_id'=>$collection['id']])}}">
                                                {{csrf_field()}}
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                    
            </div>
            <!-- /.row -->
            <!-- Button trigger modal -->
            
            @endif

                            <script src="/vendor/jquery/jquery.min.js"></script> 
                            <script>
                                jQuery('button#custSign').click(function(){
                                    var oes_id = $(this).attr('data-id');
                                    $('form#cust_form').attr('action', '/overseas-expense-statement/customer_signature/'+oes_id);
                                });

                                $("#submitForm").on('click', function() {
                                    $("#cust_form").submit();
                                });


                                function deleteFunction(oes_id) {

                                var r = confirm("Are you sure to delete this job number from your overseas expense statement?");
                                if (r == true) {
                                    // window.location.href = "/overseas-expense-statement/destroy/"+oes_id;
                                    document.getElementById('delete-'+oes_id).submit();
                                } else {
            
                                }
                            }
                            </script>
@endsection