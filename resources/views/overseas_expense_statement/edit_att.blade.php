@extends('layouts.master_layout')
@section('title', 'Overseas Expense Statement Edit Expenses')
@section('content')
<form role="form" action="{{route('update_oes_att', ['oes_id' => $oes_att->oes_id, 'oes_att_id' => $oes_att->id])}}" method="post" enctype="multipart/form-data">
{{csrf_field()}}
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Expense Statement</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Overseas Expense Statement
                        </div>
                        <div class="panel-body">
                            
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Overseas Expenses</h1>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover">
                                                <thead>
                                                    <tr>
                                                        <th>Date(s)</th>
                                                        <th>Category</th>
                                                        <th>Description(s)</th>
                                                        <th>Currency</th>
                                                        <th>Payment Method</th>
                                                        <th>Expenses</th>
                                                        <th>Attachment</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="timesheet-tbody">
                                                    <tr>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>From</label>
                                                                <input type="date" name="date2_from" class="form-control datepicker" value="{{$oes_att->date2_from}}" required>
                                                                <hr>
                                                                <label>To</label>
                                                                <input type="date" name="date2_to" class="form-control datepicker" value="{{$oes_att->date2_to}}" required>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Category</label>
                                                                <select name="category" class="form-control">
                                                                    <option value="" {{$oes_att->category == '' ? ' selected="selected"' : ''}}>Select</option>
                                                                    <option value="Airfare" {{$oes_att->category == 'Airfare' ? ' selected="selected"' : ''}}>Airfare</option>
                                                                    <option value="Hotel" {{$oes_att->category == 'Hotel' ? ' selected="selected"' : ''}}>Hotel</option>
                                                                    <option value="Laundry" {{$oes_att->category == 'Laundry' ? ' selected="selected"' : ''}}>Laundry</option>
                                                                    <option value="Taxi" {{$oes_att->category == 'Taxi' ? ' selected="selected"' : ''}}>Taxi</option>
                                                                    <option value="Mobile" {{$oes_att->category == 'Mobile' ? ' selected="selected"' : ''}}>Mobile</option>
                                                                    <option value="Visa" {{$oes_att->category == 'Visa' ? ' selected="selected"' : ''}}>Visa</option>
                                                                    <option value="Other" {{$oes_att->category == 'Other' ? ' selected="selected"' : ''}}>Other</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Description</label>
                                                                <textarea class="form-control" rows="3" name="description" placeholder="Description" >{{$oes_att->description}}</textarea>
                                                                
                                                            </div>
                                                        </td>
                                                        <td id="timesheet-travel-col">
                                                            <div class="form-group">
                                                                <label>Currency</label>
                                                                <select name="currency" class="form-control">
	<option value="USD"{{$oes_att->currency == 'USD' ? ' selected="selected"' : ''}}>United States Dollars</option>
	<option value="EUR"{{$oes_att->currency == 'EUR' ? ' selected="selected"' : ''}}>Euro</option>
	<option value="GBP"{{$oes_att->currency == 'GBP' ? ' selected="selected"' : ''}}>United Kingdom Pounds</option>
	<option value="DZD"{{$oes_att->currency == 'DZD' ? ' selected="selected"' : ''}}>Algeria Dinars</option>
	<option value="ARP"{{$oes_att->currency == 'ARP' ? ' selected="selected"' : ''}}>Argentina Pesos</option>
	<option value="AUD"{{$oes_att->currency == 'AUD' ? ' selected="selected"' : ''}}>Australia Dollars</option>
	<option value="ATS"{{$oes_att->currency == 'ATS' ? ' selected="selected"' : ''}}>Austria Schillings</option>
	<option value="BSD"{{$oes_att->currency == 'BSD' ? ' selected="selected"' : ''}}>Bahamas Dollars</option>
	<option value="BBD"{{$oes_att->currency == 'BBD' ? ' selected="selected"' : ''}}>Barbados Dollars</option>
	<option value="BEF"{{$oes_att->currency == 'BEF' ? ' selected="selected"' : ''}}>Belgium Francs</option>
	<option value="BMD"{{$oes_att->currency == 'BMD' ? ' selected="selected"' : ''}}>Bermuda Dollars</option>
	<option value="BRR"{{$oes_att->currency == 'BRR' ? ' selected="selected"' : ''}}>Brazil Real</option>
	<option value="BGL"{{$oes_att->currency == 'BGL' ? ' selected="selected"' : ''}}>Bulgaria Lev</option>
	<option value="CAD"{{$oes_att->currency == 'CAD' ? ' selected="selected"' : ''}}>Canada Dollars</option>
	<option value="CLP"{{$oes_att->currency == 'CLP' ? ' selected="selected"' : ''}}>Chile Pesos</option>
	<option value="CNY"{{$oes_att->currency == 'CNY' ? ' selected="selected"' : ''}}>China Yuan Renmimbi</option>
	<option value="CYP"{{$oes_att->currency == 'CYP' ? ' selected="selected"' : ''}}>Cyprus Pounds</option>
	<option value="CSK"{{$oes_att->currency == 'CSK' ? ' selected="selected"' : ''}}>Czech Republic Koruna</option>
	<option value="DKK"{{$oes_att->currency == 'DKK' ? ' selected="selected"' : ''}}>Denmark Kroner</option>
	<option value="NLG"{{$oes_att->currency == 'NLG' ? ' selected="selected"' : ''}}>Dutch Guilders</option>
	<option value="XCD"{{$oes_att->currency == 'XCD' ? ' selected="selected"' : ''}}>Eastern Caribbean Dollars</option>
	<option value="EGP"{{$oes_att->currency == 'EGP' ? ' selected="selected"' : ''}}>Egypt Pounds</option>
	<option value="FJD"{{$oes_att->currency == 'FJD' ? ' selected="selected"' : ''}}>Fiji Dollars</option>
	<option value="FIM"{{$oes_att->currency == 'FIM' ? ' selected="selected"' : ''}}>Finland Markka</option>
	<option value="FRF"{{$oes_att->currency == 'FRF' ? ' selected="selected"' : ''}}>France Francs</option>
	<option value="DEM"{{$oes_att->currency == 'DEM' ? ' selected="selected"' : ''}}>Germany Deutsche Marks</option>
	<option value="XAU"{{$oes_att->currency == 'XAU' ? ' selected="selected"' : ''}}>Gold Ounces</option>
	<option value="GRD"{{$oes_att->currency == 'GRD' ? ' selected="selected"' : ''}}>Greece Drachmas</option>
	<option value="HKD"{{$oes_att->currency == 'HKD' ? ' selected="selected"' : ''}}>Hong Kong Dollars</option>
	<option value="HUF"{{$oes_att->currency == 'HUF' ? ' selected="selected"' : ''}}>Hungary Forint</option>
	<option value="ISK"{{$oes_att->currency == 'ISK' ? ' selected="selected"' : ''}}>Iceland Krona</option>
	<option value="INR"{{$oes_att->currency == 'INR' ? ' selected="selected"' : ''}}>India Rupees</option>
	<option value="IDR"{{$oes_att->currency == 'IDR' ? ' selected="selected"' : ''}}>Indonesia Rupiah</option>
	<option value="IEP"{{$oes_att->currency == 'IEP' ? ' selected="selected"' : ''}}>Ireland Punt</option>
	<option value="ILS"{{$oes_att->currency == 'ILS' ? ' selected="selected"' : ''}}>Israel New Shekels</option>
	<option value="ITL"{{$oes_att->currency == 'ITL' ? ' selected="selected"' : ''}}>Italy Lira</option>
	<option value="JMD"{{$oes_att->currency == 'JMD' ? ' selected="selected"' : ''}}>Jamaica Dollars</option>
	<option value="JPY"{{$oes_att->currency == 'JPY' ? ' selected="selected"' : ''}}>Japan Yen</option>
	<option value="JOD"{{$oes_att->currency == 'JOD' ? ' selected="selected"' : ''}}>Jordan Dinar</option>
	<option value="KRW"{{$oes_att->currency == 'KRW' ? ' selected="selected"' : ''}}>Korea (South) Won</option>
	<option value="LBP"{{$oes_att->currency == 'LBP' ? ' selected="selected"' : ''}}>Lebanon Pounds</option>
	<option value="LUF"{{$oes_att->currency == 'LUF' ? ' selected="selected"' : ''}}>Luxembourg Francs</option>
	<option value="MYR"{{$oes_att->currency == 'MYR' ? ' selected="selected"' : ''}}>Malaysia Ringgit</option>
	<option value="MXP"{{$oes_att->currency == 'MXP' ? ' selected="selected"' : ''}}>Mexico Pesos</option>
	<option value="NLG"{{$oes_att->currency == 'NLG' ? ' selected="selected"' : ''}}>Netherlands Guilders</option>
	<option value="NZD"{{$oes_att->currency == 'NZD' ? ' selected="selected"' : ''}}>New Zealand Dollars</option>
	<option value="NOK"{{$oes_att->currency == 'NOK' ? ' selected="selected"' : ''}}>Norway Kroner</option>
	<option value="PKR"{{$oes_att->currency == 'PKR' ? ' selected="selected"' : ''}}>Pakistan Rupees</option>
	<option value="XPD"{{$oes_att->currency == 'XPD' ? ' selected="selected"' : ''}}>Palladium Ounces</option>
	<option value="PHP"{{$oes_att->currency == 'PHP' ? ' selected="selected"' : ''}}>Philippines Pesos</option>
	<option value="XPT"{{$oes_att->currency == 'XPT' ? ' selected="selected"' : ''}}>Platinum Ounces</option>
	<option value="PLZ"{{$oes_att->currency == 'PLZ' ? ' selected="selected"' : ''}}>Poland Zloty</option>
	<option value="PTE"{{$oes_att->currency == 'PTE' ? ' selected="selected"' : ''}}>Portugal Escudo</option>
	<option value="ROL"{{$oes_att->currency == 'ROL' ? ' selected="selected"' : ''}}>Romania Leu</option>
	<option value="RUR"{{$oes_att->currency == 'RUR' ? ' selected="selected"' : ''}}>Russia Rubles</option>
	<option value="SAR"{{$oes_att->currency == 'SAR' ? ' selected="selected"' : ''}}>Saudi Arabia Riyal</option>
	<option value="XAG"{{$oes_att->currency == 'XAG' ? ' selected="selected"' : ''}}>Silver Ounces</option>
	<option value="SGD"{{$oes_att->currency == 'SGD' ? ' selected="selected"' : ''}}>Singapore Dollars</option>
	<option value="SKK"{{$oes_att->currency == 'SKK' ? ' selected="selected"' : ''}}>Slovakia Koruna</option>
	<option value="ZAR"{{$oes_att->currency == 'ZAR' ? ' selected="selected"' : ''}}>South Africa Rand</option>
	<option value="KRW"{{$oes_att->currency == 'KRW' ? ' selected="selected"' : ''}}>South Korea Won</option>
	<option value="ESP"{{$oes_att->currency == 'ESP' ? ' selected="selected"' : ''}}>Spain Pesetas</option>
	<option value="XDR"{{$oes_att->currency == 'XDR' ? ' selected="selected"' : ''}}>Special Drawing Right (IMF)</option>
	<option value="SDD"{{$oes_att->currency == 'SDD' ? ' selected="selected"' : ''}}>Sudan Dinar</option>
	<option value="SEK"{{$oes_att->currency == 'SEK' ? ' selected="selected"' : ''}}>Sweden Krona</option>
	<option value="CHF"{{$oes_att->currency == 'CHF' ? ' selected="selected"' : ''}}>Switzerland Francs</option>
	<option value="TWD"{{$oes_att->currency == 'TWD' ? ' selected="selected"' : ''}}>Taiwan Dollars</option>
	<option value="THB"{{$oes_att->currency == 'THB' ? ' selected="selected"' : ''}}>Thailand Baht</option>
	<option value="TTD"{{$oes_att->currency == 'TTD' ? ' selected="selected"' : ''}}>Trinidad and Tobago Dollars</option>
	<option value="TRL"{{$oes_att->currency == 'TRL' ? ' selected="selected"' : ''}}>Turkey Lira</option>
	<option value="VEB"{{$oes_att->currency == 'VEB' ? ' selected="selected"' : ''}}>Venezuela Bolivar</option>
	<option value="ZMK"{{$oes_att->currency == 'ZMK' ? ' selected="selected"' : ''}}>Zambia Kwacha</option>
	<option value="EUR"{{$oes_att->currency == 'EUR' ? ' selected="selected"' : ''}}>Euro</option>
	<option value="XCD"{{$oes_att->currency == 'XCD' ? ' selected="selected"' : ''}}>Eastern Caribbean Dollars</option>
	<option value="XDR"{{$oes_att->currency == 'XDR' ? ' selected="selected"' : ''}}>Special Drawing Right (IMF)</option>
	<option value="XAG"{{$oes_att->currency == 'XAG' ? ' selected="selected"' : ''}}>Silver Ounces</option>
	<option value="XAU"{{$oes_att->currency == 'XAU' ? ' selected="selected"' : ''}}>Gold Ounces</option>
	<option value="XPD"{{$oes_att->currency == 'XPD' ? ' selected="selected"' : ''}}>Palladium Ounces</option>
	<option value="XPT"{{$oes_att->currency == 'XPT' ? ' selected="selected"' : ''}}>Platinum Ounces</option>
</select>
                                                                
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-check form-check-inline">
                                                                <label class="form-check-label" for="payment_method">Cash</label>
                                                                <select class="form-control" name="payment_method" id="payment_method">
                                                                    <option value="0" {{$oes_att->payment_method == '0' ? ' selected="selected"' : ''}}">Cash</option>
                                                                    <option value="1" {{$oes_att->payment_method == '1' ? ' selected="selected"' : ''}}>Amex Card</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Expenses</label>
                                                                <input class="form-control" name="expenses" placeholder="$ 00.00" type="text" value="{{$oes_att->expenses}}">
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                            @if($oes_att->attachment != 'no-file.png')
                                                                <label>Attachment</label>
                                                                <a class="btn btn-primary" id="view_attachment" data-id="{{$oes_att->attachment}}" data-oesid="{{$oes->id}}" data-toggle="modal" data-target="#myModal">View Attachment</a>
                                                            @else
                                                                <label>Attachment</label>
                                                                <input class="form-control" name="attachment" type="file">
                                                            @endif
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                        
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                        @include('layouts.flash_message')
                                        @include('layouts.validate')
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a id="delete" href="#" data-oesid="{{$oes_att->oes_id}}" data-oesattid="{{$oes_att->id}}" class="btn btn-danger">Delete</a>
                                        <a href="{{route('show_oes', ['oes_id' => $oes_att->oes_id])}}" class="btn btn-default">Back</a>
                                        
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Attachment</h4>
                                        </div>
                                        <div class="modal-body" id="modal-body">
                                                <input type="text" class="form-control attachment" value="" style="" disabled>
                                                <br>
                                                <embed style="width:100%" height="400px" class="attachment" src="">
                                                <!-- <embed src="file_name.pdf" width="100%" id="attachment" height="400px" /> -->
                                        </div>
                                        <div class="modal-footer" id="modal-footer">
                                            <button type="button" class="btn btn-danger" id="remove_att">Remove Attachment</button>
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Done</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            </form>
                            <form method="POST" id="delete-{{$oes_att->oes_id}}-{{$oes_att->id}}" action="{{route('destroy_oes_att', ['oes_id'=>$oes_att->oes_id,'oes_att_id'=>$oes_att->id])}}">
                                        {{csrf_field()}}
                            </form>
<script src="/vendor/jquery/jquery.min.js"></script> 
<!-- <script src="http://cdnjs.cloudflare.com/ajax/libs/processing.js/1.4.1/processing-api.min.js"></script> -->
<script src="/js/pdf.js"></script>
<script>
jQuery(document).ready(function(){

                

                $('a#delete').click(function(){
                    var oes_id = $(this).attr('data-oesid');
                    var oes_att_id = $(this).attr('data-oesattid');
                    var r = confirm("Are you sure to delete this expenses?");
                        if (r == true) {
                            // window.location.href = "/overseas-expense-statement/show/"+oes_id+"/destroy-att/"+oes_att_id;
                            document.getElementById('delete-'+oes_id+'-'+oes_att_id).submit();
                        } else {
            
                    }
                });


                jQuery('a#view_attachment').one( 'click',function(){
                    var file = $(this).attr('data-id');
                    var oes_id = $(this).attr('data-oesid');
                    $('embed.attachment').attr('src', '/storage/overseas_expense_statement/'+oes_id+'/'+'oes'+'/'+file);
                    $('input.attachment').attr('value', file);
                    // $('embed#attachment').attr('src', '/storage/overseas_expense_statement/'+oes_id+'/'+'oes'+'/'+file);
                    $('input#file').remove();
                });

                jQuery(document).one('click', 'button#remove_att', function(){
                    $('embed.attachment').remove();
                    $('input.attachment').remove();
                    // $('div#modal-body').append('<input type="file" id="file" class="form-control" name="attachment" required>');
                    $('div#modal-body').append('<input type="file" id="file" class="form-control" name="attachment"><small>Note: Please upload a valid PDF file.</small><input type="hidden" id="base64str" name="base64str" value="">');
                });

                

            });
            // function convertToBase64() {
            //     //Read File
            //     var selectedFile = document.getElementById("file").files;
            //     //Check File is not Empty
            //     if (selectedFile.length > 0) {
            //         // Select the very first file from list
            //         var fileToLoad = selectedFile[0];
            //         // FileReader function for read the file.
            //         var fileReader = new FileReader();
            //         var base64;
            //         // Onload of file read the file content
            //         fileReader.onload = function(fileLoadedEvent) {
            //             base64 = fileLoadedEvent.target.result;
            //             // Print data in console
            //             console.log(base64);
            //             document.getElementsByName("base64str")[0].setAttribute("value", base64);
            //         };
            //         // Convert data to base64
            //         fileReader.readAsDataURL(fileToLoad);
            //         // document.getElementsByName("base64str")[0].setAttribute("value", base64str);
            //     }
            // }

     
</script>
@endsection