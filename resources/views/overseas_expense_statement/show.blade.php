@extends('layouts.master_layout')
@section('title', 'Overseas Expense Statement Show')
@section('content')
<div class="modal fade" id="custSign" tabindex="-1" role="dialog" aria-labelledby="custSignLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                    <form action="{{route('customersignature_oes_att', ['oes_id'=>$oes_job['id']])}}" method="post" id="custForm" enctype="multipart/form-data">
                                        {{csrf_field()}}
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="custSignLabel">Attachment</h4>
                                        </div>
                                        <div class="modal-body">
                                            @if($oes_job->customer_signature_attachment != 'no-file.png')
                                                @php
                                                    $file = pathinfo($oes_job->customer_signature_attachment, PATHINFO_EXTENSION)
                                                @endphp
                                                @if(strtolower($file) == "pdf")
                                                <div class="form-group">
                                                    <label>Customer Signature Name</label>
                                                    <input type="text" value="{{$oes_job->customer_name}}" class="form-control" disabled>
                                                </div>
                                                    <div class="form-group" id="attachment">
                                                        <label for="">Customer Signature Attachment</label>
                                                        <!-- <input type="text" class="form-control" value="Customer signature file is available in PDF format." disabled><br><a class="btn btn-primary" href="#">Download</a> -->
                                                        <div class="row">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" value="{{$oes_job->customer_signature_attachment}}" style="" disabled>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <a href="{{route('custAttDownloadPdf_oes', [ 'oes_id' => $oes_job->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                                                                </div>
                                                            </div>
                                                            <br>
                                                        <embed src="/storage/overseas_expense_statement/{{$oes_job['id']}}/cust_sign/{{$oes_job->customer_signature_attachment}}" width="100%" height="400px">
                                                    </div>
                                                @else
                                                    <div class="form-group">
                                                        <label>Customer Signature Name</label>
                                                        <input type="text" value="{{$oes_job->customer_name}}" class="form-control" disabled>
                                                    </div>
                                                    <div class="row">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" value="{{$oes_job->customer_signature_attachment}}" style="" disabled>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <a href="{{route('custAttDownloadPdf_oes', [ 'oes_id' => $oes_job->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                                                                </div>
                                                    </div>
                                                    <br>
                                                    <img src="/storage/overseas_expense_statement/{{$oes_job['id']}}/cust_sign/{{$oes_job->customer_signature_attachment}}" class="rounded mx-auto d-block" alt="..." width="100%" id="attachment">
                                                @endif
                                            @else
                                                <div class="form-group">
                                                    <label>Customer Signature Name</label>
                                                    <input type="text" class="form-control" value="{{$oes_job->customer_name}}" name="customer_name">
                                                </div>
                                                <div class="form-group">
                                                    <label>Customer Signature Attachment</label>
                                                    <input class="form-control" name="customer_signature_attachment" type="file">
                                                </div>
                                            @endif
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                            @if($oes_job->customer_signature_attachment == 'no-file.png')
                                            <button type="button" class="btn btn-primary" id="submitForm">Upload</button>
                                            @endif
                                        </div>
                                    </form>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
        <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Expense Statement</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Overseas Expense Statement Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                    <h1>Employee Information</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>Name of employee</label>
                                            <input class="form-control" name="name" value="{{$oes_job->name}}" placeholder="Full name" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Department</label>
                                            <input class="form-control" name="department" value="{{$oes_job->department}}"  disabled>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">   
                                                    <label>From</label>
                                                    <input type="date" class="form-control" name="date_from" value="{{$oes_job->date_from}}" placeholder="From" disabled>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">   
                                                    <label>To</label> 
                                                    <input type="date" class="form-control" name="date_to" value="{{$oes_job->date_to}}" placeholder="To" disabled>
                                                </div>
                                            </div>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label>Purpose of trip</label>
                                            <input class="form-control" name="purpose_of_trip" value="{{$oes_job->purpose_of_trip}}" placeholder="Net No." disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer signature name</label>
                                            @if($oes_job->customer_name)           
                                                <input class="form-control" value="{{$oes_job->customer_name}}"  disabled>
                                            @else
                                                <input class="form-control" disabled>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Customer signature attachment</label>
                                            @if($oes_job->customer_signature_attachment != 'no-file.png')
                                                <p class="text-success">Yes</p>
                                            @else
                                                               
                                                <p class="text-danger">N/a</p>
                                            @endif
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>SSO no.</label>
                                            <input class="form-control" name="sso_no" value="{{$oes_job->sso_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" name="country" value="{{$oes_job->country}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job no.</label>
                                            <input class="form-control" name="job_no" value="{{$oes_job->job_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Network no.</label>
                                            <input class="form-control" name="network_no" value="{{$oes_job->network_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Dept signature name</label>
                                            @if($oes_job->dept_signature_name)           
                                                <input class="form-control" value="{{$oes_job->dept_signature_name}}"  disabled>
                                            @else
                                                <p class="text-danger">N/a</p>
                                            @endif
                                        </div>
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Overseas Expenses</h1>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                                <thead>
                                                    <tr>
                                                        <th>Date(s)</th>
                                                        <th>Category</th>
                                                        <th>Description(s)</th>
                                                        <th>Currency</th>
                                                        <th>Payment Method</th>
                                                        <th>Expenses</th>
                                                        <th>Attachment</th>
                                                        <th>Updated</th>
                                                        <th>Action(s)</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="timesheet-tbody">
                                                    @if($oes_att_collections)
                                                        @foreach($oes_att_collections as $collection)
                                                        @if($collection->id != null)
                                                    <tr>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>From:</label>
                                                                {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->date2_from))->formatLocalized('%d %B %Y')}}
                                                                <hr>
                                                                <label>To:</label>
                                                                {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->date2_to))->formatLocalized('%d %B %Y')}}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                {{$collection->category}}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                {{$collection->description}}
                                                            </div>
                                                        </td>
                                                        <td id="timesheet-travel-col">
                                                            <div class="form-group">
                                                                {{$collection->currency}}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-check form-check-inline">
                                                                @if($collection->payment_method == 0)
                                                                    Cash
                                                                @elseif($collection->payment_method == 1)
                                                                    Amex Card
                                                                @endif
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                {{$collection->expenses}}
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <button class="btn btn-primary" id="view_attachment" data-id="{{$collection->attachment}}" data-toggle="modal" data-oesid="{{$oes_job->id}}"  data-target="#myModal">View Attachment</button>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            {{ \Carbon\Carbon::createFromTimeStamp(strtotime($collection->updated_at))->diffForHumans() }}
                                                        </td>
                                                        <td>
                                                        @if($oes_job->sso_no == Auth::user()->sso_no)
                                                            <a href="{{route('edit_oes_att',[ 'oes_id' => $collection->oes_id, 'oes_att_id' => $collection->id])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                                            <button onclick="deleteRowFunction({{$collection->oes_id}},{{ $collection->id}})" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                                            <form method="POST" id="delete-{{$collection->oes_id}}-{{$collection->id}}" action="{{route('destroy_oes_att', ['oes_id'=>$collection->oes_id,'oes_att_id'=>$collection->id])}}">
                                                            {{csrf_field()}}
                                                            </form>
                                                        @endif
                                                        </td>
                                                    </tr>
                                                    @endif
                                                    @endforeach
                                                </tbody>
                                            </table>
                                    
                                @endif
                                @if($oes_job->sso_no == Auth::user()->sso_no)
                                <a href="{{route('create_oes_att', [ 'oes_id' => $oes_job->id])}}" class="btn btn-primary">Add New Expenses</a>
                                @endif
                                        </div>
                                        <!-- /.table-responsive -->
                                        
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                        
                                        @if($oes_job->sso_no == Auth::user()->sso_no)
                                        <a href="{{route('edit_oes',['oes_id' => $oes_job->id])}}" class="btn btn-default">Edit</a>
                                        @endif
                                        <a href="{{route('print_oes',['oes_id'=>$oes_job['id']])}}" class="btn btn-success">Download</a>
                                        <button class="btn btn-info" data-toggle="modal" data-target="#custSign" data-id="{{$oes_job->id}}" id="custSign">Customer Signature</button>
                                        @if(Auth::user()->account_type == 1)
                                        <button onclick="approve({{$oes_job->id}})" class="btn btn-info">Approve</button>
                                        @endif
                                        @if($oes_job->sso_no == Auth::user()->sso_no)
                                        <button class="btn btn-danger" onclick="deleteFunction({{$oes_job->id}})">Delete</button>
                                        @endif
                                        
                                        <a href="{{route('index_oes')}}" class="btn btn-default">Back</a>
                                        <form method="POST" id="delete-{{$oes_job->id}}" action="{{route('destroy_oes', ['oes_id'=>$oes_job->id])}}">
                                                {{csrf_field()}}
                                        </form>
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
           
            <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Attachment</h4>
                                        </div>
                                        <div class="modal-body">
                                            <input type="text" class="form-control attachment" value="{{$oes_job->customer_signature_attachment}}" style="" disabled>
                                            <br>
                                           <!-- <img style="width:100%;" class="attachment" src=""> -->
                                           <embed src="" width="100%" class="attachment" height="400px">
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            
            <script src="/vendor/jquery/jquery.min.js"></script> 
                            <script>
                                function deleteFunction(oes_id) {

                                var r = confirm("Are you sure to delete this job number?");
                                    if (r == true) {
                                        // window.location.href = "/overseas-expense-statement/destroy/"+oes_id;
                                        document.getElementById('delete-'+oes_id).submit();
                                    } else {
            
                                    }
                                }
                                function deleteRowFunction(oes_id,oes_att_id) {
                                    var r = confirm("Are you sure to delete this expense?");
                                    if (r == true) {
                                        window.location.href = "/overseas-expense-statement/show/"+oes_id+"/destroy-att/"+oes_att_id;
                                        document.getElementById('delete-'+oes_id+'-'+oes_att_id).submit();
                                    } else {
            
                                }
                                }
                                function approve(oes_id) {
                                    var r = confirm("Are you sure to include your signature into this overseas expense statement?");
                                    if (r == true) {
                                        window.location.href = "/overseas-expense-statement/approve/"+oes_id;
                                    } else {
            
                                }
                                }
                                
            jQuery(document).ready(function(){
                // jQuery('button#view_attachment').click(function(){
                    
                    
                // });
                $(document).on('click', 'button#view_attachment', function(){
                    var file = $(this).attr('data-id');
                    var oes_id = $(this).attr('data-oesid');
                    if(file != 'no-file.png'){
                        $('embed.attachment').attr('src', '/storage/overseas_expense_statement/'+oes_id+'/oes'+'/'+file);
                        $('input.attachment').attr('value', file);
                    }else{
                        $('embed#attachment').attr('src', '/img/'+file);
                    }
                });
                $("#submitForm").on('click', function() {
                                    $("#custForm").submit();
                                });
            });
                            
            </script>
@endsection