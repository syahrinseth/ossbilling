@extends('layouts.master_layout')
@section('title', 'Overseas Expense Statement Edit')
@section('content')
<form role="form" action="{{route('update_oes', [ 'oes_id' => $oes->id])}}" method="post" enctype="multipart/form-data">
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Expense Statement</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Overseas Expense Statement Edit
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <h1>Employee Information</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Name of employee</label>
                                            <input class="form-control" name="name" value="{{$oes->name}}" placeholder="Full name" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Department</label>
                                            <input class="form-control" name="department" value="{{$oes->department}}" placeholder="SSO#" required>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">   
                                                    <label>From</label>
                                                    <input type="date" class="form-control datepicker" name="date_from" value="{{$oes->date_from}}" placeholder="From" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">   
                                                    <label>To</label> 
                                                    <input type="date" class="form-control datepicker" name="date_to" value="{{$oes->date_to}}" placeholder="To" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer name</label>
                                            <input class="form-control" name="customer_name" value="{{$oes->customer_name}}" placeholder="Customer Name" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer signature</label><br>
                                                            @if($oes->customer_signature_attachment != 'no-file.png')
                                                                
                                                                <a class="btn btn-primary" id="view_attachment" data-id="{{$oes->customer_signature_attachment}}" data-toggle="modal" data-target="#myModal">View Attachment</a>
                                                            @else
                                                               
                                                                <input class="form-control" name="customer_signature_attachment" type="file">
                                                            @endif
                                        </div>
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">
                                        <div class="form-group">
                                            <label>SSO no.</label>
                                            <input class="form-control" name="sso_no" value="{{$oes->sso_no}}" placeholder="SSO No." disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" name="country" value="{{$job->country}}" placeholder="Country" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job no.</label>
                                            <input list="job_no" name="job_no" class="form-control" value="{{$job->job_no}}" disabled>
                                            <datalist id="job_no">
                                                @if($master_jobs)
                                                    @foreach($master_jobs as $item)
                                                        <option value="{{$item->job_no}}">
                                                    @endforeach
                                                @endif
                                            </datalist>
                                        </div>
                                        <div class="form-group">
                                            <label>Job Description</label>
                                            <textarea class="form-control" name="job_description" disabled>{{$job->job_description}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Network no.</label>
                                            <input class="form-control" name="network_no" value="{{$job->network_no}}" placeholder="Network No" disabled>
                                        </div>

                                        <div class="form-group">
                                            <label>Purpose of trip</label>
                                            <input class="form-control" name="purpose_of_trip" value="{{$oes->purpose_of_trip}}" placeholder="Net No." required>
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                            <div class="row">
                                <div class="col-lg-12">
                                        @include('layouts.flash_message')
                                    @include('layouts.validate')
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('show_oes', [ 'oes_id' => $oes->id ])}}" class="btn btn-default">Cancel</a>
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->
           
           <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                            <h4 class="modal-title" id="myModalLabel">Attachment</h4>
                                        </div>
                                        <div class="modal-body" id="modal-body">
                                            <div class="form-group">
                                                <label for="signature_name" id="attachment">Customer Signature Name</label>
                                                <input type="text" class="form-control" id="signature_name" value="{{$oes['customer_name']}}" disabled>
                                            </div>
                                                @if($oes->customer_signature_attachment != 'no-file.png')
                                                    @php
                                                        $file = pathinfo($oes->customer_signature_attachment, PATHINFO_EXTENSION)
                                                    @endphp
                                                    @if(strtolower($file) == "pdf")
                                                        <div class="form-group" id="attachment">
                                                            <label for="">Customer Signature Attachment</label>
                                                            <div class="row">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" value="{{{$oes->customer_signature_attachment}}}" style="" disabled>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <a href="{{route('custAttDownloadPdf_oes', [ 'oes_id' => $oes->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                                                                </div>
                                                            </div>
                                                            <br>
                                                            <embed src="/storage/overseas_expense_statement/{{$oes->id}}/cust_sign/{{$oes->customer_signature_attachment}}" height="400px" width="100%">
                                                        </div>
                                                    @else
                                                    <div class="form-group" id="attachment">
                                                        <label for="">Customer Signature Attachment</label>
                                                        <div class="row">
                                                                <div class="col-sm-10">
                                                                    <input type="text" class="form-control" value="{{{$oes->customer_signature_attachment}}}" style="" disabled>
                                                                </div>
                                                                <div class="col-sm-2">
                                                                    <a href="{{route('custAttDownloadPdf_oes', [ 'oes_id' => $oes->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                                                                </div>
                                                        </div>
                                                        <br>
                                                        <img src="/storage/overseas_expense_statement/{{$oes->id}}/cust_sign/{{$oes->customer_signature_attachment}}" class="rounded mx-auto d-block" alt="..." width="100%" id="attachment">
                                                    </div>
                                                 @endif
                                                @else

                                                @endif

                                        </div>
                                        <div class="modal-footer" id="modal-footer">
                                            <button type="button" class="btn btn-danger" id="remove_att">Remove Attachment</button>
                                            <button type="submit" class="btn btn-default" data-dismiss="modal">Done</button>
                                        </div>
                                    </div>
                                    <!-- /.modal-content -->
                                </div>
                                <!-- /.modal-dialog -->
                            </div>
                            <!-- /.modal -->
                            </form>
                            <script src="/vendor/jquery/jquery.min.js"></script> 
<script>
jQuery(document).ready(function(){
                // jQuery('a#view_attachment').one( 'click',function(){
                    
                //     $('img#attachment').attr('src', '/storage/overseas_expense_statement/{{$oes->id}}/cust_sign/{{$oes->customer_signature_attachment}}');
                //     $('input#file').remove();
                // });

                jQuery('button#remove_att').one('click', function(){
                    $('label#attachment').remove();
                    $('div#attachment').remove();
                    $('img#attachment').remove();
                    $('a#attachment').remove();
                    $('input#attachment').remove();
                    $('label#view_attachment').remove();
                    $('input#signature_name').remove();
                    $('div#modal-body').append('<div class="form-group"><label>Customer Signature Name</label><input class="form-control" name="customer_name" value="{{$oes->customer_name}}"></div><label>Customer Signature Attachment</label><input type="file" id="file" class="form-control" name="customer_signature_attachment"><small>Note: Please upload a valid PDF or Image file.</small>');
                });

            });
</script>
@endsection