@extends('layouts.master_layout')
@section('title', 'Overseas Expense Statement Create')
@section('content')
<!-- Modal -->
<div class="modal fade" id="autopopulate" tabindex="-1" role="dialog" aria-labelledby="autopopulateModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Auto Populate</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
            <div class="row">
            <div class="col-md-3"></div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="autopopulate">Number of rows</label>
                    <select name="row" id="autopopulate" class="form-control">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                    </select>
                </div>
            </div>
            <div class="col-md-3"></div>
            </div>
        </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="autopopulate">Auto Populate</button>
      </div>
    </div>
  </div>
</div>
 <!-- Model End -->
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Expense Statement Create</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('store_oes')}}" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Overseas Expense Statement Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    
                                    <h1>Employee Information</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6 col-md-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Name of employee</label>
                                            <input class="form-control" name="name" value="{{Auth::user()->name}}" placeholder="Full name" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Department</label>
                                            <input class="form-control" name="department" value="" placeholder="Department" required>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">   
                                                    <label>From</label>
                                                    <input type="date" class="form-control datepicker" name="date_from" value="" placeholder="From" required>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">   
                                                    <label>To</label> 
                                                    <input type="date" class="form-control datepicker" name="date_to" value="" placeholder="To" required>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer name</label>
                                            <input class="form-control" name="customer_name" value="" placeholder="Customer name" required>
                                        </div>
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6 col-md-6">
                                        <div class="form-group">
                                            <label>SSO no.</label>
                                            <input class="form-control" name="sso_no" placeholder="Job No." value="{{Auth::user()->sso_no}}" readonly>
                                        </div>
                                        <div class="form-group">
                                            <label>Job no.</label>
                                            <input list="job_no" name="job_no" class="form-control" placeholder="Job Number" required>
                                            <datalist id="job_no">
                                                @if($master_jobs)
                                                    @foreach($master_jobs as $item)
                                                        <option value="{{$item->job_no}}">
                                                    @endforeach
                                                @endif
                                            </datalist>
                                        </div>
                                        <div class="form-group">
                                            <label>Purpose of trip</label>
                                            <input class="form-control" name="purpose_of_trip" value="" placeholder="Purpose of trip" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer signature attachment</label>
                                            <input type="file" class="form-control" name="customer_signature_attachment">
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                        @include('layouts.flash_message')
                                    @include('layouts.validate')
                                        <button type="submit" class="btn btn-primary">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('index_oes')}}" class="btn btn-default">Back</a>
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
                </form>
            </div>
            <!-- /.row -->
            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    jQuery('a#autopopulate').click(function(){
                        var counter = $(this).attr('data-counter');
                        
                        jQuery('button#autopopulate').unbind("click").click(function(){
                            // var counter = $(this).attr('data-counter');
                            var date2_from = $('input[id="date2_from-'+counter+'"]').val();
                            var date2_to = $('input[id="date2_to-'+counter+'"]').val();
                            var category = $('select[id="category-'+counter+'"]').val();
                            var description = $('textarea[id="description-'+counter+'"]').val();
                            var currency = $('select[id="currency-'+counter+'"]').val();
                            var payment_method = $('select[id="payment_method-'+counter+'"]').val();
                            var expenses = $('input[id="expenses-'+counter+'"]').val();
                            var row = $('select[name="row"]').val();

                            
                            if(row > 0){
                                
                                // date from and to to copy
                                var date2_from_temp = new Date(date2_from);
                                var date2_to_temp = new Date(date2_to);
                                var day_from_temp, month_from_temp, today_from_temp, day_to_temp, month_to_temp, today_to_temp;
                                
                                day_from_temp = ("0" + date2_from_temp.getDate()).slice(-2);
                                month_from_temp = ("0" + (date2_from_temp.getMonth() + 1)).slice(-2);
                                today_from_temp = date2_from_temp.getFullYear()+"-"+(month_from_temp)+"-"+(day_from_temp);

                                day_to_temp = ("0" + date2_to_temp.getDate()).slice(-2);
                                month_to_temp = ("0" + (date2_to_temp.getMonth() + 1)).slice(-2);
                                today_to_temp = date2_to_temp.getFullYear()+"-"+(month_to_temp)+"-"+(day_to_temp);
                                // date increment
                                

                                

                                // for loop the data
                                for(i=0;i<row;i++){
                                    
                                    $('tbody#timesheet-tbody').append('<tr id="overseas_expenses_row"><td><div class="form-group"><label>From</label><input type="date" name="date2_from[]" value="'+today_from_temp+'" id="date2_from-" class="form-control datepicker" required><hr><label>To</label><input type="date" name="date2_to[]" value="'+today_to_temp+'" id="date2_to-" class="form-control" required></div></td><td><div class="form-group"><label>Category</label><select name="category[]" id="category-" class="form-control"><option value="'+category+'" selected>'+category+'</option><option value="Airfare">Airfare</option><option value="Hotel">Hotel</option><option value="Laundry">Laundry</option><option value="Taxi">Taxi</option><option value="Mobile">Mobile</option><option value="Visa">Visa</option><option value="Other">Other</option></select></div></td><td><div class="form-group"><label>Description</label><textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description[]" id="description-" placeholder="Description" >'+description+'</textarea></div></td><td id="timesheet-travel-col"><div class="form-group"><label>Currency</label><select name="currency[]" id="currency-" class="form-control"><option value="'+currency+'">'+currency+'</option><option value="USD">United States Dollars - USD</option><option value="EUR">Euro - EUR</option><option value="GBP">United Kingdom Pounds - GBP</option><option value="DZD">Algeria Dinars - DZD</option><option value="ARP">Argentina Pesos - ARP</option><option value="AUD">Australia Dollars - AUD</option><option value="ATS">Austria Schillings - ATS</option><option value="BSD">Bahamas Dollars - BSD</option><option value="BBD">Barbados Dollars - BBD</option><option value="BEF">Belgium Francs - BEF</option><option value="BMD">Bermuda Dollars - BMD</option><option value="BRR">Brazil Real - BRR</option><option value="BGL">Bulgaria Lev - BGL</option><option value="CAD">Canada Dollars - CAD</option><option value="CLP">Chile Pesos - CLP</option><option value="CNY">China Yuan Renmimbi - CNY</option><option value="CYP">Cyprus Pounds - CYP</option><option value="CSK">Czech Republic Koruna - CSK</option><option value="DKK">Denmark Kroner - DKK</option><option value="NLG">Dutch Guilders - NLG</option><option value="XCD">Eastern Caribbean Dollars - XCD</option><option value="EGP">Egypt Pounds - EGP</option><option value="FJD">Fiji Dollars - FJD</option><option value="FIM">Finland Markka - FIM</option><option value="FRF">France Francs - FRF</option><option value="DEM">Germany Deutsche Marks - DEM</option><option value="XAU">Gold Ounces - XAU</option><option value="GRD">Greece Drachmas - GRD</option><option value="HKD">Hong Kong Dollars - HKD</option><option value="HUF">Hungary Forint - HUF</option><option value="ISK">Iceland Krona - ISK</option><option value="INR">India Rupees - INR</option><option value="IDR">Indonesia Rupiah - IDR</option><option value="IEP">Ireland Punt - IEP</option><option value="ILS">Israel New Shekels - ILS</option><option value="ITL">Italy Lira - ITL</option><option value="JMD">Jamaica Dollars - JMD</option><option value="JPY">Japan Yen - JPY</option><option value="JOD">Jordan Dinar - JOD</option><option value="KRW">Korea (South) Won - KRW</option><option value="LBP">Lebanon Pounds - LBP</option><option value="LUF">Luxembourg Francs - LUF</option><option value="MYR">Malaysia Ringgit - MYR</option><option value="MXP">Mexico Pesos - MXP</option><option value="NLG">Netherlands Guilders - NLG</option><option value="NZD">New Zealand Dollars - NZD</option><option value="NOK">Norway Kroner - NOK</option><option value="PKR">Pakistan Rupees - PKR</option><option value="XPD">Palladium Ounces - XPD</option><option value="PHP">Philippines Pesos - PHP</option><option value="XPT">Platinum Ounces - XPT</option><option value="PLZ">Poland Zloty - PLZ</option><option value="PTE">Portugal Escudo - PTE</option><option value="ROL">Romania Leu - ROL</option><option value="RUR">Russia Rubles - RUR</option><option value="SAR">Saudi Arabia Riyal - SAR</option><option value="XAG">Silver Ounces - XAG</option><option value="SGD">Singapore Dollars - SGD</option><option value="SKK">Slovakia Koruna - SKK</option><option value="ZAR">South Africa Rand - ZAR</option><option value="KRW">South Korea Won - KRW</option><option value="ESP">Spain Peset - ESPas</option><option value="XDR">Special Drawing Right (IMF)< - XDR/option><option value="SDD">Sudan Dinar - SDD</option><option value="SEK">Sweden Krona - SEK</option><option value="CHF">Switzerland Francs - CHF</option><option value="TWD">Taiwan Dollars - TWD</option><option value="THB">Thailand Baht - THB</option><option value="TTD">Trinidad and Tobago Dollars - TTD</option><option value="TRL">Turkey Lira - TRL</option><option value="VEB">Venezuela Bolivar - VEB</option><option value="ZMK">Zambia Kwacha - ZMK</option><option value="EUR">Euro - EUR</option><option value="XCD">Eastern Caribbean Dollars - XCD</option><option value="XDR">Special Drawing Right (IMF)< - XDR/option><option value="XAG">Silver Ounces - XAG</option><option value="XAU">Gold Ounces - XAU</option><option value="XPD">Palladium Ounces - XPD</option><option value="XPT">Platinum Ounces - XPT</option></select></div></td><td><div class="form-check form-check-inline"><label class="form-check-label" for="payment_method">Cash</label><select class="form-control" name="payment_method[]" id="payment_method-" id="payment_method"><option value="'+payment_method+'" selected>'+(payment_method == 1 ? 'Amex Card':'Cash')+'</option><option value="0" >Cash</option><option value="1">Amex Card</option></select></div></td><td><div class="form-group"><label>Expenses</label><input class="form-control" name="expenses[]" value="'+expenses+'" id="expenses-" placeholder="$ 00.00" type="text" required></div></td><td><div class="form-group"><label>Attachment</label><input class="form-control" name="attachment[]" id="attachment-" type="file" required></div></td><td></td></tr>');
                                    
                                    // alert(travel2_finish);
                                }
                            }
                            // currency selected code
                            // $('option[value="'+counter+'"]').attr('selected', 'selected');
                            alert('Auto Populate Success!');

                            
                            date = null;
                            designation = null;
                            travel_start = null;
                            travel_finish = null;
                            travel2_start = null;
                            travel2_finish = null;
                            work_start = null;
                            work_finish = null;
                            work2_start = null;
                            work2_finish = null;
                            remark = null;
                            d1 = null;
                            d2 = null;
                            row = null;
                            date_increment = null;
                            day = null;
                            month = null;
                            today = null;    
                            counter = null;
                        });
                    });




                    jQuery('a#overseas_timesheet_add_row').click(function(){
                        $('tbody#timesheet-tbody').append('<tr id="overseas_expenses_row"><td><div class="form-group"><label>From</label><input type="date" name="date2_from[]" class="form-control datepicker" required><hr><label>To</label><input type="date" name="date2_to[]" class="form-control" required></div></td><td><div class="form-group"><label>Category</label><select name="category[]" class="form-control"><option value="">Select</option><option value="Airfare">Airfare</option><option value="Hotel">Hotel</option><option value="Laundry">Laundry</option><option value="Taxi">Taxi</option><option value="Mobile">Mobile</option><option value="Visa">Visa</option><option value="Other">Other</option></select></div></td><td><div class="form-group"><label>Description</label><textarea class="form-control" id="exampleFormControlTextarea1" rows="3" name="description[]" placeholder="Description" ></textarea></div></td><td id="timesheet-travel-col"><div class="form-group"><label>Currency</label><select name="currency[]" class="form-control"><option value="USD">United States Dollars</option><option value="EUR">Euro</option><option value="GBP">United Kingdom Pounds</option><option value="DZD">Algeria Dinars</option><option value="ARP">Argentina Pesos</option><option value="AUD">Australia Dollars</option><option value="ATS">Austria Schillings</option><option value="BSD">Bahamas Dollars</option><option value="BBD">Barbados Dollars</option><option value="BEF">Belgium Francs</option><option value="BMD">Bermuda Dollars</option><option value="BRR">Brazil Real</option><option value="BGL">Bulgaria Lev</option><option value="CAD">Canada Dollars</option><option value="CLP">Chile Pesos</option><option value="CNY">China Yuan Renmimbi</option><option value="CYP">Cyprus Pounds</option><option value="CSK">Czech Republic Koruna</option><option value="DKK">Denmark Kroner</option><option value="NLG">Dutch Guilders</option><option value="XCD">Eastern Caribbean Dollars</option><option value="EGP">Egypt Pounds</option><option value="FJD">Fiji Dollars</option><option value="FIM">Finland Markka</option><option value="FRF">France Francs</option><option value="DEM">Germany Deutsche Marks</option><option value="XAU">Gold Ounces</option><option value="GRD">Greece Drachmas</option><option value="HKD">Hong Kong Dollars</option><option value="HUF">Hungary Forint</option><option value="ISK">Iceland Krona</option><option value="INR">India Rupees</option><option value="IDR">Indonesia Rupiah</option><option value="IEP">Ireland Punt</option><option value="ILS">Israel New Shekels</option><option value="ITL">Italy Lira</option><option value="JMD">Jamaica Dollars</option><option value="JPY">Japan Yen</option><option value="JOD">Jordan Dinar</option><option value="KRW">Korea (South) Won</option><option value="LBP">Lebanon Pounds</option><option value="LUF">Luxembourg Francs</option><option value="MYR">Malaysia Ringgit</option><option value="MXP">Mexico Pesos</option><option value="NLG">Netherlands Guilders</option><option value="NZD">New Zealand Dollars</option><option value="NOK">Norway Kroner</option><option value="PKR">Pakistan Rupees</option><option value="XPD">Palladium Ounces</option><option value="PHP">Philippines Pesos</option><option value="XPT">Platinum Ounces</option><option value="PLZ">Poland Zloty</option><option value="PTE">Portugal Escudo</option><option value="ROL">Romania Leu</option><option value="RUR">Russia Rubles</option><option value="SAR">Saudi Arabia Riyal</option><option value="XAG">Silver Ounces</option><option value="SGD" selected="selected">Singapore Dollars</option><option value="SKK">Slovakia Koruna</option><option value="ZAR">South Africa Rand</option><option value="KRW">South Korea Won</option><option value="ESP">Spain Pesetas</option><option value="XDR">Special Drawing Right (IMF)</option><option value="SDD">Sudan Dinar</option><option value="SEK">Sweden Krona</option><option value="CHF">Switzerland Francs</option><option value="TWD">Taiwan Dollars</option><option value="THB">Thailand Baht</option><option value="TTD">Trinidad and Tobago Dollars</option><option value="TRL">Turkey Lira</option><option value="VEB">Venezuela Bolivar</option><option value="ZMK">Zambia Kwacha</option><option value="EUR">Euro</option><option value="XCD">Eastern Caribbean Dollars</option><option value="XDR">Special Drawing Right (IMF)</option><option value="XAG">Silver Ounces</option><option value="XAU">Gold Ounces</option><option value="XPD">Palladium Ounces</option><option value="XPT">Platinum Ounces</option></select></div></td><td><div class="form-check form-check-inline"><label class="form-check-label" for="payment_method">Cash</label><select class="form-control" name="payment_method[]" id="payment_method"><option value="0" checked="checked">Cash</option><option value="1" checked="checked">Amex Card</option></select></div></td><td><div class="form-group"><label>Expenses</label><input class="form-control" name="expenses[]" placeholder="$ 00.00" type="text" required></div></td><td><div class="form-group"><label>Attachment</label><input class="form-control" name="attachment[]" type="file" required></div></td><td></td></tr>');
                    });
                    
                    // function for remove timesheet row
                    jQuery('a#overseas_timesheet_remove_row').click(function(){
                        $('#overseas_expenses_row').remove();
                    });


                });
            </script>
@endsection