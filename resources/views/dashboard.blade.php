@extends('layouts.master_layout')
@section('title', 'Dashboard')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Dashboard</h1>
                    @include('layouts.flash_message')
                </div>
                <!-- /.col-lg-12 -->
            </div>
            @if(Auth::user()->account_type == 1 || Auth::user()->account_type == 2)
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$local_ts}}</div>
                                    <div>Pending Local <br>Timesheet</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('indexpending_lt')}}" >
                            <div class="panel-footer">
                                <span class="pull-left">View Pending LT</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$overseas_ts}}</div>
                                    <div>Pending Overseas<br>Timesheet</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('indexpending_otf')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Pending OT List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$overseas_es}}</div>
                                    <div>Pending Overseas Expense Statement</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('indexpending_oes')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Pending OES List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-briefcase fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$master_jobs}}</div>
                                    <div>Master <br>Jobs</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('index_usr_mjb')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View Master Jobs List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-green">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-dollar fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$mcr}}</div>
                                    <div>This Month <br>Master Currency Rate</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('index_cr')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View MCR List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                 <!--<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Support Tickets!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
            <!-- /.row -->
            
            <div class="row">
                <div class="col-lg-8">
                    
                </div>
                <!-- /.col-lg-8 -->
                <div class="col-lg-4">
                   
                    
                </div>
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
        @elseif(Auth::user()->account_type == 0)
            <div class="row">
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$local_ts}}</div>
                                    <div>Local <br>Timesheet</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('index_lt')}}" >
                            <div class="panel-footer">
                                <span class="pull-left">View Local Timesheet</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$overseas_ts}}</div>
                                    <div>Overseas<br>Timesheet</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('view_form_list_otf')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View OT List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-lg-3 col-md-6">
                    <div class="panel panel-primary">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-file-text fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">{{$overseas_es}}</div>
                                    <div>Overseas Expense Statement</div>
                                </div>
                            </div>
                        </div>
                        <a href="{{route('index_oes')}}">
                            <div class="panel-footer">
                                <span class="pull-left">View OES List</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div>
                
                <!-- </div> -->
                 <!--<div class="col-lg-3 col-md-6">
                    <div class="panel panel-red">
                        <div class="panel-heading">
                            <div class="row">
                                <div class="col-xs-3">
                                    <i class="fa fa-support fa-5x"></i>
                                </div>
                                <div class="col-xs-9 text-right">
                                    <div class="huge">13</div>
                                    <div>Support Tickets!</div>
                                </div>
                            </div>
                        </div>
                        <a href="#">
                            <div class="panel-footer">
                                <span class="pull-left">View Details</span>
                                <span class="pull-right"><i class="fa fa-arrow-circle-right"></i></span>
                                <div class="clearfix"></div>
                            </div>
                        </a>
                    </div>
                </div> -->
            </div>
            <!-- /.row -->
            
        @endif
            
@endsection