@extends('layouts.master_layout')
@section('title', 'Rate Setting')
@section('content')
@if(Auth::user()->account_type == 1)
<form role="form" action="{{route('ratesettingupdate_ot')}}" method="post">
{{csrf_field()}}
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Rate Setting</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Rate Setting
                        </div>
                        <div class="panel-body">
                           
                            <div class="row">
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="rate_1x">Standard Rate $USD</label>
                                        <input type="text" class="form-control" name="rate_st" value="{{$rate->rate_st}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="rate_1x">1.5x Rate $USD</label>
                                        <input type="text" class="form-control" name="rate_1_5x" value="{{$rate->rate_1_5x}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="rate_1x">2.0 Rate $USD</label>
                                        <input type="text" class="form-control" name="rate_2x" value="{{$rate->rate_2x}}">
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-12">
                                    <div class="form-group">
                                        <label for="rate_1x">Last Changed By</label>
                                        <input type="text" class="form-control" name="sso_no" value="{{$user->name}}" disabled>
                                    </div>
                                </div>
                                
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                             <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-12">
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <br>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                
            </div>
            <!-- /.row -->

           </form>
@endif
@endsection