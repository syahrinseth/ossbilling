@extends('layouts.master_layout')
@section('title', 'Overseas Timesheet View Form')
@section('content')
<!-- Modal -->
<div class="modal fade" id="custSign" tabindex="-1" role="dialog" aria-labelledby="custSignLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    <form action="{{route('custSign_ot', ['ot_id'=>$overseasTimesheetInfo['id']])}}" method="post" enctype="multipart/form-data" id="custForm">
        {{csrf_field()}}
      <div class="modal-header">
        <h5 class="modal-title" id="custSignLabel">Customer Signature Attachment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
      @if($overseasTimesheetInfo->customer_signature_attachment != 'no-file.png')
            @php
                $file = pathinfo($overseasTimesheetInfo->customer_signature_attachment, PATHINFO_EXTENSION)
            @endphp
            @if(strtolower($file) == "pdf")
            <div class="form-group">
                <label>Customer Signature Name</label>
                <input type="text" value="{{$overseasTimesheetInfo->customer_signature_name}}" class="form-control" disabled>
            </div>
                <div class="form-group" id="attachment">
                    <label for="">Customer Signature Attachment</label>
                    <div class="row">
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{{$overseasTimesheetInfo->customer_signature_attachment}}}" style="" disabled>
                        </div>
                        <div class="col-sm-2">
                            <a href="{{route('custAttDownloadPdf_ot', [ 'ot_id' => $overseasTimesheetInfo->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                        </div>
                    </div>
                    <br>
                    <embed src="/storage/overseas-timesheet/{{$overseasTimesheetInfo['id']}}/cust_sign/{{$overseasTimesheetInfo->customer_signature_attachment}}" height="400px" width="100%">
                </div>
            @else
            <div class="form-group">
                <label>Customer Signature Name</label>
                <input type="text" value="{{$overseasTimesheetInfo->customer_signature_name}}" class="form-control" disabled>
            </div>
                <label>Customer Signature Attachment</label>
                <div class="row">
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{{$overseasTimesheetInfo->customer_signature_attachment}}}" style="" disabled>
                        </div>
                        <div class="col-sm-2">
                            <a href="{{route('custAttDownloadPdf_ot', [ 'ot_id' => $overseasTimesheetInfo->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                        </div>
                    </div>
                <br>
                <img src="/storage/overseas-timesheet/{{$overseasTimesheetInfo['id']}}/cust_sign/{{$overseasTimesheetInfo->customer_signature_attachment}}" class="rounded mx-auto d-block" alt="..." width="100%" id="attachment">
            @endif
        @else
            <div class="form-group">
                <label>Customer Signature Name</label>
                <input type="text" class="form-control" name="customer_signature_name" value="{{$overseasTimesheetInfo->customer_signature_name}}">
            </div>
            <div class="form-group">
                <label>Customer Signature Attachment</label>
                <input class="form-control" name="customer_signature_attachment" type="file">
                <small>Note: Please upload a valid PDF or Image file.</small>
            </div>
        @endif
          
      </div>
      <div class="modal-footer">
        <input type="hidden" class="form-control" name="job_no" id="job_no">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        @if($overseasTimesheetInfo->customer_signature_attachment == 'no-file.png')
        <button type="button" class="btn btn-primary" id="submitForm">Upload</button>
        @endif
      </div>
    </form>  
    </div>
  </div>
</div>
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Overseas Timesheet</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
             
            <div class="row">
                <div class="panel panel-default">
                   
                        <div class="panel-heading">
                            Employee Overseas Timesheet
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                        @include('layouts.flash_message')
                        @include('layouts.validate')
                           <div class="row">
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Employee Name</label>
                                            <input class="form-control" name="name" value="{{$overseasTimesheetInfo['name']}}" placeholder="Full name" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job No.</label>
                                            <input class="form-control" name="job_no" placeholder="Job No." value="{{$job->job_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job Description</label>
                                            <textarea class="form-control" name="job_description" disabled>{{$job->job_description}}</textarea>
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Signature Name</label>
                                            @if($overseasTimesheetInfo->customer_signature_name != null)
                                                <input class="form-control" value="{{$overseasTimesheetInfo->customer_signature_name}}" disabled>
                                            @else
                                                <p class="text-denger">N/a</p>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <label>Customer Signature Attachment</label>
                                            @if($overseasTimesheetInfo->customer_signature_attachment != 'no-file.png')
                                            <p class="text-success">Yes</p>
                                            @else
                                            <p class="text-danger">N/a</p>
                                            @endif
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Employee SSO No.</label>
                                            <input class="form-control" name="sso_no" value="{{$overseasTimesheetInfo['sso_no']}}" placeholder="SSO#" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Network No.</label>
                                            <input class="form-control" name="network_no" value="{{$job->network_no}}" placeholder="Net No." disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" name="country" placeholder="Site/Country" value="{{$job->country}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Department Signature</label>
                                            @if($overseasTimesheetInfo->dept_signature_name)
                                            <p class="text-success">{{$overseasTimesheetInfo->dept_signature_name}}</p>
                                            @else
                                            <p class="text-danger">N/a</p>
                                            @endif
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                           <hr>
                            
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th colspan="2"></th>
                                        <th colspan="2">Travel Hour(s)</th>
                                        <th colspan="2">Work Hour(s)</th>
                                        <th colspan="2">Work Hour(s)</th>
                                        <th colspan="1"></th>
                                        <th colspan="3">Total Hour(s)</th>
                                        <th></th>
                                        <th></th>
                                        <th colspan="1"></th>
                                    </tr>
                                    <tr>
                                        <th>Date(s)</th>
                                        <th>Day(s)</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Start</th>
                                        <th>Finish</th>
                                        <th>Remark(s)</th>
                                        <th>1.0x</th>
                                        <th>1.5x</th>
                                        <th>2.0x</th>
                                        <th>Total Travel Hour(s)</th>
                                        <th>Total Work Hour(s)</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                
                                    
                                    @foreach($overseasTimesheetCollections as $collection)
                                    @if($collection->ot_id)
                                    <tr>
                                        <td rowspan="">
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->date))->formatLocalized('%d %B %Y')}}
                                        </td>
                                        <td rowspan="">
                                            {{$collection->day}}
                                        </td>
                                        <td rowspan="1">
                                            @if($collection->travel_start)
                                                {{$collection->travel_start}}
                                            @endif
                                            <hr>
                                            @if($collection->travel2_start)
                                                {{$collection->travel2_start}}
                                            @endif
                                        </td>
                                        <td rowspan="1">

                                            @if($collection->travel_finish)
                                                {{$collection->travel_finish}}
                                            @endif
                                            <hr>
                                            
                                            @if($collection->travel2_finish)
                                                {{$collection->travel2_finish}}
                                            @endif
                                            
                                            
                                        </td>
                                        <td rowspan="1">
                                            @if($collection->work_start)
                                                {{$collection->work_start}}
                                            @endif
                                        </td>
                                        <td rowspan="1">
                                            @if($collection->work_finish)
                                                {{$collection->work_finish}}
                                            @endif
                                        </td>
                                        <td rowspan="1">
                                            @if($collection->work2_start)
                                                {{$collection->work2_start}}
                                            @endif
                                        </td>
                                        <td rowspan="1">
                                            @if($collection->work2_finish)
                                                {{$collection->work2_finish}}
                                            @endif
                                        </td>
                                        <td rowspan="1">
                                            @if($collection->remark)
                                                {{ucfirst($collection->remark)}}
                                            @endif
                                        </td>
                                        <td rowspan="1">
                                            {{$collection->total_hour_1x}}
                                        </td>
                                        <td rowspan="1">
                                            {{$collection->total_hour_1_5x}}
                                        </td>
                                        <td rowspan="1">
                                            {{$collection->total_hour_2x}}
                                        </td>
                                        <td>
                                            {{$collection->total_travel_hour}}
                                        </td>
                                        <td>
                                            {{$collection->total_work_hour}}
                                        </td>
                                        <td>
                                        @if($overseasTimesheetInfo->sso_no == Auth::user()->sso_no)
                                            <a href="{{route('edit_date_otf', ['ot_id' => $collection->ot_id, 'work_dates_id' => $collection->id])}}" class="btn btn-default" title="Edit"><i class="fa fa-edit"></i></a>
                                            <button onclick="deleteDate({{$collection->ot_id}},{{$collection->id}})" class="btn btn-danger" title="Delete"><i class="fa fa-trash"></i></button>
                                            <form method="POST" id="delete-{{$collection->ot_id}}-{{$collection->id}}" action="{{route('delete_date_otf', ['ot_id'=>$collection->ot_id,'date_id'=>$collection->id])}}">
                                                {{csrf_field()}}
                                            </form>    
                                        @endif
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                   
                                </tbody>
                                <tfoot>
                                    <tr>
                                        <th colspan="9">Total Hour(s)</th>
                                        
                                        <th>{{$sum1}}</th>
                                        <th>{{$sum1_5}}</th>
                                        <th>{{$sum2}}</th>
                                        <th></th>
                                    </tr>
                                </tfoot>
                            </table>
                            <!-- /.table-responsive -->
                            <div class="form-row">

                            @if($overseasTimesheetInfo->sso_no == Auth::user()->sso_no)
                                <a href="{{route('add_new_date', [$overseasTimesheetInfo['id']])}}" class="btn btn-primary">Add New Date</a>
                            @endif 
                            <hr>
                                <a href="{{route('edit_form_otf', [ 'ot_id' => $overseasTimesheetInfo->id])}}" class="btn btn-default">Edit</a>
                                <a href="{{route('download_form_otf', [ 'ot_id' => $overseasTimesheetInfo->id])}}" class="btn btn-success">Download</a>
                                <button class="btn btn-info" data-toggle="modal" data-target="#custSign" data-id="{{$overseasTimesheetInfo->id}}" data-jobno="{{$overseasTimesheetInfo->job_no}}" id="custSign">Customer Signature</button>
                            @if(Auth::user()->account_type == 1)
                                <button class="btn btn-info" onclick="approve({{$overseasTimesheetInfo->id}})">Approve</button>
                                <button onclick="destroyFunction({{$overseasTimesheetInfo['id']}})" class="btn btn-danger">Delete</button>
                            @endif
                                <a href="{{route('view_form_list_otf')}}" class="btn btn-default">Back</a>

                                <form method="POST" id="delete-{{$overseasTimesheetInfo['id']}}" action="{{route('destroy_form_otf', ['ot_id'=>$overseasTimesheetInfo['id']])}}">
                                                {{csrf_field()}}
                                </form>
                    
                            </div>
                        </div>
                        <!-- /.panel-body -->
                
                    </div>
                    <!-- /.panel -->
                    
            </div>
            <!-- /.row -->
            

          <script src="/vendor/jquery/jquery.min.js"></script>    
<script>

    function approve(ot_id) {

                var r = confirm("Are you sure to add your signature into this timesheet?");
                if (r == true) {
            window.location.href = "/overseas-timesheet/view-form/"+ot_id+"/approve";
                } else {
            
                }
            }

            function destroyFunction(ot_id) {

                                var r = confirm("Are you sure to delete this overseas timesheet?");
                                    if (r == true) {
                                        // window.location.href = "/overseas-timesheet/delete-form/"+ot_id;
                                        document.getElementById('delete-'+ot_id).submit();
                                    } else {
            
                                    }
                                }

            function deleteDate(ot_id,date_id) {

            var r = confirm("Are you sure to delete this date inside this overseas timesheet?");
                if (r == true) {
                    // window.location.href = "/overseas-timesheet/delete-date/"+ot_id+"/"+date_id;
                    document.getElementById('delete-'+ot_id+'-'+date_id).submit();
                } else {
            
                }
            }

            

        jQuery(document).ready(function(){
                jQuery('button#custSign').click(function(){
                                    var ot_id = $(this).attr('data-id');
                                    var job_no = $(this).attr('data-jobno');
                                    $('form#custForm').attr('action', '/overseas-timesheet/cust-sign/'+ot_id);
                                    // $('input#job_no').attr('value', job_no);
                                });

                                $("#submitForm").on('click', function() {
                                    $("#custForm").submit();
                                });
            });
                                
</script>
            
@endsection