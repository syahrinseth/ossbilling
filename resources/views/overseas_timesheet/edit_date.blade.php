@extends('layouts.master_layout')
@section('title', 'Overseas Timesheet Edit Form')
@section('content')
@php
    use App\work_datesModel;
@endphp
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Timesheet Date Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('edit_date_otf', [ 'date_id' => $work_date_hour->id, 'ot_id' => $work_date_hour->ot_id ])}}" method="post">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Overseas Timesheet Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Employee Information</h1>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Employee Name</label>
                                            <input class="form-control" name="name" value="{{$overseasTimesheet['name']}}" placeholder="Full name" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job No.</label>
                                            <input class="form-control" name="job_no" placeholder="Job No." value="{{$job->job_no}}" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job Description</label>
                                            <textarea class="form-control" name="job_description" disabled>{{$job->job_description}}</textarea>
                                        </div>
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Employee SSO No.</label>
                                            <input class="form-control" name="sso_no" value="{{$overseasTimesheet['sso_no']}}" placeholder="SSO#" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Network No.</label>
                                            <input class="form-control" name="network_no" value="{{$job->network_no}}" placeholder="Net No." disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Country</label>
                                            <input class="form-control" name="country" placeholder="Site/Country" value="{{$job->country}}" disabled>
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Timesheet</h1>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered table-hover" id="">
                                                <thead>
                                                    <tr>
                                                        <!-- <th>Days</th> -->
                                                        <th>Dates</th>
                                                        <th>Travel Hours</th>
                                                        <th>Work Hours</th>
                                                        <th>Work Hours</th>
                                                        <th>Remarks</th>
                                                        <th>Allowance Claim</th>
                                                    </tr>
                                                </thead>
                                                <tbody id="timesheet-tbody">
                                                @if($work_date_hour)
                                                    <tr>
                                                        
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Pick:</label>
                                                                <input class="form-control datepicker" name="date" value="{{$work_date_hour->date}}" placeholder="Date" type="date" disabled>
                                                            </div>
                                                        </td>
                                                        <td id="timesheet-travel-col">
                                                            <div class="form-group">
                                                                <label>To Site:</label><br>
                                                                <label>Start</label><br>
                                                                <select class="form-control" name="travel_start">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->travel_start == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->travel_start == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->travel_start == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->travel_start == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->travel_start == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->travel_start == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->travel_start == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->travel_start == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->travel_start == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->travel_start == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->travel_start == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->travel_start == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->travel_start == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->travel_start == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->travel_start == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->travel_start == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->travel_start == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->travel_start == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->travel_start == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->travel_start == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->travel_start == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->travel_start == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->travel_start == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->travel_start == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->travel_start == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->travel_start == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->travel_start == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->travel_start == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->travel_start == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->travel_start == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->travel_start == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->travel_start == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->travel_start == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->travel_start == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->travel_start == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->travel_start == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->travel_start == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->travel_start == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->travel_start == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->travel_start == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->travel_start == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->travel_start == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->travel_start == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->travel_start == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->travel_start == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->travel_start == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->travel_start == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->travel_start == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                                <br>
                                                                <label>Finish</label><br>
                                                                <select class="form-control" name="travel_finish">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->travel_finish == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->travel_finish == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->travel_finish == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->travel_finish == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->travel_finish == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->travel_finish == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->travel_finish == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->travel_finish == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->travel_finish == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->travel_finish == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->travel_finish == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->travel_finish == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->travel_finish == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->travel_finish == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->travel_finish == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->travel_finish == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->travel_finish == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->travel_finish == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->travel_finish == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->travel_finish == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->travel_finish == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->travel_finish == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->travel_finish == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->travel_finish == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->travel_finish == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->travel_finish == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->travel_finish == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->travel_finish == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->travel_finish == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->travel_finish == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->travel_finish == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->travel_finish == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->travel_finish == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->travel_finish == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->travel_finish == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->travel_finish == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->travel_finish == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->travel_finish == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->travel_finish == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->travel_finish == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->travel_finish == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->travel_finish == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->travel_finish == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->travel_finish == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->travel_finish == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->travel_finish == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->travel_finish == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->travel_finish == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                                <label for="nextdaysite">Next Day</label>
                                                                <input type="checkbox" id="nextdaysite" name="nextdaysite" value="1" class="nextdaysite" {{$work_date_hour->nextdaysite == 1 ? 'checked="checked"' : ''}}>
                                                                <input type="hidden" name="nextdaysite" class="nextdaysite-hidden" id="nextdaysite-hidden" value="0" {{$work_date_hour->nextdaysite == 1 ? 'disabled' : ''}}>
                                                            </div>
                                                            <hr>
                                                            <div class="form-group">
                                                            <label>To Home:</label><br>
                                                                <label>Start</label><br>
                                                                <select class="form-control" name="travel2_start">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->travel2_start == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->travel2_start == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->travel2_start == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->travel2_start == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->travel2_start == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->travel2_start == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->travel2_start == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->travel2_start == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->travel2_start == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->travel2_start == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->travel2_start == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->travel2_start == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->travel2_start == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->travel2_start == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->travel2_start == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->travel2_start == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->travel2_start == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->travel2_start == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->travel2_start == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->travel2_start == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->travel2_start == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->travel2_start == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->travel2_start == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->travel2_start == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->travel2_start == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->travel2_start == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->travel2_start == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->travel2_start == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->travel2_start == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->travel2_start == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->travel2_start == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->travel2_start == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->travel2_start == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->travel2_start == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->travel2_start == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->travel2_start == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->travel2_start == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->travel2_start == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->travel2_start == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->travel2_start == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->travel2_start == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->travel2_start == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->travel2_start == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->travel2_start == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->travel2_start == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->travel2_start == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->travel2_start == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->travel2_start == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                                <br>
                                                                <label>Finish</label><br>
                                                                <select class="form-control" name="travel2_finish">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->travel2_finish == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->travel2_finish == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->travel2_finish == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->travel2_finish == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->travel2_finish == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->travel2_finish == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->travel2_finish == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->travel2_finish == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->travel2_finish == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->travel2_finish == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->travel2_finish == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->travel2_finish == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->travel2_finish == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->travel2_finish == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->travel2_finish == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->travel2_finish == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->travel2_finish == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->travel2_finish == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->travel2_finish == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->travel2_finish == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->travel2_finish == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->travel2_finish == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->travel2_finish == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->travel2_finish == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->travel2_finish == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->travel2_finish == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->travel2_finish == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->travel2_finish == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->travel2_finish == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->travel2_finish == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->travel2_finish == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->travel2_finish == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->travel2_finish == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->travel2_finish == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->travel2_finish == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->travel2_finish == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->travel2_finish == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->travel2_finish == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->travel2_finish == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->travel2_finish == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->travel2_finish == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->travel2_finish == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->travel2_finish == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->travel2_finish == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->travel2_finish == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->travel2_finish == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->travel2_finish == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->travel2_finish == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                                <label for="nextdayhome">Next Day</label>
                                                                <input type="checkbox" id="nextdayhome" name="nextdayhome" value="1" class="nextdayhome" {{$work_date_hour->nextdayhome == 1 ? 'checked="checked"' : ''}}>
                                                                <input type="hidden" name="nextdayhome" class="nextdayhome-hidden" id="nextdayhome-hidden" value="0" {{$work_date_hour->nextdayhome == 1 ? 'disabled' : ''}}>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                            <label>Before Lunch:</label><br>
                                                                <label>Start</label><br>
                                                                <select class="form-control" name="work_start">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->work_start == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->work_start == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->work_start == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->work_start == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->work_start == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->work_start == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->work_start == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->work_start == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->work_start == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->work_start == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->work_start == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->work_start == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->work_start == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->work_start == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->work_start == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->work_start == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->work_start == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->work_start == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->work_start == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->work_start == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->work_start == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->work_start == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->work_start == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->work_start == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->work_start == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->work_start == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->work_start == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->work_start == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->work_start == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->work_start == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->work_start == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->work_start == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->work_start == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->work_start == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->work_start == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->work_start == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->work_start == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->work_start == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->work_start == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->work_start == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->work_start == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->work_start == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->work_start == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->work_start == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->work_start == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->work_start == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->work_start == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->work_start == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                                <br>
                                                                <label>Finish</label><br>
                                                                <select class="form-control" name="work_finish">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->work_finish == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->work_finish == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->work_finish == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->work_finish == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->work_finish == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->work_finish == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->work_finish == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->work_finish == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->work_finish == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->work_finish == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->work_finish == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->work_finish == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->work_finish == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->work_finish == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->work_finish == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->work_finish == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->work_finish == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->work_finish == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->work_finish == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->work_finish == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->work_finish == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->work_finish == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->work_finish == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->work_finish == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->work_finish == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->work_finish == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->work_finish == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->work_finish == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->work_finish == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->work_finish == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->work_finish == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->work_finish == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->work_finish == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->work_finish == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->work_finish == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->work_finish == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->work_finish == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->work_finish == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->work_finish == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->work_finish == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->work_finish == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->work_finish == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->work_finish == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->work_finish == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->work_finish == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->work_finish == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->work_finish == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->work_finish == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                            <label>After Lunch:</label><br>
                                                                <label>Start</label><br>
                                                                <select class="form-control" name="work2_start">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->work2_start == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->work2_start == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->work2_start == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->work2_start == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->work2_start == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->work2_start == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->work2_start == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->work2_start == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->work2_start == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->work2_start == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->work2_start == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->work2_start == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->work2_start == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->work2_start == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->work2_start == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->work2_start == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->work2_start == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->work2_start == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->work2_start == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->work2_start == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->work2_start == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->work2_start == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->work2_start == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->work2_start == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->work2_start == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->work2_start == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->work2_start == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->work2_start == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->work2_start == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->work2_start == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->work2_start == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->work2_start == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->work2_start == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->work2_start == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->work2_start == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->work2_start == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->work2_start == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->work2_start == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->work2_start == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->work2_start == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->work2_start == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->work2_start == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->work2_start == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->work2_start == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->work2_start == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->work2_start == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->work2_start == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->work2_start == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                                <br>
                                                                <label>Finish</label><br>
                                                                <select class="form-control" name="work2_finish">
                                                                    <option value="">Select</option>
                                                                    <option value="00:00:00" {{$work_date_hour->work2_finish == '00:00:00' ? ' selected="selected"' : ''}}>00:00</option>
                                                                    <option value="00:30:00" {{$work_date_hour->work2_finish == '00:30:00' ? ' selected="selected"' : ''}}>00:30</option>
                                                                    <option value="01:00:00" {{$work_date_hour->work2_finish == '01:00:00' ? ' selected="selected"' : ''}}>01:00</option>
                                                                    <option value="01:30:00" {{$work_date_hour->work2_finish == '01:30:00' ? ' selected="selected"' : ''}}>01:30</option>
                                                                    <option value="02:00:00" {{$work_date_hour->work2_finish == '02:00:00' ? ' selected="selected"' : ''}}>02:00</option>
                                                                    <option value="02:30:00" {{$work_date_hour->work2_finish == '02:30:00' ? ' selected="selected"' : ''}}>02:30</option>
                                                                    <option value="03:00:00" {{$work_date_hour->work2_finish == '03:00:00' ? ' selected="selected"' : ''}}>03:00</option>
                                                                    <option value="03:30:00" {{$work_date_hour->work2_finish == '03:30:00' ? ' selected="selected"' : ''}}>03:30</option>
                                                                    <option value="04:00:00" {{$work_date_hour->work2_finish == '04:00:00' ? ' selected="selected"' : ''}}>04:00</option>
                                                                    <option value="04:30:00" {{$work_date_hour->work2_finish == '04:30:00' ? ' selected="selected"' : ''}}>04:30</option>
                                                                    <option value="05:00:00" {{$work_date_hour->work2_finish == '05:00:00' ? ' selected="selected"' : ''}}>05:00</option>
                                                                    <option value="05:30:00" {{$work_date_hour->work2_finish == '05:30:00' ? ' selected="selected"' : ''}}>05:30</option>
                                                                    <option value="06:00:00" {{$work_date_hour->work2_finish == '06:00:00' ? ' selected="selected"' : ''}}>06:00</option>
                                                                    <option value="06:30:00" {{$work_date_hour->work2_finish == '06:30:00' ? ' selected="selected"' : ''}}>06:30</option>
                                                                    <option value="07:00:00" {{$work_date_hour->work2_finish == '07:00:00' ? ' selected="selected"' : ''}}>07:00</option>
                                                                    <option value="07:30:00" {{$work_date_hour->work2_finish == '07:30:00' ? ' selected="selected"' : ''}}>07:30</option>
                                                                    <option value="08:00:00" {{$work_date_hour->work2_finish == '08:00:00' ? ' selected="selected"' : ''}}>08:00</option>
                                                                    <option value="08:30:00" {{$work_date_hour->work2_finish == '08:30:00' ? ' selected="selected"' : ''}}>08:30</option>
                                                                    <option value="09:00:00" {{$work_date_hour->work2_finish == '09:00:00' ? ' selected="selected"' : ''}}>09:00</option>
                                                                    <option value="09:30:00" {{$work_date_hour->work2_finish == '09:30:00' ? ' selected="selected"' : ''}}>09:30</option>
                                                                    <option value="10:00:00" {{$work_date_hour->work2_finish == '10:00:00' ? ' selected="selected"' : ''}}>10:00</option>
                                                                    <option value="10:30:00" {{$work_date_hour->work2_finish == '10:30:00' ? ' selected="selected"' : ''}}>10:30</option>
                                                                    <option value="11:00:00" {{$work_date_hour->work2_finish == '11:00:00' ? ' selected="selected"' : ''}}>11:00</option>
                                                                    <option value="11:30:00" {{$work_date_hour->work2_finish == '11:30:00' ? ' selected="selected"' : ''}}>11:30</option>
                                                                    <option value="12:00:00" {{$work_date_hour->work2_finish == '12:00:00' ? ' selected="selected"' : ''}}>12:00</option>
                                                                    <option value="12:30:00" {{$work_date_hour->work2_finish == '12:30:00' ? ' selected="selected"' : ''}}>12:30</option>
                                                                    <option value="13:00:00" {{$work_date_hour->work2_finish == '13:00:00' ? ' selected="selected"' : ''}}>13:00</option>
                                                                    <option value="13:30:00" {{$work_date_hour->work2_finish == '13:30:00' ? ' selected="selected"' : ''}}>13:30</option>
                                                                    <option value="14:00:00" {{$work_date_hour->work2_finish == '14:00:00' ? ' selected="selected"' : ''}}>14:00</option>
                                                                    <option value="14:30:00" {{$work_date_hour->work2_finish == '14:30:00' ? ' selected="selected"' : ''}}>14:30</option>
                                                                    <option value="15:00:00" {{$work_date_hour->work2_finish == '15:00:00' ? ' selected="selected"' : ''}}>15:00</option>
                                                                    <option value="15:30:00" {{$work_date_hour->work2_finish == '15:30:00' ? ' selected="selected"' : ''}}>15:30</option>
                                                                    <option value="16:00:00" {{$work_date_hour->work2_finish == '16:00:00' ? ' selected="selected"' : ''}}>16:00</option>
                                                                    <option value="16:30:00" {{$work_date_hour->work2_finish == '16:30:00' ? ' selected="selected"' : ''}}>16:30</option>
                                                                    <option value="17:00:00" {{$work_date_hour->work2_finish == '17:00:00' ? ' selected="selected"' : ''}}>17:00</option>
                                                                    <option value="17:30:00" {{$work_date_hour->work2_finish == '17:30:00' ? ' selected="selected"' : ''}}>17:30</option>
                                                                    <option value="18:00:00" {{$work_date_hour->work2_finish == '18:00:00' ? ' selected="selected"' : ''}}>18:00</option>
                                                                    <option value="18:30:00" {{$work_date_hour->work2_finish == '18:30:00' ? ' selected="selected"' : ''}}>18:30</option>
                                                                    <option value="19:00:00" {{$work_date_hour->work2_finish == '19:00:00' ? ' selected="selected"' : ''}}>19:00</option>
                                                                    <option value="19:30:00" {{$work_date_hour->work2_finish == '19:30:00' ? ' selected="selected"' : ''}}>19:30</option>
                                                                    <option value="20:00:00" {{$work_date_hour->work2_finish == '20:00:00' ? ' selected="selected"' : ''}}>20:00</option>
                                                                    <option value="20:30:00" {{$work_date_hour->work2_finish == '20:30:00' ? ' selected="selected"' : ''}}>20:30</option>
                                                                    <option value="21:00:00" {{$work_date_hour->work2_finish == '21:00:00' ? ' selected="selected"' : ''}}>21:00</option>
                                                                    <option value="21:30:00" {{$work_date_hour->work2_finish == '21:30:00' ? ' selected="selected"' : ''}}>21:30</option>
                                                                    <option value="22:00:00" {{$work_date_hour->work2_finish == '22:00:00' ? ' selected="selected"' : ''}}>22:00</option>
                                                                    <option value="22:30:00" {{$work_date_hour->work2_finish == '22:30:00' ? ' selected="selected"' : ''}}>22:30</option>
                                                                    <option value="23:00:00" {{$work_date_hour->work2_finish == '23:00:00' ? ' selected="selected"' : ''}}>23:00</option>
                                                                    <option value="23:30:00" {{$work_date_hour->work2_finish == '23:30:00' ? ' selected="selected"' : ''}}>23:30</option>
                                                                </select>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <div class="form-group">
                                                                <label>Remark:</label>
                                                                <input class="form-control remark" name="remark" value="{{$work_date_hour->remark}}" placeholder="Remark" type="text" required>
                                                                <hr>
                                                                <label for="standby">Stand By:</label>
                                                                <input type="checkbox" name="standby" id="standby" value="1" checked>
                                                                <br>
                                                                <small>(Optional)</small>
                                                            </div>
                                                        </td>
                                                        <td>
                                                            <label>Overseas Night Shift Allow: </label><br>
                                                            @if($work_date_hour->overseas_night_shift_allow == work_datesModel::getTotalOverseasNightShiftAllowRate())
                                                                <input type="checkbox" name="overseas_night_shift_allow" id="overseas_night_shift_allow" value="1" {{$work_date_hour->overseas_night_shift_allow == null || $work_date_hour->overseas_night_shift_allow == 0 ? '' : 'checked'}}> ${{work_datesModel::getTotalOverseasNightShiftAllowRate()}}
                                                            @elseif($work_date_hour->overseas_night_shift_allow != null)
                                                                <input type="checkbox" name="" id="" value="1" {{$work_date_hour->overseas_night_shift_allow == null || $work_date_hour->overseas_night_shift_allow == 0 ? '' : 'checked'}} disabled> ${{$work_date_hour->overseas_night_shift_allow}}
                                                                <br>
                                                                <input type="checkbox" name="overseas_night_shift_allow" id="overseas_night_shift_allow" value="1"> ${{work_datesModel::getTotalOverseasNightShiftAllowRate()}}
                                                                <small>Select this checkbox will replace the old value.</small>
                                                            @else
                                                                <input type="checkbox" name="overseas_night_shift_allow" id="overseas_night_shift_allow" value="1"> ${{work_datesModel::getTotalOverseasNightShiftAllowRate()}}
                                                            @endif
                                                        </td>
                                                    </tr>
                                                @endif
                                                    
                                                </tbody>
                                            </table>
                                        </div>
                                        <!-- /.table-responsive -->
                                        
                                </div>
                            </div>
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                        @include('layouts.flash_message')
                                        @include('layouts.validate')
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('view_form_otf', [ 'id' => $work_date_hour->ot_id])}}" class="btn btn-default">Back</a>
                                        <br>
                                        
                                        <input name="from" value="Create" hidden>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                </form>
            </div>
            <!-- /.row -->
            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    var remark_standby = $('input.remark').attr("value")
                    if(remark_standby == "standby"){
                        $('input#standby').prop('checked', true);
                        $('input.remark').removeAttr('required');
                        $('input.remark').attr("type", "hidden");
                        $('input.remark').attr('value', "");
                        $('input.remark').prop('readonly', 'readonly');
                    }else{
                        $('input#standby').prop('checked', false);
                        $('input.remark').removeAttr('readonly');
                        $('input.remark').attr("type", "text");
                        $('input.remark').prop('required', true);
                    }
                    
                    // $('input#standby').change(function(){
                    $(document).on('change','input#standby',function(){
                        checkbox = $(this).prop('checked');
                        if(checkbox == true){
                            $('input.remark').removeAttr('required');
                            $('input.remark').attr("type", "hidden");
                            $('input.remark').prop('readonly', 'readonly');
                        }else{
                            $('input.remark').removeAttr('readonly');
                            $('input.remark').attr("type", "text");
                            $('input.remark').prop('required', true);
                        }
                    });
                    

                    $.extend( true, $.fn.dataTable.defaults, {
                        "searching": false,
                        "ordering": false
                    } );

                    // $('input.nextdaysite').prop('checked', false);
                    $(document).on('change', 'input.nextdaysite', function(){
                        var checkbox = $('input#nextdaysite').prop('checked');
                        if(checkbox == true){
                            console.log(checkbox);
                            $('input#nextdaysite-hidden').attr('disabled', 'disabled');
                        }else{
                            $('input#nextdaysite-hidden').removeAttr('disabled');
                        }
                    });
                    // $('input.nextdayhome').prop('checked', false);
                    $(document).on('change', 'input.nextdayhome', function(){
                        var checkbox = $('input#nextdayhome').prop('checked');
                        console.log(checkbox);
                        if(checkbox == true){
                            $('input#nextdayhome-hidden').attr('disabled', 'disabled');
                        }else{
                            $('input#nextdayhome-hidden').removeAttr('disabled');
                        }
                    });

                    
                });

            
                
            </script>
@endsection