@extends('layouts.master_layout')
@section('title', 'Overseas Timesheet Edit Form')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">Overseas Timesheet Edit</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            
            <!-- /.row -->
            <div class="row">
                <form role="form" action="{{route('edit_form_update_otf', [ 'id' => $overseasTimesheet->id ])}}" method="post" enctype="multipart/form-data">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Overseas Timesheet Form
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                    <h1>Employee Information</h1>
                                </div>
                            </div>
                           <div class="row">
                                <div class="col-lg-6">
                                        {{ csrf_field() }}
                                        <div class="form-group">
                                            <label>Employee Name</label>
                                            <input class="form-control" name="name" value="{{$overseasTimesheet['name']}}" placeholder="Full name" disabled>
                                        </div>
                                         @if($overseasTimesheet->customer_signature_attachment != "no-file.png")
                                                <div class="form-group">
                                                <label>Customer Signature Attachment</label><br>
                                                <a class="btn btn-primary" id="view_attachment" data-id="{{$overseasTimesheet->customer_signature_attachment}}" data-toggle="modal" data-target="#viewAttachment">View Attachment</a>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                <label>Customer Signature Name</label>
                                                <input type="text" class="form-control" name="customer_signature_name" value="{{$overseasTimesheet->customer_signature_name}}">
                                                </div>
                                                <div class="form-group">
                                                <label>Customer Signature Attachment</label><br>
                                                <input class="form-control" name="customer_signature_attachment" type="file">
                                                </div>
                                            @endif
                                        
                                        
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                                <div class="col-lg-6">

                                        <div class="form-group">
                                            <label>Employee SSO No.</label>
                                            <input class="form-control" name="sso_no" value="{{$overseasTimesheet['sso_no']}}" placeholder="SSO#" disabled>
                                        </div>
                                        <div class="form-group">
                                            <label>Job No.</label>
                                            <input list="job_no" name="job_no" class="form-control" value="{{$overseasTimesheet->job_no}}" placeholder="Job Number" disabled>
                                        </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            </div>
                            <!-- /.row (nested) -->
                            
                            <hr>
                            <div class="row">
                                <div class="col-lg-12">
                                    @include('layouts.flash_message')
                                    @include('layouts.validate')
                                        <button type="submit" class="btn btn-primary">Update</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <a href="{{route('view_form_otf',['ot_id' =>$overseasTimesheet->id])}}" class="btn btn-default">Cancel</a>
                                        <br>
                                        
                                        <input name="from" value="Create" hidden>
                                    
                                </div>
                            </div>
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
                 <!-- Modal -->
<div class="modal fade" id="viewAttachment" tabindex="-1" role="dialog" aria-labelledby="viewAttachmentLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="viewAttachmentLabel">Customer Signature Attachment</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" id="modal-body">
        <div class="form-group">
            <label for="signature_name" id="attachment">Customer Signature Name</label>
            <input type="text" class="form-control" id="signature_name" value="{{$overseasTimesheet['customer_signature_name']}}" disabled>
        </div>
        @if($overseasTimesheet->customer_signature_attachment != 'no-file.png')
            @php
                $file = pathinfo($overseasTimesheet->customer_signature_attachment, PATHINFO_EXTENSION)
            @endphp
            @if(strtolower($file) == "pdf")
                <div class="form-group" id="attachment">
                    <label for="">Customer Signature Attachment</label>
                    <div class="row">
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{{$overseasTimesheet->customer_signature_attachment}}}" style="" disabled>
                        </div>
                        <div class="col-sm-2">
                            <a href="{{route('custAttDownloadPdf_ot', [ 'ot_id' => $overseasTimesheet->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                        </div>
                    </div>
                    <br>
                    <embed src="/storage/overseas-timesheet/{{$overseasTimesheet->id}}/cust_sign/{{$overseasTimesheet->customer_signature_attachment}}" height="400px" width="100%">
                </div>
            @else
            <div class="form-group" id="attachment">
            <label for="" >Customer Signature Attachment</label>
            <div class="row">
                        <div class="col-sm-10">
                            <input type="text" class="form-control" value="{{{$overseasTimesheet->customer_signature_attachment}}}" style="" disabled>
                        </div>
                        <div class="col-sm-2">
                            <a href="{{route('custAttDownloadPdf_ot', [ 'ot_id' => $overseasTimesheet->id])}}" class="btn btn-success" title="Download"><i class="fa fa-download" style="display:inline;"></i></a>
                        </div>
                    </div>
            <br>
                <img src="/storage/overseas-timesheet/{{$overseasTimesheet->id}}/cust_sign/{{$overseasTimesheet->customer_signature_attachment}}" class="rounded mx-auto d-block" alt="..." width="100%" id="attachment">
            </div>
            @endif
        @else

        @endif
        
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Done</button>
        <button type="button" class="btn btn-danger" id="remove_att">Remove Attachment</button>
      </div>
    </div>
  </div>
</div>
                </form>
            </div>
            <!-- /.row -->
            <!-- jQuery -->
            <script src="/vendor/jquery/jquery.min.js"></script>
            <script>
                // jQuery function start

                // function for expend timesheet row
                jQuery(document).ready(function(){
                    
                    jQuery('button#remove_att').one('click', function(){
                    $('label#attachment').remove();
                    $('div#attachment').remove();
                    $('img#attachment').remove();
                    $('a#attachment').remove();
                    $('input#attachment').remove();
                    $('label#view_attachment').remove();
                    $('input#signature_name').remove();
                    $('div#modal-body').append('<div class="form-group"><label>Customer Signature Name</label><input class="form-control" name="customer_signature_name" value="{{$overseasTimesheet->customer_signature_name}}"></div><label>Customer Signature Attachment</label><input type="file" id="file" class="form-control" name="customer_signature_attachment"><small>Note: Please upload a valid PDF or Image file.</small>');
                    });

                    $.extend( true, $.fn.dataTable.defaults, {
                        "searching": false,
                        "ordering": false
                    } );
                });
            </script>
@endsection