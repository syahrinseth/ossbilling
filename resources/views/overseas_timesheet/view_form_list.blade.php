@extends('layouts.master_layout')
@section('title', 'Overseas Timesheet View Form List')
@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header">View Overseas Timesheet List</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            @if(Auth::user()->account_type == 1)
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Employee Overseas Timesheet List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Employee Name</th>
                                        <th>Job Number(s)</th>
                                        <th>Status</th>
                                        <th>Customer/Department Signature</th>
                                        <th>Updated</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($overseasTimesheetCollections as $collection)
                                    
                                    <tr>
                                        <td>
                                            {{$collection->name}}
                                        </td>
                                        <td>
                                            {{$collection->job_no}}
                                        </td>
                                        <td>
                                            @if($collection->status == 0)
                                                <p class="text-warning">Waiting For Approval</p>
                                            @else
                                                <p class="text-success">Approve</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($collection->customer_signature_attachment != 'no-file.png')
                                                <p class="text-success">Cust Sign: Yes</p>
                                            @else
                                                <p class="text-warning">Cust Sign: N/a</p>
                                            @endif
                                            <hr>
                                            @if($collection->dept_signature_name)
                                                <p class="text-success">Dept Sign: Yes</p>
                                            @else
                                                <p class="text-warning">Dept Sign: N/a</p>
                                            @endif
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->updated_at))->diffForHumans()}}
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->created_at))->formatLocalized('%A %d %B %Y')}}
                                        </td>
                                        <td>
                                        @if($collection->sso_no == Auth::user()->sso_no)
                                            <a href="{{route('view_form_otf', [ 'id' => $collection->id ])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('edit_form_otf', [ 'id' => $collection->id ])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('download_form_otf', [ 'ot_id' => $collection->id])}}" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                            <button onclick="deleteFunction('{{$collection->job_no}}','{{$collection->id}}')" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                            <form method="POST" id="delete-{{$collection->id}}" action="{{route('destroy_form_otf', ['ot_id'=>$collection->id])}}">
                                                {{csrf_field()}}
                                            </form>
                                        @else
                                            <a href="{{route('view_form_otf', [ 'id' => $collection->id ])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('download_form_otf', [ 'ot_id' => $collection->id])}}" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                        @endif
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
            @elseif(Auth::user()->account_type == 0)
            <div class="row">
                <div class="panel panel-default">
                        <div class="panel-heading">
                            Employee Overseas Timesheet List
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            @include('layouts.flash_message')
                            <table width="100%" class="table table-striped table-bordered table-hover" id="dataTables-example">
                                <thead>
                                    <tr>
                                        <th>Job Number(s)</th>
                                        <th>Status</th>
                                        <th>Customer/Department Signature</th>
                                        <th>Updated</th>
                                        <th>Created</th>
                                        <th>Actions</th>
                                    </tr>
                                </thead>
                                <tbody>
                                   
                                    @foreach($overseasTimesheetCollections as $collection)
                                    @if($collection->sso_no == Auth::user()->sso_no)
                                    <tr>
                                        <td>
                                            {{$collection->job_no}}
                                        </td>
                                        <td>
                                            @if($collection->status == 0)
                                                <p class="text-warning">Waiting For Approval</p>
                                            @else
                                                <p class="text-success">Approve</p>
                                            @endif
                                        </td>
                                        <td>
                                            @if($collection->customer_signature_attachment != 'no-file.png')
                                                <p class="text-success">Cust Sign: Yes</p>
                                            @else
                                                <p class="text-warning">Cust Sign: N/a</p>
                                            @endif
                                            <hr>
                                            @if($collection->dept_signature_name)
                                                <p class="text-success">Dept Sign: Yes</p>
                                            @else
                                                <p class="text-warning">Dept Sign: N/a</p>
                                            @endif
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->updated_at))->diffForHumans()}}
                                        </td>
                                        <td>
                                            {{\Carbon\Carbon::createFromTimeStamp(strtotime($collection->created_at))->formatLocalized('%A %d %B %Y')}}
                                        </td>
                                        <td>
                                            <a href="{{route('view_form_otf', [ 'id' => $collection->id ])}}" class="btn btn-primary btn-sm" title="View"><i class="fa fa-eye"></i></a>
                                            <a href="{{route('edit_form_otf', [ 'id' => $collection->id ])}}" class="btn btn-default btn-sm" title="Edit"><i class="fa fa-edit"></i></a>
                                            <a href="{{route('download_form_otf', [ 'ot_id' => $collection->id])}}" class="btn btn-success btn-sm" title="Download"><i class="fa fa-download"></i></a>
                                            <button onclick="deleteFunction('{{$collection->job_no}}','{{$collection->id}}')" class="btn btn-danger btn-sm" title="Delete"><i class="fa fa-trash"></i></button>
                                            <form method="POST" id="delete-{{$collection->id}}" action="{{route('destroy_form_otf', ['ot_id'=>$collection->id])}}">
                                                {{csrf_field()}}
                                            </form>
                                        
                                        </td>
                                    </tr>
                                    @endif
                                    @endforeach
                                </tbody>
                            </table>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
            </div>
            <!-- /.row -->
            @endif
            <script src="/vendor/jquery/jquery.min.js"></script> 
            <script>
            function deleteFunction(job_no, id) {

                var r = confirm("Are you sure to delete this job number?");
                if (r == true) {
                // window.location.href = "/overseas-timesheet/delete-form/"+id;
                document.getElementById('delete-'+id).submit();
                }
            }



            </script>

        
@endsection